let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Root
var root = 'resources/assets/';
var rootJs = root + 'js/';
var scripts = [];

scripts.push(rootJs + 'script.js');
scripts.push(rootJs + 'libs/modules/*');
scripts.push(rootJs + 'app/app.bootstrap.js');

scripts.push(rootJs + 'libs/components/components.js');
scripts.push(rootJs + 'libs/directives/*');
scripts.push(rootJs + 'libs/services/*');
scripts.push(rootJs + 'libs/factories/*');

scripts.push(rootJs + 'app/app.js');
scripts.push(rootJs + 'app/config.js');
scripts.push(rootJs + 'app/controllers/*');

mix.scripts(scripts, 'public/build/app.js').version();

// Copy view template
mix.copy(rootJs + '/app/views/', 'public/build/views');
mix.copy(rootJs + '/libs/components/*.html', 'public/build/libs/components/');

// CSS
mix.styles([root + 'css/style.css'], 'public/build/css/style.css').version();
