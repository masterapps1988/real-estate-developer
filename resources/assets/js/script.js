// Math is an object, no need to use prototype keyword
Math.getRandomHash = function() {
    var s1, s2;
    s1 = String(Math.floor(Math.random()*1000));
    s2 = String(new Date().getTime());

    return s1 + s2;
};

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

Array.prototype.removeByIndex = function(index) {
    if (index > -1) {
      this.splice(index, 1);
    }
};

Array.prototype.removeByObject = function(object) {
    for(var i=0; i<this.length; i++) {
      if(this[i] == object) {
        this.removeByIndex(i);
      }
    }
};

// Scroll to animation
function scrollTo($dom, duration) {
    $('html, body').animate({
        scrollTop: $dom.offset().top
    }, duration);
}

function getId() {
    var id = Cookies.get('js_id');

    if(!id) {
        id = 0;
    }

    id = parseInt(id);
    id++;
    Cookies.set('js_id', id);

    return id;
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

var version = getId();

var viewList = [];
function v(path) {
    var $return = path + '?v=' + version;

    viewList.push($return);

    return $return;
}

// Path list
var viewPath = '/build/views/';
var path = {
    dashboard: v('/build/views/dashboard.html'),

    admin: {
        index: v('/build/views/admin.html'),
        create: v('/build/views/admin.create.html')
    },

    penawaran: {
        index: v('/build/views/penawaran.html'),
        create: v('/build/views/penawaran.create.html'),
        schedule: v('/build/views/penawaran.schedule.html')
    },

    pricePlan: {
        index: v('/build/views/price-plan.html')
    },

    schedule: {
        index: v('/build/views/schedule.html')
    },

    project: {
        index: v('/build/views/project.html'),
        create: v('/build/views/project.create.html'),
        worker: v('/build/views/project.worker.html'),
        stock: v('/build/views/project.stock.html'),
        setHpp1: v('/build/views/project.set-hpp-1.html'),
        pricePlanCreate: v('/build/views/price-plan.create.html'),
        priceSummary: v('/build/views/price-summary.detail.html'),
        pricePlanJobs: v('/build/views/price-plan.jobs.html'),
    },

    stockOpname: {
        list: v('/build/views/stock-opname.html'),
        create: v('/build/views/stock-opname.create.html'),
    },

    sp: {
        index: v('/build/views/sp.html'),
        create: v('/build/views/sp.create.html')
    },

    po: {
        index: v('/build/views/po.html'),
        create: v('/build/views/po.create.html')
    },

    receipt: {
        index: v('/build/views/receipt.html'),
        create: v('/build/views/receipt.create.html')
    },

    expense: {
        index: v('/build/views/expense.html'),
        create: v('/build/views/expense.create.html')
    },

    invoice: {
        index: v('/build/views/invoice.html'),
        create: v('/build/views/invoice.create.html')
    },

    invoicePayment: {
        index: v('/build/views/invoice-payment.html'),
        create: v('/build/views/invoice-payment.create.html')
    },

    customer: {
        index: v('/build/views/customer.html'),
        create: v('/build/views/customer.create.html')
    },

    category: {
        index: v('/build/views/category.html')
    },

    bank: {
        index: v('/build/views/bank.html')
    },

    role: {
        index: v('/build/views/role.html'),
        create: v('/build/views/role.create.html'),
        setPermission: v('/build/views/role.set-permission.html'),
    },

    item: {
        index: v('/build/views/item.html'),
        create: v('/build/views/item.create.html')
    },

    transfer: {
        index: v('/build/views/transfer.html'),
        create: v('/build/views/transfer.create.html')
    },

    mutasi: {
        index: v('/build/views/mutasi.html')
    },

    stockFlow: {
        index: v('/build/views/stock-flow.html')
    },

    jobs: {
        index: v('/build/views/jobs.html'),
        create: v('/build/views/jobs.create.html')
    },

    jobOpname: {
        index: v('/build/views/job-opname.html'),
        create: v('/build/views/job-opname.create.html')
    },

    checklist: {
        index: v('/build/views/checklist.html'),
        create: v('/build/views/checklist.create.html')
    },

    checklistTick: {
        create: v('/build/views/checklist-tick.create.html'),
    },

    setting: {
        index: v('/build/views/setting.html')
    },

    demo: {
        index: v('/build/views/demo.html'),
    },

    component: {
        boxOverlay: v('/build/libs/components/box.overlay.html'),
        buttonLoading: v('/build/libs/components/button.loading.html'),
        alertMessage: v('/build/libs/components/alert.message.html'),
        popUp: v('/build/libs/components/popup.modal.html'),
        flashMessage: v('/build/libs/components/flash-message.html'),
        flashMessageStacked: v('/build/libs/components/flash-message-stacked.html'),
        paging: v('/build/libs/components/paging.html'),
        sortable: v('/build/libs/components/sortable.html')
    }
};
