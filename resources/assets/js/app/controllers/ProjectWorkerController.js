(function() {
'use strict';

angular.module('app').controller('ProjectWorkerController', ProjectWorkerController);

function ProjectWorkerController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.MAX_WORKER = 3;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;
    this._delete = [];
    this.parent_id = $stateParams.id;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            name: null,
            customer_id: null,
            start: null,
            finish: null,
            ref_no: null,
            city_id: null,
            address: null,
            status_id: null
        };
    };

    this.init = function() {
        vm.ls.get('loading').on();
        this._delete = [];

        ApiService.Worker.all($stateParams.id).then(function(resp) {
            console.log(resp);
            var data = resp.data.data;

            var group = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].role_name != 'vendor' && data[i].role_name != 'mandor') {
                    group.push(data[i]);
                }
            }
            vm.list = group;

            // If list less than 3
            for(var i=vm.list.length; i<vm.MAX_WORKER; i++) {
                vm.list.push({
                    name: 'Test'
                });
            }
            vm.ls.get('loading').off();
        });

        ApiService.Admin.all({
            roles: 'manager-operasional,operasional,logistic'
        }).then(function(resp) {
            var data = resp.data;
            vm.users = data.data;
        });
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        var data = this._createPostData();
        ApiService.Project.changeWorker(data).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('project-detail', { 'id': resp.data.data });
                else
                    vm.init();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });

    };

    this.delete = function(worker) {
        if(worker.id)
            this._delete.push(worker.id);

        worker.id = null;
        worker.role_name = null;
        worker.name = null;

        console.log(this._delete);
    };

    this._createPostData = function(){
        var userIdList = [];

        for(var i=0; i<this.list.length; i++) {
            if(this.list[i].id)
                userIdList.push(this.list[i].id);
        }

        var postData = {
            project_id: $stateParams.id,
            users: userIdList,
            _delete: vm._delete
        };

        console.log('post data', postData);

        return postData;
    };
};
})();