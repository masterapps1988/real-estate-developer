(function() {
    'use strict';
    
    angular.module('app').controller('TransferController', TransferController);
    
    function TransferController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        this.isEditItem = false;
        this.listPending = false;
        
        // Paginator
        this.paginator = {};
    
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this._delete = [];
        
        this.tempItem = {
            id: '',
            item_id: '',
            name: '',
            qty: 0,
            unit: '?',
            price: 0
        };
        
        this.init = function() {
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.status)
                search.status = $stateParams.status;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            if($stateParams.project_id_from)
                search.project_id_from = $stateParams.project_id_from;
            if($stateParams.project_id_to)
                search.project_id_to = $stateParams.project_id_to;
            if($stateParams.date_from)
                search.date_from = $stateParams.date_from;
            if($stateParams.date_to)
                search.date_to = $stateParams.date_to;
            if($stateParams.status_page == 'pending')
                this.listPending = true;

            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];

                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }

            vm.search = search;

            vm._doSearch();
        };

        this.getPermission = function(name) {
            var list = [];
            switch(name) {
                case 'detail':
                    list = ['transfer_detail', 'transfer_delete'];
                    if(this.listPending)
                        list = ['transfer_detail', 'transfer_delete', 'transfer_change_status'];
                    break;
            }

            return list;
        };

        this.createEmptyData = function() {
            this.data = {
                id: null,
                project_id_from: '',
                project_id_to: '',
                ref_no: null,
                notes: null,
                created: null,
                expired: null,
                created_at: null,
                updated_at: null,
                status: {
                    name: 'draft'
                },
                detail: [],
    
                // List of detail id that want to be deleted
                _delete_detail: []
            };
        };

        this.isCreate = function() {
            return !$stateParams.id;
        };
    
        this.isDraft = function() {
            return this.isStatus('draft');
        };

        this.isDecline = function() {
            return this.isStatus('decline');
        };

        this.isApprove = function() {
            return this.isStatus('approve');
        };
    
        this.isPending = function() {
            return this.isStatus('pending');
        };
    
        this.isStatus = function(status) {
            return this.data.status.name === status;
        };

        // Search transfer
        this._doSearch = function() {
            if (this.listPending == true) {
                vm.search.status = 'pending';
            }
            vm.ls.get('loading').on();
            ApiService.Transfer.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;

                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }

                // Paginator
                vm.paginator = Paginator(data);
            });

            ApiService.Project.all({page: 'all'}).then(function (resp) {
                var data = resp.data;
                vm.projects = data.data;

                vm.ls.get('loading').off();
            });
        };

        this.create = function() {
            this.createEmptyData();
            // get Id
            var id = $stateParams.id;
    
            if(id) {
                vm.ls.get('loading').on();
                ApiService.Transfer.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        data.project_id_from = String(data.project_id_from);
                        data.project_id_to = String(data.project_id_to);

                        // Init for delete detail
                        data._delete_detail = [];
    
                        vm.data = data;
                        // vm.details = vm.data.detail;
                        for(var i=0; i<vm.data.detail.length; i++) {
                            vm.data.detail[i].item_id = String(vm.data.detail[i].item_id);
                            vm.data.detail[i]._hash = vm._getRandomHash();
                        }

                        vm.changeProject(vm.data.project_id_from);

                        vm.loadProject();

                        console.log(vm.data);
                    } else {
                        vm.ls.get('loading').on();
                        vm.message = resp.data.errors;
                    }
                    vm.ls.get('loading').off();
                });
            }

            this.loadProject();

        };

        this.loadProject = function(){
            // Load Project list
            ApiService.Project.all({page: 'all'}).then(function(resp) {
                var data = resp.data.data;
                for (var i = 0; i < data.length; i++) {
                    data[i].id = String(data[i].id);
                }
                vm.projects = data;
                console.log(vm.projects);
            });
        };

        // Submit to DB
        this.submit = function(){
            vm.ls.get('loading').on();
            var postData = this._createPostData(this.data);
    
            if(vm.sendToManager == 1) {
                postData.status.name = 'pending';
            }
            vm.sendToManager = 0; // Set back to 0

            ApiService.Transfer.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    if(!$stateParams.id)
                        $state.go('transfer-detail', { 'id': resp.data.data });
                    else
                        vm.create();
                    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                vm.ls.get('loading').off();
            });
    
        };

        this._createPostData = function(data){
            var postData = JSON.parse(JSON.stringify(data));
    
            delete postData.revisions;
    
            for(var i=0; i<postData.detail.length; i++) {
                delete postData.detail[i]._hash;
                delete postData.detail[i].$$hashKey;
            }
    
            return postData;
        };

        // Project Stock
        this.changeProject = function(id){
            vm.ls.get('loading-item').on();
            var params = {
                project_id: id
            };
            ApiService.Project.stock(params).then(function(resp) {
                var data = resp.data.data;
                for (var i = 0; i < data.length; i++) {
                    data[i].item_id = String(data[i].item_id);
                }
                vm.items = data;

                vm.ls.get('loading-item').off();
            });
        };

        //Load bahan
        this.changeBahan = function(itemId){
            var item = null;

            for(var i=0; i<this.items.length; i++) {
                var x = this.items[i];
                if(x.item_id == itemId) {
                    item = x;
                    break;
                }
            }

            if(!item){
                this.newItem();
            } else {
                this.tempItem = {
                    id: null,
                    item_id: String(item.item_id),
                    name: item.name,
                    qty: item.qty,
                    unit: item.unit,
                    price: 0,
                    _hash: Math.getRandomHash()
                };
            }
        };

        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.Transfer.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                vm.ls.get('loading').off();
            });
        };

        this.doSearch = function() {
            this.search.order_by = this.orderBy.toString();
            if(this.listPending == true){
                this.search.status_page = 'pending';
                $state.go('transfer-pending', this.search);
            } else {
                $state.go('transfer', this.search);    
            }
        };

        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };

        this.newItem = function() {
            this.tempItem = {
                id: '',
                item_id : '',
                name: '',
                qty: 0,
                unit: '?',
                price: 0,
                _hash: this._getRandomHash()
            };
        };

        this.deleteItem = function(item) {
            if(item.id)
                this.data._delete_detail.push(item.id);
    
            this.data.detail.removeByObject(item);
        }
    
        this.editItem = function(item) {
            this.tempItem = item;
        };
    
        this.addItem = function() {
            var isNew = true;
    
            // Check if this job update
            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i]._hash === this.tempItem._hash) {
                    isNew = false;
                }
            }

            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i].name === this.tempItem.name) {
                    isNew = false;
                }
            }
    
            if(isNew)
                // If new just push to detail
                // this.tempItem.name = this.data.item_id.name;
                this.data.detail.push(this.tempItem);
    
            // and rerender it
            this.newItem();
    
            return;
        };
    
        this._getRandomHash = function() {
            var s1, s2;
            s1 = String(Math.floor(Math.random()*1000));
            s2 = String(new Date().getTime());
            return s1 + s2;
        };

        this.setApproved = function(id) {
            this.setStatus(id, 'approve');
        };
    
        this.setDeclined = function(id) {
            this.setStatus(id, 'decline');
        };

        this.setDraft = function(id) {
            this.setStatus(id, 'draft');
        };

        this.setPending = function(id) {
            this.setStatus(id, 'pending');
        };

        this.setStatus = function(id, status) {
            vm.ls.get('loading-' + id).on();
            ApiService.Transfer.setStatus(id, status).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {
                    //For refresh / re-init
                    if (vm.listPending == true) {
                        vm.init();
                    } else {
                        vm.create();
                    }
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                vm.ls.get('loading-' + id).off();
            });
        };

    };
})();