(function() {
    'use strict';
    
    angular.module('app').controller('ProjectStockController', ProjectStockController);
    
    function ProjectStockController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        // Paginator
        this.paginator = {};
    
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this._delete = [];
        this.parent_id = $stateParams.id;
    
        this.init = function() {
            vm.ls.get('loading').on();
            this._delete = [];

            var search = {};
            search.type = 'bahan';

            if($stateParams.id)
                search.project_id = $stateParams.id;

            console.log('ds',search);

            ApiService.Project.stock(search).then(function(resp) {
                console.log(resp);
                var data = resp.data;
                vm.list = data.data;
    
                // If list less than 3
                vm.ls.get('loading').off();
            });
        };    
    };
})();