(function() {
    'use strict';
    
    angular.module('app').controller('StockController', StockController);
    
    function StockController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        
        // Paginator
        this.paginator = {};
    
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this._delete = [];
        this.parent_id = $stateParams.id;
    
        this.init = function() {
            console.log('Stock list init');
            var search = {};

            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.status)
                search.status = $stateParams.status;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;

            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];

                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }

            vm.search = search;

            vm._doSearch();
        };

        // Search transfer
        this._doSearch = function() {
            vm.ls.get('loading').on();
            console.log('do search', this.search);
            // ApiService.Stock.all(vm.search).then(function(resp) {
            //     var data = resp.data;
            //     vm.list = data.data;

            //     if(parseInt(vm.search.page) > data._meta.last_page){
            //         // Redirect to last_page
            //         vm.search.page = data._meta.last_page;
            //         vm.doSearch();
            //     }

            //     // Paginator
            //     vm.paginator = Paginator(data);
            vm.ls.get('loading').off();
            // });
        };

        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();