(function() {
    'use strict';
    
    angular.module('app').controller('UnitController', UnitController);
    
    function UnitController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
    
        // Paginator
        this.paginator = {};
        
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this.selectedHash = null;
        this.propertyName = 'label';
        this.reverse = true;
        this.prop = 0;
    
        this.createEmptyData = function() {
            return {
                id: null,
                name: null,
                label: null,
                group_by: 'unit',
                _hash: Math.getRandomHash(),
            };
        };
    
        this.init = function() {
            console.log('Unit list init');
            this.createEmptyData();
            this.selectedHash = null;
            var search = {};
    
            // if($stateParams.created)
            //     search.created = $stateParams.created;
            // if($stateParams.status)
            //     search.status = $stateParams.status;
            search.group_by = 'unit';
    
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm.search = search;
            vm.prop = 1;
            vm._doSearch();
        };
    
        // Search invoice
        this._doSearch = function() {
            vm.ls.get('loading').on();
            console.log('do search', this.search);
            ApiService.Category.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
                vm.list.forEach(function(x) {
                    x._hash = Math.getRandomHash();
                    x.prop = 1;
                })
    
                vm.ls.get('loading').off();
            });
        };

        // Add blank category
        this.add = function() {
            // if(vm.selectedHash === null) {
                // Create new empty unit data
                var unit = vm.createEmptyData();
                vm.prop = 2;
            //     vm.selectedHash = unit._hash;
            //     vm.list.push(unit);
            // }
        };
    
        this.create = function() {
            vm.ls.get('loading').on();
            this.createEmptyData();
            var id = $stateParams.id;
            if(id){
                ApiService.Category.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;

                        vm.data = data;
                    } else {
                        vm.error.on();
                        vm.message = resp.data.errors;
                        window.location = "#!/unit/list/";
                    }
                });
            }
        };
    
        // Submit to DB
        this.submit = function(unit){
            vm.ls.get('loading').on();
            var postData = this._createPostData(unit);
            ApiService.Category.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.init();
    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
    
                vm.ls.get('loading').off();
            });
    
        };
    
        this._createPostData = function(data){
            var postData = {
                id: data.id,
                name: data.name,
                label: data.label,
                group_by: 'unit'
            };
    
            return postData;
        };
    
        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.Category.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading').off();
            });
        };

        // Redirect to correct route
        this.doSearch = function() {
            console.log('search', this.search);
            this.search.order_by = this.orderBy.toString();
            $state.go('category', this.search);
        };

        // Edit unit
        this.edit = function(unit) {
            vm.selectedHash = unit._hash;
        };
  
        
        this.sortBy = function(columnName) {
            vm.reverse = (vm.propertyName === columnName) ? !vm.reverse : false;
            vm.propertyName = columnName;
            
            // this.orderBy.setColumn(columnName);
    
            // Generate search params
            // this.search.order_by = this.orderBy.toString();
            // this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();