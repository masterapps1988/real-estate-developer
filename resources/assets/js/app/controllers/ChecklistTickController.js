(function() {
    'use strict';
    
angular.module('app').controller('ChecklistTickController', ChecklistTickController);

function ChecklistTickController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.orderBy = new OrderBy();
        // Paginator
    this.paginator = {};
    this.list = [];
    this.data = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    this.createEmptyData = function() {
        return {
            user_id: null,
            detail: [],
            _delete: [],
            _hash: Math.getRandomHash(),
        };
    };

    this.init = function() {
        this.createEmptyData();
        var search = {};

        vm.search = search;

        vm._doSearch();
    };

    this._doSearch = function() {
        vm.ls.get('loading').on();
    };

    this.create = function() {
        // vm.ls.get('loading').on();
        var id = $stateParams.project_id;
        var userId = $stateParams.user_id;

        if(id){
            vm.ls.get('loading').on();
            ApiService.Project.Checklist.Tick.detail(id, userId).then(function(resp) {
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    data.project_id = String(id);
                    data.user_id = String(userId);
                    
                    vm.data = data;
                    vm.data.project_id = String(vm.data.project_id);
                    // vm.data.user_id = String(vm.data.user_id);
                    data._delete_detail = [];
                    data.detail = [];

                    vm.ls.get('loading').off();
                } else {
                    vm.fm.error(resp.data.message);
                    $state.go('project'); 
                }
            });
        }
    };

    // Add blank category
    this.add = function(x) {
        this.temp = {
            id: x.id,
            is_checked: x.is_checked,
            _hash: Math.getRandomHash()
        };
        console.log(this.temp);
        this.data.detail.push(this.temp);
    };

   // Submit to DB
   this.submit = function(){
        vm.ls.get('loading').on();
        console.log('submit',this.data);
        var postData = this._createPostData(this.data);
        
        ApiService.Project.Checklist.Tick.create(postData).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });
    };

    this._createPostData = function(data){
        // var postData = data;
        var postData = {
            user_id: data.user_id,
            project_id: data.project_id,
            detail: data.detail
        };

        for(var i=0; i<postData.detail.length; i++) {
            delete postData.detail[i]._hash;
            delete postData.detail[i].is_edit;
            delete postData.detail[i].$$hashKey;
        }
        
        return postData;
    };

    this.delete = function(checklist) {
        if(checklist.id) {
            this.data._delete_detail.push(checklist.id);
        }

        this.data.detail.removeByObject(checklist);
    };
};
    
})();