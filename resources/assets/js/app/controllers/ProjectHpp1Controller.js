(function() {
'use strict';

angular.module('app').controller('ProjectHpp1Controller', ProjectHpp1Controller);

function ProjectHpp1Controller($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.editMode = false;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;
    this.parent_id = $stateParams.id;

    // Warranty card
    this.list = null;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            name: null,
            customer_id: null,
            start: null,
            finish: null,
            ref_no: null,
            city_id: null,
            address: null,
            status_id: null
        };
    };

    this.init = function() {
        console.log('Project list init');
        var search = {};

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm.search = search;

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        ApiService.Project.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    // Redirect to correct route
    this.doSearch = function() {
        this.search.order_by = this.orderBy.toString();
        $state.go('project', this.search);
    };

    this.setHpp1 = function(quotation_id) {
        ApiService.Project.setHpp(this.parent_id, quotation_id).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                vm.fm.success(resp.data.message);
                $state.go('project-detail', { 'id': vm.parent_id });
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });
    };

    this.create = function() {
        this.createEmptyData();
        var projectId = $stateParams.id;
        if(projectId){
            vm.ls.get('loading-project').on();
            ApiService.Project.get(projectId).then(function(resp) {
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    data.customer_id = String(data.customer_id);
                    data.city_id = String(data.city_id);
                    vm.data = data;
                    vm.ls.get('loading-project').off();

                    // Load quotation(HPP 1)
                    var search = {
                        status: 'approve',
                        condition: 'available',
                        customer_id: data.customer_id
                    };

                    vm.ls.get('loading-quotation').on();
                    ApiService.Quotation.all(search).then(function(resp) {
                        console.log(resp);
                        var data = resp.data;
                        vm.list = data.data;

                        vm.ls.get('loading-quotation').off();
                    });
                } else {
                    vm.message = resp.data.errors;
                    window.location = "#!/project/list/";
                }
            });
        }

        // Load customer list
        ApiService.Customer.all({page: 'all'}).then(function(resp) {
            var data = resp.data;
            vm.customers = data.data;
            vm.customers.id = String(vm.customers.id);
        });

        // Load cities list
        ApiService.City.all().then(function(resp) {
            var data = resp.data;
            vm.cities = data.data;
            vm.cities.id = String(vm.cities.id);
            vm.ls.get('loading').off();
        });

    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        $state.go('project', this.search);
    };

    this._createPostData = function(data){
        var postData = {
            id: data.id,
            name: data.name,
            customer_id: data.customer_id,
            start_date: data.start,
            finish_date: data.finish,
            status_id: data.status_id,
            ref_no: data.ref_no,
            city_id: data.city_id,
            address : data.address
        };

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Project.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.edit = function() {
        this.editMode = true;
    };

    this.isEditMode = function() {
        return this.editMode;
    };
};
})();