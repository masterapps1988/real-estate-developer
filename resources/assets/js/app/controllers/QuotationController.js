(function() {
'use strict';

angular.module('app').controller('QuotationController', QuotationController);

function QuotationController($state, $window, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.isEditJob = false;
    this.listPending = false;
    this.reverse = true;
    this.propertyName = 'name';
    this.sort = {
        active: '',
        descending: undefined
    }

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;
    this.expandList = {
        id: [],
        state: []
    };

    // Quotation List
    this.list = null;

    // Temp job in box bottom
    this.tempJob = {
        id: 0,
        item_id: '',
        qty: 0,
        unit: '?',
        price: 0,
        price_profit: 0,
        detail: []
    };

    this.createPriceDetail = function() {
        this.priceDetail = {
            detailTotal: 0,
            total: 0,
            dpp: 0,
            ppn: 0,
            pph: 0,
            grandTotal: 0
        };
    }

    this.setStatus = null;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            quotation_id: null,
            customer_id: null,
            city_id: null,
            ref_no: null,
            name: null,
            address: null,
            created: null,
            created_at: null,
            updated_at: null,
            is_tax: "0",
            pph: "2",
            profit: 0,
            risk_factor: 0,
            status: {
                name: 'draft'
            },
            price_profit: 0,
            revisions: [],
            detail: [],

            // List of detail id that want to be deleted
            _delete_detail: []
        };
    };

    this.scrollToJob = function() {
        scrollTo($('#job'), 500);
    };

    this.isCreate = function() {
        return !$stateParams.id;
    };

    this.isDraft = function() {
        return this.isStatus('draft') || $stateParams.parent_id;
    };

    this.isPending = function() {
        return this.isStatus('pending');
    };

    this.isRevision = function() {
        return this.isStatus('revision');
    };

    this.isStatus = function(status) {
        var data = this.data;

        return this.data.status.name === status;
    };

    this.init = function() {
        console.log('Quotation list init');
        var search = vm.search;

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
        if($stateParams.status_page == 'pending')
            this.listPending = true;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        if (this.listPending == true) {
            vm.search.status = 'pending';
        }
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        this.search.in_project = 0;
        ApiService.Quotation.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
        // Load Customer list
        var status = {
            group_by: "status",
            name: "draft,pending,revision,approve"
        };
        ApiService.Category.all(status).then(function(resp) {
            var data = resp.data;
            vm.status = data.data;
        });
    };

    this.create = function() {
        this.createEmptyData();
        // For detail quotation
        var id = $stateParams.id;

        if($stateParams.parent_id) {
            id = $stateParams.parent_id;
        }

        if(id) {
            vm.ls.get('loading').on();
            ApiService.Quotation.get(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm.loadJobs();
                    vm.loadUnit();
                    vm.loadCustomer();
                    vm.detailTotal = 0;

                    var data = resp.data.data;
                    data.customer_id = String(data.customer_id);
                    data.city_id = String(data.city_id);
                    data.is_tax = String(data.is_tax);
                    data.pph = String(data.pph);
                    if (data.is_tax == 0) {
                        data.pph = '2';
                    }

                    if($stateParams.parent_id) {
                        data.parent_id = $stateParams.parent_id;
                    }

                    // Init for delete detail
                    data._delete_detail = [];

                    vm.data = data;
                    for(var i=0; i<vm.data.detail.length; i++) {
                        vm.expandList.id.push(vm.data.detail[i].id);
                        vm.expandList.state.push(false);
                        vm.data.detail[i]._hash = vm._getRandomHash();
                        vm.data.detail[i].item_id = String(vm.data.detail[i].item_id);
                    }
                    vm.groups = vm._groupingDetail(vm.data.detail);
                    vm.countAll();
                } else {
                    vm.ls.get('loading').on();
                    vm.message = resp.data.errors;
                }
                vm.ls.get('loading').off();
            });
        }

        // Load cities list
        ApiService.City.all().then(function(resp) {
            var data = resp.data;
            vm.cities = data.data;
        });

        this.loadCustomer();
        this.loadJobs();
        this.loadUnit();
    };

    this.loadJobs = function() {
        var search = {
            page: 'all'
        }
        ApiService.Jobs.all(search).then(function(resp) {
            var data = resp.data;
            vm.jobs = data.data;
            for (var i = 0; i < vm.jobs.length; i++) {
                vm.jobs[i].id = String(vm.jobs[i].id);
            }
        });
    };

    this.loadCustomer = function() {
    // Load Customer list
        ApiService.Customer.all({page: 'all'}).then(function(resp) {
            var data = resp.data;
            vm.customers = data.data;
            for (var i = 0; i < vm.customers.length; i++) {
                vm.customers[i].id = String(vm.customers[i].id);
            }
        });
    };

    this.loadUnit = function() {
        var search = {
            group_by: 'unit',
            page: 'all'
        };

        ApiService.Category.all(search).then(function(resp) {
            var data = resp.data;
            vm.units = data.data;
        });
    };

    this.loadJobDetail = function(id) {
        vm.ls.get('job').on();
        ApiService.Jobs.get(id).then(function(resp) {
            var data = resp.data;
            vm.tempJob = data.data;
            vm.tempJob.item_id = String(data.data.id);
            vm.tempJob.id = null;
            vm.tempJob.qty = 0;
            vm.tempJob.price = 0;
            for (var i = 0; i < vm.tempJob.detail.length; i++) {
                var total = vm.tempJob.detail[i].qty * vm.tempJob.detail[i].price;
                vm.tempJob.detail[i].id = null;
                vm.tempJob.detail[i].item_id = vm.tempJob.detail[i].item_id_detail;
                vm.tempJob.price += total;
            }
            vm.tempJob.price_profit = vm.countProfit(vm.tempJob.price);
            vm.tempJob._hash = vm._getRandomHash();
            vm.ls.get('job').off();
            console.log(vm.tempJob);
        });
    };

    this.countPercentage = function() {
        var profit = 0;
        var risk_factor = 0;

        if (vm.data.profit != null && vm.data.profit != '') {
            profit = parseFloat(vm.data.profit);
        }

        if (vm.data.risk_factor != null && vm.data.risk_factor != '') {
            risk_factor = parseFloat(vm.data.risk_factor);
        }

        var percent = profit + risk_factor;
        return parseFloat(percent);
    };

    this.countProfit = function(price) {
        var percent = vm.countPercentage();

        var total = parseFloat(price) * (percent / 100);
        return parseFloat(price) + total;
    };

    this.countTotalDetail = function() {
        vm.tempJob.price = 0;

        for (var i = 0; i < vm.tempJob.detail.length; i++) {
            var total = vm.tempJob.detail[i].qty * vm.tempJob.detail[i].price_profit;
            vm.tempJob.price += total;
        }
        vm.tempJob.price_profit = vm.tempJob.price;
    };

    this.countTotal = function() {

    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        if(this.listPending == true){
            this.search.order_by = this.orderBy.toString();
            this.search.status_page = 'pending';
            $state.go('quotation-pending', this.search);
        } else {
            this.search.order_by = this.orderBy.toString();
            $state.go('quotation', this.search);
        }
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        console.log('Data', this.data);

        var postData = this._createPostData(this.data);

        if(vm.sendToManager == 1) {
            postData.status = 'pending';
        }
        vm.sendToManager = 0; // Set back to 0

        console.log('post data', postData);
        ApiService.Quotation.create(postData).then(function(resp){
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('quotation-detail', { 'id': resp.data.data });
                else
                    vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            console.log(resp);
            vm.ls.get('loading').off();
        });

    };

    this.newJob = function() {
        this.tempJob = {
            item_id: '',
            qty: 0,
            unit: '?',
            price: 0,
            price_profit: 0,
            _hash: this._getRandomHash()
        };
    };

    this.deleteJob = function(job) {
        if(job.id)
            this.data._delete_detail.push(job.id);

        this.data.detail.removeByObject(job);
        this.groups = this._groupingDetail(this.data.detail);
        this.countAll();
    }

    this.editJob = function(job) {
        console.log(job);
        this.tempJob = job;
    };

    this.addJob = function() {
        var isNew = true;
        console.log('add job', this.tempJob);

        // Check if this job update
        for(var i=0; i<this.data.detail.length; i++) {
            if(this.data.detail[i]._hash === this.tempJob._hash) {
                this.data.detail[i] = this.tempJob;
                isNew = false;
            }
        }

        if(isNew){
            // If new just push to detail
            var isError = false;
            for (var i = 0; i < this.data.detail.length; i++) {
                if (this.tempJob.item_id == this.data.detail[i].item_id) {
                    isError = true;
                    vm.fm.error('Pekerjaan sudah ada.');
                }
            }
            if (!isError && this.tempJob.item_id) {
                this.data.detail.push(this.tempJob);
            }
        }

        // and rerender it
        this.groups = this._groupingDetail(this.data.detail);
        this.newJob();
        this.countAll();
        return;
    };

    this._getRandomHash = function() {
        var s1, s2;
        s1 = String(Math.floor(Math.random()*1000));
        s2 = String(new Date().getTime());
        return s1 + s2;
    };

    // Grouping quote details
    this._groupingDetail = function(details) {
        var gd = new GroupingDetail(details);

        return gd.get();
    };

    this._createPostData = function(data){
        var postData = JSON.parse(JSON.stringify(data));

        console.log("Data : " + postData);

        if (postData.is_tax == 0) {
            postData.pph = 0;
        }

        delete postData.revisions;

        for(var i=0; i<postData.detail.length; i++) {
            delete postData.detail[i]._hash;
            delete postData.detail[i].$$hashKey;
        }

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Quotation.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.setApproved = function(id) {
        this.setStatus(id, 'approve');
        this.create();
    };

    this.setRevision = function(id) {
        this.setStatus(id, 'revision');
        this.create();
    };

    this.setDraft = function(id) {
        this.setStatus(id, 'draft');
        this.create();
    };

    this.setPending = function(id) {
        this.setStatus(id, 'pending');
        this.create();
    };

    this.setStatus = function(id, status) {
        vm.ls.get('loading-' + id).on();
        ApiService.Quotation.setStatus(id, status).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                //For refresh / re-init
                if (vm.listPending == true) {
                    vm.init();
                } else {
                    vm.create();
                }
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading-' + id).off();
        });
    };

    this.sortByAngular = function(propertyName) {
        var sort = this.sort;

        if (sort.active == propertyName) {
            sort.descending = !sort.descending;
        }
        else {
            sort.active = propertyName;
            sort.descending = false;
        }

        this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
        this.propertyName = propertyName;
    };

    this.getIconSort = function(column) {
    var sort = vm.sort;
    if (sort.active == column) {
        return sort.descending
        ? 'fa-sort-desc'
        : 'fa-sort-asc';
        }
    return 'fa-sort';
    };

    this.download = function(id, customer){
        vm.ls.get('print').on();
        var params = {
            id: id,
            customer: customer
        };
        ApiService.Quotation.download(params).then(function(resp) {
            if(!resp.data.is_error) {
                $window.open(resp.data.data.url);
                vm.ls.get('print').off();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

    this.changePrice = function() {
        var percent = vm.countPercentage();

        for (var i = 0; i < vm.data.detail.length; i++) {
            var total = vm.data.detail[i].price * (percent / 100);
            vm.data.detail[i].price_profit = parseFloat(vm.data.detail[i].price) + total;
        }

        vm.groups = vm._groupingDetail(vm.data.detail);

        vm.countAll();
    };

    this.countAll = function() {

        vm.createPriceDetail();

        for (var i = 0; i < vm.data.detail.length; i++) {
            vm.priceDetail.total += vm.data.detail[i].price_profit;
        }
        vm.priceDetail.dpp = vm.priceDetail.total * 1.1;

        if (vm.data.is_tax == 1) {
            vm.priceDetail.ppn = vm.priceDetail.dpp * 0.1;
        } else {
            vm.priceDetail.ppn = 0;
        }

        if (vm.data.pph == 2 && vm.data.is_tax == 1) {
            vm.priceDetail.pph = vm.priceDetail.dpp * 0.2;
        } else if (vm.data.pph == 4 && vm.data.is_tax == 1) {
            vm.priceDetail.pph = vm.priceDetail.dpp * 0.4;
        } else {
            vm.priceDetail.pph = 0;
        }

        vm.priceDetail.grandTotal = vm.priceDetail.dpp + vm.priceDetail.ppn + vm.priceDetail.pph;

    };

    this.setPpn = function() {
        vm.countAll();
    };

    this.setPph = function() {
        vm.countAll();
    };

    this.selectGroup = function(name) {
        vm.tempJob.group_by = name;
    };

    this.expand = function(id) {
        for (var i = 0; i < vm.expandList.id.length; i++) {
            if (vm.expandList.id[i] == id) {
                if (vm.expandList.state[i] == false) {
                    vm.expandList.state[i] = true;
                } else if (vm.expandList.state[i] == true){
                    vm.expandList.state[i] = false;
                }
            }
        }
    };

    this.getExpandList = function(id) {
        for (var i = 0; i < vm.expandList.id.length; i++) {
            if (vm.expandList.id[i] == id) {
                return vm.expandList.state[i];
            }
        }
    };

    this.createRevision = function() {
        var id = vm.data.id;

        vm.ls.get('revision').on();
        ApiService.Quotation.createRevision(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm.ls.get('revision').off();
                vm.create();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            console.log(resp);
        });
    };

};
})();