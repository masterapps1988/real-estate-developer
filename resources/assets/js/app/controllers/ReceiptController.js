(function() {
    'use strict';
    
    angular.module('app').controller('ReceiptController', ReceiptController);
    
    function ReceiptController($state, $window, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        this.isEditItem = false;
        this.isList = true;
        this.listPending = false;
        this.reverse = true;
        this.propertyName = 'name';
        this.sort = {
            active: '',
            descending: undefined
        };
        this.project = [];
        this.purchaseOrderList = [];
    
        // Paginator
        this.paginator = {};
    
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Receipt List
        this.list = null;
    
        // Temp item in box bottom
        this.tempItem = {
            item_id: 0,
            name: '',
            qty: 0,
            unit: '',
        };
    
        this.setStatus = null;
    
        this.createEmptyData = function() {
            this.data = {
                id: null,
                purchase_order_id: null,
                ref_no: null,
                is_in: 1,
                created: null,
                created_at: null,
                updated_at: null,
                status: {
                    name: 'draft'
                },
                detail: [],
    
                // List of detail id that want to be deleted
                _delete_detail: []
            };
        };
    
        this.scrollToItem = function() {
            scrollTo($('#item'), 500);
        };

        this.clear = function() {
            vm.search.keyword = '';
            vm.search.project_id = '';
            vm.search.purchase_order_id = '';

            vm._doSearch();
        };
    
        this.isCreate = function() {
            return !$stateParams.id;
        };
    
        this.isDraft = function() {
            return this.isStatus('draft');
        };

        this.isDecline = function() {
            return this.isStatus('decline');
        };
    
        this.isPending = function() {
            return this.isStatus('pending');
        };
    
        this.isStatus = function(status) {
            return this.data.status.name === status;
        };
    
        this.init = function() {
            console.log('Receipt list init');
            var search = vm.search;
            this.isList = true;
   
            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.ref_no)
                search.ref_no = $stateParams.ref_no;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            if($stateParams.project_id)
                search.project_id = $stateParams.project_id;
            if($stateParams.purchase_order_id)
                search.purchase_order_id = $stateParams.purchase_order_id;
            if($stateParams.status)
                search.status = $stateParams.status;
            if($stateParams.status == 'pending')
                this.listPending = true;
            
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm._doSearch();
        };
    
        // Search invoice
        this._doSearch = function() {
            if (this.listPending == true) {
                vm.search.status = 'pending';
            }
            vm.ls.get('loading').on();
            console.log('do search', this.search);
            ApiService.Receipt.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }
    
                // Paginator
                vm.paginator = Paginator(data);
    
                vm.ls.get('loading').off();
            });
            this.getProjectList();
            this.getPurchaseOrderList();
        };

        this.getProjectList = function() {
            ApiService.Project.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.projects = data.data;
            });
        };

        this.getPurchaseOrderList = function() {
            ApiService.PurchaseOrder.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.purchaseOrderList = data.data;
            });
        };
        
        this.create = function() {
            this.createEmptyData();
            this.isList = false;
            // For detail quotation
            var id = $stateParams.id;
            var poId = $stateParams.purchase_order_id;
            if(poId)
                vm.data.purchase_order_id = poId;
            
            if(id) {
                vm.ls.get('loading').on();
                ApiService.Receipt.get(id).then(function(resp) {
                    console.log(resp);
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        data.purchase_order_id = String(data.purchase_order_id);
                        
                        ApiService.PurchaseOrder.itemListAvailable(data.purchase_order_id).then(function(resp) {
                            var data = resp.data;
                            vm.items = data.data;
                        });

                        // Init for delete detail
                        data._delete_detail = [];
    
                        vm.data = data;
                        for(var i=0; i<vm.data.detail.length; i++) {
                            vm.data.detail[i]._hash = vm._getRandomHash();
                        }

                        console.log('ajax detail', vm.data);
                    } else {
                        vm.ls.get('loading').on();
                        vm.message = resp.data.errors;
                    }
                    vm.ls.get('loading').off();
                });
            }

            ApiService.PurchaseOrder.itemListAvailable(vm.data.purchase_order_id).then(function(resp) {
                var data = resp.data;
                vm.items = data.data;
            });
    
            // Load Project list
            ApiService.PurchaseOrder.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.purchases = data.data;
            });
    
            console.log(vm.data);
        };
    
        this.showPage = function(page) {
            this.search.page = page;
            this.doSearch();
        };
    
        // Redirect to correct route
        this.doSearch = function() {
            console.log('search', this.search);
            this.search.order_by = this.orderBy.toString();
            $state.go('receipt', this.search);    
        
        };
    
        // Submit to DB
        this.submit = function(){
            vm.ls.get('loading').on();
            var postData = this._createPostData(this.data);
    
            if(vm.sendToManager == 1) {
                postData.status.name = 'pending';
            }
            vm.sendToManager = 0; // Set back to 0

            console.log('post data', postData);
            ApiService.Receipt.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    if(!$stateParams.id)
                        $state.go('receipt-detail', { 'id': resp.data.data });
                    else
                        vm.create();
    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                console.log(resp);
                vm.ls.get('loading').off();
            });
    
        };
    
        this.newItem = function() {
            this.tempItem = {
                id: null,
                item_id: '',
                name: '',
                qty: 0,
                unit: '',
                _hash: this._getRandomHash()
            };
        };
    
        this.deleteItem = function(item) {
            if(item.id)
                this.data._delete_detail.push(item.id);
    
            this.data.detail.removeByObject(item);
        }
    
        this.editItem = function(item) {
            this.tempItem = item;
            this.tempItem.item_id = String(this.tempItem.item_id);

            vm.items.id = String(item.id);
        };
    
        this.addItem = function() {
            var isNew = true;
            console.log('add job');
    
            // Check if this job update
            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i]._hash === this.tempItem._hash) {
                    isNew = false;
                }
            }

            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i].name === this.tempItem.name) {
                    isNew = false;
                }
            }
    
            if(isNew)
                // If new just push to detail
                this.data.detail.push(this.tempItem);
    
            // and rerender it
            this.newItem();
    
            return;
        };
    
        this._getRandomHash = function() {
            var s1, s2;
            s1 = String(Math.floor(Math.random()*1000));
            s2 = String(new Date().getTime());
            return s1 + s2;
        };

        this.changePO = function(id){
            vm.ls.get('loading-item').on();
            ApiService.PurchaseOrder.itemListAvailable(id).then(function(resp) {
                var data = resp.data;
                vm.items = data.data;
                vm.ls.get('loading-item').off();
            });
        };
        
        this.changeItem = function(itemId){
            var item = null;

            for(var i=0; i<this.items.length; i++) {
                var x = this.items[i];
                if(x.item_id == itemId) {
                    item = x;
                    break;
                }
            }

            if(!item){
                this.newItem();                
            } else {
                this.tempItem = {
                    id: item.id,
                    item_id: String(item.item_id),
                    name: item.name,
                    qty: item.qty,
                    unit: item.unit,
                    _hash: Math.getRandomHash()
                };
            }
        }
    
        this._createPostData = function(data){
            var postData = JSON.parse(JSON.stringify(data));
    
            console.log("Data : " + postData);
    
            delete postData.revisions;
    
            for(var i=0; i<postData.detail.length; i++) {
                delete postData.detail[i]._hash;
                delete postData.detail[i].$$hashKey;
            }
    
            return postData;
        };
    
        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.Receipt.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading').off();
            });
        };
    
        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    
        this.sortByAngular = function(propertyName) {
            var sort = this.sort;
    
            if (sort.active == propertyName) {
                sort.descending = !sort.descending;
            } 
            else {
                sort.active = propertyName;
                sort.descending = false;
            }
    
            this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
            this.propertyName = propertyName;
        };
    
        this.getIconSort = function(column) {
        var sort = vm.sort;
        if (sort.active == column) {
            return sort.descending
            ? 'fa-sort-desc'
            : 'fa-sort-asc';
            }
        return 'fa-sort';
        }

        this.download = function(id){
            ApiService.Receipt.download(id).then(function(resp) {
                if(!resp.data.is_error) {
                    $window.open(resp.data.data.url);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
            });
        };

        this.setApproved = function(id) {
            this.setStatus(id, 'approve');
        };
    
        this.setDeclined = function(id) {
            this.setStatus(id, 'decline');
        };

        this.setStatus = function(id, status) {
            vm.ls.get('loading-' + id).on();
            ApiService.Receipt.setStatus(id, status).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {
                    if(vm.isList)
                        vm.init();
                    else
                        vm.create();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading-' + id).off();
            });
        };

    };
    })();