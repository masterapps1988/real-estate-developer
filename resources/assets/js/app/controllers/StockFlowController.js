(function() {
    'use strict';
    
    angular.module('app').controller('StockFlowController', StockFlowController);
    
    function StockFlowController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;

        this.Math = Math;
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        
        // Paginator
        this.paginator = {};
    
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this._delete = [];
        this.parent_id = $stateParams.id;
    
        this.init = function() {
            console.log('Stock list init');
            var search = {};

            if($stateParams.project_id)
                search.project_id = $stateParams.project_id;
            if($stateParams.item_id)
                search.item_id = $stateParams.item_id;
            if($stateParams.category)
                search.category = $stateParams.category;
            if($stateParams.date_from)
                search.date_from = $stateParams.date_from;
            if($stateParams.date_to)
                search.date_to = $stateParams.date_to;
            if($stateParams.page)
                search.page = $stateParams.page;

            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];

                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }

            vm.search = search;

            vm._doSearch();

            // Load category
            vm.loadType();
        };

        // Search transfer
        this._doSearch = function() {
            vm.ls.get('loading').on();
            console.log('do search', this.search);
            ApiService.StockFlow.all(vm.search).then(function(resp) {
                vm.loadProject();
                vm.loadItem();

                var data = resp.data;
                vm.list = data.data;

                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }

                // Paginator
                vm.paginator = Paginator(data);
            vm.ls.get('loading').off();
            });
        };

        // Redirect to correct route
        this.doSearch = function() {
            console.log('search', this.search);
            this.search.order_by = this.orderBy.toString();
            $state.go('stock-flow', this.search);    
        
        };

        this.loadProject = function() {
            var search = {
                page: 'all'
            };

            ApiService.Project.all(search).then(function(resp) {
                var data = resp.data;
                vm.projects = data.data;
            });
        };

        this.loadItem = function() {
            var search = {
                page: 'all',
                item: 'item,vendor,mandor'
            };

            ApiService.Item.all(search).then(function(resp) {
                var data = resp.data;
                vm.items = data.data;
            });
        };

        this.loadType = function() {
            this.categories = [
                {name: 'receipt', value: 'LPB(Masuk)'},
                {name: 'expense', value: 'LPB(Keluar)'},
                {name: 'transfer', value: 'Transfer'}
            ];
        };

        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    };
})();