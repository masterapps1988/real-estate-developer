(function() {
'use strict';

angular.module('app').controller('CustomerController', CustomerController);

function CustomerController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            name: null,
            address: null,
            city_id: null,
            phone: null,
            npwp: null,
            notes: null
        };
    };

    this.init = function() {
        console.log('Customer list init');
        this.createEmptyData();
        var search = {};

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm.search = search;

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        console.log(vm.search);
        ApiService.Customer.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    this.create = function() {
        vm.ls.get('loading').on();
        this.createEmptyData();
        var id = $stateParams.id;
        if(id){
            ApiService.Customer.get(id).then(function(resp) {
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    if (data.city_id) {
                        data.city_id = String(data.city_id);
                    }

                    vm.data = data;
                } else {
                    vm.error.on();
                    vm.message = resp.data.errors;
                    window.location = "#!/customer/list/";
                }
            });
        }

        // Load cities list
        ApiService.City.all().then(function(resp) {
            var data = resp.data;
            vm.cities = data.data;
            
            vm.ls.get('loading').off();
        });
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        $state.go('customer', this.search);
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        console.log(this.data);
        var postData = this._createPostData(this.data);
        ApiService.Customer.add(postData).then(function(resp){
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('customer-detail', { 'id': resp.data.data });

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });

    };

    this._createPostData = function(data){
        var postData = {
            id: data.id,
            name: data.name,
            address: data.address,
            city_id: data.city_id,
            phone: data.phone,
            npwp: data.npwp,
            notes: data.notes
        };

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Customer.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };
};
})();