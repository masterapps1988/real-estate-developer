(function() {
    'use strict';
    
    angular.module('app').controller('InvoicePaymentController', InvoicePaymentController);
    
    function InvoicePaymentController($state, $window, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        this.reverse = true;
        this.listPending = false;
        this.sort = {
            active: '',
            descending: undefined
        }
        this.isList = true;
    
        // Paginator
        this.paginator = {};
    
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Invoice List
        this.list = null;

        this.invoice = true;
        this.pembayaran = false;
        this.tabTitle = 'Invoice';
    
        this.createEmptyData = function() {
            this.data = {
                id: null,
                projects: null,
                project_id: null,
                user_id: null,
                ref_no: null,
                name: null,
                address: null,
                hp: null,
                city: null,
                created: null,
                created_at: null,
                updated_at: null,
                status: {
                    name: 'draft'
                },
                detail: [],

                // List of detail id that want to be deleted
                _delete_detail: []
    
            };

            this.payment = {
                id: null,
                invoice_id: null,
                bank_category_id: null,
                amount: 0,
                account_number: null,
                notes: null,
                created: null
            };
        };

        // Temp item in box bottom
        this.tempJob = {
            notes: '',
            amount: '',
        };

        this.isCreate = function() {
            return !$stateParams.id;
        };
    
        this.isDraft = function() {
            return this.isStatus('draft');
        };

        this.isApproved = function() {
            return this.isStatus('approve');
        };

        this.isDecline = function() {
            return this.isStatus('decline');
        };
    
        this.isPending = function() {
            return this.isStatus('pending');
        };
    
        this.isStatus = function(status) {
            return this.data.status.name === status;
        };

        this.init = function() {
            console.log('Invoice Mandor Vendor list init');
            var search = vm.search;
    
            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.status)
                search.status = $stateParams.status;
            if($stateParams.city_id)
                search.city_id = $stateParams.city_id;
            if($stateParams.ref_no)
                search.ref_no = $stateParams.ref_no;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.project_id)
                search.project_id = $stateParams.project_id;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            if($stateParams.status == 'pending')
                this.listPending = true;
    
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
            this.isList = true;
    
            vm.getProjectList();
            vm._doSearch();
        };

        this.getPermission = function(name) {
            var list = [];
            switch(name) {
                case 'detail':
                    list = ['invoice_mandor_vendor_detail', 'invoice_mandor_vendor_delete'];
                    if(this.listPending)
                        list = ['invoice_mandor_vendor_detail', 'invoice_mandor_vendor_delete',
                            'invoice_mandor_vendor_change_status'];
                    break;
            }

            return list;
        };
    
        // Search invoice
        this._doSearch = function() {
            if (this.listPending == true) {
                vm.search.status = 'pending';
            }
            vm.search.is_sell = 'false';
            vm.ls.get('loading').on();
            console.log('do search', this.search);
            ApiService.Invoice.all(vm.search).then(function(resp) {
                var data = resp.data;
                
                vm.list = data.data;
                vm.list.total = vm._totalInvoicePerPage(data.data);
    
                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }
    
                // Paginator
                vm.paginator = Paginator(data);
    
                vm.ls.get('loading').off();
            });
        };
        
        this.getProjectList = function() {
            ApiService.Project.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.projects = data.data;
            });
        };

        this.create = function() {
            this.createEmptyData();
            // For detail quotation
            var id = $stateParams.id;

            if($stateParams.parent_id) {
                id = $stateParams.parent_id;
            }

            if($stateParams.opname_id) {
                this.data.opname_id = $stateParams.opname_id;
                this.loadOpname($stateParams.opname_id);
            }

            vm.data.created = DateFormatter.date(new Date())
            if(id) {
                vm.ls.get('loading').on();
                ApiService.Invoice.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        data.project_id = String(data.project_id);
    
                        // Init for delete detail
                        data._delete_detail = [];
                        
                        vm.loadPaymentList(data.id);

                        vm.data = data;
                        for(var i=0; i<vm.data.detail.length; i++) {
                            vm.data.detail[i]._hash = vm._getRandomHash();
                        }
                        vm.total = vm._totalInvoice(vm.data.detail);
                        console.log('ajax detail', vm.data);
                    } else {
                        vm.ls.get('loading').on();
                        vm.message = resp.data.errors;
                    } console.log(vm.data);
                    vm.ls.get('loading').off();
                });
            }
            this.getProjectList();
            this.loadBankList();
            this.isList = false;
        };

        this.newPayment = function() {
            this.payment = {
                id: null,
                invoice_id: null,
                bank_category_id: null,
                amount: 0,
                account_number: null,
                notes: null,
                created: DateFormatter.date()
            };
        };

        this.editPayment = function(id) {
            ApiService.Invoice.Payment.get(id).then(function(resp) {
                console.log(resp);
                var data = resp.data;
                vm.payment = data.data;

                vm.loadBankList();

                vm.payment.bank_category_id = String(vm.payment.bank_category_id);
            });
        };

        this.loadUser = function(id) {
            ApiService.User.getDetail(id).then(function(resp) {
                var data = resp.data;
                vm.data.customer = data.data;
            });
        };

        this.loadOpname = function(id) {
            ApiService.InvoiceMandorVendor.getOpname(id).then(function(resp) {
                var data = resp.data;
                vm.data = data.data;
                vm.data.project_id = String(vm.data.project_id);
                vm.data._delete_detail = [];
                vm.total = vm._totalInvoice(vm.data.detail);
            });
        };

        this.submitPayments = function() {
            vm.ls.get('loading').on();
            var postData = this._createPayment(this.payment);
            console.log('post data', postData);
            ApiService.Invoice.Payment.add(postData).then(function(resp){
                if(!resp.data.is_error) {
                    vm.loadPaymentList(vm.data.id);
                    vm.newPayment();

                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                console.log(resp);
                vm.ls.get('loading').off();
            });
        };

        this._createPayment = function(data){
            data.invoice_id = vm.data.id;
            var postData = JSON.parse(JSON.stringify(data));
    
            console.log("Data : " + postData);
    
            return postData;
        };
    
        this.loadPaymentList = function(invoiceId) {
            var search = {
                invoice_id: invoiceId,
                page: 'all'
            }

            ApiService.Invoice.Payment.all(search).then(function(resp) {
                console.log(resp);
                var data = resp.data;
                vm.payments = data.data;
            });
        };

        this.deletePayment = function(id) {
            vm.ls.get('loading').on();
            ApiService.Invoice.Payment.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm.create();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading').off();
            });
        };

        this.loadBankList = function(invoiceId) {
            var search = {
                group_by: 'bank',
                page: 'all'
            }

            ApiService.Bank.all(search).then(function(resp) {
                console.log(resp);
                var data = resp.data;
                vm.banks = data.data;
                vm.bank = vm.banks[0];
                for (var i = 0; i < vm.banks.length; i++) {
                    vm.banks[i].id = String(vm.banks[i].id);
                }

            });
        };

        this.showPage = function(page) {
            this.search.page = page;
            this.doSearch();
        };
    
        // Redirect to correct route
        this.doSearch = function() {
            console.log('search', this.search);
            this.search.order_by = this.orderBy.toString();
            $state.go('invoice-payment', this.search);
        };
    
        // Submit to DB
        this.submit = function(){
            vm.ls.get('loading').on();
            var postData = this._createPostData(this.data);
    
            if(vm.sendToManager == 1) {
                postData.status.name = 'pending';
            }
            vm.sendToManager = 0; // Set back to 0

            console.log('post data', postData);
            ApiService.InvoiceMandorVendor.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    if(!$stateParams.id)
                        $state.go('invoice-payment-detail', { 'id': resp.data.data });
                    else
                        vm.create();
    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                console.log(resp);
                vm.ls.get('loading').off();
            });
    
        };
    
        this._createPostData = function(data){
            var postData = JSON.parse(JSON.stringify(data));
    
            console.log("Data : " + postData);
    
            for(var i=0; i<postData.detail.length; i++) {
                delete postData.detail[i]._hash;
                delete postData.detail[i].$$hashKey;
            }
    
            return postData;
        };
    
        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.Invoice.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading').off();
            });
        };

        this.newJob = function() {
            this.tempJob = {
                notes: '',
                amount: '',
                _hash: this._getRandomHash()
            };
        };
    
        this.deleteJob = function(job) {
            if(job.id)
                this.data._delete_detail.push(job.id);
    
            this.data.detail.removeByObject(job);
            this.totals = this._totalInvoice(this.data.detail);
        }
    
        this.editJob = function(job) {
            this.tempJob = job;
        };
    
        this.addJob = function() {
            var isNew = true;
            console.log('add job');
    
            // Check if this job update
            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i]._hash === this.tempJob._hash) {
                    isNew = false;
                }
            }
    
            if(isNew)
                // If new just push to detail
                this.data.detail.push(this.tempJob);
    
            // and rerender it
            vm.total = this._totalInvoice(this.data.detail);
            this.newJob();
            
            return;
        };
    
        this._getRandomHash = function() {
            var s1, s2;
            s1 = String(Math.floor(Math.random()*1000));
            s2 = String(new Date().getTime());
            return s1 + s2;
        };

        // Grouping quote details
        this._totalInvoice = function(details) {
            var list = [];
            var i, x;
            var total = 0;
            
            for(i=0; i<details.length; i++) {
                
                x = details[i];
                
                total += parseInt(x.amount);
                
            }
            return total;
        };

        // Grouping quote details
        this._totalInvoicePerPage = function(details) {
            var list = [];
            var i, x;
            var total = 0;
            
            for(i=0; i<details.length; i++) {
                
                x = details[i];
                
                total += parseInt(x.total);
                
            }
            return total;
        };
    
        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    
        this.sortByAngular = function(propertyName) {
            var sort = this.sort;
    
            if (sort.active == propertyName) {
                sort.descending = !sort.descending;
            } 
            else {
                sort.active = propertyName;
                sort.descending = false;
            }
    
            this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
            this.propertyName = propertyName;
        };
    
        this.getIconSort = function(column) {
            var sort = vm.sort;
            if (sort.active == column) {
                return sort.descending
                ? 'fa-sort-desc'
                : 'fa-sort-asc';
                }
            return 'fa-sort';
        }

        this.download = function(id){
            ApiService.Invoice.download(id).then(function(resp) {
                if(!resp.data.is_error) {
                    $window.open(resp.data.data.url);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
            });
        };

        this.setApproved = function(id) {
            this.setStatus(id, 'approve');
        };
    
        this.setDeclined = function(id) {
            this.setStatus(id, 'decline');
        };

        this.setStatus = function(id, status) {
            vm.ls.get('loading-' + id).on();
            ApiService.Invoice.setStatus(id, status).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {
                    if(vm.isList)
                        vm.init();
                    else
                        vm.create();

                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading-' + id).off();
            });
        };

        this.openTab = function(name) {
            if (name == 'invoice') {
                vm.pembayaran = false;
                vm.invoice = true;
                vm.tabTitle = 'Invoice';
            }
            if (name == 'pembayaran') {
                vm.invoice = false;
                vm.pembayaran = true;
                vm.tabTitle = 'Pembayaran';
            }
        };
    
    };
    })();