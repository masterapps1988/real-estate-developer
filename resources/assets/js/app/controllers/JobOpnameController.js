(function() {
'use strict';

angular.module('app').controller('JobOpnameController', JobOpnameController);

function JobOpnameController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;
    this.listPending = false;
    this.isList = true;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            project_id: null,
            user_id: null,
            ref_no: null,
            created: DateFormatter.date(new Date()),
            is_stock: 0,
            status: {
                name: 'draft'
            },
            is_invoice_created: 0,
            detail: [],
            // List of detail id that want to be deleted
            _delete_detail: [],
            _delete_images: []
        };
    };

    this.images = [];

    this.init = function() {
        var search = {};

        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
        if($stateParams.status == 'pending')
            this.listPending = true;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        this.isList = true;
        vm.search = search;

        vm._doSearch();
    };

    this.getPermission = function(name) {
        var list = [];
        switch(name) {
            case 'detail':
                list = ['opname_job_detail', 'opname_job_delete'];
                if(this.listPending)
                    list = ['opname_job_detail', 'opname_job_delete',
                        'opname_job_change_status'];
                break;
        }

        return list;
    };

    // Search invoice
    this._doSearch = function() {
        vm.ls.get('loading').on();
        if (this.listPending == true) {
            vm.search.status = 'pending';
        }
        console.log('do search', this.search);
        console.log(vm.search);
        ApiService.JobsOpname.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;
            console.log(vm.list);

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    this.getStocks = function(project_id, user_id) {
        var search = {
            project_id: project_id,
            user_id: user_id
        };
        if (project_id && user_id) {
            ApiService.Project.progress(search).then(function(resp){
                var data = resp.data.data;
                for (var i = 0; i < data.length; i++) {
                    data[i].progress = parseFloat(data[i].done);
                    data[i].real = parseFloat(data[i].real);
                    data[i].difference = parseFloat(data[i].target) - data[i].real - data[i].progress;
                }
                vm.data.detail = data;
                console.log(vm.data.detail);
                vm.groups = vm._groupingDetail(vm.data.detail);
                vm.countTotal();
            });
        }
    };

    this.create = function() {
        this.createEmptyData();
        var id = $stateParams.id;
        if(id){
            vm.ls.get('loading').on();
            ApiService.JobsOpname.get(id).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {
                    var data = resp.data.data;

                    data._delete_detail = [];
                    data._delete_images = [];

                    for (var i = 0; i < data.detail.length; i++) {
                        data.detail[i].real = parseFloat(data.detail[i].real);
                        data.detail[i].progress = parseFloat(data.detail[i].done);
                        data.detail[i].difference = parseFloat(data.detail[i].target) - data.detail[i].real - data.detail[i].progress;
                    }

                    vm.data = data;
                    vm.data.project_id = String(vm.data.project_id);
                    vm.data.user_id = String(vm.data.user_id);

                    vm.loadProject();
                    vm.groups = vm._groupingDetail(vm.data.detail);
                    console.log(vm.groups);
                    vm.countTotal();
                    vm.isList = false;
                    vm.ls.get('loading').off();
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
            });
        } else {
            vm.getStocks(id);
        }
        this.loadProject();
        this.loadUser();
        this.createPriceDetail();
    };

    // Grouping quote details
    this._groupingDetail = function(details) {
        var list = [];
        var i, x;
        var group;

        for(i=0; i<details.length; i++) {
            x = details[i];
            x.difference = x.target - x.progress;

            group = list.find(function(el) {
                return el.name === x.group_by;
            });

            if(group){
                group.detail.push(x);
            }else{
                list.push({
                    name: x.group_by,
                    detail: [x]
                });
            }
        }
        return list;
    };

    this.loadProject = function() {
        ApiService.Project.all({page: 'all'}).then(function(resp) {
            var data = resp.data;
            vm.projects = data.data;
            for (var i = 0; i < vm.projects.length; i++) {
                vm.projects.id = String(vm.projects.id);
            }
        });
    };

    this.loadUser = function() {
        var search = {
            roles: 'mandor,vendor',
            page: 'all'
        };

        ApiService.Admin.all(search).then(function(resp) {
            var data = resp.data;
            vm.users = data.data;
            for (var i = 0; i < vm.users.length; i++) {
                vm.users.id = String(vm.users.id);
            }
        });
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        $state.go('job-opname', this.search);
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        var postData = this._createPostData(this.data, this.images);
        console.log('postData', postData);

        if(vm.sendToManager == 1) {
            postData.data.status.name = 'pending';
        }
        vm.sendToManager = 0; // Set back to 0

        ApiService.JobsOpname.create(postData).then(function(resp){
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                        $state.go('job-opname-detail', { 'id': resp.data.data });
                    else
                        vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });

    };

    this._createPostData = function(data, images){
        var postData = JSON.parse(JSON.stringify(data));

        console.log("Data : " + postData);

        delete postData.revisions;

        var temp = [];

        // add edited data to detail
        for(var i = 0; i < vm.groups.length; i++) {
            for(var j = 0; j < vm.groups[i].detail.length; j++) {
                temp.push(vm.groups[i].detail[j]);
            }
        }

        postData.detail = temp;

        return {data: postData, images: images};
    };

    this.deleteImage = function(image) {
        if(image.id)
            this.data._delete_images.push(image.id);

        this.data.images.removeByObject(image);
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.JobsOpname.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.createPriceDetail = function() {
        this.priceDetail = {
            total: 0,
            retensi: 0,
            paid: 0
        };
    };

    this.countTotal = function() {
        vm.createPriceDetail();

        //counting All Group
        for (var j = 0; j < vm.groups.length; j++) {
            for (var i = 0; i < vm.groups[j].detail.length; i++) {
                vm.priceDetail.total += vm.groups[j].detail[i].progress * vm.groups[j].detail[i].price;
            }
        }
        vm.priceDetail.retensi = vm.priceDetail.total * (10 / 100);

        vm.priceDetail.paid = vm.priceDetail.total - vm.priceDetail.retensi;
    };

    this.createInvoice = function() {
        var params = {
            opname_id: vm.data.id
        }
        $state.go('invoice-payment-create', params);
    };

    this.detailInvoice = function() {
        var params = {
            id: vm.data.invoice_id
        }
        $state.go('invoice-payment-detail', params);
    };

    this.isDraft = function() {
        return this.isStatus('draft');
    };

    this.isApproved = function() {
        return this.isStatus('approve');
    };

    this.isDecline = function() {
        return this.isStatus('decline');
    };

    this.isPending = function() {
        return this.isStatus('pending');
    };

    this.isStatus = function(status) {
        return this.data.status.name === status;
    };

    this.setApproved = function(id) {
        this.setStatus(id, 'approve');
    };

    this.setDeclined = function(id) {
        this.setStatus(id, 'decline');
    };

    this.setStatus = function(id, status) {
        vm.ls.get('loading-' + id).on();
        ApiService.JobsOpname.setStatus(id, status).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                if(vm.isList)
                    vm.init();
                else
                    vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading-' + id).off();
        });
    };

};
})();