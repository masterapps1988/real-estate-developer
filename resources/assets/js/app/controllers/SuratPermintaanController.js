(function() {
    'use strict';
    
    angular.module('app').controller('SuratPermintaanController', SuratPermintaanController);
    
    function SuratPermintaanController($state, $window, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
    
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        this.isEditItem = false;
        this.listPending = false;
        this.reverse = true;
        this.propertyName = 'name';
        this.sort = {
            active: '',
            descending: undefined
        } 
    
        // Paginator
        this.paginator = {};
    
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Quotation List
        this.list = null;
    
        // Temp job in box bottom
        this.tempItem = {
            id: '',
            item_id: '',
            name: '',
            qty: 0,
            unit: '?',
            price: 0
        };
    
        this.setStatus = null;
    
        this.createEmptyData = function() {
            this.data = {
                id: null,
                project_id: null,
                status_id: null,
                is_open: null,
                ref_no: null,
                notes: null,
                created: null,
                created_at: null,
                updated_at: null,
                status: {
                    name: 'draft'
                },
                detail: [],
    
                // List of detail id that want to be deleted
                _delete_detail: []
            };
        };
    
        this.scrollToItem = function() {
            scrollTo($('#item'), 500);
        };
    
        this.isCreate = function() {
            return !$stateParams.id;
        };
    
        this.isDraft = function() {
            return this.isStatus('draft');
        };

        this.isDecline = function() {
            return this.isStatus('decline');
        };

        this.isApprove = function() {
            return this.isStatus('decline');
        };
    
        this.isPending = function() {
            return this.isStatus('pending');
        };
    
        this.isStatus = function(status) {
            return this.data.status.name === status;
        };
    
        this.init = function() {
            console.log('Surat Permintaan list init');
            var search = vm.search;
    
            if($stateParams.created)
                search.created = $stateParams.created;
            if($stateParams.status)
                search.status = $stateParams.status;
            if($stateParams.city_id)
                search.city_id = $stateParams.city_id;
            if($stateParams.ref_no)
                search.ref_no = $stateParams.ref_no;
            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.project_id)
                search.project_id = $stateParams.project_id;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            if($stateParams.status == 'pending')
                this.listPending = true;
    
            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];
    
                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }
    
            vm._doSearch();
        };

        this.getPermission = function(name) {
            var list = [];
            switch(name) {
                case 'detail':
                    list = ['sp_detail', 'sp_delete'];
                    if(this.listPending)
                        list = ['sp_detail', 'sp_delete', 'sp_change_status'];
                    break;
            }

            return list;
        };
    
        // Search invoice
        this._doSearch = function() {
            if (this.listPending == true) {
                vm.search.status = 'pending';
            }
            vm.ls.get('loading').on();
            console.log('do search', this.search);
            ApiService.PurchaseOrder.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;
    
                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }
    
                // Paginator
                vm.paginator = Paginator(data);
    
                vm.ls.get('loading').off();
            });
            this.getProjectList();
        };

        this.getProjectList = function() {
            ApiService.Project.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.projects = data.data;
            });
        };
        
        this.create = function() {
            this.createEmptyData();
            // For detail quotation
            var id = $stateParams.id;

            if($stateParams.parent_id) {
                id = $stateParams.parent_id;
            }
            
            if ($stateParams.project_id) {
                this.changeProject($stateParams.project_id);
            }
    
            if(id) {
                vm.ls.get('loading').on();
                ApiService.PurchaseOrder.get(id).then(function(resp) {
                    if(!resp.data.is_error) {
                        var data = resp.data.data;
                        data.project_id = String(data.project_id);
                        
                        if($stateParams.parent_id) {
                            data.parent_id = $stateParams.parent_id;
                        }
    
                        // Init for delete detail
                        data._delete_detail = [];
    
                        vm.data = data;
                        // vm.details = vm.data.detail;
                        for(var i=0; i<vm.data.detail.length; i++) {
                            vm.data.detail[i].item_id = String(vm.data.detail[i].item_id);
                            vm.data.detail[i]._hash = vm._getRandomHash();
                        }

                        vm.changeProject(vm.data.project_id);

                        // console.log('ajax detail', vm.data);
                    } else {
                        vm.ls.get('loading').on();
                        vm.message = resp.data.errors;
                    }
                    vm.ls.get('loading').off();
                });
            }
    
            // Load Project list
            ApiService.Project.all({page: 'all'}).then(function(resp) {
                var data = resp.data;
                vm.projects = data.data;
            });
            
            //If have state for project_id and status is create or undefined id
            if($stateParams.project_id && !id){
                vm.data.project_id = String($stateParams.project_id); 
            }
            
            // console.log(vm.data);
        };
    
        this.showPage = function(page) {
            this.search.page = page;
            this.doSearch();
        };
    
        // Redirect to correct route
        this.doSearch = function() {
            console.log('search', this.search);
            if(this.listPending == true){
                this.search.order_by = this.orderBy.toString();
                this.search.status = 'pending';
                $state.go('sp', this.search);
            } else {
                this.search.order_by = this.orderBy.toString();
                $state.go('sp', this.search);    
            }
        };
    
        // Submit to DB
        this.submit = function(){
            vm.ls.get('loading').on();
            console.log(vm.sendToManager, this.data);
    
            var postData = this._createPostData(this.data);
    
            if(vm.sendToManager == 1) {
                postData.status.name = 'pending';
            }
            vm.sendToManager = 0; // Set back to 0

            if(this.isDraft() || this.isPending() || this.isDecline()){
                postData.is_open = 1;
            } else {
                postData.is_open = 0;
            }
    
            console.log('post data', postData);
            ApiService.PurchaseOrder.create(postData).then(function(resp){
                if(!resp.data.is_error) {
                    if(!$stateParams.id)
                        $state.go('sp-detail', { 'id': resp.data.data });
                    else
                        vm.create();
    
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                }
                console.log(resp);
                vm.ls.get('loading').off();
            });
    
        };
    
        this.newItem = function() {
            this.tempItem = {
                id: '',
                item_id : '',
                name: '',
                qty: 0,
                unit: '?',
                price: 0,
                _hash: this._getRandomHash()
            };
        };
    
        this.deleteItem = function(item) {
            if(item.id)
                this.data._delete_detail.push(item.id);
    
            this.data.detail.removeByObject(item);
        }
    
        this.editItem = function(item) {
            this.tempItem = item;
        };
    
        this.addItem = function() {
            var isNew = true;
            console.log('add item');
    
            // Check if this job update
            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i]._hash === this.tempItem._hash) {
                    isNew = false;
                }
            }

            for(var i=0; i<this.data.detail.length; i++) {
                if(this.data.detail[i].name === this.tempItem.name) {
                    isNew = false;
                }
            }
    
            if(isNew)
                // If new just push to detail
                // this.tempItem.name = this.data.item_id.name;
                this.data.detail.push(this.tempItem);
    
            // and rerender it
            this.newItem();
    
            return;
        };
    
        this._getRandomHash = function() {
            var s1, s2;
            s1 = String(Math.floor(Math.random()*1000));
            s2 = String(new Date().getTime());
            return s1 + s2;
        };
    
        this._createPostData = function(data){
            var postData = JSON.parse(JSON.stringify(data));
    
            console.log("Data : " + postData);
    
            delete postData.revisions;
    
            for(var i=0; i<postData.detail.length; i++) {
                delete postData.detail[i]._hash;
                delete postData.detail[i].$$hashKey;
            }
    
            return postData;
        };
    
        this.delete = function(id) {
            vm.ls.get('loading').on();
            ApiService.PurchaseOrder.delete(id).then(function(resp) {
                if(!resp.data.is_error) {
                    vm._doSearch();
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading').off();
            });
        };
    
        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };
    
        this.setApproved = function(id) {
            this.setStatus(id, 'approve');
        };
    
        this.setDeclined = function(id) {
            this.setStatus(id, 'decline');
        };

        this.setDraft = function(id) {
            this.setStatus(id, 'draft');
        };

        this.setPending = function(id) {
            this.setStatus(id, 'pending');
        };
    
        this.setStatus = function(id, status) {
            vm.ls.get('loading-' + id).on();
            ApiService.PurchaseOrder.setStatus(id, status).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {
                    if(vm.listPending == true){
                        if(status == 'approve'){
                            $state.go('po-detail', { 'id': id });
                        } else {
                            vm.init();
                        }
                    } else {
                        if(status == 'approve'){
                            $state.go('po-detail', { 'id': id });
                        } else {
                            $state.go($state.current, {}, {reload: true});
                        }
                    }
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading-' + id).off();
            });
        };
    
        this.sortByAngular = function(propertyName) {
            var sort = this.sort;
    
            if (sort.active == propertyName) {
                sort.descending = !sort.descending;
            } 
            else {
                sort.active = propertyName;
                sort.descending = false;
            }
    
            this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
            this.propertyName = propertyName;
        };
    
        this.getIconSort = function(column) {
        var sort = vm.sort;
        if (sort.active == column) {
            return sort.descending
            ? 'fa-sort-desc'
            : 'fa-sort-asc';
            }
        return 'fa-sort';
        }

        this.changeProject = function(id){
            vm.ls.get('loading-item').on();
            ApiService.Project.itemListAvailable(id).then(function(resp) {
                var data = resp.data.data;
                for (var i = 0; i < data.length; i++) {
                    data[i].item_id = String(data[i].item_id);
                }
                vm.items = data;

                vm.ls.get('loading-item').off();
            });
        };
        
        this.changeBahan = function(itemId){
            var item = null;

            for(var i=0; i<this.items.length; i++) {
                var x = this.items[i];
                if(x.item_id == itemId) {
                    item = x;
                    break;
                }
            }

            if(!item){
                this.newItem();
            } else {
                this.tempItem = {
                    id: null,
                    item_id: String(item.item_id),
                    name: item.name,
                    qty: item.qty,
                    unit: item.unit,
                    price: 0,
                    _hash: Math.getRandomHash()
                };
            }
        };

        this.download = function(id){
            ApiService.PurchaseOrder.downloadSp(id).then(function(resp) {
                if(!resp.data.is_error) {
                    $window.open(resp.data.data.url);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
            });
        };
    };
    })();
