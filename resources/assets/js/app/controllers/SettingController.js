(function() {
'use strict';

angular.module('app').controller('SettingController', SettingController);

function SettingController($state, $stateParams, ApiService, LoadingService, StateFactory,
FlashMessageService) {
    var vm = this;

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;
    this.data = {};

    this.searchHistory = {};

    this.init = function() {
        this.ls.get('loading').on();
        ApiService.Setting.get().then(function(resp) {
            var data = resp.data.data;

            // Prevent vm.data array
            console.log('init', data, Array.isArray(data));
            if(!Array.isArray(data))
                vm.data = data;

            vm.ls.get('loading').off();
        });
    };

    // Submit to DB
    this.submit = function()
    {
        this.ls.get('loading').on();
        console.log(this.data);
        ApiService.Setting.create(this.data).then(function(resp) {
            if(!resp.data.is_error) {
                vm.init();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
        });
        vm.ls.get('loading').off();
    };
};
})();