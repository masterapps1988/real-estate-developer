(function() {
'use strict';

angular.module('app').controller('ProjectPricePlanJobsController', ProjectPricePlanJobsController);

function ProjectPricePlanJobsController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.MAX_WORKER = 3;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;
    this._delete = [];
    this.parent_id = $stateParams.id;

    this.groupTotal = 0;
    this.quotationTotal = 0; // Parent total

    this.total = 0;
    this.group_name = null;
    this.item_category = 'item';

    this.tempJob = {
            id: null,
            name: null,
            unit: '?',
            qty: 0,
            price: 0,
            notes: null
    };

    this.params = 0;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            project_id: null,
            ref_no: null,
            detail: [],
            _delete_detail: []
        };
    };


    this.init = function() {
        vm.ls.get('loading').on();
        this._delete = [];

        ApiService.Worker.all($stateParams.id).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            // If list less than 3
            for(var i=vm.list.length; i<vm.MAX_WORKER; i++) {
                vm.list.push({
                    name: 'Test'
                });
            }
            vm.ls.get('loading').off();
        });

        ApiService.Admin.all({
            roles: 'manager-operasional,operasional,logistic'
        }).then(function(resp) {
            var data = resp.data;
            vm.users = data.data;
        });
    };

    this.create = function() {
        vm.ls.get('loading').on();
        this.createEmptyData();
        var quotation_id = $stateParams.quotation_id;
        var quotation_detail_id = $stateParams.quotation_detail_id;
        if(quotation_id){
            vm.loadItems('item');
            var params = {
                id: quotation_id
            };

            ApiService.PricePlan.get(params).then(function(resp) {
                if(!resp.data.is_error) {
                    var respData = resp.data.data;
                    respData._delete_detail = [];
                    vm.data = respData;

                    for (var i = 0; i < vm.data.detail.length; i++) {
                        if (quotation_detail_id == vm.data.detail[i].id) {
                            vm.params = i;
                        }
                        vm.data.detail[i]._delete_detail = [];
                        for (var j = 0; j < vm.data.detail[i].detail.length; j++) {
                            vm.data.detail[i].detail[j]._hash = vm._getRandomHash();
                            vm.data.detail[i].detail[j].item_id = String(vm.data.detail[i].detail[j].item_id);
                            vm.data.detail[i].detail[j].is_additional = String(vm.data.detail[i].detail[j].is_additional);
                        }
                    }

                    console.log(vm.data);

                    vm.createTotal();
                    // vm.groups = vm._groupingDetail(vm.data.detail);
                    vm.ls.get('loading').off();
                } else {
                    vm.message = resp.data.errors;
                    $state.go('project-detail', {id: $stateParams.project_id});
                }
            });
        }
    };

    // Grouping quote details
    this._groupingDetail = function(details) {
        var list = [];
        var i, x;
        var group;
        var total = 0;

        for(i=0; i<details.length; i++) {
            x = details[i];

            group = list.find(function(el) {
                return el.name === x.group_by;
            });

            if(group){
                total += (x.qty * x.price);
                group.detail.push(x);
                group.total = total;
            }else{
                total = (x.qty * x.price);
                list.push({
                    name: x.group_by,
                    total : total,
                    detail: [x]
                });
            }
        }
        return list;
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();

        this.data.quotation_detail_id = $stateParams.quotation_detail_id;
        console.log(this.data);
        var postData = this._createPostData(this.data);

        if(vm.sendToManager == 1) {
            postData.status = 'pending';
        }
        vm.sendToManager = 0; // Set back to 0
        console.log('postdata', postData);
        ApiService.PricePlan.create(postData).then(function(resp){
            if(!resp.data.is_error) {
                vm.create();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            vm.ls.get('loading').off();
        });

    };

    this.delete = function(worker) {
        if(worker.id)
            this._delete.push(worker.id);

        worker.id = null;
        worker.role_name = null;
        worker.name = null;

    };

    this._createPostData = function(data){
        var postData = JSON.parse(JSON.stringify(data));

        postData.detail[vm.params].price = postData.detail[vm.params].total;

        delete postData.revisions;

        for(var i=0; i<postData.detail.length; i++) {
            delete postData.detail[i]._hash;
            delete postData.detail[i].$$hashKey;
        }

        return postData;
    };

    // Load item based on category
    // @param string item, vendor, mandor
    this.loadItems = function(data){
        vm.ls.get('loading-item').on();

        ApiService.Item.all({item: data, page: 'all'}).then(function(resp) {
            if(!resp.data.is_error) {
                var data = resp.data.data;
                vm.items = data;
                for (var i = 0; i < vm.items.length; i++) {
                    vm.items[i].id = String(vm.items[i].id);
                }
                vm.ls.get('loading-item').off();
            } else {
                vm.error.on();
                vm.message = resp.data.errors;
                $state.go('project-detail', { 'id': resp.data.data });
            }
        });
    };

    this.newJob = function() {
        this.items = {
            id: ''
        };
        this.tempJob = {
            id: null,
            item_id: '',
            name: null,
            unit: '?',
            qty: 0,
            price: 0,
            notes: null,
            is_additional: 0,
            _hash: this._getRandomHash()
        };
        this.item_category = 'item';
    };

    // Grouping quote details
    this.createTotal = function() {
        var total = 0;
        for (var i = 0; i < vm.data.detail.length; i++) {
            for (var j = 0; j < vm.data.detail[i].detail.length; j++) {
                total += vm.data.detail[i].detail[j].qty * vm.data.detail[i].detail[j].price;
            }
            vm.data.detail[i].total = total;
        }
    };

    this.addJob = function() {
        var isNew = true;

        for(var i=0; i<this.data.detail[vm.params].detail.length; i++) {
            var x = this.data.detail[vm.params].detail[i];
            if(x._hash === this.tempJob._hash) {
                isNew = false;
            }
        }

        if(isNew){
            var isError = false;
            for(var i=0; i<this.data.detail[vm.params].detail.length; i++) {
                var x = this.data.detail[vm.params].detail[i];
                if (x.item_id == this.tempJob.item_id && this.tempJob.is_additional == 0) {
                    isError = true;
                    vm.fm.error('Bahan/vendor/mandor ini sudah pernah ditambahkan. Gagal menambahkan. Silahkan lakukan edit');
                }
                if (x.item_id == this.tempJob.item_id && x.is_additional == 1) {
                    isError = true;
                    vm.fm.error('Bahan/vendor/mandor ini sudah pernah ditambahkan. Gagal menambahkan. Silahkan lakukan edit');
                }
            }
            if (!isError) {
                this.data.detail[vm.params].detail.push(this.tempJob);
            }
        }

        this.createTotal();
        this.newJob();
        return;
    };

    this._getRandomHash = function() {
        var s1, s2;
        s1 = String(Math.floor(Math.random()*1000));
        s2 = String(new Date().getTime());
        return s1 + s2;
    };

    this.setItem = function(itemId) {
        var x = null;
        for(var i=0; i<this.items.length; i++) {
            x = this.items[i];
            if(x.id == itemId) {
                vm.tempJob.item_id = String(x.id);
                vm.tempJob.name = x.name;
                vm.tempJob.qty = x.qty;
                vm.tempJob.price = x.price;
                vm.tempJob.unit = x.unit;
                vm.tempJob.notes = x.notes;
                vm.tempJob.is_additional = '0';
            }
        }
    };

    this.loadData = function(){
        ApiService.Item.get(vm.tempJob.id).then(function(resp) {
            if(!resp.data.is_error) {
                var data = resp.data.data;
                data.user_id = String(data.user_id);
                vm.tempJob.qty = data.qty;
                vm.tempJob.price = data.price;
                vm.tempJob.unit = data.unit;
                vm.tempJob.notes = data.notes;
            } else {
                // vm.error.on();
                vm.message = resp.data.errors;
            }
            vm.ls.get('loading').off();
        });
    };

    this.deleteJob = function(job) {
        if(job.id)
            this.data.detail[vm.params]._delete_detail.push(job.id);

        this.data.detail[vm.params].detail.removeByObject(job);
        this.groups = this._groupingDetail(this.data.detail);
        vm.createTotal();
    };

    this.editJob = function(job) {
        this.tempJob = job;
        console.log(job);
        // vm.item_category = data;
        this.tempJob.item_id = String(this.tempJob.item_id);

        console.log('job', this.tempJob);
        vm.loadItems(this.tempJob.item_category);
    };
};
})();