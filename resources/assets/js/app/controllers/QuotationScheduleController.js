(function() {
'use strict';

angular.module('app').controller('QuotationScheduleController', QuotationScheduleController);

function QuotationScheduleController($window, $state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.listPending = false;
    this.project = {};

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            quotation_id: null,
            customer_id: null,
            city_id: null,
            ref_no: null,
            name: null,
            address: null,
            created: null,
            created_at: null,
            updated_at: null,
            schedule_status_id: {
                name: 'draft'
            },
            revisions: [],
            detail: [],

            // List of detail id that want to be deleted
            _delete_detail: []
        };
    };

    this.isCreate = function() {
        return !this.data.schedule_status_id;
    };
    
    this.isDraft = function() {
        return this.isStatus('draft');
    };

    this.isDecline = function() {
        return this.isStatus('decline');
    };
    
    this.isPending = function() {
        return this.isStatus('pending');
    };
    
    this.isStatus = function(status) {
        if (!this.isCreate()) {
            return this.data.schedule_status_id.name === status;
        }
    };

    this.init = function() {
        console.log('schedule list init');
        this.createEmptyData();
        var search = {};

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
        if($stateParams.status == 'pending')
                this.listPending = true;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm.search = search;

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        if (this.listPending == true) {
            vm.search.status = 'pending';
        }
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        ApiService.Quotation.scheduleList(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    this.create = function() {
        vm.ls.get('loading').on();
        this.createEmptyData();
        var id = $stateParams.id;
        if(id) {
            // this.loadProject($stateParams.project_id);
            this.loadQuotation(id);
        }
    };

    this.loadQuotation = function(id) {
        ApiService.Quotation.get(id).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                var data = resp.data.data;
                for (var i = 0; i < data.detail.length; i++) {
                    var start = moment(data.detail[i].schedule_start);
                    var finish = moment(data.detail[i].schedule_finish);
                    var duration = finish.diff(start, 'days');
                    data.detail[i].duration = duration;
                }
                data._delete_detail = [];
                vm.data = data;
            } else {
                vm.message = resp.data.errors;
            }
            vm.ls.get('loading').off();
        });
    }

    this.loadProject = function(id) {
        ApiService.Project.get(id).then(function(resp) {
            if(!resp.data.is_error) {
                var data = resp.data.data;
                vm.project = data;
            } else {
                vm.message = resp.data.errors;
            }
            vm.ls.get('loading').off();
        });
    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Redirect to correct route
    this.doSearch = function() {
        console.log('search', this.search);
        this.search.order_by = this.orderBy.toString();
        $state.go('schedule', this.search);
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        var postData = this._createPostData(this.data);
        if(vm.sendToManager == 1) {
            postData.schedule_status_id = {
                name: 'pending'
            }
        }
        vm.sendToManager = 0;
        console.log(postData);
        ApiService.Quotation.schedule(postData).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                vm.create();

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });

    };

    this._createPostData = function(data){
        var postData = JSON.parse(JSON.stringify(data));

        console.log("Data : " + postData);

        delete postData.revisions;

        for(var i=0; i<postData.detail.length; i++) {
            delete postData.detail[i]._hash;
            delete postData.detail[i].$$hashKey;
        }

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Quotation.deleteSchedule(id).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.sortBy = function(columnName) {
        this.orderBy.setColumn(columnName);

        // Generate search params
        this.search.order_by = this.orderBy.toString();
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.changeDuration = function(date, duration, id) {
        if(!isNaN(duration)){
            var newDate = moment(date).add(duration, 'day').format('YYYY-MM-DD');
            vm.data.detail[id].schedule_finish = newDate;
        }
    };

    this.changeFinish = function(dataStart, dataFinish, id) {
        console.log('clicked');
        var start = moment(dataStart);
        var finish = moment(dataFinish);
        var duration = finish.diff(start, 'days');
        vm.data.detail[id].duration = duration;
    };

    this.download = function(id){
        vm.ls.get('print').on();
        ApiService.Quotation.downloadSchedule(id).then(function(resp) {
            if(!resp.data.is_error) {
                $window.open(resp.data.data.url);
                vm.ls.get('print').off();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

    this.setApproved = function(id) {
        this.setStatus(id, 'approve');
    };
    
    this.setDeclined = function(id) {
        this.setStatus(id, 'revision');
    };

    this.setDraft = function(id) {
        this.setStatus(id, 'draft');
    };

    this.setPending = function(id) {
        this.setStatus(id, 'pending');
    };

    this.setRevision = function(id) {
        this.setStatus(id, 'revision');
        this.create();
    };

    this.setStatus = function(id, status) {
            vm.ls.get('loading-' + id).on();
            ApiService.Quotation.setStatusSchedule(id, status).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {
                    //For refresh / re-init
                    if (vm.listPending == true) {
                        vm.init();
                    } else {
                        vm.create();
                    }
                    vm.fm.success(resp.data.message);
                } else {
                    vm.fm.error(resp.data.message);
                    console.log('errors', resp.data.errors);
                }
                vm.ls.get('loading-' + id).off();
            });
        };

};
})();