(function() {
'use strict';

angular.module('app').controller('ProjectController', ProjectController);

function ProjectController($state, $stateParams, ApiService, LoadingService,
FlashMessageService) {
    var vm = this;

    this.search = {};
    this.message = null;
    this.orderBy = new OrderBy();
    this.editMode = false;
    this.filter_by = null;
    this.filter_order_by = 'asc';
    this.is_search = true;

    // Paginator
    this.paginator = {};

    this.fm = FlashMessageService;
    this.ls = LoadingService;

    // Warranty card
    this.list = null;

    this.createEmptyData = function() {
        this.data = {
            id: null,
            name: null,
            customer_id: null,
            start: null,
            finish: null,
            ref_no: null,
            city_id: null,
            address: null,
            status_id: null
        };
    };

    this.init = function() {
        console.log('Project list init');
        var search = {};

        if($stateParams.created)
            search.created = $stateParams.created;
        if($stateParams.status)
            search.status = $stateParams.status;
        if($stateParams.city_id)
            search.city_id = $stateParams.city_id;
        if($stateParams.ref_no)
            search.ref_no = $stateParams.ref_no;
        if($stateParams.page)
            search.page = $stateParams.page;
        if($stateParams.keyword)
            search.keyword = $stateParams.keyword;
        if($stateParams.rangeStart)
            search.rangeStart = $stateParams.rangeStart;
        if($stateParams.rangeFinish)
            search.rangeFinish = $stateParams.rangeFinish;
        if($stateParams.filter_by)
            this.filter_by = $stateParams.filter_by;
        if($stateParams.filter_order_by)
            this.filter_order_by = $stateParams.filter_order_by;

        // Order By
        if($stateParams.order_by) {
            var list = $stateParams.order_by.split(':');
            search.order_by = {};
            search.order_by.column = list[0];
            search.order_by.ordered = list[1];

            // Generate sort
            this.orderBy.data.columnName = list[0];
            this.orderBy.data.order = list[1] == 'asc';
        }

        vm.search = search;

        vm._doSearch();
    };

    // Search invoice
    this._doSearch = function() {
        vm.ls.get('loading').on();
        console.log('do search', this.search);
        ApiService.Project.all(vm.search).then(function(resp) {
            var data = resp.data;
            vm.list = data.data;

            if(parseInt(vm.search.page) > data._meta.last_page){
                // Redirect to last_page
                vm.search.page = data._meta.last_page;
                vm.doSearch();
            }

            // Paginator
            vm.paginator = Paginator(data);

            vm.ls.get('loading').off();
        });
    };

    this.deletePricePlan = function(id) {
        vm.ls.get('loading').on();
        ApiService.PricePlan.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm.create();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    }

    // Redirect to correct route
    this.doSearch = function() {
        //For Search, convert object params to String
        if (this.is_search == true) {
            this.search.order_by = this.orderBy.toString();
        }
        //If not search, just go with the params
        $state.go('project', this.search);
    };

    this.worker = function() {
        ApiService.Worker.all($stateParams.id).then(function(resp) {
            console.log(resp);
            var data = resp.data;
            vm.worker = data.data;
        });
    };

    this.ProjectChecklist = function() {
        ApiService.Project.Checklist.Tick.all($stateParams.id).then(function(resp) {
            var data = resp.data;
            
            vm.checklist = data.data.tick;
            vm.checklist.total = data.data.total;
        });
    };

    this.create = function() {
        vm.ls.get('loading').on();
        vm.ls.get('loading-worker').on();
        vm.ls.get('loading-user').on();
        vm.ls.get('loading-hpp').on();
        this.createEmptyData();
        
        var id = $stateParams.id;
        if(id){
            vm.ls.get('loading').on();
            ApiService.Project.get(id).then(function(resp) {
                console.log(resp);
                if(!resp.data.is_error) {
                    var data = resp.data.data;
                    if (data.customer_id) {
                        data.customer_id = String(data.customer_id);
                    }
                    if (data.city_id) {
                        data.city_id = String(data.city_id);
                    }
                    data.laba = data.quotation.total - data.price_plan.total;
                    vm.data = data;
                    vm.ls.get('loading-hpp').off();
                } else {
                    vm.fm.error(resp.data.message);
                    $state.go('project'); 
                }
            });
        }

        // Load customer list
        ApiService.Customer.all({page: 'all'}).then(function(resp) {
            var data = resp.data;
            vm.customers = data.data;
            vm.customers.id = String(vm.customers.id);
        });

        // Load cities list
        ApiService.City.all().then(function(resp) {
            var data = resp.data;
            vm.cities = data.data;
            vm.cities.id = String(vm.cities.id);
            vm.ls.get('loading').off();
        });

        ApiService.Worker.all(id).then(function(resp) {
            var data = resp.data.data;
            var vendormandor = [];
            var other = [];

            for (var i = 0; i < data.length; i++) {
                if (data[i].role_name == 'vendor' || data[i].role_name == 'mandor') {
                    vendormandor.push(data[i]);
                } else {
                    other.push(data[i]);
                }
            }

            vm.worker.vendormandor = vendormandor;
            vm.worker.other = other;
            vm.ls.get('loading-worker').off();
        });

        this.ProjectChecklist();

    };

    this.showPage = function(page) {
        this.search.page = page;
        this.doSearch();
    };

    // Submit to DB
    this.submit = function(){
        vm.ls.get('loading').on();
        console.log(this.data);
        var postData = this._createPostData(this.data);
        ApiService.Project.create(postData).then(function(resp){
            console.log(resp);
            if(!resp.data.is_error) {
                if(!$stateParams.id)
                    $state.go('project-detail', { 'id': resp.data.data });

                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });

    };

    this._createPostData = function(data){
        var postData = {
            id: data.id,
            name: data.name,
            customer_id: data.customer_id,
            start_date: data.start,
            finish_date: data.finish,
            status_id: data.status_id,
            ref_no: data.ref_no,
            city_id: data.city_id,
            address : data.address
        };

        return postData;
    };

    this.delete = function(id) {
        vm.ls.get('loading').on();
        ApiService.Project.delete(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm._doSearch();
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    };

    this.stock = function(id){
        vm.ls.get('loading').on();
        var params = {
            project_id: id
        };
        ApiService.Project.stock(params).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                var data = resp.data;
                vm.stock = data;
                
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
            vm.ls.get('loading').off();
        });
    }

    this.sortBy = function(columnName, orderedBy) {
        this.search.order_by = columnName + ":" + orderedBy;
        this.search.filter_by = columnName;
        this.search.filter_order_by = orderedBy;
        this.is_search = false;
        // this.orderBy.setColumn(columnName);

        // Generate search params
        // this.search.order_by = this.orderBy.toString();
        // console.log(this.search.order_by);
        this.doSearch();
    };

    this.getOrderBy = function(columnName) {
        return this.orderBy.getClass(columnName);
    };

    this.edit = function() {
        this.editMode = true;
    };

    this.isEditMode = function() {
        return this.editMode;
    };

    this.unsetQuotation = function() {
        ApiService.Project.unsetHpp($stateParams.id).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                vm.fm.success(resp.data.message);
                vm.create();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });
    };

    this.unsetPricePlan = function() {
        ApiService.Project.unsetPricePlan($stateParams.id).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                vm.fm.success(resp.data.message);
                vm.create();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }

            vm.ls.get('loading').off();
        });
    };

    this.changeStatus = function(status) {
        vm.ls.get('loading').on();
        var params = {
            id: $stateParams.id,
            status: status
        }
        ApiService.Project.changeStatus(params).then(function(resp) {
            console.log(resp);
            if(!resp.data.is_error) {
                vm.fm.success(resp.data.message);
                vm.create();

                vm.ls.get('loading').off();
            } else {
                vm.fm.error(resp.data.message);
                console.log('errors', resp.data.errors);
            }
        });
    };

    this.createPricePlan = function() {
        var id = vm.data.id;

        vm.ls.get('create-price-plan').on();
        ApiService.Project.createPricePlan(id).then(function(resp) {
            if(!resp.data.is_error) {
                vm.ls.get('create-price-plan').off();
                var data = {
                    id: resp.data.data
                };
                $state.go('price-plan-detail', data);
                vm.fm.success(resp.data.message);
            } else {
                vm.fm.error(resp.data.message);
            }
            console.log(resp);
        });
    };
};
})();