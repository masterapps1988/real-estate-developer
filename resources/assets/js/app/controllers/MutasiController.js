(function() {
    'use strict';
    
    angular.module('app').controller('MutasiStokController', MutasiStokController);
    
    function MutasiStokController($state, $stateParams, ApiService, LoadingService,
    FlashMessageService) {
        var vm = this;
        this.search = {};
        this.message = null;
        this.orderBy = new OrderBy();
        
        // Paginator
        this.paginator = {};
    
        this.fm = FlashMessageService;
        this.ls = LoadingService;
    
        // Warranty card
        this.list = null;
        this._delete = [];
        
        this.init = function() {
            console.log('Mutasi list init');
            var search = {};

            if($stateParams.page)
                search.page = $stateParams.page;
            if($stateParams.keyword)
                search.keyword = $stateParams.keyword;
            if($stateParams.date_from)
                search.date_from = $stateParams.date_from;
            if($stateParams.date_to)
                search.date_to = $stateParams.date_to;
            if($stateParams.item)
                search.item = $stateParams.item;
            if($stateParams.type)
                search.type = $stateParams.type;

            // Order By
            if($stateParams.order_by) {
                var list = $stateParams.order_by.split(':');
                search.order_by = {};
                search.order_by.column = list[0];
                search.order_by.ordered = list[1];

                // Generate sort
                this.orderBy.data.columnName = list[0];
                this.orderBy.data.order = list[1] == 'asc';
            }

            vm.search = search;

            vm._doSearch();
        };

        // Search Mutasi
        this._doSearch = function() {
            vm.ls.get('loading').on();
            console.log('do search', this.search);
            ApiService.Mutasi.all(vm.search).then(function(resp) {
                var data = resp.data;
                vm.list = data.data;

                if(parseInt(vm.search.page) > data._meta.last_page){
                    // Redirect to last_page
                    vm.search.page = data._meta.last_page;
                    vm.doSearch();
                }

                // Paginator
                vm.paginator = Paginator(data);
                vm.ls.get('loading').off();
            });

            // Load customer list
            ApiService.Item.all({page: 'all'}).then(function(resp) {
                var data = resp.data.data;
                data.id = String(data.id);
                vm.items = data;
            });

            ApiService.Mutasi.all({page: 'all', isNotList: 'true'}).then(function(resp) {
                var data = resp.data.data;
                vm.types = data;
            });
        };

        this.doSearch = function() {
            console.log('search', this.search);
            this.search.order_by = this.orderBy.toString();
            $state.go('mutasi-stok', this.search);
        };

        this.sortBy = function(columnName) {
            this.orderBy.setColumn(columnName);
    
            // Generate search params
            this.search.order_by = this.orderBy.toString();
            this.doSearch();
        };
    
        this.getOrderBy = function(columnName) {
            return this.orderBy.getClass(columnName);
        };

        this.showPage = function(page) {
            this.search.page = page;
            this.doSearch();
        };
    };
})();