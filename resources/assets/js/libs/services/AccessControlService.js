(function() {

'use strict';

angular.module('app.service').service('AccessControlService', AccessControlService);

function AccessControlService(ApiService) {
  var vm = this;

  this.user = {};

  this.refresh = function() {
    ApiService.User.profile().then(function(resp) {
        var data = resp.data;
        vm.user = data.data;
    });
  };

  this.getUser = function() {
      return this.user;
  };

  this.setUser = function(user) {
      this.user = user;
  };

  this.hasAccess = function(name) {
      var temp = false;
      var permission = this.user.permission;

      if(Array.isArray(permission))
        for(var i=0; i<permission.length; i++) {
            if(permission[i] === name) {
                temp = true;
                break;
            }
        }

      return temp;
  };

  // Check if has accesses one of them
  this.hasAccesses = function(list) {
      var temp = false;
      for(var i = 0; i<list.length; i++) {
          if(this.hasAccess(list[i])) {
              temp = true;
              break;
          }
      }

      return temp;
  };

  this.mustHasAccesses = function(list) {
      var temp = true;
      for(var i = 0; i<list.length; i++) {
          if(!this.hasAccess(list[i])) {
              temp = false;
              break;
          }
      }

      return temp;
  };
};

})();