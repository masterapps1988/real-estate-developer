<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<style>
body {
  margin: 18pt 18pt 24pt 18pt;
}

* {
  font-family: helvetica;
}

p {
    font-size: 16px;
    font-weight: bold;
}

.pt {
    font-size: 10px;
    text-align: right;
    padding-right: 75px;
}

.no-border {
    border-spacing: 0px;
}

td {
  font-size: 10px;
}

th {
  font-size: 10px;
}

.heading table {
    width: 100%;
}

.heading .htd {
    width: 50%;
}

.body table {
    width: 100%;
}

.body .tdb {
    width: 50%;
}

.footer {
    width: 100%;
}

.footer .ttd {
    text-align: right;
    vertical-align: top;
    padding-right: 50px;
    height:100px;
}

.content table {
    width: 100%;
    border: 1px solid black;
}

.content thead tbody {
    width: 100%;
}

table.tb {
    margin-top: 20px;
    margin-bottom: 20px;
}

.tb tr:first-child th {
    border: solid black 1px;;
}

.tb tr:last-child td {
    border: solid black 1px;;
}

.thc {
    border-right: 1px solid black;
    border-bottom: 1px solid black;
    text-align: center;
    /* width: 10%; */
}

.thr {
    border-bottom: 1px solid black;
    text-align: center;
    /* width: 25%; */
}

.tdl{
    border-right: 1px solid black;
    border-left: 1px solid black;
    padding-left: 5px;
    text-align: center;
}

.tdr{
    text-align: left;
    padding-left: 5px;
    border-right: solid black 1px;
}

.tdc{
    width: 50%;
    text-align: left;
    border-right: 1px solid black;
    padding-left: 5px;
}

.tdg {
    text-align: right;
    padding-right: 5px;
    border-top: 1px solid black;
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tdgt {
    text-align: right;
    padding-right: 5px;
}

.header {
    text-align: center;
}

.header h1,
.header h2,
.header h3,
.header h4,
.header h5,
.header h6 {
    margin: 0px;
}

hr.header {
    border: 2px solid black;
}

h3 {
    margin: 0px;
}

/* ==== Table Detail ==== */
table.table-detail {
    margin-top: 10px;
    margin-bottom: 20px;
}

table.table-detail td {
    border: 1px solid black;
}

table.table-detail thead td {
    font-weight: bold;
}

table.table-detail tfoot td {
    font-weight: normal;
}

table.table-detail tbody td {
    border-top: 0px solid black;
    border-bottom: 0px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
}

/* ==== Table Footer ==== */
table.table-footer {
    width: 100%;
}

table.table-footer td.col-sign {
    width: 30%;
    height: 100px;
    border-bottom: 1px solid black
}

.col-id {
    width: 5%;
}

.col-price {
    width: 15%;
}

.text-left {
    text-align: left;
}

.text-right {
    text-align: right;
}

.text-center {
    text-align: center;
}

.text-top {
    vertical-align: top;
}

.text-bottom {
    vertical-align: bottom;
}

.ref-no {
    font-size: 10px;
}

.info-container {
    margin-top: 50px;
}

.clear {
    clear: both;
}

.info {
    margin-top: 50px;
}

p{
    margin: 0px;
    font-size: 10px;
    font-weight: normal;
}
</style>
</head>
<body>

<script type="text/php">

if ( isset($pdf) ) {

    $font = Font_Metrics::get_font("helvetica");;
    $size = 6;
    $color = array(0,0,0);
    $text_height = Font_Metrics::get_font_height($font, $size);

    $foot = $pdf->open_object();

    $w = $pdf->get_width();
    $h = $pdf->get_height();

    // Draw a line along the bottom
    $y = $h - $text_height - 24;
    $pdf->line(16, $y, $w - 16, $y, $color, 0.5);

    $pdf->close_object();
    $pdf->add_object($foot, "all");

    $text = "Page {PAGE_NUM} of {PAGE_COUNT}";

    // Center the text
    $width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
    $pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

}
</script>

<div class="header">
    <h1>{{ Setting::get('company_name') }}</h1
    <h3>{{ Setting::get('company_tag') }}</h3>
    <h6>{{ Setting::get('company_address') }}</h6>
    <h6>Tlp: {{ Setting::get('company_phone') }}</h6>
</div>
<hr class="header" />
<div style="margin-top: 30px"></div>

@section('content')
    This is the master content.
@show

</body>
</html>