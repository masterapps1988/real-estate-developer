<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<style>
body {
  margin: 18pt 18pt 24pt 18pt;
}

.footer table {
    width: 100%;
}

.footer .ttd {
    text-align: right; 
    vertical-align: top; 
    padding-right: 50px;
    height:100px;
}

.ttk{
    text-align: right; 
    width: 70%;
}

.ttkr{
    text-align: right;
    width: 30%;
    padding-right: 15px;
}

* {
  font-family: helvetica;
}

p {
    font-size: 16px;
    font-weight: bold;
}

td {
  font-size: 10px;
}

th {
  font-size: 10px;
}

.tquo tr:last-child td{
    border-bottom : solid black 1px;
}

.htd {
    width: 10%;
}
.ntd {
    width: 70%;
}

.thc {
    border : solid black 1px;
    height : 55px;
}

.grc{
    text-align: center;
    border: solid black 1px;
    width: 5%;
}
.grl{
    text-align: left;
    border: solid black 1px;
    padding-left: 10px;
}
.grr{
    text-align: right;
    border: solid black 1px;
    padding-right: 10px;
    width: 10%;
}
.grp{
    text-align: right;
    border: solid black 1px;
    padding-right: 10px;
    width: 20%;
}

.tdt{
    font-weight: bold;
    text-align: right;
    padding-right: 10px;
    border: solid black 1px;
}

.tds{
    text-align: right;
    padding-right: 10px;
    border: solid black 1px;
}

.tdr{
    text-align: right;
    padding-right: 10px;
    border: solid black 1px;
}

</style>
</head>
<body>

<script type="text/php">
    if ( isset($pdf) ) {

        $font = Font_Metrics::get_font("helvetica");;
        $size = 6;
        $color = array(0,0,0);
        $text_height = Font_Metrics::get_font_height($font, $size);

        $foot = $pdf->open_object();

        $w = $pdf->get_width();
        $h = $pdf->get_height();

        // Draw a line along the bottom
        $y = $h - $text_height - 24;
        $pdf->line(16, $y, $w - 16, $y, $color, 0.5);

        $pdf->close_object();
        $pdf->add_object($foot, "all");

        $text = "Page {PAGE_NUM} of {PAGE_COUNT}";

        // Center the text
        $width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
        $pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

    }
</script>

<div class="heading">
    <table style="width: 100%;">
        <tr>
            <td style="text-align: center;">
                <p>Purchase Order</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <table width="100%">
                    <tr><td class='htd'>No.</td><td style="width:5px;"> : </td><td class='ntd'>{{ $row->ref_no }} </td></tr>
                    <tr><td class='htd'>Project</td><td> : </td><td class='ntd'>{{ $row->project_name}} </td></tr>
                    <tr><td class='htd'>Tanggal</td><td> : </td><td class='ntd'>{{ $tanggal }}</td></tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<br>
<br>
<div class="body">
    <table class="table table-bordered tquo" cellspacing='0' width="100%">
        <thead align="center">
            <tr>
                <th class="thc">#</th>
                <th class="thc">Nama</th>
                <th class="thc">Qty</th>
                <th class="thc">Unit</th>
                <th class="thc">Harga</th>
                <th class="thc">Sub-Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($detail as $no => $detail)
                <tr>
                    <td class="grc">{{ $no + 1 }}</td>
                    <td class="grl">{{ $detail->name }}</td>
                    <td class="grr">{{ $detail->qty }}</td>
                    <td class="grr">{{ $detail->unit }}</td>
                    <td class="grp">Rp {{ number_format($detail->price, 0, '.', '.') }},-</td>
                    <td class="grp">Rp {{ number_format($detail->qty * $detail->price, 0, '.', '.') }},-</td>
                </tr>    
            @endforeach
            @if($row->is_tax == 1)
            <tr>
                <td class="tds" colspan="5">Sub-Total</td>
                <td class="tdr">Rp {{ number_format($total, 0, '.', '.') }},-</td>
            </tr>
            <tr>
                <td class="tds" colspan="5">PPN 10%</td>
                <td class="tdr">Rp {{ number_format($ppn, 0, '.', '.') }},-</td>
            </tr>
            @endif
            <tr>
                <td class="tdt" colspan="5">TOTAL</td>
                <td class="tdr"><b>Rp {{ number_format($total + $ppn, 0, '.', '.') }},-</b></td>
            </tr>
        </tbody>
    </table>
</div>
<br>
<br>
<div class="footer">
	<table cellspacing='0'>
		<tr>		
            <td class="ttd" colspan="2">Surabaya, 01 Januari 2019</td>
        </tr>
        <tr>
            <td class="ttk">(</td>
            <td class="ttkr">)</td>
        </tr>
	</table>
</div>


</body> </html>
