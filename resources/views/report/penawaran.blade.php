<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<style>
body {
  margin: 18pt 18pt 24pt 18pt;
}

* {
  font-family: helvetica;
}

p {
    font-size: 16px;
    font-weight: bold;
}

td {
  font-size: 10px;
}

th {
  font-size: 10px;
}

.tquo tr:last-child td{
    border-bottom : solid black 1px;
}

.htd {
    width: 10%;
}
.ntd {
    width: 70%;
}

.thc {
    border : solid black 1px;
    height : 55px;
    background : lightcyan; 
}

.grc{
    text-align: center;
    border: solid black 1px;
}
.grl{
    text-align: left;
    border: solid black 1px;
    padding-left: 10px;
}
.grr{
    text-align: right;
    border: solid black 1px;
    padding-right: 10px;
}

.tdt{
    font-size: 14px;
    font-weight: bold;
    text-align: right;
    padding-right: 5px;
    border: solid black 1px;
}

.number{
    text-align: left;
    border: solid black 1px;
    padding-right: 10px;
}

.currency{
    border: solid black 1px;
    text-align: right;
    padding-right: 10px;
}

</style>
</head>
<body>

<script type="text/php">

if ( isset($pdf) ) {

  $font = Font_Metrics::get_font("helvetica");;
  $size = 6;
  $color = array(0,0,0);
  $text_height = Font_Metrics::get_font_height($font, $size);

  $foot = $pdf->open_object();

  $w = $pdf->get_width();
  $h = $pdf->get_height();

  // Draw a line along the bottom
  $y = $h - $text_height - 24;
  $pdf->line(16, $y, $w - 16, $y, $color, 0.5);

  $pdf->close_object();
  $pdf->add_object($foot, "all");

  $text = "Page {PAGE_NUM} of {PAGE_COUNT}";

  // Center the text
  $width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
  $pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

}
</script>

<div class="heading">
    <table style="width: 100%;">
        <tr>
            <td style="text-align: center;">
                <p>Penawaran Harga</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <table width="100%">
                    <tr><td class='htd'>No.</td><td style="width:5px;"> : </td><td class='ntd'>{{ $quotation['ref_no'] }}</td></tr>
                    <tr><td class='htd'>Pekerjaan</td><td> : </td><td class='ntd'>{{ $quotation['name'] }} </td></tr>
                    <tr><td class='htd'>Lokasi</td><td> : </td><td class='ntd'>{{ $city['name'] }}, {{ $province['name']}}</td></tr>
                    <tr><td class='htd'>Alamat</td><td> : </td><td class='ntd'>{{ $quotation['address'] }}</td></tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<br>
<br>
<div class="body">
    <table class="table table-bordered tquo" cellspacing='0' width="100%">
        <thead align="center">
            <tr>
                <th class="thc">No</th>
                <th class="thc">Jenis Pekerjaan</th>
                <th class="thc">Qty</th>
                <th class="thc">Satuan</th>
                <th class="thc">Harga (Rp)</th>
                <th class="thc">Total</th>
            </tr>
        </thead>
        <tbody>
          <?php 
            $numGroup = 1;
            $numItem = 1;
          ?>
          @foreach($group as $g)
            <tr>
                <td class="grc number">{{ $numGroup }}</td>
                <td class="grl"><b>{{ $g }}</b></td>
                <td class="grc"></td>
                <td class="grc"></td>
                <td class="grr"></td>
                <td class="grr"></td>
            </tr>
          @foreach($details[$g] as $x)
            <tr>
                <td class="grc number">{{ $numGroup }}.{{ $numItem }}</td>
                <td class="grl">{{ $x['name'] }}</td>
                <td class="grc">{{ $x['qty'] }}</td>
                <td class="grc">{{ $x['unit'] }}</td>
                <td class="currency">{{ NumberFormat::currency($x['price']) }}</td>
                <td class="currency">{{ NumberFormat::currency($x['price'] * $x['qty']) }}</td>
            </tr>
            {{ $numItem++ }}
          @endforeach
          <tr>
                <td class="grc number">-</td>
                <td class="grl"></td>
                <td class="grc"></td>
                <td class="grc"></td>
                <td class="currency"></td>
                <td class="currency"></td>
            </tr>
            {{ $numGroup++ }}
          @endforeach
            <tr>
                <td colspan="5" class="tdt">Total</td>
                <td class="tdt">{{ NumberFormat::currency($total) }}</td>
            </tr>
        </tbody>
    </table>
</div>



</body> </html>
