@extends('report.layout')

@section('content')
<h3 class="text-center">INVOICE</h3>
<div class="ref-no text-center">#{{ $row->ref_no }}</div>

<table class="info" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="text-top">
                <p>Kepada yth,</p>
                <p><strong>{{ $row->customer_name }}</strong></p>
                <p>{{ $row->address }}</p>
                <p>{{ $row->phone }}</p>
            </td>
            <td class="text-top">
                <p><strong>Project: {{ $row->project_name }}</strong></p>
                <p>Tgl: {{ DateFormat::fullDate(new \DateTime($row->created)) }}</p>
            </td>
        </tr>
    </tbody>
</table>

<table cellspacing="0" class="table-detail" width="100%">
    <thead>
        <tr>
            <td class="col-id text-center">No.</td>
            <td class="text-left">Keterangan</td>
            <td class="col-price text-right">Jumlah</td>
        </tr>
    </thead>
    <tbody
        <!--Detail-->
        @foreach($detail as $index => $details)
        <tr>
            <td class="text-center">{{ $index + 1}}</td>
            <td class="text-left">{{ $details->notes }}</td>
            <td class="text-right">{{ NumberFormat::currency($details->amount) }},-</td>
        </tr>
        @endforeach
    </tbody>

    <tfoot>
        <tr>
            <td class="text-right" colspan="2"><strong>TOTAL</strong></td>
            <td class="text-right"><strong>{{ NumberFormat::currency($total) }},-</strong></td>
        </tr>
        <tr>
            <td class="text-right" colspan="2">Lunas</td>
            <td class="text-right">{{ NumberFormat::currency($paid) }},-</td>
        </tr>
        <tr>
            <td class="text-right" colspan="2"><strong>Jumlah Tertagih</strong></td>
            <td class="text-right"><strong>{{ NumberFormat::currency($collectible) }},-</strong></td>
        </tr>
    </tfoot>
</table>

<table class="table-footer">
    <tr>
        <td></td>
        <td class="text-top text-center">Surabaya, {{ DateFormat::fullDate(new \DateTime()) }}</td>
    </tr>
    <tr>
        <td></td>
        <td class="text-bottom text-center col-sign"><strong>Finance<strong></td>
    </tr>
</table>
@endsection