<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left" style='color: white;'>
          <p>{{ Auth::user()->name }}</p>
          {{ Auth::user()->username }}
        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu</li>
        <!-- Optionally, you can add icons to the links -->
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('project_list')"><a ui-sref="project"><i class="fa fa-circle-o"></i><span>List Project</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('quotation_list')"><a ui-sref="quotation"><i class="fa fa-circle-o"></i><span>List Penawaran</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('price_plan_list')"><a ui-sref="price-plan"><i class="fa fa-circle-o"></i><span>List HPP 2</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('quotation_schedule_list')"><a ui-sref="schedule"><i class="fa fa-circle-o"></i><span>List Penjadwalan</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('sp_list')"><a ui-sref="sp"><i class="fa fa-circle-o"></i><span>List SP</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('po_list')"><a ui-sref="po"><i class="fa fa-circle-o"></i><span>List PO</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('receipt_list')"><a ui-sref="receipt"><i class="fa fa-circle-o"></i><span>List LPB(Masuk)</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('expense_list')"><a ui-sref="expense"><i class="fa fa-circle-o"></i><span>List LPB(Keluar)</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('invoice_list')"><a ui-sref="invoice"><i class="fa fa-circle-o"></i><span>List Invoice Penjualan</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('invoice_mandor_vendor_list')"><a ui-sref="invoice-payment"><i class="fa fa-circle-o"></i><span>List Invoice Mandor/Vendor</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('transfer_list')"><a ui-sref="transfer"><i class="fa fa-circle-o"></i><span>List Transfer/Mutasi</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('opname_stock_list')"><a ui-sref="stock-opname({status: null})"><i class="fa fa-circle-o"></i><span>Stock Opname</span></a></li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('opname_job_list')"><a ui-sref="job-opname({status: null})"><i class="fa fa-circle-o"></i><span>Opname Pekerjaan</span></a></li>
        <!--         <li><a href="#"><i class="fa fa-circle-o"></i><span>List HPP 1</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i><span>List HPP 2</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i><span>List SP</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i><span>List Opname Pekerjaan</span></a></li> -->
        <li class="treeview" ui-sref-active="treeview active">
          <a href="javascript:void(0)"><i class="fa fa-circle-o"></i><span>Master</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('customer_list')"><a ui-sref="customer"><i class="fa fa-circle-o"></i><span>List Customer</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('unit_list')"><a ui-sref="category"><i class="fa fa-circle-o"></i><span>List Satuan</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('bank_list')"><a ui-sref="bank"><i class="fa fa-circle-o"></i><span>List Bank</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('role_list')"><a ui-sref="role"><i class="fa fa-circle-o"></i><span>List Jabatan</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('admin_list')"><a ui-sref="admin"><i class="fa fa-circle-o"></i><span>List Admin</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('supplier_list')"><a ui-sref="admin-role({role: 'supplier'})"><i class="fa fa-circle-o"></i><span>List Supplier</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('vendor_list')"><a ui-sref="admin-role({role: 'vendor'})"><i class="fa fa-circle-o"></i><span>List Vendor</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('mandor_list')"><a ui-sref="admin-role({role: 'mandor'})"><i class="fa fa-circle-o"></i><span>List Mandor</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('job_list')"><a ui-sref="jobs"><i class="fa fa-circle-o"></i><span>List Pekerjaan</span></a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('item_list')"><a ui-sref="item({role: 'supplier'})"><i class="fa fa-circle-o"></i>List Bahan</a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('item_mandor_list')"><a ui-sref="item({role: 'mandor'})"><i class="fa fa-circle-o"></i>List Jasa: Mandor</a></li>
            <li ui-sref-active="active" ng-if="main.ac.hasAccess('item_vendor_list')"><a ui-sref="item({role: 'vendor'})"><i class="fa fa-circle-o"></i>List Jasa: Vendor</a></li>
            
            <!-- <li><a href="#"><i class="fa fa-circle-o"></i><span>List Kota</span></a></li> -->
            <!-- <li><a href="#"><i class="fa fa-circle-o"></i><span>List Provinsi</span></a></li> -->
          </ul>
        </li>
        <li ui-sref-active="active" ng-if="main.ac.hasAccess('setting')"><a ui-sref="setting"><i class="fa fa-circle-o"></i><span>Setting</span></a></li>
        <li><a href="/logout"><i class="fa fa-circle-o"></i><span>Logout</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>