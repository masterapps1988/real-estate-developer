<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      AIS
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="http://wevelope.net/">TheBadu's</a>.</strong> All rights reserved.
  </footer>