<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth.api')->group(function(){
    // User
    Route::get('/admin/list',
    ['as' => 'api.admin.list', 'uses' => 'Api\ApiUserController@index']);
    Route::post('/admin/create',
	['as' => 'api.admin.create', 'uses' => 'Api\ApiUserController@store']);
    Route::get('/admin/detail/{id}',
    ['as' => 'api.admin.detail', 'uses' => 'Api\ApiUserController@show']);
    Route::delete('/admin/{id}',
    ['as' => 'api.admin.delete', 'uses' => 'Api\ApiUserController@destroy']);

    // Customer
    Route::get('/customer/list',
    ['as' => 'api.customer.list', 'uses' => 'Api\ApiCustomerController@index']);
    Route::get('/customer/detail/{id}',
    ['as' => 'api.customer.detail', 'uses' => 'Api\ApiCustomerController@show']);
    Route::post('/customer/create/',
    ['as' => 'api.customer.create', 'uses' => 'Api\ApiCustomerController@store']);
    Route::delete('/customer/{id}',
    ['as' => 'api.customer.create', 'uses' => 'Api\ApiCustomerController@destroy']);

    // Item
    Route::get('/item/list',
    ['as' => 'api.item.list', 'uses' => 'Api\ApiItemController@index']);
    Route::get('/item/detail/{id}',
    ['as' => 'api.item.detail', 'uses' => 'Api\ApiItemController@show']);
    Route::delete('/item/delete/{id}',
    ['as' => 'api.item.delete', 'uses' => 'Api\ApiItemController@destroy']);
    Route::post('/item/create',
    ['as' => 'api.item.create', 'uses' => 'Api\ApiItemController@store']);
    Route::get('/item/download',
    ['as' => 'api.item.download', 'uses' => 'Api\ApiItemController@download']);
    Route::get('/item/export',
    ['as' => 'api.item.export', 'uses' => 'Api\ApiItemController@export']);
    Route::post('/item/import/',
    ['as' => 'api.item.import', 'uses' => 'Api\ApiItemController@import']);

    // Item - Price History
    Route::get('/item/price-history/list',
    ['as' => 'api.item.price-history.list',
        'uses' => 'Api\ApiItemHistoryController@index']);
    Route::post('/item/price-history/create',
    ['as' => 'api.item.price-history.create',
        'uses' => 'Api\ApiItemHistoryController@store']);
    Route::delete('/item/price-history/{id}',
    ['as' => 'api.item.price-history.destory',
        'uses' => 'Api\ApiItemHistoryController@destroy']);

    // Supplier
    // Route::get('/supplier/list',
    // ['as' => 'api.supplier.list', 'uses' => 'Api\ApiUserController@getByRole']);

    // Purchase Order
    Route::get('/purchase-order/list',
    ['as' => 'api.purchase-order.list',
        'uses' => 'Api\ApiPurchaseOrderController@index']);
    Route::get('/purchase-order/detail/{id}',
    ['as' => 'api.purchase-order.list',
        'uses' => 'Api\ApiPurchaseOrderController@show']);
    Route::post('/purchase-order/create',
    ['as' => 'api.purchase-order.create',
        'uses' => 'Api\ApiPurchaseOrderController@store']);
    Route::delete('/purchase-order/{id}',
    ['as' => 'api.purchase-order.delete',
        'uses' => 'Api\ApiPurchaseOrderController@destroy']);
    Route::get('/purchase-order/{id}/set-status/{status}',
    ['as' => 'api.purchase-order.set_status',
        'uses' => 'Api\ApiPurchaseOrderController@setStatus']);
    Route::get('/purchase-order/{id}/item/list/available',
    ['as' => 'api.purchase-order.item/list.available', 
        'uses' => 'Api\ApiPurchaseOrderController@itemListAvailable']);
    Route::get('/purchase-order/{id}/set-tax/{tax}',
        ['as' => 'api.purchase-order.set_tax',
            'uses' => 'Api\ApiPurchaseOrderController@setTax']);
        
    // Quotation
    Route::get('/quotation/list',
    ['as' => 'api.quotation.list',
        'uses' => 'Api\ApiQuotationController@index']);
    Route::post('/quotation/create',
    ['as' => 'api.quotation.create',
        'uses' => 'Api\ApiQuotationController@store']);
    Route::get('/quotation/detail/{id}',
    ['as' => 'api.quotation.list',
        'uses' => 'Api\ApiQuotationController@show']);
    Route::delete('/quotation/{id}',
    ['as' => 'api.quotation.destroy',
        'uses' => 'Api\ApiQuotationController@destroy']);
    Route::get('/quotation/{id}/set-status/{status}',
    ['as' => 'api.quotation.set_status',
        'uses' => 'Api\ApiQuotationController@setStatus']);
    Route::get('/schedule/list',
    ['as' => 'api.quotation.schedule',
        'uses' => 'Api\ApiQuotationController@listSchedule']);
    Route::post('/quotation/schedule/create',
    ['as' => 'api.schedule.create',
        'uses' => 'Api\ApiQuotationController@createSchedule']);
    Route::get('/quotation/{id}/set-status/schedule/{status}',
    ['as' => 'api.quotation.set_status_schedule',
        'uses' => 'Api\ApiQuotationController@setStatusSchedule']);
    Route::delete('/schedule/{id}',
    ['as' => 'api.schedule.destroy',
        'uses' => 'Api\ApiQuotationController@destroySchedule']);
    Route::get('/quotation/create-revision/{id}',
    ['as' => 'api.quotation.create-revision',
        'uses' => 'Api\ApiQuotationController@createRevision']);

    //Price Plan
    Route::get('/price-plan/list',
    ['as' => 'api.price-plan.list',
        'uses' => 'Api\ApiPricePlanController@index']);
    Route::post('/price-plan/create',
    ['as' => 'api.price_plan.create',
        'uses' => 'Api\ApiPricePlanController@store']);
    Route::get('/price-plan/detail/{id}',
    ['as' => 'api.price-plan.detail',
        'uses' => 'Api\ApiPricePlanController@show']);
    Route::get('/price-plan/{id}/set-status/{status}',
    ['as' => 'api.price-plan.set_status',
        'uses' => 'Api\ApiPricePlanController@setStatus']);
    Route::delete('/price-plan/{id}',
    ['as' => 'api.price-plan.delete', 'uses' => 'Api\ApiPricePlanController@destroy']);

    //HPP3
    Route::get('/project/{project_id}/price-summary',
    ['as' => 'api.project.priceSummary.detail',
        'uses' => 'Api\ApiProjectController@priceSummary']);

    // Project
    Route::get('/project/list',
    ['as' => 'api.project.list', 'uses' => 'Api\ApiProjectController@index']);
    Route::post('/project/create',
    ['as' => 'api.project.create', 'uses' => 'Api\ApiProjectController@store']);
    Route::get('/project/detail/{id}',
    ['as' => 'api.project.detail', 'uses' => 'Api\ApiProjectController@show']);
    Route::delete('/project/{id}',
    ['as' => 'api.project.delete', 'uses' => 'Api\ApiProjectController@destroy']);
    Route::get('/project/{id}/create-price-plan',
    ['as' => 'api.project.create-price-plan', 'uses' => 'Api\ApiProjectController@createPricePlan']);

    // Stok Project
    Route::get('/project/{id}/stock',
    ['as' => 'api.project.stock', 'uses' => 'Api\ApiProjectController@stock']);
    Route::get('/project/{id}/mandor-vendor-progress',
    ['as' => 'api.project.progress', 'uses' => 'Api\ApiProjectController@progress']);

    // Stok Opname
    Route::get('/stock-opname/detail/{id}/',
    ['as' => 'api.stock-opname.detail', 'uses' => 'Api\ApiStockOpnameController@show']);
    Route::get('/stock-opname/list/',
    ['as' => 'api.stock-opname.list', 'uses' => 'Api\ApiStockOpnameController@index']);
    Route::post('/stock-opname/create/',
    ['as' => 'api.stock-opname.create', 'uses' => 'Api\ApiStockOpnameController@store']);
    Route::get('/stock-opname/{id}/set-status/{status}',
    ['as' => 'api.stock-opname.set_status', 'uses' => 'Api\ApiStockOpnameController@setStatus']);
    Route::delete('/stock-opname/{id}',
    ['as' => 'api.stock-opname.delete', 'uses' => 'Api\ApiStockOpnameController@destroy']);

    // Job Opname
    Route::get('/job-opname/list',
    ['as' => 'api.job-opname.list', 'uses' => 'Api\ApiJobOpnameController@index']);
    Route::get('/job-opname/detail/{id}',
    ['as' => 'api.job-opname.detail', 'uses' => 'Api\ApiJobOpnameController@show']);
    Route::post('/job-opname/create',
    ['as' => 'api.job-opname.create', 'uses' => 'Api\ApiJobOpnameController@store']);
    Route::delete('/job-opname/{id}',
    ['as' => 'api.job-opname.delete', 'uses' => 'Api\ApiJobOpnameController@destroy']);
    Route::get('/job-opname/{id}/set-status/{status}',
    ['as' => 'api.job-opname.set_status', 'uses' => 'Api\ApiJobOpnameController@setStatus']);

    // Invoice
    Route::get('/invoice/list',
    ['as' => 'api.invoice.list', 'uses' => 'Api\ApiInvoiceController@index']);
    Route::post('/invoice/create',
    ['as' => 'api.invoice.create', 'uses' => 'Api\ApiInvoiceController@store']);
    Route::get('/invoice/detail/{id}',
    ['as' => 'api.invoice.detail', 'uses' => 'Api\ApiInvoiceController@show']);
    Route::delete('/invoice/{id}',
    ['as' => 'api.invoice.delete', 'uses' => 'Api\ApiInvoiceController@destroy']);
    Route::get('/invoice/{id}/set-status/{status}',
    ['as' => 'api.invoice.set_status', 'uses' => 'Api\ApiInvoiceController@setStatus']);

    //Invoice Payment Mandor Vendor
    Route::get('/invoice/mandor-vendor/opname/{opname_id}',
    ['as' => 'api.mandor-vendor.opname', 'uses' => 'Api\ApiInvoiceMandorVendorController@getOpname']);
    Route::post('/invoice/mandor-vendor/create',
    ['as' => 'api.mandor-vendor.create', 'uses' => 'Api\ApiInvoiceMandorVendorController@store']);

    // Receipt
    Route::get('/receipt/list',
    ['as' => 'api.receipt.list', 'uses' => 'Api\ApiReceiptController@index']);
    Route::post('/receipt/create',
    ['as' => 'api.receipt.create', 'uses' => 'Api\ApiReceiptController@store']);
    Route::get('/receipt/detail/{id}',
    ['as' => 'api.receipt.detail', 'uses' => 'Api\ApiReceiptController@show']);
    Route::delete('/receipt/{id}',
    ['as' => 'api.receipt.delete', 'uses' => 'Api\ApiReceiptController@destroy']);
    Route::get('/receipt/{id}/set-status/{status}',
    ['as' => 'api.receipt.set_status', 'uses' => 'Api\ApiReceiptController@setStatus']);
    
    //Expense
    Route::get('/expense/list',
    ['as' => 'api.expense.list', 'uses' => 'Api\ApiExpenseController@index']);
    Route::post('/expense/create',
    ['as' => 'api.expense.create', 'uses' => 'Api\ApiExpenseController@store']);
    Route::get('/expense/detail/{id}',
    ['as' => 'api.expense.detail', 'uses' => 'Api\ApiExpenseController@show']);
    Route::delete('/expense/{id}',
    ['as' => 'api.expense.delete', 'uses' => 'Api\ApiExpenseController@destroy']);
    Route::get('/expense/{id}/set-status/{status}',
    ['as' => 'api.expense.set_status', 'uses' => 'Api\ApiExpenseController@setStatus']);

    // Transfer
    Route::get('/transfer/list',
    ['as' => 'api.transfer.list', 'uses' => 'Api\ApiTransferController@index']);
    Route::delete('/transfer/{id}',
    ['as' => 'api.transfer.delete', 'uses' => 'Api\ApiTransferController@destroy']);
    Route::get('/transfer/detail/{id}',
    ['as' => 'api.transfer.detail', 'uses' => 'Api\ApiTransferController@show']);
    Route::post('/transfer/create',
    ['as' => 'api.transfer.create', 'uses' => 'Api\ApiTransferController@store']);
    Route::get('/transfer/{id}/set-status/{status}',
    ['as' => 'api.transfer.set_status', 'uses' => 'Api\ApiTransferController@setStatus']);
    
    // Stock Flow
    Route::get('/stock-flow/list',
    ['as' => 'api.stock-flow.list', 'uses' => 'Api\ApiStockFlowController@index']);
    Route::get('/stock-flow/get-category',
    ['as' => 'api.stock-flow.get-category', 'uses' => 'Api\ApiStockFlowController@getCategory']);

    // Jobs
    Route::get('/jobs/list',
    ['as' => 'api.jobs.list', 'uses' => 'Api\ApiJobsController@index']);
    Route::delete('/jobs/{id}',
    ['as' => 'api.jobs.delete', 'uses' => 'Api\ApiJobsController@destroy']);
    Route::get('/jobs/detail/{id}',
    ['as' => 'api.jobs.detail', 'uses' => 'Api\ApiJobsController@show']);
    Route::post('/jobs/create',
    ['as' => 'api.jobs.create', 'uses' => 'Api\ApiJobsController@store']);

    //Change Status
    Route::post('/project/change-status',
    ['as' => 'api.project.change-status', 'uses' => 'Api\ApiProjectController@changeStatus']);
    //Worker
    Route::post('/project/change-worker',
    ['as' => 'api.project.change-worker', 'uses' => 'Api\ApiProjectController@changeWorker']);
    Route::get('/project/{id}/worker',
    ['as' => 'api.project.worker', 'uses' => 'Api\ApiProjectController@listWorker']);
    //Set HPP 1
    Route::get('/project/{id}/set-hpp-1/{quotation_id}',
    ['as' => 'api.project.set.hpp', 'uses' => 'Api\ApiProjectController@setHpp1']);
    //unset HPP 1
    Route::get('/project/{id}/unset-hpp-1',
    ['as' => 'api.project.unset.hpp', 'uses' => 'Api\ApiProjectController@unsetHpp1']);
    //unset HPP 2
    Route::get('/project/{id}/unset-hpp-2',
    ['as' => 'api.project.unset.priceplan', 'uses' => 'Api\ApiProjectController@unsetHpp2']);
    //Item List Available
    Route::get('/project/{id}/price-plan/item/list/available',
    ['as' => 'api.project.priceplan.item/list.available', 
        'uses' => 'Api\ApiProjectController@itemListAvailable']);

    //User Profile
    Route::get('/profile',
    ['as' => 'api.profile.detail', 'uses' => 'Api\ApiUserController@profile']);

    // Category
    Route::get('/category/list',
    ['as' => 'api.category.list', 'uses' => 'Api\ApiCategoryController@index']);

    // Satuan
    Route::post('/category/create',
    ['as' => 'api.category.create', 'uses' => 'Api\ApiCategoryController@store']);
    Route::delete('/category/{id}',
    ['as' => 'api.category.delete', 'uses' => 'Api\ApiCategoryController@destroy']);

    // Bank
    Route::get('/bank/list',
    ['as' => 'api.bank.list', 'uses' => 'Api\ApiBankController@index']);
    Route::post('/bank/create',
    ['as' => 'api.bank.create', 'uses' => 'Api\ApiBankController@store']);
    Route::delete('/bank/{id}',
    ['as' => 'api.bank.delete', 'uses' => 'Api\ApiBankController@destroy']);

    // Payment
    Route::get('/invoice/payment/list',
    ['as' => 'api.invoice.payment.list', 'uses' => 'Api\ApiInvoicePaymentController@index']);
    Route::post('/invoice/payment/create',
    ['as' => 'api.invoice.payment.create', 'uses' => 'Api\ApiInvoicePaymentController@store']);
    Route::get('/invoice/payment/detail/{id}',
    ['as' => 'api.invoice.payment.detail', 'uses' => 'Api\ApiInvoicePaymentController@show']);
    Route::delete('/invoice/payment/{id}',
    ['as' => 'api.invoice.payment.delete', 'uses' => 'Api\ApiInvoicePaymentController@destroy']);

    // City
    Route::get('/city',
    ['as' => 'api.city', 'uses' => 'Api\ApiCityController@index']);

    //Download Quotation
    Route::get('/quotation/download/{id}',
    ['as' => 'api.quotation.download', 'uses' => 'Api\ApiQuotationController@download']);
    Route::get('/quotation/download/schedule/{id}',
    ['as' => 'api.quotation.schedule', 'uses' => 'Api\ApiQuotationController@downloadSchedule']);
    Route::get('/price-plan/download/{id}',
    ['as' => 'api.price-plan.download', 'uses' => 'Api\ApiPricePlanController@download']);

    //Download SP & PO
    Route::get('/purchase-order/download-sp/{id}',
    ['as' => 'api.purchase-order.download', 'uses' => 'Api\ApiPurchaseOrderController@downloadSp']);
    Route::get('/purchase-order/download-po/{id}',
    ['as' => 'api.purchase-order.download', 'uses' => 'Api\ApiPurchaseOrderController@downloadPo']);
    Route::get('/purchase-order/payment/list',
    ['as' => 'api.purchase-order.payment.list', 'uses' => 'Api\ApiPurchaseOrderPaymentController@index']);
    Route::post('/purchase-order/payment/create',
    ['as' => 'api.purchase-order.payment.create', 'uses' => 'Api\ApiPurchaseOrderPaymentController@store']);
    Route::delete('/purchase-order/payment/{id}',
    ['as' => 'api.purchase-order.payment.delete', 'uses' => 'Api\ApiPurchaseOrderPaymentController@destroy']);
    Route::get('/purchase-order/payment/detail/{id}',
    ['as' => 'api.purchase-order.payment.detail', 'uses' => 'Api\ApiPurchaseOrderPaymentController@show']);

    //Download Receipt
    Route::get('/receipt/download/{id}',
    ['as' => 'api.receipt.download', 'uses' => 'Api\ApiReceiptController@download']);

    //Download Invoice
    Route::get('/invoice/download/{id}',
    ['as' => 'api.invoice.download', 'uses' => 'Api\ApiInvoiceController@download']);

    //Checklist
    Route::post('/project/checklist/create',
    ['as' => 'api.project.checklist.create', 'uses' => 'Api\ApiChecklistController@store']);
    Route::get('/project/{project_id}/checklist/detail',
    ['as' => 'api.project.checklist.detail', 'uses' => 'Api\ApiChecklistController@show']);
    Route::delete('/project/checklist/{id}',
    ['as' => 'api.project.checklist.delete', 'uses' => 'Api\ApiChecklistController@destroy']);

    //Tick
    Route::get('/project/{project_id}/checklist/tick',
    ['as' => 'api.project.checklist.tick', 'uses' => 'Api\ApiChecklistTickController@index']);
    Route::post('/project/{project_id}/checklist/tick/create',
    ['as' => 'api.project.checklist.tick.create', 'uses' => 'Api\ApiChecklistTickController@store']);
    Route::get('/project/{project_id}/checklist/tick/detail/{user_id}',
    ['as' => 'api.project.checklist.tick.detail', 'uses' => 'Api\ApiChecklistTickController@show']);

    //Role
    Route::get('/role/{role_id}/permission',
    ['as' => 'api.role.permission', 'uses' => 'Api\ApiRolePermissionController@getPermission']);
    Route::post('/role/create',
    ['as' => 'api.role.create', 'uses' => 'Api\ApiRolePermissionController@store']);
    Route::get('/role/detail/{id}',
    ['as' => 'api.role.detail', 'uses' => 'Api\ApiRolePermissionController@show']);
    Route::post('/role/set-permission',
    ['as' => 'api.role.set-permission', 'uses' => 'Api\ApiRolePermissionController@setPermission']);

    //Setting
    Route::get('/setting',
    ['as' => 'api.setting', 'uses' => 'Api\ApiSettingController@index']);
    Route::post('/setting',
    ['as' => 'api.setting', 'uses' => 'Api\ApiSettingController@store']);

    // Demo
    Route::post('/demo/upload/',
    ['as' => 'api.demo.upload', 'uses' => 'Api\ApiDemoController@upload']);
    Route::post('/demo/upload2/',
    ['as' => 'api.demo.upload2', 'uses' => 'Api\ApiDemoController@upload2']);
});
