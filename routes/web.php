<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ===== REAL ======
Route::get('/login', 'HomeController@login')->name('login');
Route::post('/login', 'HomeController@postLogin');
Route::get('/quotation/gantt-chart/{id}', 'QuotationController@index');

// Forgot Password
Route::get('/lupa-password', 'HomeController@forgotPassword');
Route::post('/lupa-password', 'HomeController@postForgotPassword');

Route::middleware('auth')->group(function () {
    // Dashboard
    Route::get('/', 'HomeController@dashboard');
    Route::get('/logout', 'HomeController@logout')->name('logout');
});

// Home
Route::get('/theme', 'ThemeController@index');

// Login
Route::get('/theme/login', 'ThemeController@login');
Route::get('/theme/list-penawaran', 'ThemeController@listPenawaran');
Route::get('/theme/detail-penawaran', 'ThemeController@detailPenawaran');
Route::get('/theme/list-hpp1', 'ThemeController@listHPP1');
Route::get('/theme/detail-hpp1', 'ThemeController@detailHPP1');
Route::get('/theme/list-project', 'ThemeController@listProject');
Route::get('/theme/detail-project', 'ThemeController@detailProject');
Route::get('/theme/list-hpp2', 'ThemeController@listHPP2');
Route::get('/theme/detail-hpp2', 'ThemeController@detailHPP2');
Route::get('/theme/list-sp', 'ThemeController@listSp');
Route::get('/theme/detail-sp', 'ThemeController@detailSp');
Route::get('/theme/list-opname-pekerjaan', 'ThemeController@listOpnamePekerjaan');
Route::get('/theme/detail-opname-pekerjaan', 'ThemeController@detailOpnamePekerjaan');
Route::get('/theme/list-customer', 'ThemeController@listCustomer');
Route::get('/theme/detail-customer', 'ThemeController@detailCustomer');
Route::get('/theme/list-kota', 'ThemeController@listKota');
Route::get('/theme/list-provinsi', 'ThemeController@listProvinsi');
Route::get('/theme/detail-pekerjaan', 'ThemeController@detailPekerjaan');
Route::get('/theme/project-checklist', 'ThemeController@projectChecklist');
Route::get('/theme/detail-checklist', 'ThemeController@detailChecklist');
Route::get('/theme/list-admin', 'ThemeController@listAdmin');
Route::get('/theme/detail-admin', 'ThemeController@detailAdmin');
Route::get('/theme/detail-bahan', 'ThemeController@detailBahan');
Route::get('/theme/list-bahan', 'ThemeController@listBahan');
Route::get('/theme/list-po', 'ThemeController@listPurchaseOrder');
Route::get('/theme/detail-po', 'ThemeController@detailPurchaseOrder');
Route::get('/theme/list-lpb', 'ThemeController@listLPB');
Route::get('/theme/detail-lpb', 'ThemeController@detailLPB');
Route::get('/theme/list-supplier', 'ThemeController@listSupplier');
Route::get('/theme/detail-supplier', 'ThemeController@detailSupplier');
Route::get('/theme/list-mandor', 'ThemeController@listMandor');
Route::get('/theme/detail-mandor', 'ThemeController@detailMandor');
Route::get('/theme/list-invoice', 'ThemeController@listInvoice');
Route::get('/theme/detail-invoice', 'ThemeController@detailInvoice');
Route::get('/theme/list-stok-opname', 'ThemeController@listStokOpname');
Route::get('/theme/detail-stok-opname', 'ThemeController@detailStokOpname');
Route::get('/theme/gantt-chart', 'ThemeController@ganttChart');
Route::get('/theme/penawaran-penjadwalan', 'ThemeController@penjadwalan');
// PDF Theme
Route::get('/theme/pdf/layout', 'ThemeController@pdfLayout');
Route::get('/theme/pdf/quotation', 'ThemeController@pdfQuotation');
Route::get('/theme/pdf/penawaran', 'ThemeController@pdfPenawaran');
Route::get('/theme/pdf/invoice', 'ThemeController@pdfInvoice');
Route::get('/theme/pdf/sp', 'ThemeController@pdfSp');
Route::get('/theme/pdf/po', 'ThemeController@pdfPo');
Route::get('/theme/pdf/receipt', 'ThemeController@pdfReceipt');