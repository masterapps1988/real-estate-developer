<?php

use Faker\Generator as Faker;
use App\StokOpname;

$factory->define(App\StokOpnameDetail::class, function (Faker $faker) {
    $stockOpname = StokOpname::select('id')->pluck('id');
    $item = App\Item::select('id')->pluck('id');
    return [
        'stok_opname_id' => $faker->randomElement($stockOpname),
        'item_id' => $faker->randomElement($item),
        'qty_before' => $faker->biasedNumberBetween($min = 10, $max = 20),
        'qty_after' => $faker->biasedNumberBetween($min = 10, $max = 20)
    ];
});
