<?php

use Faker\Generator as Faker;
use App\Project;

$factory->define(App\StokOpname::class, function (Faker $faker) {
    static $counter = 0;
    $projectIdList = Project::select('id')->pluck('id');
    return [
        'project_id' => $faker->randomElement($projectIdList),
        'ref_no' => $counter++,
        'is_stock' => 1,
        'created' => $faker->dateTimeBetween('-1 months', 'now')
    ];
});
