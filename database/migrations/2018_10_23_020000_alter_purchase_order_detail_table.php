<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPurchaseOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_order_detail', function (Blueprint $table) {
            $table->index('item_id');
            $table->foreign('item_id')
            ->references('id')->on('item');

            $table->index('purchase_order_id');
            $table->foreign('purchase_order_id')
            ->references('id')->on('purchase_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order_detail', function (Blueprint $table) {
            $table->dropForeign(['item_id']);
            $table->dropIndex(['item_id']);

            $table->dropForeign(['purchase_order_id']);
            $table->dropIndex(['purchase_order_id']);
        });
    }
}
