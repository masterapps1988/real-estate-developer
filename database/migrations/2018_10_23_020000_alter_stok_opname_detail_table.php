<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStokOpnameDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stok_opname_detail', function (Blueprint $table) {
            $table->index('stok_opname_id');
            $table->foreign('stok_opname_id')
            ->references('id')->on('stok_opname');

            $table->index('quotation_detail_id');
            $table->foreign('quotation_detail_id')
            ->references('id')->on('quotation_detail');

            $table->index('item_id');
            $table->foreign('item_id')
            ->references('id')->on('item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stok_opname_detail', function (Blueprint $table) {
            $table->dropForeign(['stok_opname_id']);
            $table->dropIndex(['stok_opname_id']);

            $table->dropForeign(['quotation_detail_id']);
            $table->dropIndex(['quotation_detail_id']);

            $table->dropForeign(['item_id']);
            $table->dropIndex(['item_id']);
        });
    }
}
