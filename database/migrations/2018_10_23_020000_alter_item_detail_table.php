<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItemDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_detail', function (Blueprint $table) {
            $table->index('item_id');
            $table->foreign('item_id')
            ->references('id')->on('item');

            $table->index('item_id_detail');
            $table->foreign('item_id_detail')
            ->references('id')->on('item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_detail', function (Blueprint $table) {
            $table->dropForeign(['item_id']);
            $table->dropIndex(['item_id']);

            $table->dropForeign(['item_id_detail']);
            $table->dropIndex(['item_id_detail']);
        });
    }
}
