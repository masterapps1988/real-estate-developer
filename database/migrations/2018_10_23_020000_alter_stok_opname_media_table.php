<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStokOpnameMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stok_opname_media', function (Blueprint $table) {
            $table->index('stok_opname_id');
            $table->foreign('stok_opname_id')
            ->references('id')->on('stok_opname');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stok_opname_media', function (Blueprint $table) {
            $table->dropForeign(['stok_opname_id']);
            $table->dropIndex(['stok_opname_id']);
        });
    }
}
