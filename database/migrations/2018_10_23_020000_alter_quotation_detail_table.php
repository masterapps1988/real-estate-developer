<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuotationDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_detail', function (Blueprint $table) {
            $table->index('quotation_id');
            $table->foreign('quotation_id')
            ->references('id')->on('quotation');

            $table->index('quotation_detail_id');
            $table->foreign('quotation_detail_id')
            ->references('id')->on('quotation_detail');

            $table->index('item_id');
            $table->foreign('item_id')
            ->references('id')->on('item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_detail', function (Blueprint $table) {
            $table->dropForeign(['quotation_id']);
            $table->dropIndex(['quotation_id']);

            $table->dropForeign(['quotation_detail_id']);
            $table->dropIndex(['quotation_detail_id']);

            $table->dropForeign(['item_id']);
            $table->dropIndex(['item_id']);
        });
    }
}
