<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStokOpnameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stok_opname', function (Blueprint $table) {
            $table->index('project_id');
            $table->foreign('project_id')
            ->references('id')->on('project');

            $table->index('user_id');
            $table->foreign('user_id')
            ->references('id')->on('user');

            $table->index('status_id');
            $table->foreign('status_id')
            ->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stok_opname', function (Blueprint $table) {
            $table->dropForeign(['project_id']);
            $table->dropIndex(['project_id']);

            $table->dropForeign(['user_id']);
            $table->dropIndex(['user_id']);

            $table->dropForeign(['status_id']);
            $table->dropIndex(['status_id']);
        });
    }
}
