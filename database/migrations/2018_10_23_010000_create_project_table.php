<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('status_id')->unsigned()->nullable();
            // Unique because a quotation cannot be used in more than one project
            $table->integer('quotation_id')->unsigned()->unique()
                    ->nullable()->comment('HPP 1');
            // Similar with quotation
            $table->integer('price_plan_id')->unsigned()->unique()
                    ->nullable()->comment('HPP 2');
            $table->string('ref_no', 45)->unique();
            $table->string('name', 255);
            $table->string('address', 255)->nullable();
            $table->date('start');
            $table->date('finish')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
