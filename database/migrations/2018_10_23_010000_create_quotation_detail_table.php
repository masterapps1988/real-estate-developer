<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotation_id')->unsigned();
            $table->integer('quotation_detail_id')->unsigned()->nullable();
            $table->integer('item_id')->unsigned()->nullable();
            $table->boolean('is_additional')->default(0);
            $table->string('name', 255);
            $table->string('unit', 45)->nullable();
            $table->decimal('qty', 15, 2)->nullable();
            $table->decimal('price', 15, 2);
            $table->string('group_by', 45)->nullable();
            $table->date('schedule_start')->nullable();
            $table->date('schedule_finish')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_detail');
    }
}
