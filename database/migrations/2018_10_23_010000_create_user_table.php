<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('username', 45)->unique()->nullable();
            $table->string('password', 60)->nullable();
            $table->string('name', 45);
            $table->string('address', 255)->nullable();
            $table->string('phone', 45)->nullable();
            $table->string('email', 45)->unique()->nullable();
            $column = $table->string('api_token', 100)->nullable();
            $column->collation = 'utf8_bin'; // Case sensitive column

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
