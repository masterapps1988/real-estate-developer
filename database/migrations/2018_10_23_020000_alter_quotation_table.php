<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuotationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation', function (Blueprint $table) {
            $table->index('quotation_id');
            $table->foreign('quotation_id')
            ->references('id')->on('quotation');

            $table->index('customer_id');
            $table->foreign('customer_id')
            ->references('id')->on('customer');

            $table->index('city_id');
            $table->foreign('city_id')
            ->references('id')->on('city');

            $table->index('status_id');
            $table->foreign('status_id')
            ->references('id')->on('category');

            $table->index('schedule_status_id');
            $table->foreign('schedule_status_id')
            ->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation', function (Blueprint $table) {
            $table->dropForeign(['quotation_id']);
            $table->dropIndex(['quotation_id']);

            $table->dropForeign(['customer_id']);
            $table->dropIndex(['customer_id']);

            $table->dropForeign(['city_id']);
            $table->dropIndex(['city_id']);

            $table->dropForeign(['status_id']);
            $table->dropIndex(['status_id']);

            $table->dropForeign(['schedule_status_id']);
            $table->dropIndex(['schedule_status_id']);
        });
    }
}
