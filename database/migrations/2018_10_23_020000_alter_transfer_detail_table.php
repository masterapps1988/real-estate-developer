<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransferDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_detail', function (Blueprint $table) {
            $table->index('transfer_id');
            $table->foreign('transfer_id')
            ->references('id')->on('transfer');

            $table->index('item_id');
            $table->foreign('item_id')
            ->references('id')->on('item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_detail', function (Blueprint $table) {
            $table->dropForeign(['transfer_id']);
            $table->dropIndex(['transfer_id']);

            $table->dropForeign(['item_id']);
            $table->dropIndex(['item_id']);
        });
    }
}
