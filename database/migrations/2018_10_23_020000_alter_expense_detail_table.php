<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExpenseDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_detail', function (Blueprint $table) {
            $table->index('expense_id');
            $table->foreign('expense_id')
            ->references('id')->on('expense');

            $table->index('item_id');
            $table->foreign('item_id')
            ->references('id')->on('item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_detail', function (Blueprint $table) {
            $table->dropForeign(['expense_id']);
            $table->dropIndex(['expense_id']);

            $table->dropForeign(['item_id']);
            $table->dropIndex(['item_id']);
        });
    }
}
