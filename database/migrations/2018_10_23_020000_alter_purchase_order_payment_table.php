<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPurchaseOrderPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('purchase_order_payment', function (Blueprint $table) {
            $table->index('purchase_order_id');
            $table->foreign('purchase_order_id')
            ->references('id')->on('purchase_order');

            $table->index('bank_category_id');
            $table->foreign('bank_category_id')
            ->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('purchase_order_payment', function (Blueprint $table) {
            $table->dropForeign(['purchase_order_id']);
            $table->dropIndex(['purchase_order_id']);

            $table->dropForeign(['bank_category_id']);
            $table->dropIndex(['bank_category_id']);
        });
    }
}
