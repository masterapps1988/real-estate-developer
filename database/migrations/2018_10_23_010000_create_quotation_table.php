<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quotation_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('status_id')->unsigned();
            $table->integer('schedule_status_id')->unsigned()->nullable();
            $table->string('ref_no',45)->unique()->nullable();
            $table->boolean('is_quotation')->default(1)
                    ->comment('Indicate HPP 1/2. 1 = HPP 1, 0 = HPP 2');

            $table->boolean('is_tax')->default(false);
            $table->string('name',255)->nullable();
            $table->string('address',255)->nullable();
            $table->decimal('profit', 15, 2)->default(0);
            $table->decimal('risk_factor', 15, 2)->default(0);
            $table->tinyInteger('pph')->nullable();
            $table->dateTime('created');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation');
    }
}
