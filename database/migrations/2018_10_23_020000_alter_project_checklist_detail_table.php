<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectChecklistDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_checklist_detail', function (Blueprint $table) {
            $table->index('project_checklist_id');
            $table->foreign('project_checklist_id')
            ->references('id')->on('project_checklist');

            $table->index('user_id');
            $table->foreign('user_id')
            ->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_checklist_detail', function (Blueprint $table) {
            $table->dropForeign(['project_checklist_id']);
            $table->dropIndex(['project_checklist_id']);

            $table->dropForeign(['user_id']);
            $table->dropIndex(['user_id']);
        });
    }
}
