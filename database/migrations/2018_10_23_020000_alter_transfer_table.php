<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer', function (Blueprint $table) {
            $table->index('project_id_from');
            $table->foreign('project_id_from')
            ->references('id')->on('project');

            $table->index('project_id_to');
            $table->foreign('project_id_to')
            ->references('id')->on('project');

            $table->index('status_id');
            $table->foreign('status_id')
            ->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer', function (Blueprint $table) {
            $table->dropForeign(['project_id_from']);
            $table->dropIndex(['project_id_from']);

            $table->dropForeign(['project_id_to']);
            $table->dropIndex(['project_id_to']);

            $table->dropForeign(['status_id']);
            $table->dropIndex(['status_id']);
        });
    }
}
