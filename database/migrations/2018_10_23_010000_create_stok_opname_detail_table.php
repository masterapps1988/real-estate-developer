<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStokOpnameDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stok_opname_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stok_opname_id')->unsigned();
            $table->integer('quotation_detail_id')->unsigned();
            $table->integer('item_id')->unsigned()->nullable();
            $table->decimal('qty_before', 15, 2);
            $table->decimal('qty', 15, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stok_opname_detail');
    }
}
