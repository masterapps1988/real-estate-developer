<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_category_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('ref_no', 45)->unique()->nullable();
            $table->string('name', 255);
            $table->string('unit', 10)->nullable();
            $table->decimal('price', 15, 2)->nullable();
            $table->decimal('qty', 15, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
