<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInvoicePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_payment', function (Blueprint $table) {
            $table->index('invoice_id');
            $table->foreign('invoice_id')
            ->references('id')->on('invoice');

            $table->index('bank_category_id');
            $table->foreign('bank_category_id')
            ->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_payment', function (Blueprint $table) {
            $table->dropForeign(['invoice_id']);
            $table->dropIndex(['invoice_id']);

            $table->dropForeign(['bank_category_id']);
            $table->dropIndex(['bank_category_id']);
        });
    }
}
