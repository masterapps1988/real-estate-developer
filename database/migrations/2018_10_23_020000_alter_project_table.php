<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project', function (Blueprint $table) {
            $table->index('customer_id');
            $table->foreign('customer_id')
            ->references('id')->on('customer');

            $table->index('quotation_id');
            $table->foreign('quotation_id')
            ->references('id')->on('quotation');

            $table->index('price_plan_id');
            $table->foreign('price_plan_id')
            ->references('id')->on('quotation');

            $table->index('city_id');
            $table->foreign('city_id')
            ->references('id')->on('city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project', function (Blueprint $table) {
            $table->dropForeign(['quotation_id']);
            $table->dropIndex(['quotation_id']);

            $table->dropForeign(['price_plan_id']);
            $table->dropIndex(['price_plan_id']);

            $table->dropForeign(['customer_id']);
            $table->dropIndex(['customer_id']);

            $table->dropForeign(['city_id']);
            $table->dropIndex(['city_id']);
        });
    }
}
