<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceiptDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipt_detail', function (Blueprint $table) {
            $table->index('receipt_id');
            $table->foreign('receipt_id')
            ->references('id')->on('receipt');

            $table->index('item_id');
            $table->foreign('item_id')
            ->references('id')->on('item');

            // $table->unique(['receipt_id', 'item_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_detail', function (Blueprint $table) {
            $table->dropForeign(['receipt_id']);
            $table->dropIndex(['receipt_id']);

            $table->dropForeign(['item_id']);
            $table->dropIndex(['item_id']);

            // $table->dropUnique(['receipt_id', 'item_id']);
        });
    }
}
