<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Item;

class SampleItemSeeder extends Seeder
{
    const MAX = 50;

    public function run()
    {
        $this->sampleItem();
        $this->sampleItemMandor();
        $this->sampleItemVendor();
    }

    public function sampleItem()
    {
        $faker = Faker\Factory::create();

        $supplierIdList = User::select('id')
                ->where('role_id', User::ROLE_SUPPLIER)->pluck('id');

        $row = [];
        for($i = 0; $i < self::MAX; $i++){
          $userId = $faker->randomElement($supplierIdList);

          $row[] = [
            'item_category_id' => Item::ITEM,
            'user_id' => $userId,
            'name' => $faker->name,
            'unit' => 'm',
            'price' => $faker->randomNumber(2)
          ];
        }
        DB::table('item')->insert($row);
    }

    public function sampleItemMandor()
    {
        $faker = Faker\Factory::create();

        $mandorIdList = User::select('id')
                ->where('role_id', User::ROLE_MANDOR)->pluck('id');

        $row = [];
        for($i = 0; $i < self::MAX; $i++){
          $userId = $faker->randomElement($mandorIdList);

          $row[] = [
            'item_category_id' => Item::MANDOR,
            'user_id' => $userId,
            'name' => $faker->name,
            'unit' => 'm',
            'price' => $faker->randomNumber(2)
          ];
        }
        DB::table('item')->insert($row);
    }

    public function sampleItemVendor()
    {
        $faker = Faker\Factory::create();

        $vendorIdList = User::select('id')
                ->where('role_id', User::ROLE_VENDOR)->pluck('id');

        $row = [];
        for($i = 0; $i < self::MAX; $i++){
          $userId = $faker->randomElement($vendorIdList);

          $row[] = [
            'item_category_id' => Item::VENDOR,
            'user_id' => $userId,
            'name' => $faker->name,
            'unit' => 'm',
            'price' => $faker->randomNumber(2)
          ];
        }
        DB::table('item')->insert($row);
    }
}
