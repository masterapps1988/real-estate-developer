<?php

use Illuminate\Database\Seeder;
use App\PurchaseOrder;
use App\PurchaseOrderDetail;
use App\Item;
use App\Category as Status;

class SampleReceiptSeeder extends Seeder
{
    const MAX = 20;
    const DETAIL_MAX = 3;
    
    public function run()
    {
        $faker = Faker\Factory::create();

        $poIdList = PurchaseOrder::select('id')->where('status_id', Status::STATUS_APPROVED)->pluck('id');
        
        $row = [];
        $counter = 0;
        for($i = 0; $i < self::MAX; $i++){
            $counter++;
            $poId = $faker->randomElement($poIdList);
            $created = $faker->dateTimeBetween('-1 months', 'now');
            $statusId = $faker->randomElement([Status::STATUS_DRAFT, Status::STATUS_PENDING, Status::STATUS_APPROVED]);

            $list[] = [
                'id' => $counter,
                'purchase_order_id' => $poId,
                'ref_no' => App\TheBadusLibs\WeString::random_str(10),
                'status_id' => $statusId,
                'created' => $created,
                'created_at' => $created,
                'updated_at' => $created
            ];
        }

        $detailList = [];
        foreach($list as $x) {
            $newDetail = $this->generateReceiptDetail($x['id'], $x['purchase_order_id']);
            $collect = collect($detailList);
            $row = $collect->where('receipt_id', $newDetail['receipt_id'])
                    ->where('item_id', $newDetail['item_id'])
                    ->first();

            // Prevent to duplicate item_id in one receipt_id
            if(empty($row)) {
                $detailList = array_merge($detailList, [$newDetail]);
            }
        }

        DB::table('receipt')->insert($list);
        DB::table('receipt_detail')->insert($detailList);
    }

    private function generateReceiptDetail($id, $poId){
        $faker = Faker\Factory::create();

        $items = PurchaseOrderDetail::where('purchase_order_id', $poId)->get();
        
        $item = $faker->unique()->randomElement($items);

        $list = [
            'receipt_id'=> $id,
            'item_id' => $item->item_id,
            'name' => $item->name,
            'qty' => $faker->randomNumber(2),
        ];

        return $list;
    }
}
