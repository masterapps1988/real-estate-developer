<?php

use Illuminate\Database\Seeder;

use App\TheBadusLibs\Csv;
use App\Category;
use App\User;
use App\Item;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Storage::disk('database')->get('csv/job.csv');
        $lines = Csv::toArray($csv);

        $category = Category::getItemTypeIdByName('job');

        foreach ($lines as $x) {
            if (!empty($x)) {
                $rows[] = [
                    'ref_no' => $x[0],
                    'item_category_id' => $category,
                    'user_id' => null,
                    'name' => $x[1],
                    'unit' => $x[2],
                    'price' => $x[3] == '' ? 0 : $x[3]
                ];
            }
        }

        // Insert Job
        DB::table('item')->insert($rows);

        $csvDetail = Storage::disk('database')->get('csv/job-detail.csv');
        $linesDetail = Csv::toArray($csvDetail);

        $item = Item::pluck('id')->toArray();

        foreach ($item as $x) {
            $data = [];
            foreach ($linesDetail as $y) {
                if (!empty($y)) {
                    $detail = array_rand($item, 1);

                    $data[] = [
                        'item_id' => $x,
                        'item_id_detail' => $item[$detail],
                        'qty' => $y[2]
                    ];
                }
            }
            // Insert Detail
            DB::table('item_detail')->insert($data);
        }
    }
}
