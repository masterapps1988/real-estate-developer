<?php

use Illuminate\Database\Seeder;

use App\Category;
use App\Customer;
use App\City;
use App\Item;

class SampleQuotationSeeder extends Seeder
{
    const MAX = 10;
    const REVISION_MAX = 3;
    const DETAIL_MAX = 3;

    public function run()
    {
        $this->generateQuotation();
    }

    private function generateQuotation()
    {
        $faker = Faker\Factory::create();

        $customerIdList = Customer::select('id')->pluck('id');
        $cityIdList = City::select('id')->pluck('id');

        $list = [];
        $counter = 0;
        $tempId = null;
        for($i=0; $i<self::MAX; $i++) {
            $counter++;

            $customerId = $faker->randomElement($customerIdList);
            $cityId = $faker->randomElement($cityIdList);
            $created = $faker->dateTimeBetween('-1 months', 'now');

            $tempCreated = clone $created;
            // Must be newest from all their child.
            $tempCreated->add(new DateInterval('P' . (self::REVISION_MAX+1) . 'D'));
            $list[] = [
                'id' => $counter,
                'quotation_id'=> null,
                'customer_id' => $customerId,
                'city_id' => $cityId,
                'status_id' => Category::STATUS_DRAFT,
                'is_quotation' => 1,
                'ref_no' => $counter,
                'name' => $faker->name,
                'address' => $faker->address,
                'created' => $created,
                'created_at' => $tempCreated,
                'updated_at' => $tempCreated,
            ];

            $tempId = $counter;
            for($j=0; $j<self::REVISION_MAX; $j++) {
                $counter++;
                $created2 = clone $created;
                $customerId = $faker->randomElement($customerIdList);

                $list[] = [
                    'id' => $counter,
                    'quotation_id'=> $tempId,
                    'customer_id' => $customerId,
                    'city_id' => $cityId,
                    'ref_no' => null,
                    'status_id' => Category::STATUS_CLOSED,
                    'is_quotation' => 1,
                    'name' => $faker->name,
                    'address' => $faker->address,
                    'created' => $created2->add(new DateInterval('P' . ($j+1) . 'D')),
                    'created_at' => $created2,
                    'updated_at' => $created2,
                ];
            }
        }

        $tempId = null;
        for($i=0; $i<self::MAX; $i++) {
            $counter++;

            $customerId = $faker->randomElement($customerIdList);
            $cityId = $faker->randomElement($cityIdList);
            $created = $faker->dateTimeBetween('-1 months', 'now');

            $tempCreated = clone $created;
            // Must be newest from all their child.
            $tempCreated->add(new DateInterval('P' . (self::REVISION_MAX+1) . 'D'));
            $list[] = [
                'id' => $counter,
                'quotation_id'=> null,
                'customer_id' => $customerId,
                'city_id' => $cityId,
                'status_id' => Category::STATUS_PENDING,
                'is_quotation' => 1,
                'ref_no' => $counter,
                'name' => $faker->name,
                'address' => $faker->address,
                'created' => $created,
                'created_at' => $tempCreated,
                'updated_at' => $tempCreated,
            ];

            $tempId = $counter;
            for($j=0; $j<self::REVISION_MAX; $j++) {
                $counter++;
                $created2 = clone $created;
                $customerId = $faker->randomElement($customerIdList);

                $list[] = [
                    'id' => $counter,
                    'quotation_id'=> $tempId,
                    'customer_id' => $customerId,
                    'city_id' => $cityId,
                    'ref_no' => null,
                    'status_id' => Category::STATUS_CLOSED,
                    'is_quotation' => 1,
                    'name' => $faker->name,
                    'address' => $faker->address,
                    'created' => $created2->add(new DateInterval('P' . ($j+1) . 'D')),
                    'created_at' => $created2,
                    'updated_at' => $created2,
                ];
            }
        }

        $tempId = null;
        for($i=0; $i<self::MAX; $i++) {
            $counter++;

            $customerId = $faker->randomElement($customerIdList);
            $cityId = $faker->randomElement($cityIdList);
            $created = $faker->dateTimeBetween('-1 months', 'now');

            $tempCreated = clone $created;
            // Must be newest from all their child.
            $tempCreated->add(new DateInterval('P' . (self::REVISION_MAX+1) . 'D'));
            $list[] = [
                'id' => $counter,
                'quotation_id'=> null,
                'customer_id' => $customerId,
                'city_id' => $cityId,
                'status_id' => Category::STATUS_APPROVED,
                'is_quotation' => 1,
                'ref_no' => $counter,
                'name' => $faker->name,
                'address' => $faker->address,
                'created' => $created,
                'created_at' => $tempCreated,
                'updated_at' => $tempCreated,
            ];

            $tempId = $counter;
            for($j=0; $j<self::REVISION_MAX; $j++) {
                $counter++;
                $created2 = clone $created;
                $customerId = $faker->randomElement($customerIdList);
                
                $list[] = [
                    'id' => $counter,
                    'quotation_id'=> $tempId,
                    'customer_id' => $customerId,
                    'city_id' => $cityId,
                    'ref_no' => null,
                    'status_id' => Category::STATUS_CLOSED,
                    'is_quotation' => 1,
                    'name' => $faker->name,
                    'address' => $faker->address,
                    'created' => $created2->add(new DateInterval('P' . ($j+1) . 'D')),
                    'created_at' => $created2,
                    'updated_at' => $created2,
                ];
            }
        }

        // Quotation Detail
        $detailList = [];
        foreach($list as $x) {
            $detailList = array_merge($detailList,
                    $this->generateQuotationDetail($x['id']));
        }

        DB::table('quotation')->insert($list);
        DB::table('quotation_detail')->insert($detailList);
    }

    private function generateQuotationDetail($id)
    {
        $faker = Faker\Factory::create();

        $items = Item::all();
        $cityIdList = City::select('id')->pluck('id');

        $list = [];
        for($i=0; $i<self::DETAIL_MAX; $i++) {
            $item = $faker->randomElement($items);

            $list[] = [
                'quotation_id'=> $id,
                'item_id' => $item->id,
                'name' => $item->name,
                'unit' => $item->unit,
                'qty' => $faker->randomNumber(2),
                'price' => $faker->randomNumber(2),
                'group_by' => 'A'
            ];
        }

        return $list;
    }
}
