<?php

use Illuminate\Database\Seeder;

class SampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Sample data
        $this->call(SampleCustomerSeeder::class);
        // $this->call(SampleItemSeeder::class);
        // $this->call(SampleItemHistorySeeder::class);
        $this->call(SampleQuotationSeeder::class);
        $this->call(SamplePricePlanSeeder::class);
        $this->call(SampleProjectSeeder::class);
        $this->call(SamplePurchaseOrderSeeder::class);
        $this->call(SampleReceiptSeeder::class);
        $this->call(SampleInvoiceSeeder::class);
        $this->call(SamplePurchaseOrderPaymentSeeder::class);
        // $this->call(SampleJobSeeder::class);
        // $this->call(StockOpnameSeeder::class);

        // $this->call(TesProjectSeeder::class);
        // $this->call(StockOpnameSeeder::class);
    }
}
