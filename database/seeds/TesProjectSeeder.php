<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\Quotation;
use App\QuotationDetail;
use App\Category;
use App\Customer;
use App\City;
use App\Item;
use App\PurchaseOrder;
use App\PurchaseOrderDetail;
use App\Receipt;


class TesProjectSeeder extends Seeder
{
    const DETAIL_HPP1 = 30;
    const DETAIL_HPP2 = 20;
    const DETAIL_PO = 15;
    const DETAIL_RECEIPT = 10;

    public function run()
    {
        $idp = Project::max('id');
        $idq = Quotation::max('id');
        $idpo = PurchaseOrder::max('id');
        $idr = Receipt::max('id');

        $this->generateHpp1($idq);
        $this->generateHpp2($idq);
        $this->generateProject($idp, $idq);
        $this->generatePO($idpo, $idp + 1, $idq + 2);
        $this->generateReceipt($idr, $idpo + 1);
    }

    private function generateHpp1($idq){
        $faker = Faker\Factory::create();
        $id = $idq + 1;
        $list = [];
        $created = $faker->dateTimeBetween('-1 months', 'now');

        $list[] = [
            'id' => $id,
            'quotation_id'=> null,
            'customer_id' => 1,
            'city_id' => 1,
            'status_id' => Category::STATUS_APPROVED,
            'is_quotation' => 1,
            'ref_no' => 'Tes Project Hpp 1 - ref',
            'name' => 'Tes Project',
            'address' => $faker->address,
            'created' => $created,
            'created_at' => $created,
            'updated_at' => $created,
        ];

        DB::table('quotation')->insert($list);
        $this->generateHppDetail1($id);
    }

    private function generateHpp2($idq){
        $faker = Faker\Factory::create();
        $id = $idq + 2;
        
        $list = [];
        $created = $faker->dateTimeBetween('-1 months', 'now');

        $list[] = [
            'id' => $id,
            'quotation_id'=> null,
            'customer_id' => 1,
            'city_id' => 1,
            'status_id' => Category::STATUS_APPROVED,
            'is_quotation' => 0,
            'ref_no' => 'Tes Project Hpp 2 - ref',
            'name' => 'Tes Project',
            'address' => $faker->address,
            'created' => $created,
            'created_at' => $created,
            'updated_at' => $created,
        ];

        DB::table('quotation')->insert($list);
        $this->generateHppDetail2($id);
    }

    private function generateHppDetail1($id){
        $faker = Faker\Factory::create();

        $items = Item::all();
        
        $list = [];
        for($i=0; $i<self::DETAIL_HPP1; $i++) {
            $item = $faker->randomElement($items);

            $list[] = [
                'quotation_id'=> $id,
                'item_id' => $item->id,
                'name' => $item->name,
                'unit' => $item->unit,
                'qty' => $faker->randomNumber(2),
                'price' => $faker->randomNumber(2),
                'group_by' => 'A'
            ];
        }

        DB::table('quotation_detail')->insert($list);
    }

    private function generateHppDetail2($id){
        $faker = Faker\Factory::create();

        $items = Item::all();
        
        $list = [];
        for($i=0; $i<self::DETAIL_HPP2; $i++) {
            $item = $faker->randomElement($items);

            $list[] = [
                'quotation_id'=> $id,
                'item_id' => $item->id,
                'name' => $item->name,
                'unit' => $item->unit,
                'qty' => $faker->randomNumber(2),
                'price' => $faker->randomNumber(2),
                'group_by' => 'A'
            ];
        }

        DB::table('quotation_detail')->insert($list);
    }

    private function generateProject($idp, $idq){
        $faker = Faker\Factory::create();

        $id = $idp + 1;
        $idh1 = $idq + 1;
        $idh2 = $idq + 2;
        $created = $faker->dateTimeBetween('-1 months', 'now');
        $finished = $faker->dateTimeBetween('now', '+1 months');

        $list = [];

        $list[] = [
            'id' =>$id,
            'customer_id' => 1,
            'city_id' => 1,
            // 'quotation_id' => $idh1,
            // 'price_plan_id' => $idh2,
            'ref_no' => 'Tes Project - ref',
            'name' => 'Tes Project',
            'address' => $faker->address,
            'start' => $created,
            'finish' => $finished,
            'created_at' => $created,
            'updated_at' => $created
        ];

        DB::table('project')->insert($list);
    }

    private function generatePO($idpo, $idp, $idq){
        $id = $idpo + 1;

        $faker = Faker\Factory::create();

        $list = [];
        $created = $faker->dateTimeBetween('-1 months', 'now');

        $list[] = [
            'id' => $id,
            'project_id' => $idp,
            'status_id' => Category::STATUS_APPROVED,
            'is_open' => 1,
            'ref_no' => 'Tes Project - ref',
            'notes' => $faker->name,
            'created' => $created,
            'created_at' => $created,
            'updated_at' => $created
        ];

        DB::table('purchase_order')->insert($list);
        $this->generateDetailPO($id, $idq);
    }

    private function generateDetailPO($id, $idq){
        $faker = Faker\Factory::create();

        $items = QuotationDetail::where('quotation_id', $idq)->get();

        $list = [];
        for($i=0; $i<self::DETAIL_PO; $i++) {
            $item = $faker->unique()->randomElement($items);

            $list[] = [
                'purchase_order_id'=> $id,
                'item_id' => $item->item_id,
                'name' => $item->name,
                'unit' => $item->unit,
                'qty' => $faker->numberBetween(1, $item->qty),
                'price' => $faker->randomNumber(2),
            ];
        }

        DB::table('purchase_order_detail')->insert($list);
    }

    private function generateReceipt($idr, $idpo){
        $faker = Faker\Factory::create();
        $id = $idr + 1;
        $created = $faker->dateTimeBetween('-1 months', 'now');

        $list[] = [
            'id' => $id,
            'purchase_order_id' => $idpo,
            'ref_no' => 'TPref',
            'status_id' => Category::STATUS_APPROVED,
            'created' => $created,
            'created_at' => $created,
            'updated_at' => $created
        ];

        DB::table('receipt')->insert($list);
        $this->generateDetailReceipt($id, $idpo);
    }

    private function generateDetailReceipt($id, $idpo){
        $faker = Faker\Factory::create();

        $items = PurchaseOrderDetail::where('purchase_order_id', $idpo)->get();

        $list = [];
        for($i=0; $i<self::DETAIL_RECEIPT; $i++) {
            $item = $faker->unique()->randomElement($items);

            $list[] = [
                'receipt_id'=> $id,
                'item_id' => $item->item_id,
                'name' => $item->name,
                'qty' => $faker->numberBetween(1, $item->qty),
            ];
        }

        DB::table('receipt_detail')->insert($list);
    }

}
