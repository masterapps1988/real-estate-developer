<?php

use Illuminate\Database\Seeder;

use App\TheBadusLibs\Csv;
use App\Category;
use App\User;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Storage::disk('database')->get('csv/item.csv');
        $lines = Csv::toArray($csv);

        foreach ($lines as $x) {
            if (!empty($x)) {

                $categoryName = $x[1];

                $user_id = null;

                if ($categoryName == 'bahan') {
                    $categoryName = 'item';
                } else {
                    $user_id = $this->getUser($categoryName);
                }

                $category = Category::getItemTypeIdByName($categoryName);

                $rows[] = [
                    'ref_no' => $x[0],
                    'item_category_id' => $category,
                    'user_id' => $user_id,
                    'name' => $x[2],
                    'unit' => $x[3],
                    'price' => $x[4] == '' ? 0 : $x[4]
                ];
            }
        }

        // Insert brands
        DB::table('item')->insert($rows);
    }

    private function getUser($category)
    {
        $role = Category::getRoleIdByName($category);
        $user = User::where('role_id', $role)->pluck('id')->toArray();

        $index = array_rand($user, 1);

        return $user[$index];
    }
}
