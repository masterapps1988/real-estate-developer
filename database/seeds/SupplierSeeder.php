<?php

use Illuminate\Database\Seeder;

use App\TheBadusLibs\Csv;
use App\Category;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Storage::disk('database')->get('csv/supplier.csv');
        $lines = Csv::toArray($csv);
        $role = Category::select('id')->where('name', 'supplier')->value('id');

        foreach ($lines as $x) {
            if (!empty($x)) {
                $rows[] = [
                    'role_id' => $role,
                    'name' => $x[0]
                ];
            }
        }

        $rows[] = [
            'role_id' => $role,
            'name' => 'Lain Lain'
        ];

        // Insert brands
        DB::table('user')->insert($rows);
    }
}
