<?php

use Illuminate\Database\Seeder;

use App\City;

class CustomerSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $cityId = 84; // Surabaya

        $row = [
            [
                'id' => 1,
                'name' => 'CV. Bangung Sarana Indah',
                'address' => 'Jl. Nginden Intan Timur V Kav. 26 No. 1A, Surabaya, Jawa Timur, 60118',
                'city_id' => $cityId,
                'phone' => $faker->tollFreePhoneNumber,
                'npwp' => $faker->creditCardNumber,
                'notes' => null
            ],
            [
                'id' => 2,
                'name' => 'Lain Lain',
                'address' => 'Lain Lain',
                'city_id' => $cityId,
                'phone' => null,
                'npwp' => null,
                'notes' => 'Lain Lain'
            ]
        ];

        DB::table('customer')->insert($row);
    }
}
