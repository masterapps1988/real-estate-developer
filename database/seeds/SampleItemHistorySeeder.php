<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Item;

class SampleItemHistorySeeder extends Seeder
{
    const MAX = 20;

    public function run()
    {
        // Get all item id
        $itemIdList = Item::select('id')->get()->pluck('id');

        // Create 10 history random number
        $faker = Faker\Factory::create();

        $supplierIdList = User::select('id')
                ->where('role_id', User::ROLE_SUPPLIER)->pluck('id');

        $row = [];
        foreach($itemIdList as $x) {
            for($i = 0; $i < self::MAX; $i++){
                $itemId = $x;
                $created = $faker->dateTimeBetween('-1 months', 'now');

                $row[] = [
                    'item_id' => $itemId,
                    'price' => $faker->randomNumber(2),
                    'created' => $created,
                ];
            }
        }
        DB::table('item_history')->insert($row);
    }
}
