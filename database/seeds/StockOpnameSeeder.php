<?php

use Illuminate\Database\Seeder;

use App\Category as Status;
use App\Project;
use App\User;

class StockOpnameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    const MAX = 20;

    public function run()
    {
        $faker = Faker\Factory::create();
        
        $projectIdList = Project::select('id')->pluck('id');
        $mandorIdList = User::select('id')
                ->where('role_id', User::ROLE_MANDOR)->pluck('id');

        $row = [];
        $counter = 0;
        for($i = 0; $i < self::MAX; $i++){
            $counter++;
            $projectId = $faker->randomElement($projectIdList);
            $userId = $faker->randomElement($mandorIdList);
            $statusId = $faker->randomElement([Status::STATUS_DRAFT, Status::STATUS_PENDING, Status::STATUS_APPROVED]);
            $created = $faker->dateTimeBetween('-1 months', 'now');

            $row[] = [
                'id' => $counter,
                'project_id' => $projectId,
                'user_id' => $userId,
                'status_id' => $statusId,
                'ref_no' => App\TheBadusLibs\WeString::random_str(10),
                'is_stock' => 0,
                'created' => $created,
                'created_at' => $created,
                'updated_at' => $created
            ];
        }
        DB::table('stok_opname')->insert($row);
        // factory(App\StokOpname::class, 20)->create();
        // factory(App\StokOpnameDetail::class, 10)->create();
    }
}
