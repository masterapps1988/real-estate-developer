<?php

use Illuminate\Database\Seeder;

use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        $list = [
            // Direktur/super admin
            [
                'role_id' => User::ROLE_DIRECTOR,
                'username' => 'developer',
                'name' => 'developer',
                'email' => 'developer@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Direktur/super admin
            [
                'role_id' => User::ROLE_DIRECTOR,
                'username' => 'admin',
                'name' => 'admin',
                'email' => 'admin@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Marketing
            [
                'role_id' => User::ROLE_MARKETING,
                'username' => 'marketing',
                'name' => 'marketing',
                'email' => 'marketing@bsi.co.id',
                'password' => Hash::make('admin')
            ],
            
            // Purchasing(item)
            [
                'role_id' => User::ROLE_PURCHASING_ITEM,
                'username' => 'purchasing-item',
                'name' => 'purchasing-item',
                'email' => 'purchasing-item@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Purchasing(Mandor-Vendor)
            [
                'role_id' => User::ROLE_PURCHASING_MANDOR_VENDOR,
                'username' => 'purchasing-mandor-vendor',
                'name' => 'purchasing-mandor-vendor',
                'email' => 'purchasing-mandor-vendor@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Manager Operasional
            [
                'role_id' => User::ROLE_MANAGER_OPERASIONAL,
                'username' => 'manager-operasional',
                'name' => 'manager-operasional',
                'email' => 'manager-operasional@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Operasional
            [
                'role_id' => User::ROLE_OPERASIONAL,
                'username' => 'operasional',
                'name' => 'operasional',
                'email' => 'operasional@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Manager Logistic
            [
                'role_id' => User::ROLE_MANAGER_LOGISTIC,
                'username' => 'manager-logistic',
                'name' => 'manager-logistic',
                'email' => 'manager-logistic@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Logistic
            [
                'role_id' => User::ROLE_LOGISTIC,
                'username' => 'logistic',
                'name' => 'logistic',
                'email' => 'logistic@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Quality Control
            [
                'role_id' => User::ROLE_QUALITY_CONTROL,
                'username' => 'quality-control',
                'name' => 'quality-control',
                'email' => 'quality-control@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Finance
            [
                'role_id' => User::ROLE_FINANCE,
                'username' => 'finance',
                'name' => 'finance',
                'email' => 'finance@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Supplier
            [
                'role_id' => User::ROLE_SUPPLIER,
                'username' => 'supplier',
                'name' => 'supplier',
                'email' => 'supplier@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Mandor
            [
                'role_id' => User::ROLE_MANDOR,
                'username' => 'mandor',
                'name' => 'mandor',
                'email' => 'mandor@bsi.co.id',
                'password' => Hash::make('admin')
            ],

            // Vendor
            [
                'role_id' => User::ROLE_VENDOR,
                'username' => 'vendor',
                'name' => 'vendor',
                'email' => 'vendor@bsi.co.id',
                'password' => Hash::make('admin')
            ]
        ];

        // Batch Insert
        DB::table('user')->insert($list);
    }
}
