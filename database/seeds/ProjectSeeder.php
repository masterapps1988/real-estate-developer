<?php

use Illuminate\Database\Seeder;

use App\Customer;
use App\City;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $customerIdList = Customer::select('id')->pluck('id');
        $cityIdList = City::select('id')->pluck('id');
        $customerId = $faker->randomElement($customerIdList);
        $cityId = $faker->randomElement($cityIdList);
        $created = $faker->dateTimeBetween('-1 months', 'now');
        $finished = $faker->dateTimeBetween('now', '+1 months');
        
        $list = [];

        $list[] = [
            'id' => 1,
            'customer_id' => 1,
            'city_id' => $cityId,
            'quotation_id' => NULL,
            'price_plan_id' => NULL,
            'ref_no' => App\TheBadusLibs\WeString::random_str(10),
            'name' => 'CV. AIS',
            'address' => $faker->address,
            'start' => $created,
            'finish' => $finished,
            'created_at' => $created,
            'updated_at' => $created
        ];

        DB::table('project')->insert($list);
    }
}
