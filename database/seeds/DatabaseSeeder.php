<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(CityProvinceSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SupplierSeeder::class);
        $this->call(ItemSeeder::class);
        $this->call(JobSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(RolePermissionSeeder::class);
    }
}
