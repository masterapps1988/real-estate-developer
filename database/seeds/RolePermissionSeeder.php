<?php

use Illuminate\Database\Seeder;
use App\TheBadusLibs\Csv;

use App\Category;

class RolePermissionSeeder extends Seeder
{
    const MAX = 64;
    const DETAIL_MAX = 3;
    
    public function run()
    {
        try {
            $csv = Storage::disk('database')->get('csv/role-permission.csv');
            $lines = Csv::toArray($csv);
            $rows = $this->generateList($lines);

            DB::table('role_permission')->insert($rows);
        } catch (\Exception $e) {
            var_dump($e->getTraceAsString());
        }
    }

    public function generateList($lines)
    {
        $created = new \DateTime;
        $roles = Category::where('group_by', 'role')
                ->get();
        $permissions = Category::where('group_by', 'permission')
                ->get();

        // Get header
        $header = $lines[0];
        foreach($header as $key => $x) {
            for($i=1; $i<count($lines); $i++) {
                $y = strtolower($lines[$i][$key]);

                // Not all each roles/column has value
                if(!empty($y)) {
                    $rows[] = [
                        'role_id' => $roles->where('name', $x)->first()->id,
                        'permission_id' => $permissions->where('name', $y)->first()->id,
                        'created_at' => $created,
                        'updated_at' => $created
                    ];                    
                }
            }
        }

        return $rows;
    }
}
