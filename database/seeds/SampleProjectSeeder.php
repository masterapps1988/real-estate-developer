<?php

use Illuminate\Database\Seeder;

use App\Customer;
use App\City;
use App\Quotation;

class SampleProjectSeeder extends Seeder
{
    const MAX = 20;

    public function run()
    {
        $faker = Faker\Factory::create();

        $customerIdList = Customer::select('id')->pluck('id');
        $cityIdList = City::select('id')->pluck('id');
        $quotationIdList = Quotation::select('id')->where('is_quotation', 1)->whereNull('quotation_id')->pluck('id');
        $pricePlanIdList = Quotation::select('id')->where('is_quotation', 0)->whereNull('quotation_id')->pluck('id');

        $list = [];
        for($i=0; $i<self::MAX; $i++) {
            $customerId = $faker->randomElement($customerIdList);
            $cityId = $faker->randomElement($cityIdList);
            $quotationId = $quotationIdList->pop();
            $pricePlanId = $faker->unique()->randomElement($pricePlanIdList);
            $created = $faker->dateTimeBetween('-1 months', 'now');
            $finished = $faker->dateTimeBetween('now', '+1 months');

            $quotation = Quotation::find($quotationId);
            $quotation->customer_id = $customerId;
            $quotation->save();

            $list[] = [
                'customer_id' => $customerId,
                'city_id' => $cityId,
                'quotation_id' => $quotationId,
                // 'price_plan_id' => $pricePlanId,
                'ref_no' => App\TheBadusLibs\WeString::random_str(10),
                'name' => $faker->name,
                'address' => $faker->address,
                'start' => $created,
                'finish' => $finished,
                'created_at' => $created,
                'updated_at' => $created
            ];
        }

        DB::table('project')->insert($list);
    }
}
