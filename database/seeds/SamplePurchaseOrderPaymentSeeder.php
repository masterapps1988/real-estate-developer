<?php

use Illuminate\Database\Seeder;

use App\Category;
use App\PurchaseOrder;



class SamplePurchaseOrderPaymentSeeder extends Seeder
{
    const MAX = 3;

    public function run()
    {
        $this->generatePurchaseOrderPayment();
    }

    private function generatePurchaseOrderPayment(){
        $faker = Faker\Factory::create();

        $purchaseIdList = PurchaseOrder::select('id')->pluck('id');
        $bankCategoryIdList = Category::select('id')->where('group_by','=','bank')->pluck('id');
        $row = [];
        $counter = 0;
        foreach($purchaseIdList as $purchaseId) {
            for($i = 0; $i < self::MAX; $i++){
                $counter++;
    
                // $purchaseId = $purchaseIdList;
                $bankCategoryId = $faker->randomElement($bankCategoryIdList);
                $created = $faker->dateTimeBetween('-1 months', 'now');
    
                $list[] = [
                    'id' => $counter,
                    'purchase_order_id' => $purchaseId,
                    'bank_category_id' => $bankCategoryId,
                    'amount' => $faker->randomNumber(2),
                    'account_number' => $faker->randomNumber(2),
                    'notes' => $faker->name,
                    'created' => $created,
                    'created_at' => $created,
                    'updated_at' => $created
                ];
            }
        }
        


        DB::table('purchase_order_payment')->insert($list);
    }

    
}
