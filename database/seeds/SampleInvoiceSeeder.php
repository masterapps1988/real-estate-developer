<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\City;
use App\Category as Status;

class SampleInvoiceSeeder extends Seeder
{
    const MAX = 20;
    const DETAIL_MAX = 3;

    public function run()
    {
        $faker = Faker\Factory::create();

        $projectIdList = Project::select('id')->pluck('id');
        $cityIdList = City::all()->pluck('id');
        
        $row = [];
        $counter = 0;
        for($i = 0; $i < self::MAX; $i++){
            $counter++;

            $projectId = $faker->randomElement($projectIdList);
            $cityId = $faker->randomElement($cityIdList);
            $created = $faker->dateTimeBetween('-1 months', 'now');
            $statusId = $faker->randomElement([Status::STATUS_DRAFT, Status::STATUS_PENDING, Status::STATUS_APPROVED]);

            $list[] = [
                'id' => $counter,
                'project_id' => $projectId,
                'ref_no' => App\TheBadusLibs\WeString::random_str(10),
                'name' => $faker->name,
                'address' => $faker->address,
                'hp' => $faker->tollFreePhoneNumber,
                'city' => $cityId,
                'status_id' => $statusId,
                'created' => $created,
                'created_at' => $created,
                'updated_at' => $created
            ];
        }

        $detailList = [];
        foreach($list as $x) {
            $detailList = array_merge($detailList,
                    $this->generateInvoiceDetail($x['id']));
        }

        DB::table('invoice')->insert($list);
        DB::table('invoice_detail')->insert($detailList);
    }

    private function generateInvoiceDetail($id){
        $faker = Faker\Factory::create();
        
        $list = [];
        for($i=0; $i<self::DETAIL_MAX; $i++) {
            $list[] = [
                'invoice_id'=> $id,
                'notes' => $faker->name,
                'amount' => $faker->randomNumber(2) * 1000,
            ];
        }

        return $list;
    }
}
