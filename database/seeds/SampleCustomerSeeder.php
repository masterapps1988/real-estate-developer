<?php

use Illuminate\Database\Seeder;

use App\User;
use App\City;

class SampleCustomerSeeder extends Seeder
{
    const MAX = 100;

    public function run()
    {
        $faker = Faker\Factory::create();

        $cityIdList = City::all()->pluck('id');

        $row = [];
        for($i = 0; $i < self::MAX; $i++){
        $cityId = $faker->randomElement($cityIdList);

          $row[] = [
            'name' => $faker->name,
            'address' => $faker->address,
            'city_id' => $cityId,
            'phone' => $faker->tollFreePhoneNumber,
            'npwp' => $faker->creditCardNumber
          ];
        }
        DB::table('customer')->insert($row);
    }
}
