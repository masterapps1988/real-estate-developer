<?php

use Illuminate\Database\Seeder;

use App\TheBadusLibs\Csv;

class CategorySeeder extends Seeder
{
    public function run()
    {
        $csv = Storage::disk('database')->get('csv/category.csv');
        $lines = Csv::toArray($csv);
        foreach ($lines as $x) {
            if (!empty($x)) {
                $rows[] = [
                    'id' => $x[0],
                    'name' => strtolower($x[1]),
                    'label' => $x[2],
                    'notes' => $x[3],
                    'group_by' => $x[4]
                ];
            }
        }

        // Insert brands
        DB::table('category')->insert($rows);
    }
}
