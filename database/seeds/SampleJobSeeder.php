<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Item;
use App\Category;

class SampleJobSeeder extends Seeder
{
    const MAX = 20;

    public function run()
    {
        $this->sampleJobs();
    }

    public function sampleJobs()
    {
        $id = Item::max('id') + 1;
        $faker = Faker\Factory::create();

        $row = [];
        $detailList = [];
        for($i = 0; $i < self::MAX; $i++){
            // $userId = $faker->randomElement($supplierIdList);
            $row[] = [
                'id' => $id,
                'ref_no' => App\TheBadusLibs\WeString::random_str(10),
                'item_category_id' => Category::ITEM_JOB,
                'name' => $faker->name,
                'unit' => null,
                'price' => null
            ];

            $detailList = array_merge($detailList,
                    $this->generateDetail($id));

            $id++;
        }
        DB::table('item')->insert($row);
        DB::table('item_detail')->insert($detailList);
    }

    public function generateDetail($itemId)
    {
        $faker = Faker\Factory::create();

        $list = [];

        $itemList = Item::where('item_category_id', '!=', Category::ITEM_JOB)->get()->pluck('id');

        for($i=0; $i<4; $i++) {
            $itemIdRandom = $faker->randomElement($itemList);
            $qty = $faker->randomNumber(2);

            $list[] = [
                'item_id' => $itemId,
                'item_id_detail' => $itemIdRandom,
                'qty' => $qty
            ];
        }

        return $list;
    }
}
