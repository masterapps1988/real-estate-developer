<?php

use Illuminate\Database\Seeder;
use App\Category as Status;
use App\Project;
use App\Item;

class PurchaseOrderSeeder extends Seeder
{
    const MAX = 50;
    const DETAIL_MAX = 3;

    public function run()
    {
        $this->generatePurchaseOrder();
    }

    private function generatePurchaseOrder(){
        $faker = Faker\Factory::create();

        $projectIdList = Project::select('id')->pluck('id');
        
        $row = [];
        $counter = 0;
        for($i = 0; $i < self::MAX; $i++){
            $counter++;

            $projectId = $faker->randomElement($projectIdList);
            $statusId = $faker->randomElement([Status::STATUS_DRAFT, Status::STATUS_PENDING, Status::STATUS_APPROVED]);
            $created = $faker->dateTimeBetween('-1 months', 'now');

            $list[] = [
                'id' => $counter,
                'project_id' => $projectId,
                'status_id' => $statusId,
                'is_open' => $statusId == Status::STATUS_DRAFT ? 1 : 0,
                'ref_no' => $counter,
                'notes' => $faker->name,
                'created' => $created,
                'created_at' => $created,
                'updated_at' => $created
            ];
        }

        $detailList = [];
        foreach($list as $x) {
            $detailList = array_merge($detailList,
                    $this->generatePurchaseDetail($x['id']));
        }

        DB::table('purchase_order')->insert($list);
        DB::table('purchase_order_detail')->insert($detailList);
    }

    private function generatePurchaseDetail($id){
        $faker = Faker\Factory::create();

        $items = Item::all();
        
        $list = [];
        for($i=0; $i<self::DETAIL_MAX; $i++) {
            $item = $faker->randomElement($items);

            $list[] = [
                'purchase_order_id'=> $id,
                'item_id' => $item->id,
                'name' => $item->name,
                'unit' => $item->unit,
                'qty' => $faker->randomNumber(2),
                'price' => $faker->randomNumber(2),
            ];
        }

        return $list;
    }
}
