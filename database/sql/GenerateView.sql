-- Create stock flow view
drop view if exists view_stock_flow;
create view view_stock_flow AS
    -- LPB(IN)
    select 'receipt' as 'category', p.id project_id, r.id, rd.item_id, rd.qty, r.created
    from receipt_detail rd
        join receipt r on r.id = rd.receipt_id
        join purchase_order po on po.id = r.purchase_order_id
        join project p on p.id = po.project_id
    where po.status_id = 52 AND r.status_id = 52
    union all
    -- LPB(OUT)
    select 'expense', e.project_id, e.id, ed.item_id, -qty, e.created
    from expense e
        join expense_detail ed on e.id = ed.expense_id
    where e.status_id = 52
    union all
    -- Transfer(OUT)
    select 'transfer', t.project_id_from project_id, t.id, item_id, -qty, t.created
    from transfer t
        join transfer_detail td on t.id = td.transfer_id
    where t.status_id = 52
    union all
    -- Transfer(IN)
    select 'transfer', t.project_id_to project_id, t.id, item_id, qty, t.created
    from transfer t
        join transfer_detail td on t.id = td.transfer_id
    where t.status_id = 52;

-- Create stock view
-- Stock = LPB(IN) - LPB(OUT) - Transfer(OUT) + Transfer(IN)
-- Input: project_id
-- Output: project_id, item_id, qty
drop view if exists view_stock;
create view view_stock as
-- SUM
select project_id, item_id, SUM(qty) qty
from (
    -- LPB(IN)
    select po.project_id project_id, rd.item_id, SUM(rd.qty) qty
    from receipt_detail rd
        join receipt r on r.id = rd.receipt_id
        join purchase_order po on po.id = r.purchase_order_id
    where po.status_id = 52 AND r.status_id = 52 AND po.project_id = 2
    group by po.project_id, rd.item_id
    union all
    -- LPB(OUT)
    select e.project_id, ed.item_id, -SUM(qty) qty
    from expense e
        join expense_detail ed on e.id = ed.expense_id
    where e.status_id = 52
    group by e.project_id, ed.item_id
    union all
    -- Transfer(OUT)
    select t.project_id_from project_id, item_id, -SUM(qty) qty
    from transfer t
        join transfer_detail td on t.id = td.transfer_id
    where t.status_id = 52
    group by t.project_id_from, td.item_id
    union all
    -- Transfer(IN)
    select t.project_id_to project_id, item_id, SUM(qty) qty
    from transfer t
        join transfer_detail td on t.id = td.transfer_id
    where t.status_id = 52
    group by t.project_id_to, td.item_id
    union all
    -- Opname
    select so.project_id project_id, item_id, SUM(qty) qty
    from stok_opname so
        join stok_opname_detail sod on so.id = sod.stok_opname_id
    where so.status_id = 52
    group by so.project_id, item_id
) stock
group by project_id, item_id