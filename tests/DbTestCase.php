<?php

namespace Tests;

use DB;

// Would automatically rollback when test is done
abstract class DbTestCase extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        DB::beginTransaction();
    }

    public function tearDown()
    {
        DB::rollback();
    }

}
