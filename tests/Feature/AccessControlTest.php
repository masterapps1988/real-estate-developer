<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\UnauthorizedException;

use App\Ais\Repository\AccessControl;

use App\User;
use App\Category;

class AccessControlTest extends DbTestCase
{
    public function testBasic()
    {
        // Test admin and staff
        // Test admin
        $user = $this->getAdmin();
        $this->assertNotEmpty($user);

        $accessControl = new AccessControl($user);
        $this->assertTrue($accessControl->hasAccess('admin_create'));
        $this->assertTrue($accessControl->hasAccesses(['admin_create']));
    }

    public function getAdmin()
    {
        return User::where('role_id', Category::getRoleIdByName('direktur'))
                ->first();
    }
}
