<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use Hash;
use Illuminate\Http\File;

use App\Ais\Helpers\FullPath;
use App\Ais\Repository\StokOpnameMedia;

use App\StokOpnameMedia as Model;

class StockOpnameImageTest extends DbTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->initialTest();
    }

    public function testCreate()
    {
        try {
            $id = 1;
            $filename = FullPath::testAssets('sample.png');
            $this->assertTrue(file_exists($filename));
            $model = $this->getModel(new Model);

            $image = new StokOpnameMedia($model);
            $image->setImage(new File($filename));
            $image->save();

            $highResImage = FullPath::opname($model->name);
            $thumbImage = FullPath::opname($image->getThumbImageName());

            var_dump($highResImage, $thumbImage);
            $this->assertTrue(file_exists($highResImage));
            $this->assertTrue(file_exists($thumbImage));
        } catch(ValidationException $e) {
            var_dump($e->getMessage());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    private function initialTest()
    {
        try {
            // Get test asset
            $filename = 'sample.png';
            $source = FullPath::testAssets($filename);
            $dest = FullPath::opname($filename);

            // Test if sample exists in test folder
            $this->assertTrue(file_exists($source));

            // Copy to public/assets/opname
            copy($source, $dest);

            // Test if image correctly copied to dest folder
            $this->assertTrue(file_exists($dest));

            // Delete after success
            unlink($dest);
            $this->assertFalse(file_exists($dest));
        } catch(Exception $e) {
            var_dump($e->getMessage());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    private function getModel(Model $model)
    {
        $stockOpname = \App\StokOpname::first();
        $this->assertNotEmpty($stockOpname);

        $stockOpnameId = $stockOpname->id;
        $model->stok_opname_id = $stockOpnameId;

        return $model;
    }
}
