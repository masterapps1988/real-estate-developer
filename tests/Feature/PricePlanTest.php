<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use App\Ais\Repository\PricePlan;

use App\Quotation as QuotationModel;
use App\Project;
use App\City;
use App\Customer;
use App\Category;
use App\Item;

class PricePlanTest extends DbTestCase
{
    // Test create or update
    public function testCreate()
    {
        try {
            $faker = \Faker\Factory::create();

            $items = Item::all()->pluck('id');

            $model = $this->getModel(new QuotationModel);
            $model->status_id = Category::STATUS_DRAFT;
            $model->is_quotation = 0;

            $quotation = QuotationModel::first();

            $project = Project::first();
            $this->assertNotEmpty($project);

            $project->quotation_id = $quotation->id;
            $project->price_plan_id = null;
            $project->save();
            // var_dump($quotation->id, $project->toArray());

            // Must have quotation_id
            $this->assertNotEmpty($project->quotation_id);

            // Prepare quotation detail to link with PricePlan
            $quotationDetailIdList = $quotation->detail->pluck('id');
            // var_dump($quotationDetailId, $items); die;
            // var_dump('test', $project->id);
            // var_dump('test', $project->quotation->id, $quotation->id, $quotationDetail);

            $row = new PricePlan($model);
            // Add price plan to a project
            $row->addProject($project);

            // Create job
            for($i=0; $i<3; $i++) {
                $detailId = $faker->randomElement($quotationDetailIdList);
                $itemId = $faker->randomElement($items);
                // var_dump($detailId, $quotationDetailId); die;

                $this->assertNotEmpty($detailId);
                $this->assertNotEmpty($itemId);

                $id = null;
                $quotationDetailId = $detailId;
                $name = 'Job ' . $i;
                $unit = 'm2';
                $qty = 10 * $i;
                $price = 5000 * $i;
                $row->addItem($id, $quotationDetailId, $itemId, $name,
                        $unit, $qty, $price);
            }

            $row->save();

            $this->assertNotEmpty($model->id);
            $this->assertNotEmpty($project->price_plan_id);
        } catch (ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Create price plan is invalid');
        }
    }

    // Test failed create when price_plan_id get quotation_detail_id
    // that not in the project
    public function testFailedCreate()
    {
        try {
            $faker = \Faker\Factory::create();

            $items = Item::all()->pluck('id');

            $model = $this->getModel(new QuotationModel);
            $model->status_id = Category::STATUS_DRAFT;
            $model->is_quotation = 0;

            $quotation = QuotationModel::first();

            $project = Project::first();
            $this->assertNotEmpty($project);

            $project->quotation_id = $quotation->id;
            $project->price_plan_id = null;
            $project->save();

            // Must have quotation_id
            $this->assertNotEmpty($project->quotation_id);

            // Generate quotation detail that not belongs to project
            $tempQuotation = QuotationModel::where('id', '!=', $project->quotation_id)
                    ->first();
            $quotationDetail = $tempQuotation->detail->toArray();

            $row = new PricePlan($model);
            // Add price plan to a project
            $row->addProject($project);

            // Create job
            for($i=0; $i<3; $i++) {
                $detailIndex = array_rand($quotationDetail);
                $itemId = $faker->randomElement($items);

                $this->assertNotEmpty($quotationDetail[$detailIndex]['id']);
                $this->assertNotEmpty($itemId);

                $id = null;
                $quotationDetailId = $quotationDetail[$detailIndex]['id'];
                $name = 'Job ' . $i;
                $unit = 'm2';
                $qty = 10 * $i;
                $price = 5000 * $i;
                $row->addItem($id, $quotationDetailId, $itemId, $name,
                        $unit, $qty, $price);
            }

            $row->save();

            $this->assertNotEmpty(null, 'FAILED: Create price plan should not success');
        } catch (ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(1, 'SUCCESS: Create price plan is invalid');
        }
    }

    private function getModel(QuotationModel $model)
    {
        $model->quotation_id = null;
        $model->customer_id = Customer::first()->id;
        $model->city_id = City::first()->id;
        $model->ref_no = \App\TheBadusLibs\WeString::random_str(8);
        $model->name = 'Quotation title';
        $model->address = 'Jl. jalan';
        $model->created = new \DateTime();

        return $model;
    }
    
}
