<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use Hash;

use App\User as UserModel;
use App\Category;

use App\Ais\Repository\User;

class UserTest extends DbTestCase
{
    public function testCreate()
    {
        try {
            $user = $this->getModel(new UserModel);
            $user->email = 'randomize123@bsi.co.id';

            $repo = new User($user);
            $repo->setPassword('admin', 'admin');
            $repo->save();

            // Should go here
            $this->assertNotEmpty($user->id);
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    private function getModel(UserModel $user)
    {
        $user = new UserModel;
        $user->role_id = UserModel::ROLE_DIRECTOR;
        $user->username = 'admin123';
        $user->password = Hash::make('admin');
        $user->name = 'admin';
        $user->email = 'admin@bsi.co.id';

        return $user;
    }
}
