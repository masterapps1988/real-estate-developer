<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use Hash;
use DB;

use App\Ais\Repository\Invoice;
use App\Ais\Repository\Project;

use App\Project as ProjectModel;
use App\Invoice as InvoiceModel;
use App\QuotationDetail;
use App\Item;
use App\City;

class InvoiceTest extends DbTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        try {
            $project = ProjectModel::first();
            // Make sure this project doesn't have any invoice. So we could freely
            // add whatever amount we want (<= max)
            $this->deleteAllInvoice($project->id);
            $this->assertEmpty($project->invoices);

            $projectRepo = new Project($project);

            // Generate new invoice
            $row = $this->getModel(new InvoiceModel);
            $row->project_id = $project->id; // Associate with project id

            // Create invoice repo
            $repo = new Invoice($row);
            // Add new detail
            $repo->addDetail(null, 'Test buying', $projectRepo->getTotal());
            // Save invoice
            $repo->save();

            // Should reach here
            $this->assertNotEmpty(1, 'SUCCESS: Invoice inserted');
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'FAILED: Invoice cannot be inserted.');
        }
    }

    public function testFailedCreateInvoice()
    {
        try {
            $project = ProjectModel::first();
            // Make sure this project doesn't have any invoice. So we could freely
            // add whatever amount we want (<= max)
            $this->deleteAllInvoice($project);

            $projectRepo = new Project($project);

            // Generate new invoice
            $row = $this->getModel(new InvoiceModel);
            $row->project_id = $project->id; // Associate with project id

            // Create invoice repo
            $repo = new Invoice($row);
            // Add new detail
            $repo->addDetail(null, 'Test buying', $projectRepo->getTotal() * 2);
            // Save invoice
            $repo->save();

            // Should NOT reach here
            $this->assertNotEmpty(null, 'FAILED: Invoice inserted');
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(1, 'SUCCESS: Invoice cannot be inserted.');
        }
    }

    public function testFailedInsertManyInvoiceTotalMoreThanProjectTotal()
    {
        try {
            $project = ProjectModel::whereNotNull('quotation_id')->first();
            // Make sure this project doesn't have any invoice. So we could freely
            // add whatever amount we want (<= max)
            $this->deleteAllInvoice($project);

            $projectRepo = new Project($project);

            // Create 5 invoice with each of invoce is
            // projectTotal / 2
            // So this is should projectTotal / 2 * 2 > projectTotal
            // This should failed, because total all invoices is more than
            // project value
            for($i=0; $i<10; $i++) {
                // Generate new invoice
                $row = $this->getModel(new InvoiceModel);
                $row->project_id = $project->id; // Associate with project id

                // Create invoice repo
                $repo = new Invoice($row);
                $amount = (int) ($projectRepo->getTotal() / 2);
                // Add new detail
                $repo->addDetail(null, 'Test buying ' . ($i+1), $amount);
                // Save invoice
                $repo->save();
            }

            // Should NOT reach here
            $this->assertNotEmpty(null, 'FAILED: Invoice inserted');
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(1, 'SUCCESS: Invoice cannot be inserted.');
        }
    }

    private function deleteAllInvoice($projectId)
    {
        $invoiceIdList = InvoiceModel::where('project_id', $projectId)->pluck('id');

        // Delete invoice detail
        \App\InvoiceDetail::whereIn('invoice_id', $invoiceIdList)->delete();
        InvoiceModel::whereIn('id', $invoiceIdList)->delete();
    }

    private function getModel(InvoiceModel $row)
    {
        $row->project_id = null;
        $row->ref_no = \App\TheBadusLibs\WeString::random_str(8);
        $row->name = 'Invoice Tes';
        $row->address = 'Jl. tes';
        $row->hp = 'Jl. tes';
        $row->city = City::first()->id;
        $row->created = new \DateTime;

        return $row;
    }
}
