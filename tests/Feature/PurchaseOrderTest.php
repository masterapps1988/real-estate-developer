<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use Hash;
use DB;

use App\Ais\Repository\PurchaseOrder;

use App\Project;
use App\PurchaseOrder as PurchaseOrderModel;
use App\Category as Status;
use App\Item;

class PurchaseOrderTest extends DbTestCase
{
    public function testCreate()
    {
        try {
            $faker = \Faker\Factory::create();

            $items = Item::all();

            // Generate new purchase order
            $row = $this->getModel(new PurchaseOrderModel);
            $row->project_id = Project::first()->id;
            $this->assertNotEmpty($row->project_id);
            $this->assertInternalType('int', $row->project_id);

            // Create purchase order repo
            $repo = new PurchaseOrder($row);

            for($i=0; $i<5; $i++) {
                $item = $faker->randomElement($items);
                // Add new detail/item
                $repo->addDetail(null, $item->id, $item->name, $item->unit,
                        $item->price, 1.5);
            }

            $repo->save();

            // All new PO should approved by higher roles
            $this->assertEquals(Status::STATUS_PENDING, $row->status_id);
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    public function testUpdate()
    {
        try {
            $faker = \Faker\Factory::create();

            $items = Item::all();

            // Generate new purchase order
            $row = $this->getModel(new PurchaseOrderModel);
            $row->project_id = Project::first()->id;
            $this->assertNotEmpty($row->project_id);
            $this->assertInternalType('int', $row->project_id);

            // Create purchase order repo
            $repo = new PurchaseOrder($row);

            for($i=0; $i<5; $i++) {
                $item = $faker->randomElement($items);
                // Add new detail/item
                $repo->addDetail(null, $item->id, $item->name, $item->unit,
                        $item->price, 1.5);
            }

            $repo->save();
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(1, 'Failed: PO cannot be changed. Only SP could be changed');
        }
    }

    private function getModel(PurchaseOrderModel $row)
    {
        $row->project_id = null;
        $row->ref_no = 'asdh18&';
        $row->status_id = Status::STATUS_APPROVED;
        $row->is_open = 1;
        $row->notes = 'Testing';
        $row->created = new \DateTime;

        return $row;
    }
}
