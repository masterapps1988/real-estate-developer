<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use Hash;

use App\User as UserModel;
use App\Category;
use App\Item as ItemModel;

use App\Ais\Repository\Item;

class ItemTest extends DbTestCase
{
    public function testCreate()
    {
        try {
            // Generate new item
            $row = $this->getModel(new ItemModel);
            
            // Get supplier, because item need supplier as user
            $supplier = UserModel::where('role_id', UserModel::ROLE_SUPPLIER)->first();
            $this->assertNotEmpty($supplier, 'Cannot do this test');

            // Set user_id in item
            $row->user_id = $supplier->id;

            // Use item repository
            $repo = new Item($row);
            $repo->save();

            // Should go here
            $this->assertNotEmpty($row->id);
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    private function getModel(ItemModel $item)
    {
        $item = new ItemModel;
        $item->item_category_id = ItemModel::ITEM;
        $item->user_id = UserModel::first()->id;
        $item->name = 'Semen';
        $item->unit = 'sak';
        $item->price = 1500.25;

        return $item;
    }
}
