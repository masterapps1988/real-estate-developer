<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use App\Ais\Repository\Quotation;

use App\Quotation as QuotationModel;
use App\City;
use App\Customer;
use App\Category;

class QuotationTest extends DbTestCase
{
    // Test create or update
    public function testCreate()
    {
        try {
            $model = $this->getModel(new QuotationModel);
            $model->status_id = Category::STATUS_DRAFT;

            $row = new Quotation($model);

            // Add quotation detail

            // Create job
            for($i=0; $i<3; $i++) {
                $id = null;
                $quotationDetailId = null;
                $name = 'Job ' . $i;
                $unit = 'm2';
                $qty = 10 * $i;
                $price = 5000 * $i;
                $groupBy = 'A';
                $row->addDetail($id, $name, $unit, $qty, $price, $groupBy);
            }

            $row->save();

            $this->assertNotEmpty($model->id);
        } catch (ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertInstanceOf(ValidationException::class, $e, 'Create');
        }
    }

    // Test add detail
    public function testDetailDetail()
    {
        $detail = \App\QuotationDetail::first();
        $model = QuotationModel::find($detail->quotation_id);

        $row = new Quotation($model);
        $row->deleteDetail($detail->id);

        $this->assertNotEmpty($model->id);
    }

    // Test update status from draft to pending(sent to director)
    public function testChangeStatusDraftToPending()
    {
        try {
            $model = QuotationModel::whereNotNull('ref_no')->first();
            $model->status_id = Category::STATUS_DRAFT;

            $row = new Quotation($model);

            // Add quotation detail
            // Create job
            for($i=0; $i<3; $i++) {
                $id = null;
                $quotationDetailId = null;
                $name = 'Job ' . $i;
                $unit = 'm2';
                $qty = 10 * $i;
                $price = 5000 * $i;

                $row->addDetail($id, $name, $unit, $qty, $price, 'A');
            }

            $row->save();
            $row->setStatus(Category::STATUS_PENDING);

            $this->assertEquals(Category::STATUS_PENDING, $model->status_id);
        } catch (ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertInstanceOf(ValidationException::class, $e, 'change to pending');
        }
    }

    // Test update status from open to pending(sent to director)
    public function testChangeStatusPendingToApproveAndDeclineOnly()
    {
        $model = null;
        // Pending should only could change to approved or declined

        // Success Test
        // Try to change pending to approve and decline
        try {
            $model = QuotationModel::whereNotNull('ref_no')->first();

            // Test change pending to approve
            $model->status_id = Category::STATUS_PENDING;

            $row = new Quotation($model);
            $row->setStatus(Category::STATUS_APPROVED);
            $row->save();

            $this->assertEquals(Category::STATUS_APPROVED, $model->status_id);

            // Test change pending to decline
            $model->status_id = Category::STATUS_PENDING;

            $row = new Quotation($model);
            $row->setStatus(Category::STATUS_DECLINED);
            $row->save();

            $this->assertEquals(Category::STATUS_DECLINED, $model->status_id);
        } catch (ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertInstanceOf(ValidationException::class, $e, 'Should not go here');
        }

        // Failed Test
        // Try to change pending to draft/revision
        try {
            $model = QuotationModel::whereNotNull('ref_no')->first();

            // Test change pending to draft
            $model->status_id = Category::STATUS_PENDING;

            $row = new Quotation($model);
            $row->setStatus(Category::STATUS_DRAFT);
            $row->save();

            // Test change pending to revision
            $model->status_id = Category::STATUS_PENDING;

            $row = new Quotation($model);
            $row->setStatus(Category::STATUS_REVISION);
            $row->save();

            $this->assertNotEmpty(null, 'ERROR: Should not reach this line');
        } catch (ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $message = 'Success, cannot change pending to draft or revision';
            $this->assertInstanceOf(ValidationException::class, $e, $message);
        }
    }

    /* Quotation could be revision when status is revision
     */
    public function testCreateRevision()
    {
        try {
            $model = QuotationModel::whereNotNull('ref_no')->first();
            $model->status_id = Category::STATUS_REVISION;

            $this->assertNotEmpty($model->detail);

            $row = new Quotation($model);
            $row->createRevision();
            $row->save();

            $this->assertEquals(Category::STATUS_DRAFT, $model->status_id);
        } catch (ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertInstanceOf(ValidationException::class, $e, 'create revision');
        }
    }

    private function getModel(QuotationModel $model)
    {
        $model->quotation_id = null;
        $model->customer_id = Customer::first()->id;
        $model->city_id = City::first()->id;
        $model->ref_no = \App\TheBadusLibs\WeString::random_str(8);
        $model->name = 'Quotation title';
        $model->address = 'Jl. jalan';
        $model->year = 2018;
        $model->created = new \DateTime();

        return $model;
    }
}
