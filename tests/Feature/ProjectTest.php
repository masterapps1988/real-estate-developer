<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use Hash;
use DB;

use App\Project as ProjectModel;
use App\Category;
use App\Item as ItemModel;

use App\Ais\Repository\Project;

use App\City as CityModel;
use App\Customer as CustomerModel;
use App\User as UserModel;
use App\Quotation;

class ProjectTest extends DbTestCase
{
    public function testCreate()
    {
        try {
            // Generate new item
            $row = $this->getModel(new ProjectModel);
            
            // Get supplier, because item need supplier as user
            $customer = CustomerModel::first();
            $this->assertNotEmpty($customer, 'Cannot do this test');

            // Set user_id in item
            $row->customer_id = $customer->id;

            // Use item repository
            $repo = new Project($row);
            $repo->save();

            // Should go here
            $this->assertNotEmpty($row->id);
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    public function testDelete()
    {
        try {
            // Generate new item
            $row = ProjectModel::first();

            // Get supplier, because item need supplier as user
            $customer = CustomerModel::first();
            $this->assertNotEmpty($customer, 'Cannot do this test');

            // Set user_id in item
            $row->customer_id = $customer->id;

            // Use item repository
            $repo = new Project($row);
            $repo->delete();

            // Should go here
            $this->assertNotEmpty($row->id);
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    public function testChangeWorker()
    {
        try {
            // Generate new item
            $row = $this->getModel(new ProjectModel);

            // Get supplier, because item need supplier as user
            $customer = CustomerModel::first();
            $this->assertNotEmpty($customer, 'Cannot do this test');

            // Set user_id in item
            $row->customer_id = $customer->id;

            $mOperasional = UserModel::where('role_id', UserModel::ROLE_MANAGER_OPERASIONAL)
                    ->first();
            $operasional = UserModel::where('role_id', UserModel::ROLE_OPERASIONAL)->first();
            $logistic = UserModel::where('role_id', UserModel::ROLE_LOGISTIC)->first();

            // Use item repository
            $repo = new Project($row);
            $repo->addWorker($mOperasional->id);
            $repo->addWorker($operasional->id);
            $repo->addWorker($logistic->id);
            $repo->removeWorker($mOperasional->id);
            $repo->save();

            // Should go here
            $this->assertNotEmpty($row->id);
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    public function testSetQuotation()
    {
        try {
            // Generate new item
            $row = ProjectModel::first();
            $quotation = Quotation::where('is_quotation', 1)->first();

            // Use item repository
            $repo = new Project($row);
            $repo->setQuotation($quotation);
            $repo->save();

            // Should go here
            $this->assertNotEmpty($row->quotation_id);
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }

    private function getModel(ProjectModel $row)
    {
        $row->customer_id = null;
        $row->city_id = CityModel::first()->id;
        $row->ref_no = random_int(99, 100000);
        $row->name = 'Project One';
        $row->address = 1500.25;
        $row->start = new \DateTime();

        $finish = clone $row->start;
        $finish->add(new \DateInterval('P1M'));
        $row->finish = $finish;

        return $row;
    }

    public function testPriceSummary()
    {
        $project = new Project(ProjectModel::find(2));
        $priceSummary = $project->getPriceSummaryDetail();
        var_dump($priceSummary);
        // var_dump(PricePlan::getPriceSummaryDetail(2));
    }

    public function testCreatePricePlan()
    {
        try {
            // Generate new item
            $row = ProjectModel::first();
            $quotation = Quotation::where('is_quotation', 1)->first();

            // Use item repository
            $repo = new Project($row);
            $repo->setQuotation($quotation);
            $repo->save();

            $id = $repo->createPricePlan();

            // Should go here
            $this->assertNotEmpty($id);
        } catch(ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertNotEmpty(null, 'Failed to insert');
        }
    }
}
