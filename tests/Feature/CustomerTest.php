<?php

namespace Tests\Feature;

use Tests\DbTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

use App\Ais\Repository\Customer;

use App\Customer as CustomerModel;

class CustomerTest extends DbTestCase
{
    public function testCreate()
    {
        $customer = $this->getCustomer(new CustomerModel);

        $row = new Customer($customer);
        $row->save();

        $this->assertNotEmpty($customer->id);
    }

    public function testCreateNullName()
    {
        try {
            $customer = $this->getCustomer(new CustomerModel);
            $customer->name = null;

            $row = new Customer($customer);
            $row->save();

            $this->assertNotEmpty(null);
        } catch (ValidationException $e) {
            var_dump($e->validator->errors()->all());
            $this->assertInstanceOf(ValidationException::class, $e);
        }
    }

    private function getCustomer(CustomerModel $customer)
    {
        $customer = new CustomerModel;
        $customer->name = 'name';
        $customer->address = 'name';
        $customer->phone = 'name';
        $customer->npwp = 'name';
        $customer->notes = 'name';

        return $customer;
    }
}
