<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationDetail extends Model
{
    protected $table = 'quotation_detail';

    public function parentDetail()
    {
        return $this->hasOne(QuotationDetail::class);
    }
}
