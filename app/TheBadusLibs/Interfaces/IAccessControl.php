<?php

namespace App\TheBadusLibs\Interfaces;

use App\TheBadusLibs\WeAccessControl;

// Access control interface to help business logic to determine what to do
// with various permission
interface IAccessControl
{
    public function hasAccess($name);
    public function hasAccesses($listName);
}
