<?php
namespace App\TheBadusLibs;

/**
 * CSV to Array Generation Class
 *
 * @author Johanes Surya <johanes.surya43@gmail.com>
 * @link http://www.johanessurya.com/
 * @copyright Copyright (C) 2016-2016 Johanes Surya
 * @license http://www.opensource.org/licenses/bsd-license.php
 */
class Csv {
  /** Prepare array into textual format
   *
   * @param string $csv csv string input
   */
  public static function toArray($csv) {

    // $lines = explode(PHP_EOL, $csv);
    // Prevent not split new line when string contain new-line char(\n)
    $lines = preg_split('/[\r\n]{1,2}(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/',$csv);

    $rows = [];
    foreach ($lines as $x) {
      if(!empty($x)) {
        $rows[] = str_getcsv($x);
      }
    }

    return $rows;
  }

  public static function toCsv($lines) {
    $return = '';

    foreach($lines as $x) {
      $return = $return . '"' . implode('","', $x) . '"' . "\n";
    }

    return $return;
  }
}
