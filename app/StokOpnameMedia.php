<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StokOpnameMedia extends Model
{
    protected $table = 'stok_opname_media';

    public function stokOpname()
    {
        return $this->belongsToMany(StokOpname::class);
    }
}
