<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Item;

class ItemDetail extends Model
{
    protected $table = 'item_detail';

    public function parentDetail()
    {
        return $this->hasOne(Item::class);
    }
}
