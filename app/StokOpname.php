<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StokOpname extends Model
{
    protected $table = 'stok_opname';

    public function detail()
    {
        return $this->hasMany(StokOpnameDetail::class);
    }

    public function media()
    {
        return $this->hasMany(StokOpnameMedia::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(Category::class, 'status_id');
    }
}
