<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Invoice;
use App\Category;

class Payment extends Model
{
	protected $table = 'invoice_payment';

	protected $dates = [
        'created'
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function bank()
    {
        return $this->belongsTo(Category::class);
    }
}
