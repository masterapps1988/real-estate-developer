<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptDetail extends Model
{
    protected $table = 'receipt_detail';

    public function parentDetail()
    {
        return $this->hasOne(ReceiptDetail::class);
    }
}
