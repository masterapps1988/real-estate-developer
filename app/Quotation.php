<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\City;
use App\Customer;
use App\Category;
use App\QuotationDetail;

class Quotation extends Model
{
    protected $table = 'quotation';

    protected $dates = [
        'created'
    ];

    public $timestamps = true;

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function status()
    {
        return $this->belongsTo(Category::class);
    }

    public function detail()
    {
        return $this->hasMany(QuotationDetail::class);
    }
}
