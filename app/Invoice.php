<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\InvoiceDetail;
use App\User;

class Invoice extends Model
{
    protected $table = 'invoice';

    public function detail()
    {
        return $this->hasMany(InvoiceDetail::class);
    }

    public function getTotal()
    {
        return (float) InvoiceDetail::where('invoice_id', $this->id)->sum('amount');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
