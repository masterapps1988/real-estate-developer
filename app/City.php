<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Province;

class City extends Model
{
    protected $table = 'city';

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}
