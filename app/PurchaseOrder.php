<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Category as Status;

class PurchaseOrder extends Model
{
    const VALID_STATUS = [
        Status::STATUS_DRAFT,
        Status::STATUS_PENDING,
        Status::STATUS_REVISION,
        Status::STATUS_APPROVED,
        Status::STATUS_DECLINED
    ];

    protected $table = 'purchase_order';

    protected $dates = [
        'created',
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;

    public function detail()
    {
        return $this->hasMany(PurchaseOrderDetail::class);
    }

    public function list()
    {
        return $this->hasMany(PurchaseOrderPayment::class);
    }
}
