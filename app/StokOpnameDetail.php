<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StokOpnameDetail extends Model
{
    protected $table = 'stok_opname_detail';
    protected $casts = [
        'qty_before' => 'float',
        'qty' => 'float'
    ];

    public function stokOpname()
    {
        return $this->belongsToMany(StokOpname::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
