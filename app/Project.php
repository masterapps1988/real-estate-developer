<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Customer;
use App\User;
use App\Quotation;

class Project extends Model
{
    protected $table = 'project';

    protected $dates = [
        'start',
        'finish',
        'created_at',
        'updated_at',
        'project_start',
        'project_finish'
    ];

    const MAX_WORKER = 3;
    const PREMIT_ROLE = [
        User::ROLE_MANAGER_OPERASIONAL,
        User::ROLE_OPERASIONAL,
        User::ROLE_LOGISTIC
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function quotation()
    {
        return $this->belongsTo(Quotation::class, 'quotation_id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
