<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\TheBadusLibs\WeString;

class User extends Authenticatable
{
    use Notifiable;

    const ROLE_DIRECTOR = 1;
    const ROLE_MARKETING = 2;
    const ROLE_PURCHASING_ITEM = 3;
    const ROLE_PURCHASING_MANDOR_VENDOR = 4;
    const ROLE_MANAGER_OPERASIONAL = 5;
    const ROLE_OPERASIONAL = 7;
    const ROLE_MANAGER_LOGISTIC = 10;
    const ROLE_LOGISTIC = 11;
    const ROLE_QUALITY_CONTROL = 12;
    const ROLE_FINANCE = 13;
    const ROLE_MANDOR = 8;
    const ROLE_VENDOR = 9;
    const ROLE_SUPPLIER = 14;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'role_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function removeApiToken() {
      $this->api_token = null;
      $this->save();
    }

    public function generateApiToken() {
        // Find unique token
        do {
            $apiToken = WeString::random_str(100);
            $user = self::where('id', '!=', $this->id)
              ->where('api_token', $apiToken)->count();
        }while($user > 0);

        $this->api_token = $apiToken;
        $this->save();
    }
}
