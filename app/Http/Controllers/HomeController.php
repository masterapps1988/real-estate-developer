<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use Auth;
use App\Ais\Ais;
use App\User;
use App\Ais\Mails\ForgotPassword;

class HomeController extends Controller
{
    public function dashboard()
    {
        return view('dashboard');
    }

    public function login()
    {
        return view('login');
    }

    public function postLogin(Request $request)
    {
        try {
            Ais::login($request->username, $request->password);

            return redirect('/');
        } catch(UnauthorizedHttpException $e) {
            return redirect('/login')
                    ->withInput()
                    ->with('message', $e->getMessage());
        }
    }

    public function logout()
    {
        Ais::logout(Auth::user());

        return redirect('/login')->with('message', 'Anda berhasil logout');
    }

    public function forgotPassword()
    {
        return view('forgot-password');
    }

    public function postForgotPassword(Request $request)
    {
        try {
            $param = $request->all();
            $user = User::where('email', $param['email'])->first();
            
            $password = str_random(10);

            $mail = new ForgotPassword($user);
            $mail->setPassword($password);
            $mail->setResetToken($password);
            $mail->sendMail();
            
            $user->password = Hash::make($password);
            $user->save();

            return redirect('/lupa-password')->with('message', 'Password baru telah dikirim ke email.');
        } catch(ValidationException $e) {
            return redirect('/lupa-password')->withInput()->with('message', $e->getMessage());
        } catch(\Exception $e) {
            return redirect('/lupa-password')->with('message', 'Password baru sudah terkirim jika email terdaftar kedalam database kami.');
        }
    }
}
