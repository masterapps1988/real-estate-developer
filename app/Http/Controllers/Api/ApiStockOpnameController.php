<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\File;

use App\StokOpname as Model;
use App\Category as Status;

use App\Ais\Repository\Finder\StokOpnameFinder;
use App\Ais\Repository\StokOpname;

class ApiStockOpnameController extends ApiController
{
    public function index(Request $request)
    {
        $finder = new StokOpnameFinder();
        $finder->showStokOnly();

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        if(isset($request->page))
            $finder->setPage($request->page);

        if(isset($request->keyword))
            $finder->setKeyword($request->keyword);

        if (isset($request->status)) {
            $status = explode(",", $request->status);

            $grupStatus = [];
            foreach ($status as $i => $sts) {
                $grupStatus[] = Status::getStatusIdByName($sts);
            }
            $finder->setStatusIdGroup($grupStatus);
        }

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $status = Status::find($x->status_id);
            $list[] = [
                'id' => $x->id,
                'user' => $x->user,
                'project' => $x->project,
                'project_id' => $x->project_id,
                'ref_no' => $x->ref_no,
                'created' => $x->created,
                'is_stock' => $x->is_stock,
                'status' => [
                    'name' => $status->name,
                    'label' => $status->label
                ]
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();

        //need refactor
        $data = Model::with('project')->whereNull('user_id')->get();
        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    public function show($id)
    {
        $stockOpname = $this->getModel($id);

        $repo = new StokOpname($stockOpname);
        $data = $repo->toArray();

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request)
    {
        $images = $request->file('images');
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $stockOpname = new Model;

        $id = $request->id;

        if(!empty($id)){
            $stockOpname = Model::findOrNew($id);
        }

        $statusId = Status::getStatusIdByName($request->status->name);

        $stockOpname->project_id = $request->project_id;
        $stockOpname->status_id = $statusId;
        $stockOpname->ref_no = $request->ref_no;
        $stockOpname->is_stock = $request->is_stock;
        $stockOpname->created = $request->created;

        $repo = new StokOpname($stockOpname);

        // Delete detail
        if(!empty($request->_delete_detail)) {
            foreach($request->_delete_detail as $x){
                $repo->deleteDetail($x);
            }
        }

        // Save detail quotation
        if(!empty($request->detail)){
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];
                
                $difference = $x['qty'] - $x['qty_before'];
                if ($difference != 0) {
                    $repo->addDetail($id, $x['qty_before'], $x['qty'], $x['quotation_detail_id']);
                }
            }
        }
        
        // Delete image
        if(!empty($request->_delete_images)) {
            foreach($request->_delete_images as $x){
                $repo->deleteImage($x);
            }
        }

        // Save image
        if(!empty($images)){
            foreach($images as $x) {
                $repo->addImage($id, new File($x));
            }
        }

        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Stock Opname telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new StokOpname($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Stock Opname berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status)
    {
        // If status is not approve or decline
        if (!in_array($status, ['draft', 'pending', 'approve', 'decline'])) {
            throw new NotFoundHttpException('Stock Opname tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new StokOpname($row);
        $repo->setStatus($statusId);

        switch ($statusId) {
            case Status::STATUS_APPROVED:
                $message = 'Stock Opname berhasil diterima';
                break;

            case Status::STATUS_DRAFT:
                $message = 'Stock Opname berstatus draft';
                break;

            case Status::STATUS_PENDING:
                $message = 'Stock Opname sedang diajukan';
                break;

            case Status::STATUS_APPROVED:
                $message = 'Stock Opname berhasil diterima';
                break;

            case Status::STATUS_DECLINED:
                $message = 'Stock Opname berhasil ditolak';
                break;

            default:
                $message = 'Stock Opname berhasil tersimpan';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id)
    {
        $row = Model::find($id);
        if (empty($row)) {
            throw new NotFoundHttpException('Opname tidak ditemukan');
        }

        return $row;
    }
}
