<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ais\Repository\Finder\StokOpnameFinder;
use App\Ais\Repository\JobOpname;

use App\StokOpname as Model;
use App\Category as Status;

use App\Ais\Ais;

class ApiJobOpnameController extends ApiController {
    public function index(Request $request){
        $finder = new StokOpnameFinder();
        $finder->showJobOnly();

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        if(isset($request->page))
            $finder->setPage($request->page);

        if(isset($request->keyword))
            $finder->setKeyword($request->keyword);

        if (isset($request->status)) {
            $status = explode(",", $request->status);

            $grupStatus = [];
            foreach ($status as $i => $sts) {
                $grupStatus[] = Status::getStatusIdByName($sts);
            }
            $finder->setStatusIdGroup($grupStatus);
        }

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $status = Status::find($x->status_id);
            $list[] = [
                'id' => $x->id,
                'user' => $x->user,
                'project' => $x->project,
                'project_id' => $x->project_id,
                'ref_no' => $x->ref_no,
                'created' => $x->created,
                'is_stock' => $x->is_stock,
                'status' => [
                    'name' => $status->name,
                    'label' => $status->label
                ]
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getModel($id);

        $repo = new JobOpname($row);

        $this->jsonResponse->setData($repo->toArray());

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){

        $images = $request->file('images');
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $jobOpname = new Model;

        // Get Id
        $id = $request->id;
        if(!empty($id)){
            $jobOpname = Model::findOrNew($id);
        }

        $statusId = Status::getStatusIdByName($request->status->name);

        // Update job opname
        $jobOpname->project_id = $request->project_id;
        $jobOpname->user_id = $request->user_id;
        $jobOpname->status_id = $statusId;
        $jobOpname->ref_no = $request->ref_no;
        $jobOpname->created = $request->created;
        $jobOpname->is_stock = 0;

        $repo = new JobOpname($jobOpname);

        // Delete detail
        if($request->_delete_detail) {
            foreach($request->_delete_detail as $x)
                $repo->deleteDetail($x);
        }

        // Save detail quotation
        if(!empty($request->detail))
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['quotation_detail_id'], $x['progress'], $x['real']);
            }

        // Delete image
        if(!empty($request->_delete_images)) {
            foreach($request->_delete_images as $x){
                $repo->deleteImage($x);
            }
        }

        // Save image
        if(!empty($images)){
            foreach($images as $x) {
                $repo->addImage($id, new File($x));
            }
        }

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Opname Pekerjaan telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new JobOpname($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Opname Pekerjaan berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status)
    {
        // If status is not approve or decline
        if (!in_array($status, ['draft', 'pending', 'approve', 'decline'])) {
            throw new NotFoundHttpException('Opname Pekerjaan tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new JobOpname($row);
        $repo->setStatus($statusId);

        switch ($statusId) {
            case Status::STATUS_APPROVED:
                $message = 'Opname Pekerjaan berhasil diterima';
                break;

            case Status::STATUS_DRAFT:
                $message = 'Opname Pekerjaan berstatus draft';
                break;

            case Status::STATUS_PENDING:
                $message = 'Opname Pekerjaan sedang diajukan';
                break;

            case Status::STATUS_APPROVED:
                $message = 'Opname Pekerjaan berhasil diterima';
                break;

            case Status::STATUS_DECLINED:
                $message = 'Opname Pekerjaan berhasil ditolak';
                break;

            default:
                $message = 'Opname Pekerjaan berhasil tersimpan';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Opname Pekerjaan tidak ditemukan');
        }

        return $row;
    }
}