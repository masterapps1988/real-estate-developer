<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use DB;

use App\Item as Model;
use App\ItemDetail;
use App\Category;

use App\Ais\Repository\Jobs;
use App\Ais\Repository\Finder\JobsFinder;

class ApiJobsController extends ApiController
{
    public function index(Request $request)
    {
        $finder = new JobsFinder();

        if(isset($request->keyword)) {
            $finder->setKeyword($request->keyword);
        }

        if(isset($request->page)){
            $finder->setPage($request->page);
        }

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $total = 0;
            $totalDetail = 0;
            $details = ItemDetail::where('item_id', $x->id)->get();
            foreach ($details as $y) {
                $item = Model::where('id', $y->item_id_detail)->first();
                $totalDetail = $item->price * $y->qty;
                $total += $totalDetail;
            }

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'name' => $x->name,
                'unit' => $x->unit,
                'total' => $total
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show(Request $request){
        // Get quotation model
        $row = $this->getModel($request->id);
        $repo = new Jobs($row);

        $details =  $row->detail;

        $detail = [];
        foreach($details as $x) {

            $item_detail = Model::find($x->item_id_detail);
            $category = Category::find($item_detail->item_category_id);

            if ($category->label == 'Item') {
                $category->label = 'Bahan';
            }

            $detail[] = [
                'id' => $x->id,
                'item_id' => $x->item_id,
                'item_id_detail' => $x->item_id_detail,
                'item_category_id' => $category->name,
                'item_category_name' => $category->label,
                'name' => $item_detail->name,
                'unit' => $item_detail->unit,
                'qty' => $x->qty,
                'price' => $item_detail->price
            ];
        }

        // Create data to be passed
        $data = [
            'id' => $row->id,
            'ref_no' => $row->ref_no,
            'name' => $row->name,
            'qty' => $row->qty,
            'unit' => $row->unit,
            'detail' => $detail 
        ];

        $this->jsonResponse->setData($data);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){

        $temp = json_decode($request->data);
        $request = (object) $temp;
        // If status property not exists then create the property

        $item = new Model;

        $id = $request->id;
        if(!empty($id)){
            $item = Model::findOrNew($request->id);
        }

        $item->item_category_id = Category::getItemTypeIdByName('job');
        $item->ref_no = $request->ref_no;
        $item->qty = null;
        $item->unit = $request->unit;
        $item->name = $request->name;
        $item->price = 0;

        $repo = new Jobs($item);

        // Delete detail
        if($request->_delete_detail) {
            foreach($request->_delete_detail as $x){
                $repo->deleteDetail($x);
            }
        }

        // Save detail quotation
        if(!empty($request->detail)){
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addItem($id, $x['item_id_detail'], $x['qty']);
            }
        }

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Pekerjaan telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new Jobs($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Pekerjaan berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Jobs tidak ditemukan');
        }

        return $row;
    }
}
