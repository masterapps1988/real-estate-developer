<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;

use App\Ais\Ais;
use App\Ais\Repository\Setting;
use App\TheBadusLibs\Helper\DateFormat;

use App\Config as Model;

class ApiSettingController extends ApiController
{
    public function index()
    {
        $setting = Model::get();

        $setingList = [];
        foreach($setting as $x) {
            $setingList[] = [
                'key' => $x->key,
                'value' => $x->value
            ];
        }
        
        $list = array_column($setingList, 'value', 'key');

        $this->jsonResponse->setData($list);
        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request)
    {
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $repo = new Setting();

        foreach($request as $key => $value) {
            $repo->update($key, $value);
        }

        $repo->save();

        $this->jsonResponse->setMessage('Data telah berhasil tersimpan.');
            
        return $this->jsonResponse->getResponse();
    }
}
