<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use DB;

use App\Ais\Repository\Quotation;
use App\Ais\Repository\QuotationSchedule;
use App\Ais\Repository\Finder\QuotationFinder;
use App\Ais\Repository\Finder\ScheduleFinder;


use App\Quotation as Model;
use App\Category as Status;

class ApiQuotationController extends ApiController
{
    public function index(Request $request)
    {
        $finder = new QuotationFinder();

        if(isset($request->in_project)) {
            $finder->setInProject($request->in_project);
        }

        if(isset($request->condition)) {
            $finder->setAvailable();
        }

        if(isset($request->customer_id)) {
            $finder->setCustomerId($request->customer_id);
        }

        if(isset($request->status)) {
            $statusId = Status::getStatusIdByName($request->status);
            $finder->setStatusId($statusId);
        }

        if(isset($request->keyword)) {
            $finder->setKeyword($request->keyword);
        }

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $totalRevisi = Model::where('quotation_id', $x->id)->count();
            $status = Status::find($x->status_id);

            $customerName = null;
            if($x->customer_name) {
                $customerName = $x->customer_name;
            }

            $cityName = null;
            if($x->city_name) {
                $cityName = $x->city_name;
            }

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'name' => $x->name,
                'revisi' => $totalRevisi + 1,
                'status' => [
                    'name' => $status->name,
                    'label' => $status->label
                ],
                'customer' => [
                    'name' => $customerName
                ],
                'city' => [
                    'name' => $cityName
                ],
                'created' => $x->created->format('d-m-Y')
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getModel($id);

        $repo = new Quotation($row);

        // Quotation
        $detail = $row->toArray();
        $detail['project'] = $repo->getProject();
        $detail['customer'] = $row->customer->toArray();
        $detail['city'] = $row->city->toArray();
        $detail['city']['province'] = $row->city->province->toArray();
        $detail['status'] = Status::find($row->status_id);
        $detail['schedule_status_id'] = Status::find($row->schedule_status_id);

        // Detial revision
        $list = [];
        foreach($repo->getRevisionList() as $x) {
            $name = $x->updated_at->format('d-m-Y H:m:s');
            if(empty($x->quotation_id)) {
                $name = 'Terbaru';
            }

            $list[] = [
                'id' => $x->id,
                'name' => $name
            ];
        }
        $detail['revisions'] = $list;

        // Quotation detail
        $listDetail = $repo->getAllDetail();
        $detail['detail'] = $listDetail;

        $this->jsonResponse->setData($detail);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $temp = json_decode($request->data);
        $request = (object) $temp;

        // If status property not exists then create the property
        // Prevent error in switch section below
        if(!isset($request->status))
            $request->status = null;

        // Prevent error
        if(!isset($request->parent_id))
            $request->parent_id = null;

        $quotation = new Model;

        // Get Id
        $id = $request->id;
        if(empty($id)) {
            // This mean request for revision
            $id = $request->parent_id;
        }

        if(!empty($id)){
            $quotation = Model::findOrNew($id);
        }

        $repo = new Quotation($quotation);

        // Create revision first before update quotation
        if(!empty($request->parent_id))
            $repo->createRevision();

        // Update quotation
        $quotation->quotation_id = $quotation->id;
        $quotation->customer_id = $request->customer_id;
        $quotation->city_id = $request->city_id;
        $quotation->ref_no = $request->ref_no;
        $quotation->name = $request->name;
        $quotation->address = $request->address;
        $quotation->created = $request->created;
        $quotation->profit = $request->profit;
        $quotation->risk_factor = $request->risk_factor;
        $quotation->is_tax = $request->is_tax;
        $quotation->pph = $request->pph;

        // Delete detail
        if($request->_delete_detail) {
            foreach($request->_delete_detail as $x)
                $repo->deleteDetail($x);
        }

        // Save detail quotation
        if(!empty($request->detail)) {
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['item_id'], $x['name'], $x['unit'], $x['qty'],
                        $x['price'], $x['group_by'], $x['detail']);
            }
        }

        // Save
        $repo->save();

        // After save change status
        switch ($request->status) {
            case 'pending':
                $repo->setStatus(Status::STATUS_PENDING);
                break;
        }


        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Penawaran telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status)
    {
        $message = null;
        // If status is not approve or decline
        if(!in_array($status, ['approve', 'revision', 'draft', 'pending'])){
            throw new NotFoundHttpException('Penawaran/HPP1/HPP2 tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new Quotation($row);
        $repo->setStatus($statusId);

        switch($statusId) {
            case Status::STATUS_APPROVED:
                $message = 'Penawaran berhasil diterima';
                break;

            case Status::STATUS_REVISION:
                $message = 'Penawaran berhasil ditolak';
                break;
            case Status::STATUS_DRAFT:
                $message = 'Penawaran berhasil disimpan';
                break;
            case Status::STATUS_PENDING:
                $message = 'Penawaran berhasil ditunda';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new Quotation($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Penawaran berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function listSchedule(Request $request)
    {
        $finder = new ScheduleFinder();

        if(isset($request->status)) {
            $statusId = Status::getStatusIdByName($request->status);
            $finder->setStatusId($statusId);
        }

        if(isset($request->keyword)) {
            $finder->setKeyword($request->keyword);
        }

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $totalRevisi = Model::where('quotation_id', $x->id)->count();
            $status = Status::find($x->status_id);

            $customerName = null;
            if($x->customer_name) {
                $customerName = $x->customer_name;
            }

            $cityName = null;
            if($x->city_name) {
                $cityName = $x->city_name;
            }

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'name' => $x->name,
                'revisi' => $totalRevisi + 1,
                'status' => [
                    'name' => $status->name,
                    'label' => $status->label
                ],
                'customer' => [
                    'name' => $customerName
                ],
                'city' => [
                    'name' => $cityName
                ],
                'created' => $x->created->format('d-m-Y')
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function createSchedule(Request $request){
            $temp = json_decode($request->data);
            $request = (object) $temp;

            if ($request->schedule_status_id) {
                $status = $request->schedule_status_id->name;
            } else {
                $status = 'draft';
            }

            $statusId = Status::getStatusIdByName($status);
            $quotation = $this->getModel($request->id);

            $repo = new QuotationSchedule($quotation);

            $quotation->schedule_status_id = $statusId;

            // Save detail quotation
            if(!empty($request->detail))
                foreach($request->detail as $x) {
                    if (!empty($x->schedule_start) || !empty($x->schedule_finish)) {
                        $x = (array) $x;

                        $id = empty($x['id']) ? null : $x['id'];

                        $repo->addDetail($id, $x['name'], $x['unit'], $x['qty'],
                            $x['price'], $x['group_by'], $x['schedule_start'], $x['schedule_finish']);
                    }
                }

            // Save
            $repo->save();

            $this->jsonResponse->setData($repo->getModel()->id);
            $this->jsonResponse->setMessage('Schedule telah berhasil tersimpan.');

            return $this->jsonResponse->getResponse();
    }

    public function setStatusSchedule($id, $status)
    {
        $message = '';
        // If status is not draft, pending, approve or decline
        if(!in_array($status, ['draft', 'pending', 'approve', 'revision'])){
            throw new NotFoundHttpException('Penawaran/HPP1/HPP2 tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation schedule repository
        $repo = new QuotationSchedule($row);
        $repo->setStatus($statusId);

        switch($statusId) {
            case Status::STATUS_DRAFT:
                $message = 'Penjadwalan berhasil disimpan';
                break;
            case Status::STATUS_PENDING:
                $message = 'Penjadwalan berhasil ditunda';
                break;
            case Status::STATUS_APPROVED:
                $message = 'Penjadwalan berhasil diterima';
                break;
            case Status::STATUS_REVISION:
                $message = 'Penjadwalan berhasil ditolak';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    public function destroySchedule($id){
        $row = $this->getModel($id);
        $repo = new QuotationSchedule($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Penjadwalan berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function download(Request $request){
        $row = $this->getModel($request->id);
        $customer = 0;
        $customer = $request->customer;

        $repo = new Quotation($row);

        $quotation = $row->toArray();
        $city = $row->city->toArray();
        $province = $row->city->province->toArray();
        $detail = $repo->getJobs()->toArray();
        // var_dump($detail); die;

        $total = 0;
        for ($j = 0; $j < count($detail); $j++) { 
            if ($customer == 1) {
                $profit = $row->profit;
                $risk_factor = $row->risk_factor;
                $percent = $profit + $risk_factor;

                $total = $detail[$j]['price'] * ($percent / 100);
                $detail[$j]['price'] = $detail[$j]['price'] + $total;
            }
            $totalPerItem = $detail[$j]['qty'] * $detail[$j]['price'];
            $total += $totalPerItem;
        }

        $group = [];
        for ($i = 0; $i < count($detail); $i++) { 
            if (!in_array($detail[$i]['group_by'], $group)) {
                array_push($group, $detail[$i]['group_by']);
            }
        }

        $details = [];
        foreach ($detail as $x) {
            $details[$x['group_by']][] = $x;
        }

        $data = [
            'quotation' => $quotation,
            'city' => $city,
            'province' => $province,
            'details' => $details,
            'group' => $group,
            'total' => $total
        ];

        // print_r($data);
        // die();

        $filename = $repo->download($data);

        $return = [
                'id' => $request->id,
                'url' => url('/tmp/' . $filename)
            ];

        $this->jsonResponse->setData($return);
        return $this->jsonResponse->getResponse();
    }

    public function downloadSchedule(Request $request){

        $row = $this->getModel($request->id);
        
        $repo = new QuotationSchedule($row);

        $quotation = $row->toArray();
        $city = $row->city->toArray();
        $province = $row->city->province->toArray();
        $detail = $row->detail->toArray();

        $total_duration = 0;
        $arr_start = [];
        $arr_finish = [];
        for ($i=0; $i < count($detail); $i++) { 
            $start = new \DateTime($detail[$i]['schedule_start']);
            $finish = new \DateTime($detail[$i]['schedule_finish']);
            $duration = date_diff($start, $finish);
            $detail[$i]['duration'] = $duration->days;
            $total_duration += $detail[$i]['duration'];
            array_push($arr_start, $start);
            array_push($arr_finish, $finish);
        }

        $min_start = min($arr_start);
        $max_finish = max($arr_finish);

        $data = [
            'quotation' => $quotation,
            'city' => $city,
            'province' => $province,
            'total_duration' => $total_duration,
            'min_start' => $min_start->format('Y-m-d'),
            'max_finish' => $max_finish->format('Y-m-d'),
            'details' => $detail
        ];

        // print_r($data);
        // die();

        $filename = $repo->downloadSchedule($data);

        $return = [
                'id' => $request->id,
                'url' => url('/tmp/' . $filename)
            ];

        $this->jsonResponse->setData($return);
        return $this->jsonResponse->getResponse();
    }

    public function createRevision($id){
        $row = $this->getModel($id);

        $repo = new Quotation($row);

        $model = $repo->createRevision();

        $this->jsonResponse->setData($model->id);
        $this->jsonResponse->setMessage('Revisi berhasil dibuat.');

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Penawaran/HPP1/HPP2 tidak ditemukan');
        }

        return $row;
    }
}
