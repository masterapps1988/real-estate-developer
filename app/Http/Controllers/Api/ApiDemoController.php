<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;

class ApiDemoController extends ApiController
{
    public function upload(Request $request)
    {
        $test = $request->file('files');

        var_dump($test); die;
    }
}
