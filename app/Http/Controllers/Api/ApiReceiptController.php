<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\TheBadusLibs\Helper\DateFormat;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ais\Repository\Finder\ReceiptFinder;
use App\Ais\Repository\Receipt;
use App\Receipt as Model;
use App\Ais\Ais;
use App\Category as Status;

class ApiReceiptController extends ApiController {
    public function index(Request $request){
        $finder = new ReceiptFinder();

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        if(isset($request->status)) {
            $status = explode(",", $request->status);
            if(count($status) > 1) {
                $grupStatus = [];
                foreach($status as $i => $sts){
                    $grupStatus[] = Status::getStatusIdByName($sts);
                }
                $finder->setStatusIdGroup($grupStatus);
            } else {
                $statusId = Status::getStatusIdByName($request->status);
                $finder->setStatusId($statusId);    
            }
            
        }

        if(isset($request->page))
            $finder->setPage($request->page);

        if(isset($request->keyword))
            $finder->setKeyword($request->keyword);

        if(isset($request->project_id))
            $finder->setProjectId($request->project_id);

        if(isset($request->purchase_order_id))
            $finder->setPurchaseOrderId($request->purchase_order_id);

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $purchaseRef = null;
            if($x->po) {
                $purchaseRef = $x->po;
            }

            $data = Model::find($x->id);

            $repo = new Receipt($data);
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $x->created);
            $status = Status::find($x->status_id);

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'created' => DateFormat::shortDate($date),
                'purchase_order' => [
                    'id' => $x->po_id,
                    'ref_no' => $purchaseRef
                ],
                'status' => [
                    'name' => $status->name,
                    'label' => $status->label
                ],
                'project_id' => $x->project_id,
                'project_name' => $x->project_name
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getModel($id);

        $repo = new Receipt($row);
        // change format date
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $row->created);

        $list = [];
        $list = [
            'id' => $row->id,
            'purchase_order_id' => $row->purchase_order_id,
            'ref_no' => $row->ref_no,
            'is_in' => $row->is_in,
            'created' => $date->format('Y-m-d'),
            // 'created' => DateFormat::shortDate($date),
            'created_at' => $row->created_at,
            'updated_at' => $row->updated_at,
        ];

        $detail = [];
        foreach($repo->getDetailUnit() as $x) {
            $detail[] = [
                'id' => $x->id,
                'item_id' => $x->item_id,
                'name' => $x->name,
                'qty' => $x->qty,
                'unit' => $x->unit,
            ];
        }
        $list['detail'] = $detail;
        $list['status'] = Status::find($row->status_id);

        $this->jsonResponse->setData($list);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $temp = json_decode($request->data);
        $request = (object) $temp;

        // Get Id
        $id = $request->id;

        $receipt = new Model;
        if(!empty($id)){
            $receipt = Model::findOrNew($id);
        }
        $statusId = Status::getStatusIdByName($request->status->name);

        $repo = new Receipt($receipt);

        // Update quotation
        $receipt->purchase_order_id = $request->purchase_order_id;
        $receipt->status_id = $statusId;
        $receipt->ref_no = $request->ref_no;
        $receipt->created = $request->created;

        // Delete detail
        if($request->_delete_detail) {
            foreach($request->_delete_detail as $x)
                $repo->deleteDetail($x);
        }

        // Save detail quotation
        if(!empty($request->detail))
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['item_id'], $x['name'], $x['qty']);
            }

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Receipt telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new Receipt($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Penerimaan berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status)
    {
        $message = '';
        // If status is not approve or decline
        if(!in_array($status, ['draft', 'pending', 'approve', 'decline'])){
            throw new NotFoundHttpException('LPB tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new Receipt($row);
        $repo->setStatus($statusId);

        switch($statusId) {
            case Status::STATUS_APPROVED:
                $message = 'LPB berhasil diterima';
                break;

            case Status::STATUS_DECLINED:
                $message = 'LPB berhasil ditolak';
                break;
            case Status::STATUS_DRAFT:
                $message = 'LPB berhasil di draft';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }
    
    public function download(Request $request){
        $row = $this->getModel($request->id);
        
        $repo = new Receipt($row);
        $filename = $repo->download();

        $data = [
                'id' => $request->id,
                'url' => url('/tmp/' . $filename)
            ];

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('LPB tidak ditemukan');
        }

        return $row;
    }
}