<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\City;
use App\Kuhlen\JsonResponse;

class ApiCityController extends ApiController
{
    public function index(Request $request){
        $data = [];
        $rows = City::all();

        foreach($rows as $x){
            $tamp = [];
            $tamp['id'] = $x->id;
            $tamp['name'] = $x->name . ', ' . $x->province->name;
            $data[] = $tamp;
        }

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }
}
