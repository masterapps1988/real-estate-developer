<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\TheBadusLibs\Helper\DateFormat;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category as Status;
use App\Project;

use App\Ais\Repository\Finder\PurchaseOrderFinder;
use App\Ais\Repository\PurchaseOrder;
use App\PurchaseOrder as Model;
use App\Ais\Ais;

class ApiPurchaseOrderController extends ApiController
{
    public function index(Request $request){
        $finder = new PurchaseOrderFinder();

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        } else {    
            $finder->orderBy('ref_no', 'asc');
        }

        if(isset($request->status)) {
            $status = explode(",", $request->status);
            if(count($status) > 1) {
                $grupStatus = [];
                foreach($status as $i => $sts){
                    $grupStatus[] = Status::getStatusIdByName($sts);
                }
                $finder->setStatusIdGroup($grupStatus);
            } else {
                $statusId = Status::getStatusIdByName($request->status);
                $finder->setStatusId($statusId);    
            }
            
        }

        if(isset($request->project_id))
            $finder->setProject($request->project_id);

        if(isset($request->page))
            $finder->setPage($request->page);

        if(isset($request->keyword))
            $finder->setKeyword($request->keyword);

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $projectName = null;
            if($x->project_name) {
                $projectName = $x->project_name;
            }

            $data = Model::find($x->id);
            $status = Status::find($x->status_id);

            $repo = new PurchaseOrder($data);
            $total = $repo->getTotal($x->id);
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $x->created);

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'notes' => $x->notes,
                'is_tax' => $x->is_tax,
                'status' => [
                    'name' => $status->name,
                    'label' => $status->label
                ],
                'created' => DateFormat::shortDate($date),
                'total' => $total,
                'project' => [
                    'id' => $x->project_id,
                    'name' => $projectName
                ]
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getModel($id);

        $repo = new PurchaseOrder($row);

        // po
        $status = Status::find($row->status_id);
        $project = Project::find($row->project_id);
        $total = $repo->getTotal($row->id);
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $row->created);

        $list = [];
        $list = [
            'id'=> $row->id,
            'project_id'=> $row->project_id,
            'status_id'=> $row->status_id,
            'is_open'=> $row->is_open,
            'ref_no'=> $row->ref_no,
            'notes'=> $row->notes,
            'is_tax'=> $row->is_tax,
            // 'created'=> DateFormat::shortDate($date),
            'created'=> $date->format('Y-m-d'),
            'status' => $status,
            'project' => $project,
            'total' => $total,
            'created_at' => $row->created_at,
            'updated_at' => $row->updated_at, 
        ];
        
        $detail = [];
        // po detail
        foreach($row->detail as $x){
            $receive = $repo->getReceiveItems($x->item_id);
            $status = $repo->getStatusItems($x->item_id);
            $detail[] = [
                'id' => $x->id,
                'purchase_order_id' => $x->purchase_order_id,
                'item_id' => $x->item_id,
                'name' => $x->name,
                'unit' => $x->unit,
                'status' => $status,
                'qty' => $x->qty,
                'receive' => $receive,
                'price' => $x->price
            ];
        }
        
        $list['detail'] = $detail;

        // Check qty po with qty receive
        $poStatus = '';
        $qtyPo = [];
        $qtyRi = [];
        $poList = [];
        foreach($row->detail as $x){
            $receive = $repo->getReceiveItems($x->item_id);

            $qtyPo[] = $x->qty;
            $qtyRi[] = $receive->qty;
            
            if( array_sum($qtyPo) === array_sum($qtyRi) ){
                $poStatus = 'Closed';
            } else {
                $poStatus = 'Open';
            }
        }
        $list['po_status'] = $poStatus;
        
        $this->jsonResponse->setData($list);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $purchaseOrder = new Model;
        
        // Get Id
        $id = $request->id;
        
        if(!empty($id)){
            $purchaseOrder = Model::findOrNew($id);
        }

        $statusId = Status::getStatusIdByName($request->status->name);

        $repo = new PurchaseOrder($purchaseOrder);

        // Update PO
        $purchaseOrder->project_id = $request->project_id;
        $purchaseOrder->status_id = $statusId;
        $purchaseOrder->is_open = $request->is_open;
        $purchaseOrder->ref_no = $request->ref_no;
        $purchaseOrder->notes = $request->notes;
        $purchaseOrder->created = $request->created;

        // Delete detail
        if($request->_delete_detail) {
            foreach($request->_delete_detail as $x)
                $repo->deleteDetail($x);
        }

        // Save detail quotation
        if(!empty($request->detail))
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['item_id'], $x['name'], $x['unit'], $x['price'], $x['qty']);
            }

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Purchase Order telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status){
        $message = '';
        // If status is not draft, pending, approve or decline
        if(!in_array($status, ['draft', 'pending', 'approve', 'decline'])){
            throw new NotFoundHttpException('Purchase Order tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new PurchaseOrder($row);
        $repo->setStatus($statusId);

        switch($statusId) {
            case Status::STATUS_DRAFT:
                $message = 'Purchase Order berhasil disimpan';
                break;
            case Status::STATUS_PENDING:
                $message = 'Purchase Order berhasil ditunda';
                break;
            case Status::STATUS_APPROVED:
                $message = 'Purchase Order berhasil diterima';
                break;
            case Status::STATUS_DECLINED:
                $message = 'Purchase Order berhasil ditolak';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    public function setTax($id, $tax){
        $message = '';
        
        // Get purchase-order model
        $row = $this->getModel($id);

        // Get purchase-order repository
        $repo = new PurchaseOrder($row);
        $repo->setTax($tax);

        switch($tax) {
            case 0:
                $message = 'Purchase Order Tanpa Pajak';
                break;
            case 1:
                $message = 'Purchase Order Pakai Pajak';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new PurchaseOrder($row);
        $repo->delete();

        $message = 'Surat Penawaran berhasil dihapus';
        if($repo->isPurchaseOrder())
            $message = 'Purchase Order berhasil dihapus';

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    public function downloadSp(Request $request){
        $row = $this->getModel($request->id);
        
        $repo = new PurchaseOrder($row);
        $filename = $repo->downloadSp();

        $data = [
                'id' => $request->id,
                'url' => url('/tmp/' . $filename)
            ];

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    public function downloadPo(Request $request){
        $row = $this->getModel($request->id);
        
        $repo = new PurchaseOrder($row);
        $filename = $repo->downloadPo();

        $data = [
                'id' => $request->id,
                'url' => url('/tmp/' . $filename)
            ];

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Purchase Order tidak ditemukan');
        }

        return $row;
    }

    public function itemListAvailable($id){
        // Project
        $purchaseOrder = $this->getPurchaseOrder($id);

        $repo = new PurchaseOrder($purchaseOrder);
        $list = $repo->getAvailableItems();
        
        $this->jsonResponse->setData($list);
        return $this->jsonResponse->getResponse();

    }

    private function getPurchaseOrder($id){
        $row = Model::where('purchase_order.id', $id)
                ->join('project', 'project.id', '=', 'purchase_order.project_id')
                ->select('purchase_order.*')
                ->first();
        if(empty($row)){
            throw new NotFoundHttpException('Purchase Order tidak ditemukan');
        }

        return $row;
    }


}
