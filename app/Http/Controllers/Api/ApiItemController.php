<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use DB;
use Excel;

use App\Ais\Repository\Item;
use App\Ais\Repository\ItemMandor;
use App\Ais\Repository\ItemVendor;
use App\Ais\Repository\Finder\ItemFinder;

use App\Item as ItemModel;
use App\ItemDetail as ItemDetailModel;
use App\ItemHistory as ItemHistoryModel;
use App\User;
use App\Category;

class ApiItemController extends ApiController
{
    public function index(Request $request){
        $finder = new ItemFinder();

        // Get item category id
        if(isset($request->item) && !empty($request->item)) {
            // Split to array
            $list = explode(',', $request->item);
            // Trim space
            $list = array_map('trim', $list);

            $type = [];

            for ($i=0; $i < count($list); $i++) {
                $type[$i] = Category::getItemTypeIdByName($list[$i]);
            }

            $finder->setTypeId($type);
        }

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        } else {
            $finder->orderBy('name', 'asc');
        }

        if($request->page)
            $finder->setPage($request->page);

        if($request->keyword)
            $finder->setKeyword($request->keyword);

        $paginator = $finder->get();

        $list = [];
        foreach($paginator->items() as $x) {
            $user = User::find($x->user_id);

            $temp = (array) $x;

            $userName = null;
            if ($user) {
                $userName = $user->name;
            }

            $temp['user_name'] = $userName;

            $list[] = $temp;
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getItem($id);
        $this->jsonResponse->setData($row->toArray());
    
        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        if (empty($request->id)) {
            $item = new ItemModel();
        } else {
            $item = ItemModel::findOrNew($request->id);
        }

        // Check role
        switch($request->role) {
            case 'supplier':
                $repo = new Item($item);
                break;

            case 'mandor':
                $repo = new ItemMandor($item);
                break;

            case 'vendor':
                $repo = new ItemVendor($item);
                break;

            default:
                throw new \Exception('Invalid role');
                break;
        }

        $item_category = $this->getCategoryType($request->item_category_id, $request->role);
        $item->ref_no = $request->ref_no;
        $item->user_id = $request->user_id;
        $item->name = $request->name;
        $item->unit = $request->unit;
        $item->price = $request->price;
        $item->item_category_id = $item_category;

        // Save model from repo method.
        $repo->save();

        $data = [
            'id' => $repo->getModel()->id,
            'role' => $request->role
        ];

        $this->jsonResponse->setData($data);
        $this->jsonResponse->setMessage('Item telah berhasil tersimpan.');
            
        return $this->jsonResponse->getResponse();
    }

    public function profile()
    {
        $this->jsonResponse->setData(Auth::user());
        return $this->jsonResponse->getResponse();
    }

    public function destroy($id)
    {
        $row = $this->getItem($id);
        $repo = new Item($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Item berhasil dihapuss');

        return $this->jsonResponse->getResponse();
    }

    public function export(Request $request){
        $role = ['item', 'mandor', 'vendor'];

        if (!in_array($request->role, $role)) {
            throw new NotFoundHttpException("Role tidak ada");
        }

        $data[] = [
            'ID' => null,
            'NO REF' => null,
            'ID SUPPLIER' => null,
            'NAMA BARANG' => null,
            'HARGA' => null,
            'SATUAN' => null
        ];

        $date = new \Datetime;
        $filename = $request->role . '_export_' . $date->format('Ymdhis');

        Excel::create($filename, function($excel) use($data) {
            $excel->sheet('Item', function($sheet) use($data) {
                $sheet->with($data);
            });
        })->store('xls', public_path('resources'));

        $return = [
                'url' => url('/resources/' . $filename . '.xls')
            ];

        $this->jsonResponse->setData($return);
        return $this->jsonResponse->getResponse();
    }

    public function import(Request $request)
    {
        $upload = $request->file('file');

        $role = ['item', 'mandor', 'vendor'];

        if (!in_array($request->role, $role)) {
            throw new NotFoundHttpException("Role tidak ada");
        }

        $roleId = Category::getItemTypeIdByName($request->role);

        foreach($upload as $x) {
            $file = new File($x);

            $data = Excel::load($file, function($reader) {
                //Do it later(if have more sheet)
            })->get();

            foreach ($data as $y) {
                $model = new ItemModel;
                $model->item_category_id = $roleId;
                $model->user_id = $y->id_supplier;
                $model->ref_no = $y->no_ref;
                $model->name = $y->nama_barang;
                $model->unit = $y->satuan;
                $model->qty = null;
                $model->price = $y->harga;
                $model->save();
            }
        }

        $this->jsonResponse->setMessage('Item telah berhasil tersimpan.');
        return $this->jsonResponse->getResponse();
    }

    public function download(Request $request){
        $role = ['item', 'mandor', 'vendor'];

        if (!in_array($request->role, $role)) {
            throw new NotFoundHttpException("Role tidak ada");
        }

        $roleId[] = Category::getItemTypeIdByName($request->role);

        $finder = new ItemFinder();
        $finder->setTypeId($roleId);
        $finder->setPage('all');
        $row = $finder->get();
        $data = [];
        foreach ($row as $x) {
            $data[] = [
                'ID' => $x->id,
                'NO REF' => $x->ref_no,
                'ID SUPPLIER' => $x->user_id,
                'NAMA BARANG' => $x->name,
                'HARGA' => $x->price,
                'SATUAN' => $x->unit
            ];
        }

        $date = new \Datetime;
        $filename = $request->role . '_download_' . $date->format('Ymdhis');

        Excel::create($filename, function($excel) use($data) {
            $excel->sheet('Item', function($sheet) use($data) {
                $sheet->with($data);
            });
        })->store('xls', public_path('resources'));

        $return = [
                'url' => url('/resources/' . $filename . '.xls')
            ];

        $this->jsonResponse->setData($return);
        return $this->jsonResponse->getResponse();
    }

    private function getItem($id){
        $row = ItemModel::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Item tidak ditemukan');
        }

        return $row;
    }

    private function getCategoryType($id, $role){
        $data = null;
        if (empty($id)) {
            if ($role == 'supplier') {
                $data = ItemModel::ITEM;
            } else if ($role == 'mandor') {
                $data = ItemModel::MANDOR;
            } else if ($role == 'vendor') {
                $data = ItemModel::VENDOR;
            }
        } else {
            $data = $id;
        }
        return $data;
    }
}
