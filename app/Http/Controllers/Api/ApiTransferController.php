<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ais\Repository\Finder\TransferFinder;
use App\Ais\Repository\Transfer;
use App\Project;
use App\Transfer as Model;
use App\Category as Status;
use App\Item;

use App\Ais\Ais;

class ApiTransferController extends ApiController {
    public function index(Request $request){
        $finder = new TransferFinder();

        if(isset($request->status)) {
            $statusId = Status::getStatusIdByName($request->status);
            $finder->setStatusId($statusId);
        }

        if(isset($request->project_id_from)) {
            $finder->setProjectIdFrom($request->project_id_from);
        }

        if(isset($request->project_id_to)) {
            $finder->setProjectIdTo($request->project_id_to);
        }

        if(isset($request->date_from)) {
            $finder->setDateFrom(new \DateTime($request->date_from));
        }

        if(isset($request->date_to)) {
            $finder->setDateTo(new \DateTime($request->date_to));
        }

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        if(isset($request->page))
            $finder->setPage($request->page);

        if(isset($request->keyword))
            $finder->setKeyword($request->keyword);

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $data = Model::find($x->id);
            $projectFrom = Project::find($x->project_id_from);
            $projectTo = Project::find($x->project_id_to);

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'project_from' => $projectFrom,
                'project_to' => $projectTo,
                'created' => $x->created,
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getModel($id);

        $repo = new Transfer($row);

        // Transfer
        $data = $row->toArray();
        $data['status'] = $row->status;

        $detail = [];
        foreach ($row->detail as $x) {
            $item = Item::find($x->item_id);

            $detail[] = [
                'id' => $x->id,
                'item_id' => $x->item_id,
                'qty' => $x->qty,
                'name' => $item->name,
                'unit' => $item->unit
            ];
        }

        $data['detail'] = $detail;

        $this->jsonResponse->setData($data);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $transfer = new Model;

        // Get Id
        $id = $request->id;

        if(!empty($id)){
            $transfer = Model::findOrNew($id);
        }
        $statusId = Status::getStatusIdByName($request->status->name);

        $repo = new Transfer($transfer);

        // Create revision first before update quotation
        // if(!empty($request->parent_id))
        //     $repo->createRevision();

        // Update transfer
        $transfer->project_id_from = $request->project_id_from;
        $transfer->project_id_to = $request->project_id_to;
        $transfer->status_id = $statusId;
        $transfer->notes = $request->notes;
        $transfer->ref_no = $request->ref_no;
        $transfer->created = $request->created;

        // Delete detail
        if($request->_delete_detail) {
            foreach($request->_delete_detail as $x)
                $repo->deleteDetail($x);
        }

        // Save detail quotation
        if(!empty($request->detail))
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['item_id'], $x['qty']);
            }

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Transfer telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new Transfer($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Transfer berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status)
    {
        $message = '';
        // If status is not approve or decline
        if(!in_array($status, ['draft', 'pending', 'approve', 'decline'])){
            throw new NotFoundHttpException('Transfer tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new Transfer($row);
        $repo->setStatus($statusId);

        switch($statusId) {
            case Status::STATUS_APPROVED:
                $message = 'Transfer berhasil diterima';
                break;
            case Status::STATUS_DECLINED:
                $message = 'Transfer berhasil ditolak';
                break;
            case Status::STATUS_DRAFT:
                $message = 'Transfer berhasil disimpan';
                break;
            case Status::STATUS_PENDING:
                $message = 'Transfer berhasil diajukan';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Transfer tidak ditemukan');
        }

        return $row;
    }
}