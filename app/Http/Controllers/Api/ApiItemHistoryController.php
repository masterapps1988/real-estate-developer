<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

use App\ItemHistory;

use App\Ais\Ais;

class ApiItemHistoryController extends ApiController
{
    public function index(Request $request)
    {
        if($request->item_id)
            $paginator = ItemHistory::where('item_id', $request->item_id)
                ->orderBy('created', 'desc')
                ->orderBy('id', 'desc')
                ->paginate(Ais::getPaginate());
        else
            $paginator = ItemHistory::paginate(Ais::getPaginate());

        $this->jsonResponse->setData($paginator->items());
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getItemHistory($id);
        $this->jsonResponse->setData($row->items());
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($row));
        
        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request)
    {
    }

    public function destroy($id){
        $row = $this->getItemHistory($id);
        $row->delete();
        $this->jsonResponse->setMessage('Item price history berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    private function getItemHistory($id){
        $row = ItemHistory::where('item_id', $id)
                ->orderBy('created', 'desc')
                ->orderBy('id', 'desc')
                ->paginate(Ais::getPaginate());

        if(empty($row)){
            throw new NotFoundHttpException('Item tidak ditemukan');
        }

        return $row;
    }
}
