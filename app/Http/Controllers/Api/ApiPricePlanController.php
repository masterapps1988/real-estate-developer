<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use DB;

// use App\Ais\Repository\Quotation;
use App\Ais\Repository\PricePlan;
use App\Ais\Repository\Finder\PricePlanFinder;

use App\Quotation as Model;
use App\QuotationDetail;
use App\Category as Status;
use App\Project;

class ApiPricePlanController extends ApiController
{
    public function index(Request $request)
    {
        $finder = new PricePlanFinder();

        // if(isset($request->in_project)) {
        //     $finder->setInProject($request->in_project);
        // }

        if(isset($request->status)) {
            $statusId = Status::getStatusIdByName($request->status);
            $finder->setStatusId($statusId);
        }

        if(isset($request->keyword)) {
            $finder->setKeyword($request->keyword);
        }

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $totalRevisi = Model::where('quotation_id', $x->id)->count();

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'name' => $x->name,
                'revisi' => $totalRevisi + 1,
                'status' => [
                    'name' => $x->status_name,
                    'label' => $x->status_label
                ],
                'customer' => [
                    'name' => $x->customer_name
                ],
                'city' => [
                    'name' => $x->city
                ],
                'created' => $x->created->format('d-m-Y')
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show(Request $request){
        // Get quotation model
        $row = $this->getModel($request->id);
        $repo = new PricePlan($row);

        // Get project
        $project = $repo->getProject();

        // Load quotation detail
        // Get all detail by quotation_id, OR
        // Get by quotation_detail_id(parent of)
        if ($request->quotation_detail_id) {
            $quotationDetail = PricePlan::getGroup($request->quotation_detail_id);
        } else {
            $quotationDetail = PricePlan::getQuotationDetail($project->quotation_id);
        }

        // Detial revision
        $list = [];
        foreach($repo->getRevisionList() as $x) {
            // Set value to "Terbaru" if quotation_id is empty. Empty mean LATEST
            $name = $x->updated_at->format('d-m-Y H:m:s');
            if(empty($x->quotation_id)) {
                $name = 'Terbaru';
            }

            $list[] = [
                'id' => $x->id,
                'name' => $name
            ];
        }

        // Grouping quotation_detail by quotation_detail_id(from HPP 1)
        $pricePlanId = [];
        $detail = [];
        foreach($quotationDetail as $x) {
            // Get quotation detail by group
            $pricePlanDetail = $repo->getPricePlanDetail($x->id);
            // Sum by group
            $groupTotal = $repo->getTotalQuotation($x->id);

            // Calculate sum of HPP 1
            $groupTotalQuotationDetail =
                    QuotationDetail::where('id', $x->id)
                        ->sum(DB::raw('qty * price'));

            // Store group
            $detail[] = [
                'group_name' => $x->name,
                'group_id' => $x->id,
                'list' => $pricePlanDetail,
                'total_group' => $groupTotal,
                'total_quotation' => $groupTotalQuotationDetail,
            ];
        }

        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $row->created);

        // Create data to be passed
        $data = [
            'id' => $row->id,
            'project_id' => $project->id,
            'ref_no' => $row->ref_no,
            'project' => $project,
            'is_quotation' => $row->is_quotation,
            'status_id' => $row->status_id,
            'quotation_id' => $row->quotation_id,
            'customer_id' => $row->customer_id,
            'city_id' => $row->city_id,
            'ref_no' => $row->ref_no,
            'name' => $row->name,
            'address' => $row->address,
            'created' => $date->format('Y-m-d'),
            'is_tax' => $row->is_tax,
            'pph' => $row->pph,
            'profit' => $row->profit,
            'risk_factor' => $row->risk_factor,
            'detail' => $repo->getAllDetail()
        ];
        // Store revisi
        $data['revisions'] = $list;
        $data['status'] = $row->status;

        $this->jsonResponse->setData($data);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request)
    {
        $temp = json_decode($request->data);
        $request = (object) $temp;
        // If status property not exists then create the property

        // Check if Quotation Not Set
        $project = Project::find($request->project_id);
        if(!$project->quotation){
            throw new NotFoundHttpException('Pilih penawaran terlebih dahulu');
        }

        // Prevent error
        if(!isset($request->parent_id))
            $request->parent_id = null;

        // Get Id
        $id = $request->id;
        $pricePlan = new Model;
        if(!empty($id)){
            $pricePlan = Model::findOrNew($request->id);
        }

        $statusId = Status::getStatusIdByName($request->status->name);

        // Update quotation
        $pricePlan->is_quotation = 0;
        $pricePlan->status_id = $statusId;
        $pricePlan->quotation_id = $pricePlan->id;
        $pricePlan->customer_id = $request->customer_id;
        $pricePlan->city_id = $request->city_id;
        $pricePlan->ref_no = $request->ref_no;
        $pricePlan->name = $request->name;
        $pricePlan->address = $request->address;
        $pricePlan->created = $request->created;
        $pricePlan->is_tax = $request->is_tax;
        $pricePlan->pph = $request->pph;
        $pricePlan->profit = $request->profit;
        $pricePlan->risk_factor = $request->risk_factor;

        $repo = new PricePlan($pricePlan);
        $repo->addProject(Project::find($request->project_id));

        // Delete job or items
        if($request->_delete_detail) {
            foreach($request->_delete_detail as $x)
                $repo->deleteDetail($x);
        }

        // Save detail quotation
        if(!empty($request->detail)){
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['item_id'], $x['name'], $x['unit'], $x['qty'],
                        $x['price'], $x['group_by'], $x['detail']);
            }
        }

        // Save
        $repo->save();

        // After save change status
        switch ($request->status) {
            case 'pending':
                $repo->setStatus(Status::STATUS_PENDING);
                break;
        }

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('HPP 2 telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status)
    {
        // If status is not approve or decline
        if(!in_array($status, ['draft', 'pending', 'approve', 'revision'])){
            throw new NotFoundHttpException('Penawaran/HPP1/HPP2 tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new PricePlan($row);
        $repo->setStatus($statusId);

        $message = null;
        switch($statusId) {
            case Status::STATUS_DRAFT:
                $message = 'Price-plan berhasil disimpan';
                break;
            case Status::STATUS_PENDING:
                $message = 'Price-plan berhasil ditunda';
                break;
            case Status::STATUS_APPROVED:
                $message = 'Price-plan berhasil diterima';
                break;
            case Status::STATUS_REVISION:
                $message = 'Price-plan berhasil ditolak';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new PricePlan($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Price Plan berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function storeJobs(Request $request){

        $detail = QuotationDetail::findOrNew($request->id);
        $detail->name = $request->name;
        $detail->unit = $request->unit;
        $detail->qty = $request->qty;
        $detail->price = $request->price;
        $detail->quotation_id = $request->quotation_id;
        $detail->quotation_detail_id = $request->quotation_detail_id;
        $detail->save();

        $this->jsonResponse->setData($detail->getModel()->id);
        $this->jsonResponse->setMessage('HPP 2 telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function download(Request $request){
        $row = $this->getModel($request->id);

        $repo = new PricePlan($row);

        $project = $repo->getProject();
        $pricePlandetail = $repo->getDetailPricePlan();

        $detail = [];
        $total = 0;
        foreach ($pricePlandetail as $x) {
            $details = $repo->getItemsOfDetail($x->id);

            foreach ($details as $y) {
                $temp = $y->qty * $y->price;
                $total += $temp;
            }

            $detail[] = [
                'name' => $x->name,
                'unit' => $x->unit,
                'qty' => $x->qty,
                'price' => $x->price,
                'is_additional' => $x->is_additional,
                'detail' => $details
            ];
        }

        $data = [
            'ref_no' => $row->ref_no,
            'name' => $row->name,
            'address' => $row->address,
            'city' => $row->city->name,
            'province' => $row->city->province->name,
            'detail' => $detail,
            'total' => $total
        ];

        // print_r($data);
        // die();

        $filename = $repo->download($data);

        $return = [
                'id' => $request->id,
                'url' => url('/tmp/' . $filename)
            ];

        $this->jsonResponse->setData($return);
        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('HPP2 tidak ditemukan');
        }

        return $row;
    }
}
