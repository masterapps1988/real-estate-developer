<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ais\Repository\Category;

use App\Category as Model;

use App\Ais\Ais;

class ApiCategoryController extends ApiController
{
    public function index(Request $request){
        $query = Model::orderBy('name','asc');

        if ($request->keyword) {
            $keyword = explode(",", $request->keyword);
            $query->whereIn('name', $keyword);
        }

        if($request->group_by){
            $query->where('group_by', $request->group_by);
        }

        $rows = $query->get();
        if(empty($rows->toArray())){
            throw new NotFoundHttpException('Invalid data');
        }

        $this->jsonResponse->setData($rows->toArray());

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        try{
            $category = Model::findOrNew($request->id);
            $category->name = $request->name;
            $category->label = $request->label;
            $category->group_by = $request->group_by;

            $repo = new Category($category);
            $repo->save();
            
            $this->jsonResponse->setMessage('Category telah berhasil tersimpan.');
        } catch(\Exception $e){
            $this->jsonResponse->setError(true);
            $this->jsonResponse->setMessage($e->getMessage());
        }
        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = Model::where(['id'=>$id, 'group_by'=>'unit']);
        if($row->count() <= 0){
            $this->jsonResponse->setError(true);
            $this->jsonResponse->setMessage('Category bukan Unit');
        } else {
            $row->delete();
            $this->jsonResponse->setMessage('Category berhasil dihapus');
        }
        // print_r($row);
        // $repo = new Category($row);
        // $repo->delete();
        // $this->jsonResponse->setMessage('Category berhasil dihapus');

        // return $row;
        return $this->jsonResponse->getResponse();

    }

    private function getModel($id){
        $row = Model::where(['id'=>$id, 'group_by'=>'unit']);
        if(empty($row)){
            throw new NotFoundHttpException('Category tidak ditemukan');
        }

        return $row;
    }
}
