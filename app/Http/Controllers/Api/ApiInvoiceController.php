<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use DB;

use App\TheBadusLibs\Helper\DateFormat;
use App\Ais\Repository\Invoice;
use App\Ais\Repository\Finder\InvoiceFinder;

use App\Invoice as InvoiceModel;
use App\Category as Status;
use App\Project;

class ApiInvoiceController extends ApiController
{
    public function index(Request $request)
    {
        $finder = new InvoiceFinder();

        if (isset($request->keyword)) {
            $finder->setKeyword($request->keyword);
        }

        if (isset($request->project_id)) {
            $finder->setProject($request->project_id);
        }

        if (isset($request->is_sell)) {
            $finder->setSell($request->is_sell);
        }

        // Order By
        if (isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        if (isset($request->status)) {
            $status = explode(",", $request->status);
            if (count($status) > 1) {
                $grupStatus = [];
                foreach ($status as $i => $sts) {
                    $grupStatus[] = Status::getStatusIdByName($sts);
                }
                $finder->setStatusIdGroup($grupStatus);
            } else {
                $statusId = Status::getStatusIdByName($request->status);
                $finder->setStatusId($statusId);
            }
        }

        $paginator = $finder->get();

        $list = [];
        foreach ($paginator as $x) {
            $projectName = null;
            if ($x->project_name) {
                $projectName = $x->project_name;
            }
            
            $data = InvoiceModel::find($x->id);
            $repo = new Invoice($data);
            $total = $repo->getTotal($x->id);
            $paidAmount = $repo->getTotalPaidAmount();
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $x->created);
            $status = Status::find($x->status_id);

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'name' => $x->name,
                'address' => $x->address,
                'hp' => $x->hp,
                'city' => $x->city,
                'created' => DateFormat::shortDate($date),
                'total' => $total,
                'paid'  => $paidAmount,
                'status' => [
                    'name' => $status->name,
                    'label' => $status->label
                ],
                'project' => [
                    'id' => $x->project_id,
                    'name'=>$projectName
                ],
                
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id)
    {
        $row = $this->getModel($id);
        $user = null;
        if ($row->user) {
            $user = $row->user->id;
        }

        $repo = new Invoice($row);

        $project = Project::find($row->project_id);

        // Invoice
        $list = [];
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $row->created);
        $list = [
            'id'=> $row->id,
            'project_id'=> $row->project_id,
            'project_name' => $project->name,
            'user_id' => $user,
            'ref_no'=> $row->ref_no,
            'name'=> $row->name,
            'address'=> $row->address,
            'hp'=> $row->hp,
            'city'=> $row->city,
            'created' => $date->format('Y-m-d')
            // 'created' => DateFormat::shortDate($date),
        ];
        // Invoice detail
        $list['detail'] = $row->detail;
        $list['status'] = Status::find($row->status_id);
        $list['customer'] = [
            // 'id' => $row->user->id,
            // 'name' => $row->user->name,
            // 'address' => $row->user->address,
            // 'phone' =>  $row->user->phone
        ];

        $this->jsonResponse->setData($list);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request)
    {
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $invoice = new InvoiceModel;
        
        // Get Id
        $id = $request->id;
        
        if (!empty($id)) {
            $invoice = InvoiceModel::findOrNew($id);
        }
        $statusId = Status::getStatusIdByName($request->status->name);
        $project = Project::find($request->project_id);
        $repo = new Invoice($invoice);

        // Update Invoice
        $invoice->project_id = $request->project_id;
        $invoice->status_id = $statusId;
        $invoice->ref_no = $request->ref_no;
        $invoice->name = $project->name;
        $invoice->address = $project->address;
        // $invoice->hp = $project->hp;
        $invoice->city = $project->city_id;
        $invoice->created = $request->created;

        // Delete detail
        if ($request->_delete_detail) {
            foreach ($request->_delete_detail as $x) {
                $repo->deleteDetail($x);
            }
        }

        // Save detail quotation
        if (!empty($request->detail)) {
            foreach ($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['notes'], $x['amount']);
            }
        }

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Invoice telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id)
    {
        $row = $this->getModel($id);
        $repo = new Invoice($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Invoice berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status)
    {
        // If status is not approve or decline
        if (!in_array($status, ['draft', 'pending', 'approve', 'decline'])) {
            throw new NotFoundHttpException('Invoice tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new Invoice($row);
        $repo->setStatus($statusId);

        switch ($statusId) {
            case Status::STATUS_APPROVED:
                $message = 'Invoice berhasil diterima';
                break;

            case Status::STATUS_DRAFT:
                $message = 'Invoice berstatus draft';
                break;

            case Status::STATUS_PENDING:
                $message = 'Invoice sedang diajukan';
                break;

            case Status::STATUS_APPROVED:
                $message = 'Invoice berhasil diterima';
                break;

            case Status::STATUS_DECLINED:
                $message = 'Invoice berhasil ditolak';
                break;

            default:
                $message = 'Invoice berhasil tersimpan';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }

    public function download(Request $request)
    {
        $row = $this->getModel($request->id);
        
        $repo = new Invoice($row);
        $filename = $repo->download();

        $data = [
                'id' => $request->id,
                'url' => url('/tmp/' . $filename)
            ];

        $this->jsonResponse->setData($data);

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id)
    {
        $row = InvoiceModel::find($id);
        if (empty($row)) {
            throw new NotFoundHttpException('Invoice tidak ditemukan');
        }

        return $row;
    }
}
