<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ais\Repository\RolePermission;
use App\Ais\Repository\Role;
use App\Ais\Repository\Category;
use App\RolePermission as Model;
use App\Category as CategoryModel;
use App\Ais\Ais;

class ApiRolePermissionController extends ApiController
{
    public function index(Request $request){
        $query = Model::orderBy('name','asc');

        if ($request->name) {
            $name = explode(",", $request->name);
            $query->whereIn('name', $name);
        }

        if($request->group_by){
            $query->where('group_by', $request->group_by);
        }

        $rows = $query->get();

        $this->jsonResponse->setData($rows->toArray());

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getRole($id);
        $data = $row->toArray();

        $this->jsonResponse->setData($data);
    
        return $this->jsonResponse->getResponse();
    }

    public function getPermission(Request $request){
        $all = isset($request->all);
        $roleChecklist = CategoryModel::find($request->role_id);

        if (empty($roleChecklist)) {
            throw new NotFoundHttpException('Jabatan tidak ada');
        }
        $repo = new Category($roleChecklist);
        $role = [];
        $role['details'] = $repo->getChecklistRoleSummary();
        $role['role'] = $roleChecklist->label;

        $this->jsonResponse->setData($role);

        return $this->jsonResponse->getResponse();
    }

    public function setPermission(Request $request){
        $role = new Model;

        $role->role_id = $request->role_id;
        
        $repo = new RolePermission($role);
        
        if(!empty($request->detail)) {
            foreach ($request->detail as $x) {
                switch($x['is_checked']) {
                    case 'true':
                        $repo->check($x['id']);
                    case 'false':
                        $repo->uncheck($x['id']);
                }
            }
        }    

        $repo->save();
        
        $this->jsonResponse->setData($request->user_id);
        $this->jsonResponse->setMessage('Berhasil di ubah.');

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $role = CategoryModel::findOrNew($request->id);
        $role->name = $request->name;
        $role->label = $request->label;
        $role->notes = $request->notes;

        $repo = new Role($role);
        $repo->save();

        $this->jsonResponse->setMessage('Jabatan telah berhasil tersimpan.');
        $this->jsonResponse->setData($role->name);

        return $this->jsonResponse->getResponse();
    }

    private function getRole($id){
        $row = CategoryModel::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Jabatan tidak ditemukan');
        }

        return $row;
    }

    
}
