<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use DB;

use App\Ais\Repository\InvoiceMandorVendor;

use App\Invoice as Model;
use App\Category as Status;
use App\StokOpname;
use App\StokOpnameDetail;
use App\Project;
use App\Item;
use App\User;

class ApiInvoiceMandorVendorController extends ApiController
{

    public function store(Request $request)
    {
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $invoice = new Model;

        // Get Id
        $id = $request->id;

        if (!empty($id)) {
            $invoice = Model::findOrNew($id);
        }

        $statusId = Status::getStatusIdByName($request->status->name);
        $project = Project::find($request->project_id);
        $user = User::find($request->user_id);

        // Update Invoice
        $invoice->project_id = $request->project_id;
        $invoice->status_id = $statusId;
        $invoice->ref_no = $request->ref_no;
        $invoice->name = $request->name;
        $invoice->address = $request->address;
        $invoice->hp = $request->hp;
        $invoice->city = $request->city;
        $invoice->created = $request->created;
        $invoice->user_id = $request->user_id;

        $repo = new InvoiceMandorVendor($invoice);

        // Delete detail
        if ($request->_delete_detail) {
            foreach ($request->_delete_detail as $x) {
                $repo->deleteDetail($x);
            }
        }

        // Save detail quotation
        if (!empty($request->detail)) {
            foreach ($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['notes'], $x['amount']);
            }
        }

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Invoice telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function getOpname($opname_id){
        $opname = StokOpname::find($opname_id);
        $data = InvoiceMandorVendor::opnameToArray($opname);

        $this->jsonResponse->setData($data);

        return $this->jsonResponse->getResponse();
    }
}
