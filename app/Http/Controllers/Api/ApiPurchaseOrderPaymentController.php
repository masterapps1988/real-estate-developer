<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ais\Repository\Finder\PurchaseOrderPaymentFinder;

use App\TheBadusLibs\Helper\DateFormat;
use App\Ais\Repository\PurchaseOrderPayment;
use App\PurchaseOrderPayment as PurchaseOrderModel;

use App\Ais\Ais;

class ApiPurchaseOrderPaymentController extends ApiController {
    public function index(Request $request){
        $finder = new PurchaseOrderPaymentFinder();

        if(isset($request->purchase_order_id)) {
            $finder->setPurchaseOrder($request->purchase_order_id);
        } 
            
        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        } else {    
            $finder->orderBy('created', 'asc');
        }

        if(isset($request->page)) {
            $finder->setPage($request->page);

        }

        if(isset($request->keyword))
            $finder->setKeyword($request->keyword);

        $paginator = $finder->get();
        
        $list = [];
        foreach($paginator as $x) {
            
            $list[] = [
                'id' => $x->id,
                'purchase_order_id' => $x->purchase_order_id,
                'bank' => $x->bank,
                'amount' => $x->amount,
                'account_number' => $x->account_number,
                'notes' => $x->notes,
                'created' => $x->created
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getModel($id);

        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $row->created);

        $list = [
            'id' => $row->id,
            'purchase_order_id' => $row->purchase_order_id,
            'bank_category_id' => $row->bank_category_id,
            'amount' => $row->amount,
            'account_number' => $row->account_number,
            'notes' => $row->notes,
            'created' => DateFormat::shortDate($date)
        ];

        $this->jsonResponse->setData($list);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $payment = new PurchaseOrderModel;

        if(!empty($request->id)){
            $payment = PurchaseOrderModel::findOrNew($request->id);
        }

        $payment->purchase_order_id = $request->purchase_order_id;
        $payment->bank_category_id = $request->bank_category_id;
        $payment->amount = $request->amount;
        $payment->account_number = $request->account_number;
        $payment->notes = $request->notes;
        $payment->created = new \DateTime($request->created);
        
        
        $repo = new PurchaseOrderPayment($payment);

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Payment telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new PurchaseOrderPayment($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Pembayaran berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = PurchaseOrderModel::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('PO tidak ditemukan');
        }

        return $row;
    }
}