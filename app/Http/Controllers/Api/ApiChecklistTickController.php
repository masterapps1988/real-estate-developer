<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\TheBadusLibs\Helper\DateFormat;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ais\Repository\Finder\ProjectChecklistFinder;
use App\Ais\Repository\Finder\ProjectChecklistTickFinder;
use App\Ais\Repository\Project;
use App\Ais\Repository\ProjectChecklistDetail;

use App\ProjectChecklist as ProjectChecklistModel;
use App\ProjectChecklistDetail as ProjectChecklistDetailModel;
use App\Project as ProjectModel;
use App\Ais\Ais;

class ApiChecklistTickController extends ApiController
{
    public function index(Request $request){
        $all = isset($request->all);
        $projectChecklist = ProjectModel::find($request->project_id);

        if (empty($projectChecklist)) {
            throw new NotFoundHttpException('Project Checklist tidak ditemukan');
        }

        $repo = new Project($projectChecklist);

        $user = [];
        $user['total'] = $repo->getTotalChecklist();
        $user['tick'] = $repo->getChecklistSummary();

        $this->jsonResponse->setData($user);
        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request)
    {
        $checklistTick = new ProjectChecklistDetailModel;

        $checklistTick->project_id = $request->project_id;
        $checklistTick->user_id = $request->user_id;
        
        $repo = new ProjectChecklistDetail($checklistTick);

        if(!empty($request->detail)) {
            foreach ($request->detail as $x) {
                switch($x['is_checked']) {
                    case 'true':
                        $repo->check($x['id']);
                    case 'false':
                        $repo->unchecked($x['id']);
                }
            }
        }        

        // Save
        $repo->save();

        $this->jsonResponse->setData($request->user_id);
        $this->jsonResponse->setMessage('Checklist berhasil diubah.');

        return $this->jsonResponse->getResponse();
    }

    public function show(Request $request)
    {
        $all = isset($request->all);
        $finder = new ProjectChecklistFinder();

        
        
        if(isset($request->keyword)) 
            $finder->setKeyword($request->keyword);
        
        if(isset($request->project_id))
            $finder->setProject($request->project_id);
        
        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $checklistDetail = ProjectChecklistDetailModel::where('project_checklist_id','=',$x->id)->count();
            $list[] = [
                'id' => $x->id,
                'name' => $x->name,
                'position' => $x->position,
                'is_checked' => $checklistDetail ? true : false
            ];
        }

        $this->jsonResponse->setData($all ? \App\Project::all() : $list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));
        return $this->jsonResponse->getResponse();
    }

}