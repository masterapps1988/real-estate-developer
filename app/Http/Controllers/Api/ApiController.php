<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ais\Repository\AccessControl;
use App\TheBadusLibs\JsonResponse;
use App\User;
use Auth;

class ApiController extends Controller
{
    protected $jsonResponse;

    public function __construct()
    {
        $this->jsonResponse = new JsonResponse();
    }

    public function getAccessControl()
    {
        $user = Auth::user();
        if(!empty($user))
            return new AccessControl($user);

        return null;
    }
}
