<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

use App\Ais\Repository\StockFlow;
use App\Ais\Repository\Finder\StockFlowFinder;
use App\TheBadusLibs\Helper\DateFormat;
use App\Ais\Ais;

use App\StockFlow as Model;

class ApiStockFlowController extends ApiController
{
    public function index(Request $request){

        $finder = new StockFlowFinder();

        // Order By
        if(isset($request->order_by) && !empty($request->order_by))
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);

        if(isset($request->page))
            $finder->setPage($request->page);

        if(isset($request->date_from))
            $finder->setDateFrom(new \DateTime($request->date_from));

        if(isset($request->date_to))
            $finder->setDateTo(new \DateTime($request->date_to));

        if(isset($request->item_id))
            $finder->setItem($request->item_id);

        if(isset($request->category))
            $finder->setCategory($request->category);

        if(isset($request->project_id))
            $finder->setProject($request->project_id);

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {

            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $x->created);

            $list[] = [
                'id' => $x->id,
                'created' => DateFormat::shortDate($date),
                'category' => $x->category,
                'category_name' => StockFlow::getCategoryName($x->category),
                'qty' => $x->qty,
                'unit' => $x->unit,
                'name' => $x->item_name,
                'item_category_name' => $x->item_category_name,
                'item_id' => $x->item_id
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function getCategory(){
        $StockFlow = new Model;

        $repo = new StockFlow($StockFlow);
        $data = $repo->getCategory();

        $list = [];
        foreach ($data as $x) {
            $list[] = [
                'name' => $x->category
            ];
        }

        $this->jsonResponse->setData($list);

        return $this->jsonResponse->getResponse();
    }

}