<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

use App\Ais\Repository\Finder\UserFinder;
use App\Ais\Repository\User;
use App\Ais\Repository\UserSupplier;
use App\User as UserModel;
use App\Category;

class ApiUserController extends ApiController
{
    public function index(Request $request)
    {
        $finder = new UserFinder();
        $finder->exceptUserId(Auth::user()->id);
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        } else {
            $finder->orderBy('role', 'asc');
        }

        if($request->page)
            $finder->setPage($request->page);

        // Filter except role. Example: supplier,mandor
        if($request->except_roles) {
            // Split to array
            $list = explode(',', $request->except_roles);
            // Trim space
            $list = array_map('trim', $list);
            $finder->exceptRoles($list);
        }

         // Filter role. Example: supplier,mandor
         if($request->roles) {
            // Split to array
            $list = explode(',', $request->roles);
            // Trim space
            $list = array_map('trim', $list);
            $finder->Roles($list);
        }

        // Search by keyword
        if($request->keyword)
            $finder->setKeyword($request->keyword);

        // dd()
        $paginator = $finder->get();

        $data = [];
        foreach($paginator as $x){
            $temp = (array) $x;
            $temp['role'] = Category::find($x->role_id)->label;

            $data[] = $temp;
        }
        $this->jsonResponse->setData($data);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getUser($id);
        $data = $row->toArray();
        $data['role'] = $row->category->name;
        $this->jsonResponse->setData($data);
    
        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $role_id = $this->checkRole($request->role_id, $request->role);

        $admin = UserModel::findOrNew($request->id);
        $admin->city_id = $request->city_id;
        $admin->role_id = $role_id;
        $admin->username = $request->username;
        $admin->email = $request->email;
        $admin->name = $request->name;
        $admin->address = $request->address;
        $admin->phone = $request->phone;

        switch ($request->role) {
            case 'supplier':
                $repo = new UserSupplier($admin);
                break;

            default:
                $repo = new User($admin);
                break;
        }

        if(!empty($request->password) && !empty($request->confirm_password)){
            $repo->setPassword($request->password, $request->confirm_password);
        }

        $repo->save();

        $this->jsonResponse->setMessage(ucfirst($request->role) . ' telah berhasil tersimpan.');
        $this->jsonResponse->setData($admin->id);

        return $this->jsonResponse->getResponse();
    }

    public function profile()
    {
        $data = Auth::user()->toArray();
        $data['permission'] = $this->getAccessControl()->getPermissions()->pluck('name');

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getUser($id);
        $row->delete();
        $this->jsonResponse->setMessage('User berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    private function getUser($id){
        $row = UserModel::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('User tidak ditemukan');
        }

        return $row;
    }

    public function getByRole(Request $request){
        if ($request->role == "supplier") {
            $code = UserModel::ROLE_SUPPLIER;
        }
        if ($request->role == "mandor") {
            $code = UserModel::ROLE_MANDOR;
        }
        if ($request->role == "vendor") {
            $code = UserModel::ROLE_VENDOR;
        }
        if ($request->role == "admin") {
            $code = UserModel::ROLE_DIRECTOR;
        }

        $row = UserModel::where('role_id', $code)->orderBy('name', 'asc')->get();;
        if(empty($row)){
            throw new NotFoundHttpException($role . ' tidak ditemukan');
        }

        $this->jsonResponse->setData($row);
    
        return $this->jsonResponse->getResponse();
    }

    private function getModel(UserModel $user)
    {
        $user = new UserModel;
        $user->role_id = UserModel::ROLE_DIRECTOR;
        $user->username = 'admin123';
        $user->password = Hash::make('admin');
        $user->name = 'admin';
        $user->email = 'admin@bsi.co.id';

        return $user;
    }

    private function checkRole($role_id, $role)
    {
        if (!$role_id) {
            if ($role == "supplier") {
                $role_id = UserModel::ROLE_SUPPLIER;
            }
            if ($role == "mandor") {
                $role_id = UserModel::ROLE_MANDOR;
            }
            if ($role == "vendor") {
                $role_id = UserModel::ROLE_VENDOR;
            }
        }
        return $role_id;
    }
}
