<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;

use App\Ais\Ais;

use App\Ais\Repository\Project;
use App\Ais\Repository\Quotation;
use App\Ais\Repository\PricePlan;
use App\Ais\Repository\PriceSummary;
use App\TheBadusLibs\Helper\DateFormat;

use App\Project as ProjectModel;
use App\Category;
use App\Item as ItemModel;
use App\Quotation as QuotationModel;
use App\QuotationDetail as QuotationDetail;
use App\PurchaseOrder as PurchaseOrderModel;
use App\PurchaseOrderDetail as PurchaseOrderDetail;

use App\Ais\Repository\Finder\ProjectFinder;

use App\City as CityModel;
use App\Customer as CustomerModel;
use App\User as UserModel;

class ApiProjectController extends ApiController
{
    public function index(Request $request){
        $all = isset($request->all);
        $finder = new ProjectFinder();

        if($request->rangeStart)
            $finder->setDateStart(new \DateTime($request->rangeStart));

        if($request->rangeFinish)
            $finder->setDateFinish(new \DateTime($request->rangeFinish));

        if($request->page)
            $finder->setPage($request->page);
            
        if(isset($request->keyword)) {
            $finder->setKeyword($request->keyword);
        }

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        } else {
            $finder->orderBy('created', 'desc');
        }

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'project_start' => DateFormat::shortDate($x->project_start),
                'project_finish' => DateFormat::shortDate($x->project_finish),
                'customer_name' => $x->customer_name,
                'name' => $x->name,
                
            ];
        }

        // $this->jsonResponse->setData($paginator->items());
        $this->jsonResponse->setData($all ? \App\Project::all() : $list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show(Request $request){

        if (isset($request->formatted)) {
            # code...
        }

        $data = null;
        $quotation = [];
        $pricePlan = [];
        $project = $this->getProject($request->id);
        $status = Category::find($project->status_id);
        
        // Get Quotation
        if(!empty($project->quotation_id)) {
            $rowQuotation = QuotationModel::where('id', $project->quotation_id)->first();
            $repoQuotation = new Quotation($rowQuotation);

            $quotation = [
                'id' => $rowQuotation->id,
                'ref_no' => $rowQuotation->ref_no,
                'created' => $rowQuotation->created->format(Ais::getDateFormat()),
                'revision' => $repoQuotation->countRevision(),
                'total' => $repoQuotation->getTotal()
            ];
        }

        // Get price plan
        if(!empty($project->price_plan_id)) {
            $rowPricePlan = QuotationModel::where('id', $project->price_plan_id)->first();
            $repoPricePlan = new PricePlan($rowPricePlan);

            $pricePlan = [
                'id' => $rowPricePlan->id,
                'ref_no' => $rowPricePlan->ref_no,
                'created' => $rowPricePlan->created->format(Ais::getDateFormat()),
                'revision' => $repoPricePlan->countRevision(),
                'total' => $repoPricePlan->getAllTotal($rowQuotation->id)
            ];
        }

        // Get Summary
        $repoSummary = new PriceSummary($project);
        $quotationDetail = PricePlan::getQuotationDetail($project->quotation_id);

        // Grouping quotation_detail by quotation_detail_id(from HPP 1)
        $total = 0;
        $pricePlanId = [];
        $detail = [];
        foreach($quotationDetail as $x) {
            // Get quotation detail by group
            $pricePlanDetail = $repoSummary->getDetailByQuotationDetailId($x->id);
            // $priceSummary->where('quotation_detail_id', $x->id);
            // $repo->getPricePlanDetail($x->id);

            // Sum by group
            $groupTotal = $repoSummary->getTotalByQuotationDetailId($x->id);
            // $priceSummary->where('quotation_detail_id', $x->id); // $repo->getTotalQuotation($x->id);

            // HPP 1, total quotation detail
            $totalQuotationDetail = QuotationDetail::where('id', $x->id)
                        ->sum(DB::raw('qty * price'));

            // Store group
            $detail[] = [
                'group_name' => $x->name,
                'group_id' => $x->id,
                'list' => $pricePlanDetail,
                'total_group' => $groupTotal,
                'total_quotation' => $totalQuotationDetail
            ];

            $total += $groupTotal;
        }

        $summary = [
            'total' => $total,
            'detail' => $detail
        ];

        $users = [
            'id' => $project->user_id,
            'city' => $project->user_city,
            'name' => $project->user_name,
            'address' => $project->user_address,
            'phone' => $project->user_phone,
            'npwp' => $project->user_npwp
        ];

        $data = [
            'id' => $project->id,
            'customer_id' => $project->customer_id,
            'city_id' => $project->city_id,
            'quotation_id' => $project->quotation_id,
            'price_plan_id' => $project->price_plan_id,
            'city_name' => $project->city_name,
            'province_name' => $project->province_name,
            'ref_no' => $project->ref_no,
            'name' => $project->name,
            'address' => $project->address,
            'start' => $project->start->format('Y-m-d'),
            'finish' => $project->finish->format('Y-m-d'),
            'created_at' => $project->created_at->format(Ais::getDateFormat()),
            'updated_at' => $project->updated_at->format(Ais::getDateFormat()),
            'users' => $users,
            'quotation' => $quotation,
            'price_plan' => $pricePlan,
            'summary' => $summary,
            'status' => $status
        ];

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    public function stock(Request $request){
        $data = null;
        $type = null;
        $userId = null;

        if ($request->type) {
            $type = $request->type;
        }

        if ($request->user_id) {
            $userId = $request->user_id;
        }
        
        $project = $this->getProject($request->id);

        $repo = new Project($project);
        $data = $repo->getStock($type, $userId);

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    public function progress(Request $request){
        $project = ProjectModel::find($request->id);

        $userId = null;

        if ($request->user_id) {
            $userId = $request->user_id;
        }

        $repo = new Project($project);
        $data = $repo->getProgress($userId);

        $this->jsonResponse->setData($data);
        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $project = new ProjectModel;
        if(!empty($request->id)){
            $project = ProjectModel::findOrNew($request->id);
        }

        $project->customer_id = $request->customer_id;
        $project->city_id = $request->city_id;
        $project->name = $request->name;
        $project->ref_no = $request->ref_no;
        $project->address = $request->address;
        $project->start = new \DateTime($request->start_date);
        $project->finish = new \DateTime($request->finish_date);
        $project->status_id = $request->status_id;

        $repo = new Project($project);
        $repo->save();

        $this->jsonResponse->setData($project->id);
        $this->jsonResponse->setMessage('Project telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function changeWorker(Request $request){
        // Get user list
        $userIdList = [];
        if(isset($request->users) && !empty($request->users)) {
            $userIdList = $request->users;
        }

        // Get _delete id list
        $_delete = [];
        if(isset($request->_delete) && !empty($request->_delete)) {
            $_delete = $request->_delete;
        }

        // Because getProject() is buggy then I use this for debug purpose
        // Need to refactor later
        $row = ProjectModel::find($request->project_id);

        // Use item repository
        $repo = new Project($row);

        foreach($_delete as $x)
            $repo->removeWorker($x);

        foreach($userIdList as $x)
            $repo->addWorker($x);

        $repo->save();

        $this->jsonResponse->setMessage('Pekerja telah berhasil diubah.');

        return $this->jsonResponse->getResponse();
    }

    public function changeStatus(Request $request){
        try{

            if(!in_array($request->status, ['open', 'on-progress', 'closed'])){
                throw new NotFoundHttpException('Penawaran/HPP1/HPP2 tidak ditemukan');
            }

            // Get status id
            $status_id = Category::getStatusIdByName($request->status);

            $row = ProjectModel::find($request->id);

            $repo = new Project($row);
            $repo->setStatus($status_id);

            $this->jsonResponse->setMessage('Status telah berhasil dirubah.');
        } catch(\Exception $e){
            $this->jsonResponse->setError(true);
            $this->jsonResponse->setMessage($e->getMessage());
        }
        return $this->jsonResponse->getResponse();
    }

    public function setHpp1(Request $request){
        try{

            $project = ProjectModel::find($request->id);
            $project->quotation_id = $request->quotation_id;
            $project->save();

            $this->jsonResponse->setMessage('HPP telah berhasil tersimpan.');
        } catch(\Exception $e){
            $this->jsonResponse->setError(true);
            $this->jsonResponse->setMessage($e->getMessage());
        }
        return $this->jsonResponse->getResponse();
    }

    public function unsetHpp1(Request $request){
        try{
            $project = ProjectModel::find($request->id);

            if ($project->price_plan_id) {
                throw new NotFoundHttpException('Tidak dapat menghapus HPP 1 karena telah ada HPP 2');
            }

            $project->quotation_id = null;
            $project->save();

            $this->jsonResponse->setMessage('HPP telah berhasil dihapus.');
        } catch(\Exception $e){
            $this->jsonResponse->setError(true);
            $this->jsonResponse->setMessage($e->getMessage());
        }
        return $this->jsonResponse->getResponse();
    }

    public function unsetHpp2(Request $request){
        try{

            $project = ProjectModel::find($request->id);
            $project->quotation_id = null;
            $project->save();

            $this->jsonResponse->setMessage('HPP telah berhasil dihapus.');
        } catch(\Exception $e){
            $this->jsonResponse->setError(true);
            $this->jsonResponse->setMessage($e->getMessage());
        }
        return $this->jsonResponse->getResponse();
    }

    // HPP 3
    public function priceSummary($project_id){
        // Get project
        $project = $this->getProject($project_id);
        $projectRepo = new Project($project);
        $priceSummary = new PriceSummary($project);
        // var_dump($priceSummary); die;

        // Get quotation model
        // $row = $this->getModel($request->id);
        // $repo = new PricePlan($row);

        // Load quotation detail
        // Get all detail by quotation_id, OR
        // Get by quotation_detail_id(parent of)
        $quotationDetail = PricePlan::getQuotationDetail($project->quotation_id);

        // Grouping quotation_detail by quotation_detail_id(from HPP 1)
        $pricePlanId = [];
        $detail = [];
        foreach($quotationDetail as $x) {
            // Get quotation detail by group
            $pricePlanDetail = $priceSummary->getDetailByQuotationDetailId($x->id);
            // $priceSummary->where('quotation_detail_id', $x->id);
            // $repo->getPricePlanDetail($x->id);

            // Sum by group
            $groupTotal = $priceSummary->getTotalByQuotationDetailId($x->id);
            // $priceSummary->where('quotation_detail_id', $x->id); // $repo->getTotalQuotation($x->id);

            // HPP 1, total quotation detail
            $totalQuotationDetail = QuotationDetail::where('id', $x->id)
                        ->sum(DB::raw('qty * price'));

            // Store group
            $detail[] = [
                'group_name' => $x->name,
                'group_id' => $x->id,
                'list' => $pricePlanDetail,
                'total_group' => $groupTotal,
                'total_quotation' => $totalQuotationDetail,
            ];
        }

        // Create data to be passed
        $data = [
            'id' => null, // $row->id,
            'project_id' => $project->id,
            'ref_no' => null, // $row->ref_no,
            'detail' => $detail,
            'project' => $project
        ];

        $this->jsonResponse->setData($data);

        return $this->jsonResponse->getResponse();
    }

    // Get available item on price plan that hasn't been ordered
    public function itemListAvailable($id){
        // Project
        $project = $this->getProject($id);

        $repo = new Project($project);
        $list = $repo->getAvailableItems();
        
        $this->jsonResponse->setData($list);
        return $this->jsonResponse->getResponse();

    }

    public function destroy($id){
        $row = $this->getProject($id);
        $repo = new Project($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Project berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function listWorker($id){
        $project = ProjectModel::find($id);
        
        if (empty($project)) {
            throw new NotFoundHttpException('Project tidak ditemukan');
        }

        $repo = new Project($project);
        $users = $repo->getWorkers();

        $user = [];
        foreach($users as $x) {
            $user[] = [
                'id' => $x->id,
                'name' => $x->name,
                'role_id' => $x->role_id,
                'role_name' => $x->category->name
            ];
        }

        $data = $user;

        if ($project->price_plan_id) {
            $filter = DB::select("select DISTINCT `user`.`id` FROM `quotation_detail` INNER JOIN `item` ON `quotation_detail`.`item_id` = `item`.`id` INNER JOIN `user` ON `item`.`user_id` = `user`.`id` INNER JOIN `category` ON `category`.`name` IN ('vendor', 'mandor') WHERE `item`.`item_category_id` IN (SELECT `id` FROM `category` WHERE `category`.`name` IN ('vendor', 'mandor')) AND `quotation_detail`.`quotation_id` = ".$project->price_plan_id." AND `quotation_detail`.`quotation_detail_id` IS NOT NULL");    

            $dataId = [];
            foreach ($filter as $f) {
                $dataId[] = [
                    'id' => $f->id
                ];
            }
            $dataVendorMandor = UserModel::find($dataId);
            
            $vendorMandor = [];
            foreach ($dataVendorMandor as $y) {
                $vendorMandor[] = [
                    'id' => $y->id,
                    'name' => $y->name,
                    'role_id' => $y->role_id,
                    'role_name' => $y->category->name
                ];
            }    
            $data = array_merge($user, $vendorMandor);
        }

        $this->jsonResponse->setData($data);

        return $this->jsonResponse->getResponse();
    }

    private function getProject($id){
        $row = ProjectModel::where('project.id', $id)
                ->leftjoin('customer', 'project.customer_id', '=', 'customer.id')
                ->leftjoin('category', 'project.status_id', '=', 'category.id')
                ->leftjoin('city', 'project.city_id', '=', 'city.id')
                ->leftjoin('province', 'city.province_id', '=', 'province.id')
                ->select('project.*', 'customer.id AS user_id', 'customer.city_id AS user_city', 'customer.name AS user_name', 'customer.address AS user_address', 'customer.phone AS user_phone', 'customer.npwp AS user_npwp', 'city.name AS city_name', 'province.name AS province_name', 'category.label AS status')
                ->first();
        if(empty($row)){
            throw new NotFoundHttpException('Project tidak ditemukan');
        }

        return $row;
    }

    /* Create price plan from quotation
     * @params int project id
     */
    public function createPricePlan($id)
    {
        $row = ProjectModel::find($id);

        if(empty($row)){
            throw new NotFoundHttpException('Project tidak ditemukan');
        }

        $project = new Project($row);
        $pricePlan = $project->createPricePlan();
        $this->jsonResponse->setData($pricePlan->id);
        $this->jsonResponse->setMessage('Price-plan berhasil dibuat');

        return $this->jsonResponse->getResponse();
    }

    private function getModel(ProjectModel $row)
    {
        $row->customer_id = null;
        $row->city_id = CityModel::first()->id;
        $row->ref_no = random_int(99, 100000);
        $row->name = 'Project One';
        $row->address = 1500.25;
        $row->start = new \DateTime();

        $finish = clone $row->start;
        $finish->add(new \DateInterval('P1M'));
        $row->finish = $finish;

        return $row;
    }
}
