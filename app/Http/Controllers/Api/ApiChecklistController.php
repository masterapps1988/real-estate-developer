<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\TheBadusLibs\Helper\DateFormat;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ais\Repository\Finder\ProjectChecklistFinder;
use App\Ais\Repository\Project;


use App\Ais\Repository\ProjectChecklist;
use App\ProjectChecklist as ProjectChecklistModel;
use App\Project as ProjectModel;
use App\Ais\Ais;

class ApiChecklistController extends ApiController
{
    public function index(Request $request){
        $all = isset($request->all);
        $finder = new ProjectChecklistFinder();

        if($request->page)
            $finder->setPage($request->page);
            
        if(isset($request->keyword)) 
            $finder->setKeyword($request->keyword);
        

        if(isset($request->project_id))
            $finder->setProject($request->project_id);
        
        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        $paginator = $finder->get();

        $listProject = [];
        foreach($paginator as $x) {
            $listProject[] = [
                'id' => $x->id,
                'project_id' => $x->project_id,
                'name' => $x->name,
                'position' => $x->position
            ];
        }

        $this->jsonResponse->setData($listProject);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request)
    {        
        $checklist = new ProjectChecklistModel;

        // Get project model
        if(!empty($request->project_id)){
            $project = $this->getModel($request->project_id);
        }
        $message = null;
        $repo = new ProjectChecklist($project);

        if ($request->_delete_detail) {
            foreach ($request->_delete_detail as $x) {
                $repo->deleteDetail($x);
            }
        }

        if(!empty($request->detail)) {
            foreach ($request->detail as $x) {
                $repo->addDetail($x['id'], $x['name'], $x['position']);
            }
        }        

        // Save
        $repo->save();

        $this->jsonResponse->setData($request->project_id);
        $this->jsonResponse->setMessage('Checklist telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function show($projectId)
    {
        $project = $this->getModel($projectId);
        $repo = new ProjectChecklist($project);

        $this->jsonResponse->setData($repo->toArray());
        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = ProjectChecklistModel::find($id);
        
        $repo = new ProjectChecklist($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Checklist berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    private function getModel($projectId)
    {
        $row = ProjectModel::find($projectId);
        
        if(empty($row)){
            throw new NotFoundHttpException('Project tidak ditemukan');
        }

        return $row;
    }

}

