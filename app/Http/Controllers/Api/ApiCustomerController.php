<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

use App\Ais\Repository\Customer;
use App\Ais\Repository\Finder\CustomerFinder;

use App\Customer as CustomerModel;
use App\Project;
use App\Quotation;

class ApiCustomerController extends ApiController
{
    public function index(Request $request){
        $finder = new CustomerFinder();
        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        } else {    
            $finder->orderBy('name', 'asc');
        }

        if($request->page)
            $finder->setPage($request->page);

        if($request->keyword)
            $finder->setKeyword($request->keyword);

        $paginator = $finder->get();

        $this->jsonResponse->setData($paginator->items());
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getUser($id);
        $this->jsonResponse->setData($row->toArray());
    
        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $customer = CustomerModel::findOrNew($request->id);
        $customer->city_id = $request->city_id;
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->phone = $request->phone;
        $customer->npwp = $request->npwp;
        $customer->notes = $request->notes;

        $repo = new Customer($customer);
        $repo->save();

        $this->jsonResponse->setData($customer->id);
        $this->jsonResponse->setMessage('Customer telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getUser($id);

        $project = Project::where('customer_id', $id)->first();
        if($project){
            throw new NotFoundHttpException('Customer sudah memiliki project');
        }

        $quotation = Quotation::where('customer_id', $id)->first();
        if($quotation){
            throw new NotFoundHttpException('Customer sudah memiliki project');
        }

        // $repo = new Customer($row);
        $row->delete();
        $this->jsonResponse->setMessage('Customer berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    private function getUser($id){
        $row = CustomerModel::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('User tidak ditemukan');
        }

        return $row;
    }
}
