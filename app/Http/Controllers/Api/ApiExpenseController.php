<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\TheBadusLibs\Helper\DateFormat;
// use App\Http\Requests;
// use App\Http\Controllers\Controller;
use App\Ais\Repository\Finder\ExpenseFinder;
use App\Ais\Repository\Expense;
use App\Expense as Model;
// use App\Ais\Ais;
use App\Category as Status;

class ApiExpenseController extends ApiController {
    public function index(Request $request){
        $finder = new ExpenseFinder();

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        if(isset($request->status)) {
            $status = explode(",", $request->status);
            if(count($status) > 1) {
                $grupStatus = [];
                foreach($status as $i => $sts){
                    $grupStatus[] = Status::getStatusIdByName($sts);
                }
                $finder->setStatusIdGroup($grupStatus);
            } else {
                $statusId = Status::getStatusIdByName($request->status);
                $finder->setStatusId($statusId);    
            }
            
        }

        if(isset($request->page))
            $finder->setPage($request->page);

        if(isset($request->keyword))
            $finder->setKeyword($request->keyword);

        if(isset($request->project_id))
            $finder->setProject($request->project_id);

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $x->created);
            $status = Status::find($x->status_id);

            $list[] = [
                'id' => $x->id,
                'ref_no' => $x->ref_no,
                'created' => DateFormat::shortDate($date),
                'status' => [
                    'name' => $status->name,
                    'label' => $status->label
                ],
                'project_id' => $x->project_id,
                'project_name' => $x->project_name
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getModel($id);

        $repo = new Expense($row);
        // change format date
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $row->created);

        $list = [];
        $list = [
            'id' => $row->id,
            'project_id' => $row->project_id,
            'ref_no' => $row->ref_no,
            'is_in' => $row->is_in,
            'created' => $date->format('Y-m-d'),
            // 'created' => DateFormat::shortDate($date),
            'created_at' => $row->created_at,
            'updated_at' => $row->updated_at,
        ];

        $detail = [];
        foreach($repo->getDetailUnit() as $x) {
            $detail[] = [
                'id' => $x->id,
                'item_id' => $x->item_id,
                'name' => $x->name,
                'qty' => $x->qty,
                'unit' => $x->unit,
            ];
        }
        $list['detail'] = $detail;
        $list['status'] = Status::find($row->status_id);

        $this->jsonResponse->setData($list);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $expense = new Model;

        // Get Id
        $id = $request->id;

        if(!empty($id)){
            $expense = Model::findOrNew($id);
        }
        $statusId = Status::getStatusIdByName($request->status->name);

        $repo = new Expense($expense);

        // Create revision first before update quotation
        if(!empty($request->parent_id))
            $repo->createRevision();

        // Update quotation
        $expense->project_id = $request->project_id;
        $expense->status_id = $statusId;
        $expense->ref_no = $request->ref_no;
        $expense->created = $request->created;

        // Delete detail
        if($request->_delete_detail) {
            foreach($request->_delete_detail as $x)
                $repo->deleteDetail($x);
        }

        // Save detail quotation
        if(!empty($request->detail))
            foreach($request->detail as $x) {
                $x = (array) $x;

                $id = empty($x['id']) ? null : $x['id'];

                $repo->addDetail($id, $x['item_id'], $x['qty']);
            }

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('LPB(keluar) telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new Expense($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Expense berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    public function setStatus($id, $status)
    {
        $message = null;
        // If status is not approve or decline
        if(!in_array($status, ['draft', 'pending', 'approve', 'decline'])){
            throw new NotFoundHttpException('LPB tidak ditemukan');
        }
        
        // Get status id
        $statusId = Status::getStatusIdByName($status);

        // Get quotation model
        $row = $this->getModel($id);

        // Get quotation repository
        $repo = new Expense($row);
        $repo->setStatus($statusId);

        switch($statusId) {
            case Status::STATUS_DRAFT:
                $message = 'Purchase Order berhasil disimpan';
                break;
            case Status::STATUS_PENDING:
                $message = 'Purchase Order berhasil ditunda';
                break;
            case Status::STATUS_APPROVED:
                $message = 'Purchase Order berhasil diterima';
                break;
            case Status::STATUS_DECLINED:
                $message = 'Purchase Order berhasil ditolak';
                break;
        }

        $this->jsonResponse->setMessage($message);

        return $this->jsonResponse->getResponse();
    }
    
    // public function download(Request $request){
    //     $row = $this->getModel($request->id);
        
    //     $repo = new Receipt($row);
    //     $filename = $repo->download();

    //     $data = [
    //             'id' => $request->id,
    //             'url' => url('/tmp/' . $filename)
    //         ];

    //     $this->jsonResponse->setData($data);
    //     return $this->jsonResponse->getResponse();
    // }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('LPB tidak ditemukan');
        }

        return $row;
    }
}