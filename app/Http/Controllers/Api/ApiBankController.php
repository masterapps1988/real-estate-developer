<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ais\Repository\Bank;
use App\Ais\Repository\Finder\BankFinder;

use App\Category as Model;

use App\Ais\Ais;

class ApiBankController extends ApiController
{
    public function index(Request $request){
        $finder = new BankFinder();

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        if($request->page)
            $finder->setPage($request->page);

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {
            $list[] = [
                'id' => $x->id,
                'name' => $x->name,
                'label' => $x->label,
                'notes' => $x->notes,
                'group_by' => $x->group_by
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){

        $bank = Model::findOrNew($request->id);
        $bank->name = $request->name;
        $bank->label = $request->label;
        $bank->group_by = $request->group_by;

        $repo = new Bank($bank);

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Bank telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);

        $repo = new Bank($row);
        $repo->delete($id);
        $this->jsonResponse->setMessage('Bank berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::where(['id' => $id, 'group_by'=>'bank'])->first();
        if(empty($row)){
            throw new NotFoundHttpException('Bank tidak ditemukan');
        }

        return $row;
    }
}
