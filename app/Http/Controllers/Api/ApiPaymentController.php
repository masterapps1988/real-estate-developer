<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ais\Repository\Finder\PaymentFinder;
use App\Ais\Repository\Payment;

use App\TheBadusLibs\Helper\DateFormat;

use App\Payment as Model;
use App\Category as Bank;

use App\Ais\Ais;

class ApiPaymentController extends ApiController {
    public function index(Request $request){
        $finder = new PaymentFinder();

        // Order By
        if(isset($request->order_by) && !empty($request->order_by)) {
            $finder->orderBy($request->order_by['column'], $request->order_by['ordered']);
        }

        if(isset($request->page))
            $finder->setPage($request->page);

        if(isset($request->keyword))
            $finder->setKeyword($request->keyword);

        if(isset($request->invoice_id))
            $finder->setInvoice($request->invoice_id);

        $paginator = $finder->get();

        $list = [];
        foreach($paginator as $x) {

            $date = \DateTime::createFromFormat('Y-m-d', $x->created);

            $list[] = [
                'id' => $x->id,
                'invoice_id' => $x->invoice_id,
                'bank' => $x->bank,
                'amount' => $x->amount,
                'account_number' => $x->account_number,
                'notes' => $x->notes,
                'created' => DateFormat::shortDate($date),
            ];
        }

        $this->jsonResponse->setData($list);
        $this->jsonResponse->setMeta($this->jsonResponse->getPaginatorConfig($paginator));

        return $this->jsonResponse->getResponse();
    }

    public function show($id){
        $row = $this->getModel($id);

        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $row->created);

        $list = [
            'id' => $row->id,
            'invoice_id' => $row->invoice_id,
            'bank_category_id' => $row->bank_category_id,
            'amount' => $row->amount,
            'account_number' => $row->account_number,
            'notes' => $row->notes,
            'created' => DateFormat::shortDate($date)
        ];

        $this->jsonResponse->setData($list);

        return $this->jsonResponse->getResponse();
    }

    public function store(Request $request){
        $temp = json_decode($request->data);
        $request = (object) $temp;

        $payment = new Model;

        // Get Id
        $id = $request->id;

        if(!empty($id)){
            $payment = Model::firstOrCreate(['id' => $id]);
        }

        $payment->invoice_id = $request->invoice_id;
        $payment->bank_category_id = $request->bank_category_id;
        $payment->amount = $request->amount;
        $payment->account_number = $request->account_number;
        $payment->notes = $request->notes;
        $payment->created = $request->created;

        $repo = new Payment($payment);

        // Save
        $repo->save();

        $this->jsonResponse->setData($repo->getModel()->id);
        $this->jsonResponse->setMessage('Payment telah berhasil tersimpan.');

        return $this->jsonResponse->getResponse();
    }

    public function destroy($id){
        $row = $this->getModel($id);
        $repo = new Payment($row);
        $repo->delete();
        $this->jsonResponse->setMessage('Pembayaran berhasil dihapus');

        return $this->jsonResponse->getResponse();
    }

    private function getModel($id){
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Payment tidak ditemukan');
        }

        return $row;
    }
}