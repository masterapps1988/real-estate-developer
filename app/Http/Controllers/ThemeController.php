<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class ThemeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function login()
    {
        return view('frontoffice.login');
    }

    public function listPenawaran()
    {
        return view('listPenawaran');
    }

    public function detailPenawaran()
    {
        return view('detailPenawaran');
    }

    public function listHPP1()
    {
        return view('listHPP1');
    }

    public function detailHPP1()
    {
        return view('detailHPP1');
    }

    public function listProject()
    {
        return view('listProject');
    }

    public function detailProject()
    {
        return view('theme.detailProject');
    }

    public function listHPP2()
    {
        return view('listHPP2');
    }

    public function detailHPP2()
    {
        return view('detailHPP2');
    }

    public function listSp()
    {
        return view('listSp');
    }

    public function detailSp()
    {
        return view('detailSp');
    }

    public function listOpnamePekerjaan()
    {
        return view('listOpnamePekerjaan');
    }

    public function detailOpnamePekerjaan()
    {
        return view('detailOpnamePekerjaan');
    }

    public function listCustomer()
    {
        return view('listCustomer');
    }

    public function detailCustomer()
    {
        return view('detailCustomer');
    }

    public function listKota()
    {
        return view('listKota');
    }

    public function listProvinsi()
    {
        return view('listProvinsi');
    }

    public function detailPekerjaan()
    {
        return view('detailPekerjaan');
    }

    public function projectChecklist()
    {
        return view('projectChecklist');
    }

    public function detailChecklist()
    {
        return view('detailChecklist');
    }

    public function listAdmin()
    {
        return view('listAdmin');
    }

    public function detailAdmin()
    {
        return view('detailAdmin');
    }

    public function detailBahan()
    {
        return view('detailBahan');
    }

    public function listBahan()
    {
        return view('listBahan');
    }

    public function listPurchaseOrder()
    {
        return view('listPurchaseOrder');
    }

    public function detailPurchaseOrder()
    {
        return view('detailPurchaseOrder');
    }

    public function listLPB()
    {
        return view('listLPB');
    }

    public function detailLPB()
    {
        return view('detailLPB');
    }

    public function listSupplier()
    {
        return view('listSupplier');
    }

    public function detailSupplier()
    {
        return view('detailSupplier');
    }

    public function listMandor()
    {
        return view('listMandor');
    }

    public function detailMandor()
    {
        return view('detailMandor');
    }

    public function listInvoice()
    {
        return view('listInvoice');
    }

    public function detailInvoice()
    {
        return view('detailInvoice');
    }

    public function listStokOpname()
    {
        return view('theme.listStokOpname');
    }

    public function detailStokOpname()
    {
        return view('detailStokOpname');
    }

    public function ganttChart()
    {
        return view('theme.gantt-chart');
    }
    public function penjadwalan()
    {
        return view('theme.penjadwalan');
    }

    // ================== PDF Theme ==============
    public function pdfLayout()
    {
        $pdf = PDF::loadView('theme.pdf.layout');
        return $pdf->stream();
    }

    public function pdfQuotation()
    {
        $pdf = PDF::loadView('theme.pdf.quotation');
        return $pdf->stream();
    }

    public function pdfPenawaran()
    {
        $pdf = PDF::loadView('theme.pdf.penawaran');
        return $pdf->stream();
        // return view('theme.pdf.penawaran');
    }

    public function pdfInvoice()
    {
        $pdf = PDF::loadView('theme.pdf.invoice');
        return $pdf->stream();
        // return view('theme.pdf.invoice');
    }

    public function pdfSp()
    {
        $pdf = PDF::loadView('theme.pdf.sp');
        return $pdf->stream();
        // return view('theme.pdf.invoice');
    }

    public function pdfPo()
    {
        $pdf = PDF::loadView('theme.pdf.po');
        return $pdf->stream();
        // return view('theme.pdf.invoice');
    }

    public function pdfReceipt()
    {
        $pdf = PDF::loadView('theme.pdf.receipt');
        return $pdf->stream();
        // return view('theme.pdf.invoice');
    }
}
