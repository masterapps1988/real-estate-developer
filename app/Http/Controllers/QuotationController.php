<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ais\Repository\QuotationSchedule;

use App\Project;
use App\Quotation as Model;
use App\QuotationDetail as QuotationDetailModel;

class QuotationController extends Controller
{
    public function index(Request $request)
    {
        $basicColor = '#000000 ,#FF0000 ,#00FF00 ,#0000FF ,#FFFF00 ,#00FFFF ,#FF00FF ,#C0C0C0 ,#808080 ,#800000 ,#808000 ,#008000 ,#800080 ,#008080 ,#000080';
        $listColor = array_map('trim', explode(',', $basicColor));

    	$row = $this->getModel($request->id);
        $detail = QuotationDetailModel::where('quotation_id', $row->id)
                                        ->whereNull('quotation_detail_id')
                                        ->get();

        $project = Project::where('quotation_id', $row->id)->first();

        $arr_start = [];
        $arr_finish = [];
        for ($i=0; $i < count($detail); $i++) { 
            $start = new \DateTime($detail[$i]['schedule_start']);
            $finish = new \DateTime($detail[$i]['schedule_finish']);
            array_push($arr_start, $start);
            array_push($arr_finish, $finish);
        }

        $min_start = min($arr_start);
        $max_finish = max($arr_finish);

        $data = [
            'ref_no' => $row->ref_no,
            'quotation_name' => $row->name,
            'project_name' => $project->name,
            'address' => $row->address,
            'start_date' => $min_start->format('d-m-Y'),
            'finish_date' => $max_finish->format('d-m-Y'),
            'detail' => $detail,
            'list_color' => $listColor
        ];

        return view('gantt-chart', $data);
    }

    private function getModel($id)
    {
        $row = Model::find($id);
        if(empty($row)){
            throw new NotFoundHttpException('Penawaran/HPP1/HPP2 tidak ditemukan');
        }

        return $row;
    }
}
