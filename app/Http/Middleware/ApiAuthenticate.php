<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\ValidationException;
use Validator;
use Auth;

use App\TheBadusLibs\JsonResponse;

use App\User;
use App\Reseller;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jsonResponse = new JsonResponse();
    
        try {
            $this->authenticate($request->api_token);
        } catch(ValidationException $e) {
            $jsonResponse->setValidationException($e);

            return $jsonResponse->getResponse();
        } catch(\Exception $e) {
            $jsonResponse->setError(true);
            $jsonResponse->setMessage($e->getMessage());
            $jsonResponse->setStatus(401);

            return $jsonResponse->getResponse();
        }

        return $next($request);
    }

    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate($apiToken)
    {
        $fields = [
            'api_token' => $apiToken,
        ];

        $rules = array(
            'api_token' => 'required'
        );

        $validator = Validator::make($fields, $rules);

        // Validate input
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // Validate if api token is valid
        $user = User::where('api_token', $apiToken)->first();

        if(empty($user)) {
            throw new \Exception('API TOKEN tidak valid!');
        }

        Auth::login($user);
    }
}
