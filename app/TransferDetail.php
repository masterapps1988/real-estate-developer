<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferDetail extends Model
{
    protected $table = 'transfer_detail';

    public function parentDetail()
    {
        return $this->hasOne(TransferDetail::class);
    }
}
