<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class ProjectUser extends Model
{
    protected $table = 'project_user';

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
