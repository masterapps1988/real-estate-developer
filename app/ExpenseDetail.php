<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseDetail extends Model
{
    protected $table = 'expense_detail';

    public function parentDetail()
    {
        return $this->hasOne(ExpenseDetail::class);
    }
}
