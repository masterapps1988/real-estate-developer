<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\TransferDetail;
use App\Category;

class Transfer extends Model
{
    protected $table = 'transfer';

    protected $dates = [
        'created'
    ];

    public $timestamps = true;

    public function detail()
    {
        return $this->hasMany(TransferDetail::class);
    }

    public function status()
    {
        return $this->belongsTo(Category::class);
    }
}
