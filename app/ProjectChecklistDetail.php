<?php

namespace App;

use App\User;
use App\Quotation;

use Illuminate\Database\Eloquent\Model;

class ProjectChecklistDetail extends Model
{
    protected $table = 'project_checklist_detail';
}
