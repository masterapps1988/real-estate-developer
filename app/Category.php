<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const STATUS_DRAFT = 50;
    const STATUS_PENDING = 51;
    const STATUS_APPROVED = 52;
    const STATUS_DECLINED = 53;
    const STATUS_OPEN = 54;
    const STATUS_CLOSED = 55;
    const STATUS_REVISION = 56;
    const STATUS_ON_PROGRESS = 57;

    const ITEM_ITEM = 40;
    const ITEM_MANDOR = 41;
    const ITEM_VENDOR = 42;
    const ITEM_JOB = 43;

    protected $table = 'category';

    public static function isValidStatusId($id)
    {
        $list = [
            self::STATUS_DRAFT,
            self::STATUS_PENDING,
            self::STATUS_APPROVED,
            self::STATUS_DECLINED,
            self::STATUS_OPEN,
            self::STATUS_CLOSED,
            self::STATUS_REVISION,
            self::STATUS_ON_PROGRESS
        ];

        return in_array($id, $list);
    }

    public static function getStatusIdByName($name)
    {
        $statusId = null;

        switch($name) {
            case 'draft':
                $statusId = self::STATUS_DRAFT;
                break;

            case 'pending':
                $statusId = self::STATUS_PENDING;
                break;

            case 'revision':
                $statusId = self::STATUS_REVISION;
                break;

            case 'approve':
                $statusId = self::STATUS_APPROVED;
                break;

            case 'decline':
                $statusId = self::STATUS_DECLINED;
                break;

            case 'open':
                $statusId = self::STATUS_OPEN;
                break;

            case 'closed':
                $statusId = self::STATUS_CLOSED;
                break;

            case 'on-progress':
                $statusId = self::STATUS_ON_PROGRESS;
                break;
        }

        return $statusId;
    }

    // Get category id by name
    public static function getItemTypeIdByName($name)
    {
        $statusId = null;

        switch($name) {
            case 'item':
                $statusId = self::ITEM_ITEM;
                break;

            case 'mandor':
                $statusId = self::ITEM_MANDOR;
                break;

            case 'vendor':
                $statusId = self::ITEM_VENDOR;
                break;

            case 'job':
                $statusId = self::ITEM_JOB;
                break;
        }

        return $statusId;
    }

    public static function getRoleIdByName($name)
    {
        $row = self::where('name', $name)->first();

        $id = null;
        if(!empty($row))
            $id = $row->id;

        return $id;
    }
}
