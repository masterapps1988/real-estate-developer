<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\ExpenseDetail;

class Expense extends Model
{
    protected $table = 'expense';

    protected $dates = [
        'created'
    ];

    public $timestamps = true;

    public function detail()
    {
        return $this->hasMany(ExpenseDetail::class);
    }
}
