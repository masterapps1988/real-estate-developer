<?php
namespace App\Ais\Mails;

use App\Ais\Mails\AisMail;

// Forgot password confirmation to member
class ForgotPassword extends AisMail
{
    private $resetToken;
    private $user;
    private $password;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setResetToken($resetToken)
    {
        $this->resetToken = $resetToken;
    }

    public function getData() {
        return [
            'user' => $this->user,
            'password' => $this->password,
            'reset_token' => $this->resetToken,
        ];
    }

    public function getEmail() {
        return $this->user->email;
    }

    public function getView() {
        return 'emails.forgot-password';
    }

    public function getSubject()
    {
        return '[Reset] Permintaan reset Password';
    }
}
