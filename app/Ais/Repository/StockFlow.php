<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\StockFlow as Model;

class StockFlow extends AbstractRepository
{
    private $detail = [];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function getCategory()
    {
        $data = Model::select('category')
                        ->distinct()
                        ->get();

        return $data;
    }

    // Format with different name
    public static function getCategoryName($name)
    {
        $return = null;

        $list = [
            'receipt' => 'LPB(Masuk)',
            'transfer' => 'Transfer',
            'expense' => 'LPB(Keluar'
        ];

        if(array_key_exists($name, $list)) {
            $return = $list[$name];
        }

        return $return;
    }
}
