<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\Quotation as Model;
use App\Project;
use App\QuotationDetail;
use App\Category;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

/* Quotation is for quotation only(penawaran/hpp 1).
 *
 */
class Quotation extends AbstractRepository
{
    protected $details = [];
    private $statusId = null;

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function countRevision()
    {
        if(empty($this->model->id))
            return 0;

        return Model::where('quotation_id', $this->model->id)
            ->orWhere('id', $this->model->id)
            ->count();

    }

    public function getJobs()
    {
        return QuotationDetail::where('quotation_detail.quotation_id', $this->model->id)
                ->leftjoin(DB::raw('quotation_detail qd'), 'qd.quotation_detail_id', '=', 'quotation_detail.id')
                ->whereNull('quotation_detail.quotation_detail_id')
                ->select('quotation_detail.name', 'quotation_detail.group_by', 'quotation_detail.qty', 'quotation_detail.unit', 'quotation_detail.price',
                        DB::raw('SUM(qd.qty * qd.price) as total'))
                ->groupBy('quotation_detail.name', 'quotation_detail.group_by', 'quotation_detail.qty', 'quotation_detail.unit', 'quotation_detail.price')
                ->get();
    }

    public function getTotal()
    {
        if(empty($this->model->id))
            return 0;

        return (float) QuotationDetail::where('quotation_id', $this->model->id)
            ->sum(DB::raw('qty * price'));
    }

    public function getDetail()
    {
        return $this->details;
    }

    public function getAllDetail()
    {
        $list = [];
        $detail = QuotationDetail::where('quotation_id', $this->model->id)
                                    ->whereNull('quotation_detail_id')
                                    ->get();

        foreach ($detail as $x) {
            $details = QuotationDetail::select('quotation_detail.*', DB::raw("IF(category.label = 'Item', 'Bahan', category.label) as item_category_name"))
                                    ->where('quotation_id', $this->model->id)
                                    ->where('quotation_detail_id', $x->id)
                                    ->join('item', 'item.id', '=', 'quotation_detail.item_id')
                                    ->join('category', 'category.id', '=', 'item.item_category_id')
                                    ->get();

            $percent = $this->model->profit + $this->model->risk_factor;
            $total = $x->price * ($percent / 100);
            $price_profit = $x->price + $total;

            $list[] = [
                'id' => $x->id,
                'group_by' => $x->group_by,
                'schedule_finish' => $x->schedule_finish,
                'schedule_start' => $x->schedule_start,
                'quotation_id' => $x->quotation_id,
                'quotation_detail_id' => $x->quotation_detail_id,
                'item_id' => $x->item_id,
                'unit' => $x->unit,
                'name' => $x->name,
                'qty' => $x->qty,
                'price' => $x->price,
                'price_profit' => $price_profit,
                'detail' => $details
            ];
        }

        return $list;
    }

    public function getProject()
    {
        return Project::where('quotation_id', $this->model->id)->first();
    }

    // Called before $model is updated
    public function createRevision()
    {
        // Validate value
        $fields = [
            'id' => $this->model->id,
            'status_id' => $this->model->status_id
        ];

        $rules = [
            'id' => 'required',
            'status_id' => 'in:' . Category::STATUS_REVISION
        ];

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        // ======= Create new revision ======
        // Prepare blank quotation
        $qModel = new Model;
        $qModel->quotation_id = $this->model->id;
        $qModel->customer_id = $this->model->customer_id;
        $qModel->city_id = $this->model->city_id;
        $qModel->ref_no = null;
        $qModel->name = $this->model->name;
        $qModel->address = $this->model->address;
        $qModel->created = $this->model->created;
        $qModel->is_tax = $this->model->is_tax;
        $qModel->pph = $this->model->pph;

        // Create new quotation repository
        $qRevision = new self($qModel);

        // Set detail
        foreach($this->getDetailFromModel() as $x) {
            $x = (array) $x;
            $qRevision->addDetail(null, $x['item_id'], $x['name'], $x['unit'], $x['qty'],
                    $x['price'], $x['group_by'], $x['detail']);
        }

        $qRevision->save();
        $qRevision->setStatus(Category::STATUS_CLOSED);

        // Set updated_at
        $qModel->created_at = $this->model->created_at;
        $qModel->updated_at = $this->model->updated_at;
        // Set parent because self::save() auto set to null
        $qModel->quotation_id = $this->model->id;
        $qModel->save();

        // Change status to draft
        $this->model->status_id = Category::STATUS_DRAFT;
        $this->model->touch();

        return $qModel;
    }

    // Get detail from model
    private function getDetailFromModel()
    {
        $detail = [];

        if(!empty($this->model->id)) {
            $jobs = QuotationDetail::where('quotation_id', $this->model->id)
                    ->whereNull('quotation_detail_id')
                    ->get();

            // Loop for jobs
            foreach($jobs as $x) {
                // Get item detail
                $itemDetail = QuotationDetail::where('quotation_detail_id', $x->id)
                        ->get();

                $detail[] = [
                    'id' => $x->id,
                    'quotation_detail_id' => $x->quotation_detail_id,
                    'item_id' => $x->item_id,
                    'name' => $x->name,
                    'unit' => $x->unit,
                    'qty' => $x->qty,
                    'price' => $x->price,
                    'group_by' => $x->group_by,
                    'detail' => $itemDetail
                ];
            }
        }

        return $detail;
    }

    protected function validateQuotation()
    {
        // Validate value
        $fields = [
            'ref_no' => $this->model->ref_no,
            'name' => $this->model->name,
            'address' => $this->model->address,
            'created' => $this->model->created
        ];

        $rules = array(
            'ref_no' => 'nullable|unique:quotation,ref_no,' . $this->model->id,
            'name' => 'required',
            'address' => 'nullable',
            'created' => 'required'
        );

        if ($this->model->is_tax == 0) {
            $this->model->pph = 0;
        }

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    protected function validateStatus()
    {
        // Validate status
        $fields = [
            // When status draft, then this quotation could updated again
            'only_draft_or_blank_could_save' =>
                $this->model->status_id == Category::STATUS_DRAFT ||
                empty($this->model->status_id),
        ];

        $rules = array(
            'only_draft_or_blank_could_save' => 'in:1',
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

    protected function validateDetail()
    {
        // Validate value
        $fields = [
            'detail' => $this->details
        ];

        $rules = array(
            'detail.*.id' => 'nullable',
            'detail.*.name' => 'required',
            'detail.*.unit' => 'nullable',
            'detail.*.qty' => 'nullable',
            'detail.*.price' => 'required',
            'detail.*.group_by' => 'required',
        );

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    public function prepareValidation()
    {
        $this->validateQuotation();
        $this->validateStatus();
        $this->validateDetail();
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Category::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Category::STATUS_DRAFT &&
                    $statusId == Category::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }

    public function addDetail($id, $jobId, $name, $unit,
            $qty, $price, $groupBy, $detail)
    {
        $this->_addDetail($id, null, $jobId, $name, $unit, $qty, $price, $groupBy, $detail);
    }

    protected function _addDetail($id, $quotationDetailId, $itemId, $name, $unit,
            $qty, $price, $groupBy, $detail)
    {
        $this->details[] = [
            'id' => $id,
            'quotation_detail_id' => $quotationDetailId,
            'item_id' => $itemId,
            'name' => $name,
            'unit' => $unit,
            'qty' => $qty,
            'price' => $price,
            'group_by' => $groupBy,
            'detail' => $detail
        ];
    }

    public function deleteDetail($id)
    {
        $detail = QuotationDetail::where('id', $id)
                ->where('quotation_id', $this->model->id)
                ->first();

        QuotationDetail::where('quotation_id', $this->model->id)
                        ->where('quotation_detail_id', $detail->id)
                        ->delete();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_quotation' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_quotation' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        // Check if this quotation is closed then you couldn't delete
        QuotationDetail::where('id', $id)->delete();
    }

    public function getRevisionList()
    {
        // If empty then return empty array
        if(empty($this->model->id)) {
            return [];
        }

        // If this is child quotation then grab parent id
        $id = empty($this->model->quotation_id) ?
                $this->model->id : $this->model->quotation_id;

        $query = Model::where('quotation_id', $id)
                ->orWhere('id', $id)
                ->orderBy('updated_at', 'asc');

        return $query->get();
    }

    public function delete()
    {
        //--------Revision-------
        // Get Revision quotation id
        $listRevisionQuotationId = Model::select('id')
                ->where('quotation_id', $this->model->id)
                ->pluck('id');

        //Get detail Revision
        $detailRevisionQuotation = QuotationDetail::select('id')
                            ->whereIn('quotation_id', $listRevisionQuotationId)
                            ->whereNull('quotation_detail_id')
                            ->pluck('id');

        //Delete Detail Revision
        QuotationDetail::whereIn('quotation_id', $listRevisionQuotationId)
                        ->whereIn('quotation_detail_id', $detailRevisionQuotation)
                        ->delete();

        //Delete Master Detail Revision
        QuotationDetail::whereIn('quotation_id', $listRevisionQuotationId)
                        ->whereNull('quotation_detail_id')
                        ->delete();

        //--------Master-------
        //Get Detail Model
        $detailQuotation = QuotationDetail::select('id')
                            ->where('quotation_id', $this->model->id)
                            ->whereNull('quotation_detail_id')
                            ->pluck('id');

        //Delete detail Quotatation Detail(item)
        QuotationDetail::where('quotation_id', $this->model->id)
                            ->whereIn('quotation_detail_id', $detailQuotation)
                            ->delete();

        //Delete Revision Detail reference on master detail id
        QuotationDetail::whereIn('quotation_id', $listRevisionQuotationId)
                            ->whereIn('quotation_detail_id', $detailQuotation)
                            ->delete();

        //Delete Quotation Detail (Master)
        QuotationDetail::where('quotation_id', $this->model->id)
                        ->whereNull('quotation_detail_id')
                        ->delete();

        // Delete child quotation id
        Model::whereIn('id', $listRevisionQuotationId)
                ->delete();

        // Delete child quotation
        $this->model->delete();
    }

    public function saveDetail()
    {
        // Save quotation detail
        foreach($this->details as $x) {
            $detailModel = QuotationDetail::findOrNew($x['id']);
            $detailModel->quotation_id = $this->model->id;
            $detailModel->quotation_detail_id = null;
            $detailModel->item_id = $x['item_id'];
            $detailModel->name = $x['name'];
            $detailModel->unit = $x['unit'];
            $detailModel->qty = $x['qty'];
            $detailModel->price = $x['price'];
            $detailModel->group_by = $x['group_by'];

            $detailModel->save();

            foreach ($x['detail'] as $y) {
                if(is_array($y))
                    $y = (object) $y;

                $detail = QuotationDetail::findOrNew($y->id);
                $detail->quotation_id = $this->model->id;
                $detail->quotation_detail_id = $detailModel->id;
                $detail->item_id = $y->item_id;
                $detail->name = $y->name;
                $detail->unit = $y->unit;
                $detail->qty = $y->qty;
                $detail->price = $y->price;
                $detail->group_by = null;

                $detail->save();
            }

        }
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        // If status_id still empty then set to draft
        if(empty($this->model->status_id))
            $this->model->status_id = Category::STATUS_DRAFT;

        $this->model->quotation_id = null;

        parent::save();
        $this->saveDetail();
    }

    public function download($data)
    {
        $date = new \Datetime;
        $filename = 'penawaran_' . $this->model->ref_no . '_' . $date->format('Ymdhis') . '.pdf';

        $pdf = PDF::loadView('report.penawaran', $data);
        $pdf->save('tmp/' . $filename);

        return $filename;
    }
}
