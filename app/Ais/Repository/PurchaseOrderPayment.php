<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;
use App\Ais\Repository\Project;
use App\Ais\Repository\PurchaseOrder;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;
use App\TheBadusLibs\Helper\NumberFormat;

use App\PurchaseOrderPayment as Model;
use App\PurchaseOrder as PurchaseOrderModel;
use App\Receipt as ReceiptModel;

class PurchaseOrderPayment extends AbstractRepository
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function validatePayment()
    {
        $po = new PurchaseOrder(PurchaseOrderModel::find($this->model->purchase_order_id));

        $totalPurchaseOrder= ceil($po->getTotalValidation());

        $query = Model::where('purchase_order_id', $this->model->purchase_order_id);
        if(!empty($this->model->id))
            $query->where('id', '!=', $this->model->id);
        
        $totalPayment = $query->sum('amount') + $this->model->amount;
        $totalPayment = (float) $totalPayment;
        
        // Validate value
        $errorMessage = 'Total pembayaran lebih besar dari total PO. Maksimal ' .
                NumberFormat::currency($totalPurchaseOrder);
        $fields = [
            'total_payment' => $totalPayment
        ];

        $rules = array(
            'total_payment' => 'numeric|min:1000|max:' . $totalPurchaseOrder,
        );

        $messages = [
            'total_payment.min' => 'Jumlah minimal '.NumberFormat::currency(1000,'Rp'),
            'total_payment.max' => $errorMessage
        ];
        
        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    private function prepareValidation()
    {
        $this->validatePayment();

        // $this->validateItemNotExceeded();
    }

    public function save()
    {
        $this->prepareValidation();
 
        parent::save();
    }
}
