<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\TheBadusLibs\AuthHelper;

use App\Quotation as Model;
use App\QuotationDetail;
use App\PurchaseOrder as PO;
use App\PurchaseOrderDetail as POD;
use App\StokOpname as StokOpnameModel;
use App\Category;
use App\Project;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

/* Quotation is for quotation only(penawaran/hpp 1).
 *
 */
class PricePlan extends Quotation
{
    private $project = null;

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function addProject(Project $project)
    {
        $this->project = $project;
    }

    public function getProject()
    {
        $project = $this->project;
        if(empty($project))
            if ($this->model->quotation_id == null)
                $project = Project::where('price_plan_id', $this->model->id)->first();
            else
                $project = Project::where('price_plan_id', $this->model->quotation_id)->first();
            
        return $project;
    }

    public static function getQuotationDetail($id)
    {
        $detail = QuotationDetail::where('quotation_id', $id)->get();

        return $detail;
    }

    public function getDetailPricePlan()
    {
        $data = QuotationDetail::where('quotation_id', $this->model->id)
                                    ->whereNull('quotation_detail_id')
                                    ->get();

        return $data;
    }

    public function getItemsOfDetail($id)
    {
        $data = QuotationDetail::where('quotation_id', $this->model->id)
                                    ->where('quotation_detail_id', $id)
                                    ->get();

        return $data;
    }

    public function getPricePlanDetail($id)
    {
        $detail = QuotationDetail::where('quotation_detail_id', $id)
                ->join('item', 'item.id', '=', 'quotation_detail.item_id')
                ->join('category', 'item.item_category_id', '=', 'category.id')
                ->select('quotation_detail.id', 'quotation_id', 'quotation_detail_id',
                        'item_id', 'quotation_detail.name', 'quotation_detail.unit',
                        'quotation_detail.qty', 'quotation_detail.price', 'quotation_detail.group_by',
                        'category.name as item_category')
                ->get()
                ->toArray();

        return $detail;
    }

    public function getQuotationProject($id)
    {
        $detail = Model::where('id', $id)->get();

        return $detail;
    }

    public function getQuotation($id)
    {
        $detail = QuotationDetail::where('quotation_id', $id)->get();

        return $detail;
    }

    public static function getGroup($id)
    {
        $detail = QuotationDetail::where('id', $id)->get();

        return $detail;
    }

    public function getGroupDetail($id)
    {
        $detail = QuotationDetail::where('quotation_detail_id', $id)->get();

        return $detail;
    }

    //get detail by quotation_detail_id
    public function getByQuotationDetailId($id)
    {
        $detail = QuotationDetail::where('id', $id)
                ->get();

        return $detail;
    }

    //Get total HPP 2
    public function getAllTotal($quotationId)
    {
        if(empty($this->model->id))
            return 0;

        $total = 0;
        //Get Quotation detail
        $quotationDetail = QuotationDetail::where('quotation_id', $quotationId)->get();
        foreach ($quotationDetail as $x) {
            //Get Price Plan detail
            $pricePlanDetail = QuotationDetail::where('quotation_detail_id', $x->id)->sum(DB::raw('qty * price'));
            $total += $pricePlanDetail;
        }

        return $total;
    }

    public function getAllDetail()
    {
        $list = [];
        $detail = QuotationDetail::where('quotation_id', $this->model->id)
                                    ->whereNull('quotation_detail_id')
                                    ->get();

        foreach ($detail as $x) {
            $details = QuotationDetail::select('quotation_detail.*', 'category.name as item_category',DB::raw("IF(category.label = 'Item', 'Bahan', category.label) as item_category_name"))
                                    ->where('quotation_id', $this->model->id)
                                    ->where('quotation_detail_id', $x->id)
                                    ->join('item', 'item.id', '=', 'quotation_detail.item_id')
                                    ->join('category', 'category.id', '=', 'item.item_category_id')
                                    ->get();

            $list[] = [
                'id' => $x->id,
                'group_by' => $x->group_by,
                'schedule_finish' => $x->schedule_finish,
                'schedule_start' => $x->schedule_start,
                'quotation_id' => $x->quotation_id,
                'quotation_detail_id' => $x->quotation_detail_id,
                'item_id' => $x->item_id,
                'item_category_name' => $x->item_category_name,
                'item_category' => $x->item_category,
                'unit' => $x->unit,
                'name' => $x->name,
                'qty' => $x->qty,
                'price' => $x->price,
                'detail' => $details
            ];
        }

        return $list;
    }


    //Get total quotation
    public function getTotalQuotation($id){
        if(empty($id))
            return 0;

        return QuotationDetail::where('quotation_detail_id', $id)
            ->sum(DB::raw('qty * price'));
    }

    //Get total per detail
    public function getTotalQuotationDetail($id){
        if(empty($id))
            return 0;

        return QuotationDetail::where('id', $id)
            ->sum(DB::raw('qty * price'));
    }

    protected function validateDetail()
    {
        $fields = [
            'detail' => $this->details
        ];

        $rules = array(
            'detail.*.id' => 'nullable',
            'detail.*.name' => 'required',
            'detail.*.unit' => 'nullable',
            'detail.*.qty' => 'nullable',
            'detail.*.price' => 'required',
            'detail.*.group_by' => 'required',
        );

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    // Project must have been set a quotation
    protected function validateProject()
    {
        if(!empty($this->project)) {
            // Validate value
            $fields = [
                'quotation_id' => $this->project->quotation_id,
                'price_plan_id' => empty($this->project->price_plan_id)
            ];

            $rules = array(
                'quotation_id' => 'required',
                'price_plan_id' => 'in:1'
            );

            $message = [
                'price_plan_id.in' => 'Tidak dapat ditambahkan ke project. Project ini sudah mempunyai HPP 2.',
                'quotation_id.required' => 'HPP 1 tidak boleh kosong'
            ];

            $validator = Validator::make($fields, $rules, $message);
            self::validOrThrow($validator);
        }
    }

    // Quotation detail id must all in quotation_id
    protected function validateQuotationDetailId()
    {
        $detail = $this->getDetail();
        if(!empty($detail)) {
            // Prepare project if empty
            if(empty($this->project)) {


                $this->project = Project::where('price_plan_id', $this->model->id)
                        ->first();
            }

            // Get all list id detail
            $listId = collect($detail)->pluck('quotation_detail_id');

            $listQuotationId = DB::table('quotation_detail')
                    ->whereIn('id', $listId)
                    ->get()
                    ->pluck('quotation_id');

            $fields = [
                'quotation_id' => $listQuotationId
            ];

            $rules = array(
                'quotation_id.*' => 'bail|in:' . $this->project->quotation_id,
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }
    }

    public function addItem($id, $name, $unit,
            $qty, $price, $groupBy, $detail)
    {
        // Doesn't need group, because it already grouping by
        // quotation detail ID
        $this->_addDetail($id, null, null, $name, $unit, $qty, $price, $groupBy, $detail);
    }

    protected function _addDetail($id, $quotationDetailId, $itemId, $name, $unit,
            $qty, $price, $groupBy, $detail)
    {
        $this->details[] = [
            'id' => $id,
            'quotation_detail_id' => $quotationDetailId,
            'item_id' => $itemId,
            'name' => $name,
            'unit' => $unit,
            'qty' => $qty,
            'price' => $price,
            'group_by' => $groupBy,
            'detail' => $detail
        ];
    }

    public function save()
    {
        $this->model->is_quotation = 0;

        parent::save();
        if(!empty($this->project)) {
            // Update project
            $this->project->price_plan_id = $this->model->id;
            $this->project->save();
        }
    }

    public function getDetail()
    {
        return $this->details;
    }

    public function prepareValidation()
    {
        // parent::prepareValidation();
        // $this->validateQuotationDetailId();
        // $this->validateProject();
        $this->validatePricePlan();
        // $this->validateStatus();
        $this->validateDetail();
    }

    private function validatePricePlan()
    {
        // Validate value
        $fields = [
            'ref_no' => $this->model->ref_no,
            'name' => $this->model->name,
            'address' => $this->model->address,
            'created' => $this->model->created
        ];

        $rules = array(
            'ref_no' => 'required|unique:quotation,ref_no,' . $this->model->id,
            'created' => 'required'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

    public function download($data)
    {
        $date = new \Datetime;
        $filename = 'hpp2_' . $this->model->ref_no . '_' . $date->format('Ymdhis') . '.pdf';

        $pdf = PDF::loadView('report.price-plan', $data);
        $pdf->save('tmp/' . $filename);

        return $filename;
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Category::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Category::STATUS_DRAFT &&
                    $statusId == Category::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }

    public function deleteDetail($id)
    {
        // Delete all items belong with this job
        QuotationDetail::where('quotation_detail_id', $id)
                ->where('quotation_id', $this->model->id)
                ->delete();
        
        // Delete job
        QuotationDetail::where('quotation_id', $this->model->id)
                        ->where('id', $id)
                        ->delete();
    }

    public function delete()
    {
        $project = Project::where('price_plan_id', $this->model->id)
                            ->first();

        //check SP/PO
        $sp = PO::where('project_id', $project->id)
                    ->count();

        if ($sp > 0) {
            throw new NotFoundHttpException('Project sudah memiliki SP/PO');
        }

        $stokOpname = StokOpnameModel::where('project_id', $project->id)
                    ->count();

        if ($stokOpname > 0) {
            throw new NotFoundHttpException('Project sudah memiliki Opname');
        }

        //unset project
        $project->price_plan_id = null;
        $project->save();

        //--------Revision-------
        // Get Revision quotation id
        $listRevisionPricePlanId = Model::select('id')
                ->where('quotation_id', $this->model->id)
                ->pluck('id');

        //Get detail Revision
        $detailRevisionPricePlan = QuotationDetail::select('id')
                            ->whereIn('quotation_id', $listRevisionPricePlanId)
                            ->whereNull('quotation_detail_id')
                            ->pluck('id');

        //Delete Detail Revision
        QuotationDetail::whereIn('quotation_id', $listRevisionPricePlanId)
                        ->whereIn('quotation_detail_id', $detailRevisionPricePlan)
                        ->delete();

        //Delete Master Detail Revision
        QuotationDetail::whereIn('quotation_id', $listRevisionPricePlanId)
                        ->whereNull('quotation_detail_id')
                        ->delete();

        //--------Master-------
        //Get Detail Model
        $detailPricePlan = QuotationDetail::select('id')
                            ->where('quotation_id', $this->model->id)
                            ->whereNull('quotation_detail_id')
                            ->pluck('id');

        //Delete detail Quotatation Detail(item)
        QuotationDetail::where('quotation_id', $this->model->id)
                            ->whereIn('quotation_detail_id', $detailPricePlan)
                            ->delete();

        //Delete Revision Detail reference on master detail id
        QuotationDetail::whereIn('quotation_id', $listRevisionPricePlanId)
                            ->whereIn('quotation_detail_id', $detailPricePlan)
                            ->delete();

        //Delete Quotation Detail (Master)
        QuotationDetail::where('quotation_id', $this->model->id)
                        ->whereNull('quotation_detail_id')
                        ->delete();

        // Delete child quotation id
        Model::whereIn('id', $listRevisionPricePlanId)
                ->delete();

        // Delete child quotation
        $this->model->delete();
    }

}
