<?php

namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;
use App\TheBadusLibs\Helper\NumberFormat;

use App\Ais\Repository\Project;

use App\Invoice as Model;
use App\InvoiceDetail;
use App\InvoicePayment;
use App\Category as Status;
use App\Project as ProjectModel;
use App\QuotationDetail;
use App\StokOpname as StokOpnameModel;
use App\StokOpnameDetail;
use App\User;
use App\Item;

class InvoiceMandorVendor extends AbstractRepository
{
    private $detail = [];
    private $project = null;

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function addProject(Project $project)
    {
        $this->project = $project;
    }

    public function addDetail($id, $notes, $amount)
    {
        $this->detail[] = [
            'id' => $id,
            'notes' => $notes,
            'amount' => $amount,
        ];
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function validateInvoice()
    {
        // Validate value
        $fields = [
            'project_id' => $this->model->project_id,
            'ref_no' => $this->model->ref_no,
            'name' => $this->model->name,
            'address' => $this->model->address,
            'hp' => $this->model->hp,
            'city' => $this->model->city,
            'created' => $this->model->created
        ];

        $rules = array(
            'project_id' => 'required|exists:project,id',
            'ref_no' => 'required',
            'name' => 'required',
            'address' => 'required',
            'hp' => 'required',
            'city' => 'required',
            'created' => 'required|date'
        );

        $messages = [
            'address.required' => 'Mandor/vendor belum mempunyai alamat. Silakan ditambahkan terlebih dahulu',
            'hp.required' => 'Mandor/vendor belum mempunyai nomor HP. Silakan ditambahkan terlebih dahulu'
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    // Validate: find opname for this invoice
    public function validateJobOpname()
    {
        $opname = $this->getJobOpname();

        // Validate value
        $fields = [
            'job_opname_found' => !empty($opname),
        ];

        $rules = array(
            'job_opname_found' => 'in:1',
        );

        $messages = [
            'job_opname_found.in' => 'Opname pekerjaan tidak ditemukan untuk invoice ini.'
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }
   
    public function validateDetail()
    {
        $opname = $this->getJobOpname();
        $jobOpnameRepo = new JobOpname($opname);
        $maxTotal = $jobOpnameRepo->getTotal();
        $maxTotal = JobOpname::substractWithRetention($maxTotal);

        $total = 0;
        foreach($this->detail as $x)
            $total += $x['amount'];

        // Validate value
        $fields = [
            'detail_at_least_one' => count($this->detail),
            'max_total' => $total,
            'detail' => $this->detail,
        ];

        $rules = array(
            'detail_at_least_one' => 'required|numeric|min:1',
            'max_total' => 'required|max:' . $maxTotal,

            'detail.*.notes' => 'required',
            'detail.*.amount' => 'required',
        );

        $messages = [
            'detail_at_least_one.min' => 'Tidak ada detail pada invoice'
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    private function prepareValidation()
    {
        $this->validateInvoice();
        $this->validateInvoiceDate();
        $this->validateJobOpname();
        $this->validateDetail();
    }

    // Validate invoice payment: only one invoice for mandor/vendor each day
    public function validateInvoiceDate(){
        if (!$this->model->id) {
            $invoice = (int) Model::where('project_id', $this->model->project_id)
                                ->where('user_id', $this->model->user_id)
                                ->where('created', $this->model->created)
                                ->count();

            $fields = [
                'max_invoice' => $invoice,
            ];

            $rules = array(
                'max_invoice' => 'numeric|max:0',
            );

            $msg = "Invoice pembayaran untuk mandor/vendor ybs dengan tanggal " .
                    DateFormat::fullDate(new \DateTime($this->model->created)) . " sudah pernah dibuat";
            $messages = [
                'max_invoice.max' => $msg
            ];

            $validator = Validator::make($fields, $rules, $messages);
            self::validOrThrow($validator);
        }
    }

    public function delete()
    {
        // Delete invoice detail by invoice id
        InvoiceDetail::where('invoice_id', $this->model->id)
                ->delete();

        // Delete child invoice
        $this->model->delete();
    }

    public function deleteDetail($id)
    {
        $detail = InvoiceDetail::where('id', $id)
                ->where('invoice_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_invoice' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_invoice' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        // Check if this quotation is closed then you couldn't delete
        InvoiceDetail::where('id', $id)->delete();
    }

    public function saveDetail()
    {
        // Save po detail
        foreach($this->detail as $x) {
            $detailModel = InvoiceDetail::findOrNew($x['id']);
            $detailModel->invoice_id = $this->model->id;
            $detailModel->notes = $x['notes'];
            $detailModel->amount = $x['amount'];
            $detailModel->save();

            // Check price, if change then update to price history
        }
    }

    public function getTotal()
    {
        return InvoiceDetail::where('invoice_id', $this->model->id)
                ->sum('amount');
    }

    public function getTotalPaidAmount()
    {
        return InvoicePayment::where('invoice_id', $this->model->id)
                ->sum('amount');
    }

    public function save()
    {
        $this->prepareValidation();

        parent::save();
        $this->saveDetail();
    }

    public function download()
    {
        $date = new \Datetime;
        $filename = 'invoice_' . $this->model->ref_no.'_'. $date->format('Ymdhs') . '.pdf';
        
        $data['row'] = DB::table('invoice')
                        ->select('invoice.ref_no', 'invoice.created', 'project.name as project_name', 
                        'customer.name as customer_name', 'customer.address', 'customer.phone')
                        ->join('project', 'project.id', '=', 'invoice.project_id')
                        ->join('customer', 'customer.id', '=', 'project.customer_id')
                        ->where('invoice.id', $this->model->id)
                        ->first();

        $tanggal = \DateTime::createFromFormat('Y-m-d H:i:s', $data['row']->created);
        $data['tanggal'] = DateFormat::shortDate($tanggal);
        
        $data['detail'] = \App\InvoiceDetail::where('invoice_id', $this->model->id)->get();
        $data['total'] = \App\InvoiceDetail::where('invoice_id', $this->model->id)->sum('amount');
        
        $pdf = PDF::loadView('report.invoice', $data);
        $pdf->save('tmp/' . $filename);

        return $filename;
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Status::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Status::STATUS_DRAFT &&
                    $statusId == Status::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }

    // Get correct job opname
    public function getJobOpname()
    {
        $opname = null;
        if(!empty($this->model->project_id) && !empty($this->model->user_id)
                && !empty($this->model->created)) {
            $opname = StokOpnameModel::where('project_id', $this->model->project_id)
                    ->where('user_id', $this->model->user_id)
                    ->where('created', $this->model->created)
                    ->first();
        }

        return $opname;
    }

    // Convert opname to invoice array data structure
    public static function opnameToArray(StokOpnameModel $opname)
    {
        $detail = StokOpnameDetail::where('stok_opname_id', $opname->id)
                    ->get();

        $project = ProjectModel::find($opname->project_id);

        $user = User::find($opname->user_id);
        $city = null;
        $address = null;
        $phone = null;

        if ($user->city) {
            $city = $user->city->name;
        }

        if ($user->phone) {
            $phone = $user->phone;
        }

        if ($user->address) {
            $address = $user->address;
        }

        $data = [
            'id' => null,
            'ref_no' => null,
            'project_id' => $project->id,
            'project_name' => $project->name,
            'user_id' => $opname->user_id,

            // Auto set status to draft
            'status' => [
                'name' => 'draft'
            ],
            'name' => $opname->user->name,
            'address' => $address,
            'hp' => $phone,
            'city' => $city,
            'created' => date("Y-m-d")
        ];

        $details = [];
        $total = 0;
        foreach ($detail as $x) {
            // Get from quotation detail
            $item = QuotationDetail::find($x->quotation_detail_id);
            $amount = $x->qty * $item->price;
            $details[] = [
                'notes' => $item->name,
                'amount' => $amount
            ];

            $total += $amount;
        }

        // Add retention
        $retPerc = JobOpname::RETENTION * 100;
        $details[] = [
            'notes' => 'Biaya retensi ' . $retPerc . '%',
            'amount' => -($total * JobOpname::RETENTION)
        ];

        $data['detail'] = $details;

        return $data;
    }
}