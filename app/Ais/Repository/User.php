<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\User as UserModel;
use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

class User implements IRepository
{
    protected $model;
    private $password;
    private $confirmPassword;

    public function __construct(UserModel $model)
    {
        $this->model = $model;
        $this->password = null;
        $this->confirmPassword = null;
    }

    public function setPassword($password, $confirmPassword)
    {
        $this->password = $password;
        $this->confirmPassword = $confirmPassword;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function delete()
    {
        $this->model->delete();
    }

    public function save()
    {
        // Validation
        $fields = [
            'role_id' => $this->model->role_id,
            'city_id' => $this->model->city_id,
            'username' => $this->model->username,
            'password' => $this->password,
            'confirm_password' => $this->confirmPassword,
            'name' => $this->model->name,
            'email' => $this->model->email,
        ];

        $id = 0;
        if(!empty($this->model->id))
            $id = $this->model->id;

        $rules = [
            'role_id' => 'required|exists:category,id',
            'city_id' => 'required|exists:city,id',
            'username' => 'required|unique:user,username,' . $id,
            'password' => 'min:5',
            'confirm_password' => 'same:password',
            'name' => 'required',
            'email' => 'required|email'
        ];

        // Unset if empty, so no need to validate
        if(!empty($this->model->id) && empty($this->password) && empty($this->confirmPassword)) {
            foreach(['password', 'confirm_password'] as $x)
                unset($fields[$x], $rules[$x]);
        }

        $validator = Validator::make($fields, $rules);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if(!empty($this->password))
            // Mean change password
            $this->model->password = Hash::make($this->password);

        $this->model->save();
    }

    public static function getList($keyword = null)
    {
        return UserModel::paginate(Ais::getPaginate());
    }
}
