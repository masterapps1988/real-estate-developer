<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;
use App\Ais\Repository\PurchaseOrder;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;

use App\Receipt as Model;
use App\ReceiptDetail;
use App\Category;
use App\PurchaseOrder as PurchaseOrderModel;

class Receipt extends AbstractRepository
{
    private $details = [];
    
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function getDetail()
    {
        return $this->details;
    }

    protected function validateReceipt()
    {
        // Validate value
        $fields = [
            'purchase_order_id' => $this->model->purchase_order_id,
            'ref_no' => $this->model->ref_no,
            'created' => $this->model->created
        ];

        $rules = array(
            'purchase_order_id' => 'required|exists:purchase_order,id',
            'ref_no' => 'nullable|max:10|unique:receipt,ref_no,' . $this->model->id,
            'created' => 'required'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

    protected function validateDetail()
    {
        // Validate value
        $errorMessage = 'Qty minimal :x: sampai :y:';
        $errorMessageMinDetail = 'Detail LPB tidak ada. LPB minimal mempunyai 1 detail.';

        $fields = [
            'detail_at_least_one' => count($this->details),
            'detail' => $this->details
        ];

        $rules = [
            'detail_at_least_one' => 'required|numeric|min:1',

            'detail.*.item_id' => 'required|exists:item,id',
            'detail.*.name' => 'required',
            'detail.*.qty' => 'required',
        ];

        $messages = [
            'detail_at_least_one.min' => $errorMessageMinDetail
        ];

        // Add rules for maximum qty
        // PO should not null. It should already checked before get in this method
        $po = new PurchaseOrder(PurchaseOrderModel::find($this->model->purchase_order_id));
        $availableItems = collect($po->getAvailableItems());
        foreach($this->details as $key => $val) {
            
            $item = $availableItems->where('item_id', $val['item_id'])->first();

            // If detail is already saved, then we need to substract
            // So we could get actual max value.
            if(!empty($val['id'])) {
                $oldDetail = ReceiptDetail::find($val['id']);
                $max = $item['qty'] + $oldDetail->qty; // Rollback to before inserted
            } else
                $max = $item['qty'];
            
            $errorMessage = str_replace(':x:', 0, $errorMessage);
            $errorMessage = str_replace(':y:', $max, $errorMessage);
            $rules['detail.' . $key . '.qty'] = 'required|numeric|between:0,' . $max;
            $messages = [
                'detail.' . $key . '.qty.between' => $errorMessage
            ];
        }

        // var_dump($rules); die;

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    public function prepareValidation()
    {
        $this->validateReceipt();
        $this->validateDetail();
    }

    public function addDetail($id, $itemId, $name, $qty)
    {
        $this->_addDetail($id, $this->model->id, $itemId, $name, $qty);
    }

    protected function _addDetail($id, $receiptDetailId, $itemId, $name, $qty)
    {
        $this->details[] = [
            'id' => $id,
            'receipt_detail_id' => $receiptDetailId,
            'item_id' => $itemId,
            'name' => $name,
            'qty' => $qty
        ];
    }

    public function deleteDetail($id)
    {
        $detail = ReceiptDetail::where('id', $id)
                ->where('receipt_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_receipt' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_receipt' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        ReceiptDetail::where('id', $id)->delete();
    }

    public function delete()
    {
        // Delete receipt detail by receipt id
        ReceiptDetail::where('receipt_id', $this->model->id)
                ->delete();

        // Delete child quotation
        $this->model->delete();
    }

    public function saveDetail()
    {
        // Save receipt detail
        foreach($this->details as $x) {
            $detailModel = ReceiptDetail::findOrNew($x['id']);
            $detailModel->receipt_id = $this->model->id;
            $detailModel->item_id = $x['item_id'];
            $detailModel->name = $x['name'];
            $detailModel->qty = $x['qty'];
            $detailModel->save();
        }
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        parent::save();
        $this->saveDetail();
    }

    public function download()
    {
        $date = new \Datetime;
        $filename = 'lpb_' . $this->model->ref_no .'_'. $date->format('Ymdhs') . '.pdf';
        
        $data['row'] = DB::table('receipt')
                        ->select('receipt.*', 'purchase_order.ref_no as po')
                        ->join('purchase_order', 'purchase_order.id', '=', 'receipt.purchase_order_id')
                        ->where('receipt.id', $this->model->id)
                        ->first();
                        
        $tanggal = \DateTime::createFromFormat('Y-m-d H:i:s', $data['row']->created);
        $data['tanggal'] = DateFormat::shortDate($tanggal);
        
        $data['detail'] = Model::join('receipt_detail', 'receipt.id', '=', 'receipt_detail.receipt_id')
                ->leftjoin('purchase_order_detail', function($join){
                    $join->on('receipt.purchase_order_id', '=', 'purchase_order_detail.purchase_order_id')
                    ->on('receipt_detail.item_id', '=', 'purchase_order_detail.item_id');
                })
                ->select('receipt_detail.*', 'purchase_order_detail.unit')
                ->where('receipt.id', $this->model->id)->get();

        $pdf = PDF::loadView('report.receipt', $data);
        $pdf->save('tmp/' . $filename);

        return $filename;
    }

    public function getDetailUnit(){
        // If empty then return empty array
        if(empty($this->model->id)) {
            return [];
        }

        $query = Model::join('receipt_detail', 'receipt.id', '=', 'receipt_detail.receipt_id')
                ->leftjoin('purchase_order_detail', function($join){
                    $join->on('receipt.purchase_order_id', '=', 'purchase_order_detail.purchase_order_id')
                    ->on('receipt_detail.item_id', '=', 'purchase_order_detail.item_id');
                })
                ->select('receipt_detail.*', 'purchase_order_detail.unit')
                ->where('receipt.id', $this->model->id);

        return $query->get();
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Category::STATUS_PENDING) {
            // Validation
            $errorMessage = 'Detail LPB harus diisi';

            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Category::STATUS_DRAFT &&
                    $statusId == Category::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $messages = [
                'draft_to_pending.in' => $errorMessage
            ];

            

            $validator = Validator::make($fields, $rules, $messages);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }
}