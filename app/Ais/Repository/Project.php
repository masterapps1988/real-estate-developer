<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;
use App\Ais\Repository\Quotation;
use App\Ais\Repository\PricePlan;
use App\Ais\Repository\Invoice;
use App\Ais\Repository\Finder\StockFinder;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;

use App\Project as Model;
use App\ProjectChecklist as ProjectChecklistModel;
use App\ProjectChecklistDetail as ProjectChecklistDetailModel;
use App\User as UserModel;
use App\ProjectUser;
use App\Quotation as QuotationModel;
use App\QuotationDetail as QuotationDetailModel;
use App\PurchaseOrder as PurchaseOrderModel;
use App\Receipt as ReceiptModel;
use App\ReceiptDetail as ReceiptDetailModel;
use App\Category;
use App\ProjectDetail;
use App\Invoice as InvoiceModel;
use App\Transfer as TransferModel;
use App\Expense as ExpenseModel;
use App\StokOpname as StokOpnameModel;
use App\StokOpnameDetail as StokOpnameDetailModel;

class Project extends AbstractRepository
{
    // Hold all workers
    private $workers = [];
    private $quotation = null;

    public function __construct(Model $model)
    {
        parent::__construct($model);

        // Prepare validator
        $this->prepareValidation();
    }

    public function delete()
    {
        // Delete purchase order
        $list = PurchaseOrderModel::where('project_id', $this->model->id)
                ->get();
        foreach($list as $x) {
            $row = new PurchaseOrder($x);
            $row->delete();
        }

        // Delete invoice
        $list = InvoiceModel::where('project_id', $this->model->id)
                ->get();
        foreach($list as $x) {
            $row = new Invoice($x);
            $row->delete();
        }

        // Delete project checklist
        // For quick solution. Should be use repository
        $projectChecklistListId =  ProjectChecklistModel::where('project_id', $this->model->id)
                                    ->get()->pluck('id');
        foreach($projectChecklistListId as $c) {
            $row = ProjectChecklistDetailModel::where('project_checklist_id', $c);
            $row->delete();
        }
        
        ProjectChecklistModel::where('project_id', $this->model->id)
                ->delete();

        // Delete stok opname
        $list = StokOpnameModel::where('project_id', $this->model->id)
                ->get();
        foreach($list as $x) {
            $row = new StokOpname($x);
            $row->delete();
        }

        // Delete Project User
        $list = ProjectUser::where('project_id', $this->model->id)
                ->get();
        foreach($list as $x) {
            $this->removeWorker($x->user_id);
        }

        // Delete Transfer from
        $list = TransferModel::where('project_id_from', $this->model->id)
                ->get();
        foreach($list as $x) {
            $row = new Transfer($x);
            $row->delete();
        }

        // Delete Transfer to
        $list = TransferModel::where('project_id_to', $this->model->id)
                ->get();
        foreach($list as $x) {
            $row = new Transfer($x);
            $row->delete();
        }

        // Delete Expense
        $list = ExpenseModel::where('project_id', $this->model->id)
                ->get();
        foreach($list as $x) {
            $row = new Expense($x);
            $row->delete();
        }

        $this->model->delete();

        // Delete price plan
        if(!empty($this->model->price_plan_id)) {
            $row = new PricePlan(QuotationModel::find($this->model->price_plan_id));
            $row->delete();
        }

        // Delete quotation
        if(!empty($this->model->quotation_id)) {
            $row = new Quotation(QuotationModel::find($this->model->quotation_id));
            $row->delete();
        }
    }

    public function getTotalChecklist()
    {
        return ProjectChecklistModel::where('project_id',$this->model->id)->count();
    }

    public function getTotal()
    {
        if(empty($this->model->quotation_id))
            return 0;

        $quotation = new Quotation(QuotationModel::find($this->model->quotation_id));

        return $quotation->getTotal();
    }

    public function getTotalPaid()
    {
        $invoiceIdList = \App\Invoice::where('project_id', $this->model->id)
                ->get()
                ->pluck('id');

        return (float) \App\InvoiceDetail::whereIn('invoice_id', $invoiceIdList)
                    ->sum('amount');
    }

    public function getQuotations()
    {
        return QuotationModel::limit(3)->get();
    }

    public function getWorkers()
    {
        return self::getWorkerList($this->model->id);
    }

    public function getVendors()
    {
        $users = UserModel::where('role_id', UserModel::ROLE_VENDOR)->get();
        return $users;
    }

    public function getMandors()
    {
        $users = UserModel::where('role_id', UserModel::ROLE_MANDOR)->get();
        return $users;
    }

    // Get all existing stock in a project
    public function getStock($type, $userId)
    {
        return self::getStockList($this->model->id, $type, $userId);
    }

    private function prepareValidation()
    {
        // Validation value without query
        $fields = [
            'customer_id' => $this->model->customer_id,
            'city_id' => $this->model->city_id,
            'ref_no' => $this->model->ref_no,
            'quotation_id' => $this->model->quotation_id,
            'name' => $this->model->name,
            'address' => $this->model->address,
            'start' => $this->model->start,
            'finish' => $this->model->finish
        ];

        $id = 0;
        if(!empty($this->model->id))
            $id = $this->model->id;

        $rules = [
            'customer_id' => 'required|numeric|exists:customer,id',
            'city_id' => 'required|numeric|exists:city,id',
            'ref_no' => 'required|unique:project,ref_no,' . $this->model->id,
            'quotation_id' => 'nullable|unique:project,quotation_id,' . $this->model->id,
            'name' => 'required',
            'address' => 'required|nullable',
            'start' => 'required|date',
            'finish' => 'required|date'
        ];

        $start = new \DateTime($this->model->start);
        $finish = new \DateTime($this->model->finish);

        if ($start->format('Y-m-d') >= $finish->format('Y-m-d')) {
            throw new NotFoundHttpException("Tanggal mulai tidak boleh lebih atau sama dengan tanggal selesai");
        }

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);

        // Validation worker
        if (!empty($this->workers)) {
            // Get real worker from db
            $workers = $this->model->users;
            $currentWorker = 0;
            
            if (!empty($workers)) {
                $currentWorker = count($workers);
            }

            // List of role id
            $rolesId = [];
            if(!empty($workers))
                $rolesId = $workers->pluck('role_id');

            foreach($this->workers as $x) {
                $tempWorker = UserModel::find($x);
                $rolesId[] = $tempWorker->role_id;
            }

            // Worker validation
            // Current worker + worker that want to add need to less or equal 3
            $fields = [
                // Validate max worker
                'max_worker' => $currentWorker + count($this->workers),
                // Validate role each worker
                'permit_role' => $rolesId
            ];

            $rules = [
                'max_worker' => 'bail|max:' . Model::MAX_WORKER,
                'permit_role.*' => 'bail|in:' . implode(',', Model::PREMIT_ROLE)
            ];

            $messages = [
                'max_worker.in' => 'Pekerja maksimal adalah ' . Model::MAX_WORKER,
                'permit_role.*.in' => 'Pekerja harus M. Operasional/Operasional/Logistik Lapangan'
            ];

            $validator = Validator::make($fields, $rules, $messages);
            $this->addValidator($validator);
        }

        // Validate quotation if not empty
        if(!empty($this->quotation)) {
            // Check quotation ID must not empty
            $fields = [
                // Validate max worker
                'id' => $this->quotation->id,
            ];

            $rules = [
                'id' => 'required',
            ];

            $messages = [
                'id.required' => 'Penawaran harus disimpan terlebih dahulu',
            ];

            $validator = Validator::make($fields, $rules, $messages);
            self::validOrThrow($validator);

            $fields = [
                // Quotation must be a quotation(HPP 1)
                'is_quotation' => $this->quotation->is_quotation,
                'must_approved' => $this->quotation->status_id,
            ];

            $rules = [
                'is_quotation' => 'bail|in:1',
                'must_approve' => 'bail|in:' . Category::STATUS_APPROVED,
            ];

            $messages = [
                'is_quotation' => 'Bukan termasuk HPP 1',
                'must_approve' => 'Penawaran belum disetujui',
            ];

            $validator = Validator::make($fields, $rules, $messages);
            $this->addValidator($validator);
        }
    }

    public function addWorker($userId)
    {
        $this->workers[] = $userId;
    }

    private function validateBeforeCreatePricePlan()
    {
        // Check quotation ID must not empty
        $fields = [
            'id' => $this->model->id,
            'quotation_id' => $this->model->quotation_id,
            'price_plan_id' => empty($this->model->price_plan_id),
            'customer_id' => !empty($this->model->customer_id)
        ];

        $rules = [
            'id' => 'required',
            'quotation_id' => 'required',
            'price_plan_id' => 'in:1'
        ];

        $messages = [
            'id.required' => 'Project harus disimpan terlebih dahulu',
            'quotation_id.required' => 'Penawaran harus diset terlebih dahulu',
            'price_plan_id.in' => 'HPP 2 sudah diset'
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    public function createPricePlan()
    {
        $this->validateBeforeCreatePricePlan();

        $quotation = QuotationModel::find($this->model->quotation_id);

        // Create blank price plan model
        $pricePlan = new QuotationModel;

        // Update quotation
        $pricePlan->is_quotation = 0;
        $pricePlan->status_id = Category::STATUS_DRAFT;
        $pricePlan->quotation_id = null;
        $pricePlan->customer_id = $quotation->customer_id;
        $pricePlan->city_id = $quotation->city_id;
        $pricePlan->ref_no = \App\TheBadusLibs\WeString::random_str(10);
        $pricePlan->name = $quotation->name;
        $pricePlan->address = $quotation->address;
        $pricePlan->created = new \DateTime;
        $pricePlan->is_tax = $quotation->is_tax;
        $pricePlan->pph = $quotation->pph;
        $pricePlan->profit = $quotation->profit;
        $pricePlan->risk_factor = $quotation->risk_factor;

        $repo = new PricePlan($pricePlan);
        $repo->addProject($this->model);

        // Get quotation details(jobs)
        $jobs = \App\QuotationDetail::where('quotation_id', $this->model->quotation_id)
                ->whereNull('quotation_detail_id')
                ->get();
        $detail = [];
        foreach($jobs as $x) {
            $tempDetail = [];
            $tempJob = [
                'item_id' => $x->item_id,
                'name' => $x->name,
                'unit' => $x->unit,
                'qty' => $x->qty,
                'price' => $x->price,
                'group_by' => $x->group_by,
                'detail' => null // Detail item, mandor, and vendor
            ];

            // Query all item, mandor, and vendor
            $qItems = \App\QuotationDetail::where('quotation_detail_id', $x->id)
                ->get();
            $items = [];
            foreach($qItems as $y) {
                $items[] = [
                    'id' => null,
                    'quotation_id' => null,
                    'quotation_detail_id' => null,
                    'item_id' => $y->item_id,
                    'name' => $y->name,
                    'unit' => $y->unit,
                    'qty' => $y->qty,
                    'price' => $y->price
                ];
            }

            $tempJob['detail'] = $items;
            $detail[] = $tempJob;
        }

        // Save detail quotation
        if(!empty($detail)){
            foreach($detail as $x) {
                $x = (array) $x;

                $repo->addDetail(null, $x['item_id'], $x['name'], $x['unit'], $x['qty'],
                        $x['price'], $x['group_by'], $x['detail']);
            }
        }

        // Save
        $repo->save();

        return $repo->getModel();
    }

    public function removeWorker($userId)
    {
        if(empty($this->model->id))
            return;

        ProjectUser::where('project_id', $this->model->id)
                ->where('user_id', $userId)
                ->delete();
    }

    public function setQuotation(QuotationModel $quotation)
    {
        $this->quotation = $quotation;
        $this->model->quotation_id = $quotation->id;
    }

    public function save()
    {
        $this->prepareValidation();

        parent::save();

        // Save worker
        foreach($this->workers as $x) {
            $worker = ProjectUser::where('project_id', $this->model->id)
                    ->where('user_id', $x)
                    ->first();

            if (empty($worker)) {
                $worker = new ProjectUser;
                $worker->project_id = $this->model->id;
                $worker->user_id = $x;
            }

            $worker->save();
        }
    }

    public static function getList($keyword = null)
    {
        $data = Model::leftJoin('customer', 'project.customer_id', '=', 'customer.id')->select('project.id AS id', 'project.ref_no AS ref_no', 'project.start AS project_start', 'project.finish AS project_finish', 'customer.name AS customer_name', 'project.name AS name')->paginate(Ais::getPaginate());
        return $data;
    }

    public static function getWorkerList($project_id)
    {
        $list = ProjectUser::where('project_id', $project_id)->get()->pluck('user_id');
        $users = UserModel::whereIn('id', $list)->get();

        return $users;
    }

    /*
     * $params int Project ID
     * @params string Item category
     * @params int User ID
     */
    public static function getStockList($projectId, $type, $userId)
    {
        $stockFinder = new StockFinder();
        $stockFinder->setProjectId($projectId);
        $stockFinder->setType($type);
        $stockFinder->setUserId($userId);
        $list = $stockFinder->get();

        foreach($list as $key => $val)
            $list[$key]->qty = (float) $val->qty;

        return $list;
    }

    // Get available item on price plan that hasn't been ordered
    public function getAvailableItems()
    {
        $projectId = $this->model->id;
        $pricePlanId = $this->model->price_plan_id;

        if(empty($projectId) || empty($pricePlanId)) {
            throw new \Exception('Project tidak valid. Pastikan project sudah ada HPP 2 nya.');
        }

        $type = Category::getItemTypeIdByName('item');

        $sql = <<<EOT
select q.item_id, i.name, IF((q.qty - po.qty) IS NULL, q.qty, (q.qty - po.qty)) qty, q.unit from (
	-- Get quotation items
	select item_id, sum(qty) qty, unit from quotation q
	join quotation_detail qd on q.id = qd.quotation_id
	where q.id = (select price_plan_id from project p where id = :project_id:) AND
		qd.quotation_detail_id IS NOT NULL
	group by item_id, unit
) q
left join (
	-- Get po items, include SP and PO doesn't really matter
	select item_id, sum(qty) qty, unit from purchase_order_detail pod
	join purchase_order po on po.id = pod.purchase_order_id
	where po.project_id = :project_id:
	group by item_id, unit
) po ON q.item_id = po.item_id AND q.unit = po.unit
join item i on i.id = q.item_id
where i.item_category_id = :type:;
EOT;

        $sql = str_replace(':project_id:', $projectId, $sql);
        $sql = str_replace(':type:', $type, $sql);

        $rows = DB::select(DB::raw($sql));

        $list = [];
        foreach($rows as $key => $val) {
            $rows[$key]->qty = (float) $rows[$key]->qty;

            $list[] = (array) $rows[$key];
        }

        return $list;
    }

    public function setStatus($statusId)
    {
        $this->model->status_id = $statusId;
        $this->model->save();
    }

    public function getChecklistSummary()
    {
        $listChecklistModel = ProjectUser::where('project_id', $this->model->id)->get()->pluck('user_id');
        $users = UserModel::whereIn('id', $listChecklistModel)->get();

        $list = [];
        foreach($users as $x) {
            $count = ProjectChecklistModel::where('project_checklist.project_id', $this->model->id)
                    ->leftJoin('project_checklist_detail',
                            'project_checklist.id', '=',
                            'project_checklist_detail.project_checklist_id')
                    ->where('project_checklist_detail.user_id', $x->id)
                    ->count();

            $list[] = [
                'user_id' => $x->id,
                'username' => $x->username,
                'name' => $x->name,
                'role_id' => $x->role_id,
                'role_name' => $x->category->name,
                'checklist_done' => $count
            ];
        }

        return $list;

    }

    public function getProgress($userId)
    {
        $type = 'mandor,vendor';

        $stockFinder = new StockFinder();
        $stockFinder->setProjectId($this->model->id);
        $stockFinder->setType($type);
        $stockFinder->setUserId($userId);
        $stock = $stockFinder->get();

        $data = [];
        foreach ($stock as $x) {
            // $progress = StokOpnameDetailModel::where('quotation_detail_id', $x->quotation_detail_id)
            //                             ->where('item_id', $x->item_id)
            //                             ->first();

            $real = StokOpnameDetailModel::where('quotation_detail_id', $x->quotation_detail_id)
                                            ->where('item_id', $x->item_id)
                                            ->sum('qty');

            $target = QuotationDetailModel::find($x->quotation_detail_id);

            $data[] = [
                'group_by' => $x->group_by,
                'item_id' => $x->item_id,
                'name' => $x->name,
                'price' => $x->price,
                'project_id' => $x->project_id,
                'quotation_detail_id' => $x->quotation_detail_id,
                'unit' => $x->unit,
                'target' => $target->qty,
                'done' => 0,
                'real' => $real
            ];
        }

        return $data;
    }

}
