<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;
use App\Ais\Repository\Invoice;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;
use App\TheBadusLibs\Helper\NumberFormat;

use App\Payment as Model;
use App\Invoice as InvoiceModel;
use App\InvoiceDetail as InvoiceDetail;
use App\InvoicePayment as InvoicePaymentModel;
// use App\Payment as PaymentModel;

class Payment extends AbstractRepository
{
    private $detail = [];
    
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function getDetail()
    {
        return $this->detail;
    }

    protected function validatePayment()
    {
        $invoicePayment = new Invoice(InvoiceModel::find($this->model->invoice_id));
        $totalInvoice= $invoicePayment->getTotal();

        $query = InvoicePaymentModel::where('invoice_id', $this->model->invoice_id);
        if(!empty($this->model->id))
            $query->where('id', '!=', $this->model->id);
        
        $totalInvoicePayment = $query->sum('amount') + $this->model->amount;
        $totalInvoicePayment = (float) $totalInvoicePayment;
        // Validate value
        $errorTotalPayment = 'Total pembayaran lebih besar dari total Invoice. Maksimal ' .
                NumberFormat::currency($totalInvoice);
        $errorAmount = 'Total pembayaran Maksimal Rp 999jt';

        $fields = [
            'invoice_id' => $this->model->invoice_id,
            'bank_category_id' => $this->model->bank_category_id,
            'amount' => (float)$this->model->amount,
            'account_number' => $this->model->account_number,
            'notes' => $this->model->notes,
            'total_payment' => $totalInvoicePayment,
            'created' => $this->model->created
        ];

        $rules = array(
            'invoice_id' => 'required|exists:invoice,id',
            'bank_category_id' => 'required|exists:category,id',
            'amount'        => 'numeric|min:1000|max:999999999',
            'account_number' => 'required|max:20',
            'notes' => 'nullable|max:255',
            'total_payment' => 'numeric|min:1000|max:' . $totalInvoice,
            'created' => 'required'
        );

        $messages = [
            'total_payment.max' => $errorTotalPayment,
            'amount.max'            => $errorAmount
        ];

        $validator = Validator::make($fields, $rules, $messages);
        $this->addValidator($validator);
    }

    public function prepareValidation()
    {
        $this->validatePayment();
    }

    public function delete()
    {
        // Delete child quotation
        $this->model->delete();
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        parent::save();
    }

}