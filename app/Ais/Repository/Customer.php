<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\Customer as CustomerModel;
use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

class Customer implements IRepository
{
    private $model;

    public function __construct(CustomerModel $model)
    {
        $this->model = $model;
    }

    public function delete()
    {
        $this->model->delete();
    }

    public function getModel()
    {
        return $this->model;
    }
    
    public function save()
    {
        // Validation
        $fields = [
            'name' => $this->model->name,
            'address' => $this->model->address,
            'city_id' => $this->model->city_id,
            'phone' => $this->model->phone,
            'npwp' => $this->model->npwp,
            'notes' => $this->model->notes
        ];

        $rules = array(
            'name' => 'required|max:45',
            'city_id' => 'required',
            // 'address' => ,
            'phone' => 'required'
            // 'npwp' => $this->model->npwp,
            // 'notes' => $this->model->notes
        );

        $validator = Validator::make($fields, $rules);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->model->save();
    }

    public static function getList()
    {
        return CustomerModel::paginate(Ais::getPaginate());
    }
}
