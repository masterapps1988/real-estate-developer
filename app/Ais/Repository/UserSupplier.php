<?php
namespace App\Ais\Repository;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\User as UserModel;

class UserSupplier extends User
{
    public function __construct(UserModel $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function save()
    {
        // Validation
        // No need to validate username, password, email
        // Because supplier doesn't need that field
        $fields = [
            'role_id' => $this->model->role_id,
            // 'username' => $this->model->username,
            // 'password' => $this->password,
            // 'confirm_password' => $this->confirmPassword,
            'name' => $this->model->name,
            // 'email' => $this->model->email,
        ];

        $id = 0;
        if(!empty($this->model->id))
            $id = $this->model->id;

        $rules = [
            'role_id' => 'required|exists:category,id|in:' . UserModel::ROLE_SUPPLIER,
            // 'username' => 'required|unique:user,username,' . $id,
            // 'password' => 'min:5',
            // 'confirm_password' => 'same:password',
            'name' => 'required',
            // 'email' => 'required|email'
        ];

        $validator = Validator::make($fields, $rules);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->model->username = null;
        $this->model->password = null;
        $this->model->email = null;

        $this->model->save();
    }
}
