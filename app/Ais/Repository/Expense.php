<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;

use App\Project as ProjectModel;
use App\Expense as Model;
use App\ExpenseDetail;
use App\Category;

class Expense extends AbstractRepository
{
    private $detail = [];
    
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function getDetail()
    {
        return $this->detail;
    }

    protected function validateExpense()
    {
        // Validate value
        $fields = [
            'project_id' => $this->model->project_id,
            'ref_no' => $this->model->ref_no,
            'created' => $this->model->created
        ];

        $rules = array(
            'project_id' => 'required|exists:project,id',
            'ref_no' => 'nullable|max:10|unique:expense,ref_no,' . $this->model->id,
            'created' => 'required'
        );

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    protected function validateDetail()
    {
        // Validate value
        $fields = [
            'detail' => $this->detail
        ];

        $rules = [];

        // Project should not null. It should already checked before get in this method
        $project = new Project(ProjectModel::find($this->model->project_id));
        $availableItems = $project->getStock('item', null);
        foreach($this->detail as $key => $val) {
            $item = (array) $availableItems->where('item_id', $val['item_id'])->first();

            // If detail is already saved, then we need to substract
            // So we could get actual max value.
            if(!empty($val['id']) && $this->model->status_id == Category::STATUS_APPROVED) {
                $oldDetail = ExpenseDetail::find($val['id']);
                $max = $item['qty'] + $oldDetail->qty; // Rollback to before inserted
            } else
                $max = $item['qty'];

            $rules['detail.' . $key . '.qty'] = 'required|numeric|between:0,' . $max;
        }

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    public function prepareValidation()
    {
        $this->validateExpense();
        $this->validateDetail();
    }

    public function addDetail($id, $itemId, $qty)
    {
        $this->_addDetail($id, $this->model->id, $itemId, $qty);
    }

    protected function _addDetail($id, $expenseDetailId, $itemId, $qty)
    {
        $this->detail[] = [
            'id' => $id,
            'expense_id' => $expenseDetailId,
            'item_id' => $itemId,
            'qty' => $qty
        ];
    }

    public function deleteDetail($id)
    {
        $detail = ExpenseDetail::where('id', $id)
                ->where('expense_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_expense' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_expense' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        ExpenseDetail::where('id', $id)->delete();
    }

    public function delete()
    {
        // Delete expense detail by expense id
        ExpenseDetail::where('expense_id', $this->model->id)
                ->delete();

        // Delete child quotation
        $this->model->delete();
    }

    public function saveDetail()
    {
        // Save expense detail
        foreach($this->detail as $x) {
            $detailModel = ExpenseDetail::findOrNew($x['id']);
            $detailModel->expense_id = $this->model->id;
            $detailModel->item_id = $x['item_id'];
            $detailModel->qty = $x['qty'];
            $detailModel->save();
        }
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        parent::save();
        $this->saveDetail();
    }

    // public function download()
    // {
    //     $date = new \Datetime;
    //     $filename = 'lpb_' . $this->model->ref_no .'_'. $date->format('Ymdhs') . '.pdf';
        
    //     $data['row'] = DB::table('expense')
    //                     ->select('expense.*', 'purchase_order.ref_no as po')
    //                     ->join('purchase_order', 'purchase_order.id', '=', 'expense.purchase_order_id')
    //                     ->where('expense.id', $this->model->id)
    //                     ->first();
                        
    //     $tanggal = \DateTime::createFromFormat('Y-m-d H:i:s', $data['row']->created);
    //     $data['tanggal'] = DateFormat::shortDate($tanggal);
        
    //     $data['detail'] = Model::join('expense_detail', 'expense.id', '=', 'expense_detail.expense_id')
    //             ->leftjoin('purchase_order_detail', function($join){
    //                 $join->on('expense.purchase_order_id', '=', 'purchase_order_detail.purchase_order_id')
    //                 ->on('expense_detail.item_id', '=', 'purchase_order_detail.item_id');
    //             })
    //             ->select('expense_detail.*', 'purchase_order_detail.unit')
    //             ->where('expense.id', $this->model->id)->get();

    //     $pdf = PDF::loadView('report.expense', $data);
    //     $pdf->save('tmp/' . $filename);

    //     return $filename;
    // }

    public function getDetailUnit(){
        // If empty then return empty array
        if(empty($this->model->id)) {
            return [];
        }

        $data = ExpenseDetail::select('expense_detail.id', 'expense_detail.qty', 'item.name', 'item.unit', 'expense_detail.item_id')
                                ->where('expense_id', $this->model->id)
                                ->join('item', 'expense_detail.item_id', 'item.id')
                                ->get();

        return $data;
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Category::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Category::STATUS_DRAFT &&
                    $statusId == Category::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }
}