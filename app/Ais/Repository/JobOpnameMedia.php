<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\File;

use Intervention\Image\ImageManagerStatic as Image;

use App\TheBadusLibs\AuthHelper;

use App\StokOpname as Model;

use App\Ais\Ais;
use App\Ais\Helpers\FullPath;
use App\Ais\Repository\AbstractRepository;

class JobOpnameMedia extends AbstractRepository
{
    const MAX_WIDTH = 1000;
    const MAX_WIDTH_THUMB = 250; // Crop would be square
    const IMAGE_QUALITY = 85;

    private $image = null;

    public function getThumbImageName()
    {
        return 'thumb_' . $this->model->name;
    }

    public function setImage(File $image)
    {
        $this->image = $image;
    }

    public function delete()
    {
        // Unlink image
        $path = FullPath::opname($this->model->name);
        if(file_exists($path))
            unlink($path);

        // Unlinke thumb image
        $path = FullPath::opname($this->getThumbImageName());
        if(file_exists($path))
            unlink($path);

        // Delete database
        parent::delete();
    }

    public function save()
    {
        // Delete first if image exists
        if(!empty($this->model->name)) {
            self::deleteImage($this->model->name);
        }
        
        parent::save();

        $this->model->name = $this->generateName();
        $this->model->save();

        // Store file to public/assets/opname
        $this->saveImage();
    }

    // Store image to public/assets/opname
    private function saveImage()
    {
        $image = Image::make($this->image);
        $width = $image->width();

        if($width > self::MAX_WIDTH)
            $image->resize(self::MAX_WIDTH, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            
        $image->save(FullPath::opname($this->model->name), self::IMAGE_QUALITY);

        // Create thumb
        $image->fit(self::MAX_WIDTH_THUMB, self::MAX_WIDTH_THUMB);
        $image->save(FullPath::opname($this->getThumbImageName()), self::IMAGE_QUALITY);
    }

    private function generateName()
    {
        $hash = md5(time());
        $ext = $this->image->extension();

        return vsprintf("%s_%s.%s", [$this->model->id, $hash, $ext]);
    }

    public static function deleteImage($filename)
    {
        $fullpath = FullPath::opname($filename);
        return unlink($fullpath);
    }
}
