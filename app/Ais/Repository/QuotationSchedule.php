<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\Quotation as Model;
use App\QuotationDetail;
use App\Category;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

class QuotationSchedule extends AbstractRepository
{
    private $details = [];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getDetail()
    {
        return $this->details;
    }

    public function prepareValidation()
    {
        $this->validateDetail();
    }

    protected function validateDetail()
    {
        // Validate value
        $fields = [
            'detail' => $this->details
        ];
        
        $rules = array(
            'detail.*.id' => 'nullable',
            'detail.*.name' => 'required',
            'detail.*.unit' => 'required',
            'detail.*.qty' => 'required',
            'detail.*.price' => 'required',
            'detail.*.group_by' => 'required',
            'detail.*.schedule_start' => 'required',
            'detail.*.schedule_finish' => 'required'
        );

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    public function addDetail($id, $name, $unit,
            $qty, $price, $groupBy, $scheduleStart, $scheduleFinish)
    {
        $this->_addDetail($id, null, null, $name, $unit, $qty, $price, $groupBy, $scheduleStart, $scheduleFinish);
    }

    protected function _addDetail($id, $quotationDetailId, $itemId, $name, $unit,
            $qty, $price, $groupBy, $scheduleStart, $scheduleFinish)
    {
        $this->details[] = [
            'id' => $id,
            'quotation_detail_id' => $quotationDetailId,
            'item_id' => $itemId,
            'name' => $name,
            'unit' => $unit,
            'qty' => $qty,
            'price' => $price,
            'group_by' => $groupBy,
            'schedule_start' => $scheduleStart,
            'schedule_finish' => $scheduleFinish
        ];
    }

    public function saveDetail()
    {
        // Save quotation detail
        foreach($this->details as $x) {
            $detailModel = QuotationDetail::findOrNew($x['id']);
            $detailModel->quotation_id = $this->model->id;
            $detailModel->quotation_detail_id = null;
            $detailModel->item_id = null;
            $detailModel->name = $x['name'];
            $detailModel->unit = $x['unit'];
            $detailModel->qty = $x['qty'];
            $detailModel->price = $x['price'];
            $detailModel->group_by = $x['group_by'];
            $detailModel->schedule_start = $x['schedule_start'];
            $detailModel->schedule_finish = $x['schedule_finish'];

            $detailModel->save();
        }
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        parent::save();
        $this->saveDetail();
    }

    public function downloadSchedule($data)
    {
        $date = new \Datetime;
        $filename = 'quotation-schedule_' . $this->model->ref_no . '_' . $date->format('Ymdhis') . '.pdf';

        $pdf = PDF::loadView('report.quotation-schedule', $data);
        $pdf->save('tmp/' . $filename);

        return $filename;
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Category::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->schedule_status_id == Category::STATUS_DRAFT &&
                    $statusId == Category::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->schedule_status_id = $statusId;
        $this->model->save();
    }

    public function delete()
    {
        //Delete date record
        foreach ($this->model->detail as $x) {
            $detailModel = QuotationDetail::find($x->id);
            $detailModel->schedule_start = null;
            $detailModel->schedule_finish = null;
            $detailModel->save();
        }
        //Delete status record
        $this->model->schedule_status_id = null;
        $this->model->save();
    }

}
