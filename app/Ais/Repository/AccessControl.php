<?php
namespace App\Ais\Repository;

use Illuminate\Validation\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\WeAccessControl;

use App\User;
use App\Reseller;
use App\Category;
use App\RolePermission;

class AccessControl extends WeAccessControl
{
    protected $model = null;
    private $permission = [];
    
    public function __construct(User $model)
    {
        $this->model = $model;
        $this->permission = $this->getPermissions();
    }

    public function getPermissions()
    {
        // Get list permissions
        $list = RolePermission::join('category', 'category.id', '=',
                'role_permission.permission_id')
                ->join('user', 'user.role_id', '=', 'role_permission.role_id')
                ->where('user.id', $this->model->id)
                ->orderBy('category.name', 'asc')
                ->select('category.id', 'category.name')
                ->get();

        return $list;
    }

    public function hasAccess($name)
    {
        return !empty($this->permission->where('name', $name)->first());
    }

    public function hasAccesses($listName)
    {
        $isHasAccess = $this->permission;
        foreach($listName as $x)
            $isHasAccess->where('name', $x);

        return !empty($isHasAccess);
    }
}