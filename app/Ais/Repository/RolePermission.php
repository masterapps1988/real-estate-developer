<?php

namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\Ais\Ais;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;
use App\TheBadusLibs\Helper\NumberFormat;

use App\Ais\Repository\Project;

use App\RolePermission as Model;

class RolePermission extends AbstractRepository
{
     // Hold all 
     private $detail = [];
     private $checked = [];
     private $unchecked = [];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function check($id)
    {
        $this->checked[] = $id;
    }

    public function uncheck($id)
    {
        $this->unchecked[] = $id;
    }

    public function addDetail($id, $is_checked)
    {
        $this->detail[] = [
            'id' => $id,
            'is_checked' => $is_checked
        ];
    }

    private function prepareValidation()
    {

    }
    
    public function save()
    {
        $this->prepareValidation();
        
        // Unchecked
        Model::where('role_id','=',$this->model->role_id)->whereIn('permission_id', $this->unchecked)
                ->delete();
                
        foreach($this->checked as $x) {
            $role = Model::findOrNew($x);
            
            $role->permission_id = $x;
            $role->role_id = $this->model->role_id;
            
            $role->save();
        }
    }
}
