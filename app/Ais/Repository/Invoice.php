<?php

namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\Ais\Ais;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;
use App\TheBadusLibs\Helper\NumberFormat;

use App\Ais\Repository\Project;

use App\Invoice as Model;
use App\InvoiceDetail;
use App\InvoicePayment;
use App\Category as Status;
use App\Project as ProjectModel;
use App\QuotationDetail;

class Invoice extends AbstractRepository
{
    private $detail = [];
    private $project = null;

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function addProject(Project $project)
    {
        $this->project = $project;
    }

    public function addDetail($id, $notes, $amount)
    {
        $this->detail[] = [
            'id' => $id,
            'notes' => $notes,
            'amount' => $amount,
        ];
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function validateInvoice()
    {
        // Validate value
        $fields = [
            'project_id' => $this->model->project_id,
            'ref_no' => $this->model->ref_no,
            'name' => $this->model->name,
            'address' => $this->model->address,
            'hp' => $this->model->hp,
            'city' => $this->model->city,
            'created' => $this->model->created,
        ];

        $rules = array(
            'project_id' => 'required|exists:project,id',
            'ref_no' => 'required',
            'name' => 'required',
            'address' => 'nullable',
            'hp' => 'nullable',
            'city' => 'nullable',
            'created' => 'date'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

   
    public function validateDetail()
    {
        // Validate value
        $fields = [
            'detail_at_least_one' => count($this->detail),
            'detail' => $this->detail,
        ];

        $rules = array(
            'detail_at_least_one' => 'required|numeric|min:1',

            'detail.*.notes' => 'required',
            'detail.*.amount' => 'required',
        );

        $messages = [
            'detail_at_least_one.min' => 'Tidak ada detail pada invoice'
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    // Validate $this->project is valid or not
    public function validateProject()
    {
        if(!empty($this->project)) {
            // Validate value
            $fields = [
                'project_must_open' => $this->project->status_id,
                'project_must_have_quotation' => $this->project->quotation_id
            ];

            $rules = [
                'project_must_open' => 'in:' . Status::STATUS_OPEN,
                'project_must_have_quotation' => 'required'
            ];

            $messages = [
                'project_must_open' => 'Project harus berstatus OPEN',
                'project_must_have_quotation.required' => 'Project belum ada HPP 1'
            ];

            $validator = Validator::make($fields, $rules, $messages);
            self::validOrThrow($validator);
        }
    }

    private function prepareValidation()
    {
        $this->validateProject();
        $this->validateInvoice();
        $this->validateDetail();
        $this->validateInvoiceTotal();
    }

    public function validateInvoiceTotal(){
        $totalInvoice = 0;
        foreach($this->detail as $x){
            $totalInvoice += $x['amount'];
        }

        // If this invoice is new then current amount should 0
        // But if not then get total paid should substract with
        // this invoice amount, because we updated it
        $currentAmount = 0;
        if(!empty($this->model->id)) {
            $currentAmount = $this->model->getTotal(); // need to be refactored
        }

        $project = new Project(ProjectModel::find($this->model->project_id));
        $projectTotalRemaining = $project->getTotal() - ($project->getTotalPaid() - $currentAmount);

        // var_dump($currentAmount, $totalInvoice, $project->getTotal(), $project->getTotalPaid(), $projectTotalRemaining);
        
        $fields = [
            'max_invoice' => (float) $totalInvoice
        ];

        $rules = [
            'max_invoice' => 'integer|max:' . $projectTotalRemaining,
        ];

        $messages = [
            'max_invoice.max' => 'Total invoice melebihi nilai project. Maks: '
            . NumberFormat::currency($projectTotalRemaining, 'Rp')
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    public function delete()
    {
        // Delete invoice detail by invoice id
        InvoiceDetail::where('invoice_id', $this->model->id)
                ->delete();

        // Delete child invoice
        $this->model->delete();
    }

    public function deleteDetail($id)
    {
        $detail = InvoiceDetail::where('id', $id)
                ->where('invoice_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_invoice' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_invoice' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        // Check if this quotation is closed then you couldn't delete
        InvoiceDetail::where('id', $id)->delete();
    }

    public function saveDetail()
    {
        // Save po detail
        foreach($this->detail as $x) {
            $detailModel = InvoiceDetail::findOrNew($x['id']);
            $detailModel->invoice_id = $this->model->id;
            $detailModel->notes = $x['notes'];
            $detailModel->amount = $x['amount'];
            $detailModel->save();

            // Check price, if change then update to price history
        }
    }

    public function getTotal()
    {
        return InvoiceDetail::where('invoice_id', $this->model->id)
                ->sum('amount');
    }

    public function getTotalPaidAmount()
    {
        return InvoicePayment::where('invoice_id', $this->model->id)
                ->sum('amount');
    }

    public function save()
    {
        $this->prepareValidation();

        parent::save();
        $this->saveDetail();
    }

    public function download()
    {
        $date = new \Datetime;
        $filename = 'invoice_' . $this->model->ref_no.'_'. $date->format('Ymdhs') . '.pdf';
        
        $data['row'] = DB::table('invoice')
                        ->select('invoice.ref_no', 'invoice.created', 'project.name as project_name', 
                        'customer.name as customer_name', 'customer.address', 'customer.phone')
                        ->join('project', 'project.id', '=', 'invoice.project_id')
                        ->join('customer', 'customer.id', '=', 'project.customer_id')
                        ->where('invoice.id', $this->model->id)
                        ->first();

        $data['detail'] = \App\InvoiceDetail::where('invoice_id', $this->model->id)->get();

        $total = \App\InvoiceDetail::where('invoice_id', $this->model->id)->sum('amount');
        $data['total'] = $total;

        $paid = InvoicePayment::where('invoice_id', $this->model->id)->sum('amount');
        $data['paid'] = $paid;

        $data['collectible'] = $total - $paid;

        $pdf = PDF::loadView('report.invoice', $data);
        $pdf->save('tmp/' . $filename);

        return $filename;
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Status::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Status::STATUS_DRAFT &&
                    $statusId == Status::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $messages = [
                'draft_to_pending.in' => 'Tidak dapat mengubah ke status Diajukan'
            ];

            $validator = Validator::make($fields, $rules, $messages);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }
}
