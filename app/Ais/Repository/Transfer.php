<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;

use App\Transfer as Model;
use App\TransferDetail;
use App\Category;

class Transfer extends AbstractRepository
{
    private $details = [];
    
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function getDetail()
    {
        return $this->details;
    }

    protected function validateTransfer()
    {
        // Validate value
        $fields = [
            'project_id_from' => $this->model->project_id_from,
            'project_id_to' => $this->model->project_id_to,
            'ref_no' => $this->model->ref_no,
            'created' => $this->model->created
        ];

        $rules = array(
            'project_id_from' => 'required|different:project_id_to',
            'project_id_to' => 'required|different:project_id_from',
            'ref_no' => 'required|unique:transfer,ref_no,' . $this->model->id,
            'created' => 'required'
        );

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    protected function validateDetail()
    {
        // Validate value
        $fields = [
            'detail' => $this->details
        ];

        $rules = array(
            'detail.*.id' => 'nullable',
            'detail.*.item_id' => 'required',
            'detail.*.qty' => 'required',
        );

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    public function prepareValidation()
    {
        $this->validateTransfer();
        $this->validateDetail();
    }

    public function addDetail($id, $itemId, $qty)
    {
        $this->_addDetail($id, $this->model->id, $itemId, $qty);
    }

    protected function _addDetail($id, $transferId, $itemId, $qty)
    {
        $this->details[] = [
            'id' => $id,
            'transfer_id' => $transferId,
            'item_id' => $itemId,
            'qty' => $qty
        ];
    }

    public function deleteDetail($id)
    {
        $detail = TransferDetail::where('id', $id)
                ->where('transfer_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_transfer' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_transfer' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        TransferDetail::where('id', $id)->delete();
    }

    public function delete()
    {
        // Delete transfer detail by transfer id
        TransferDetail::where('transfer_id', $this->model->id)
                ->delete();

        // Delete child quotation
        $this->model->delete();
    }

    public function saveDetail()
    {
        // Save receipt detail
        foreach($this->details as $x) {
            $detailModel = TransferDetail::findOrNew($x['id']);
            $detailModel->transfer_id = $this->model->id;
            $detailModel->item_id = $x['item_id'];
            $detailModel->qty = $x['qty'];
            $detailModel->save();
        }
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        parent::save();
        $this->saveDetail();
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Category::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Category::STATUS_DRAFT &&
                    $statusId == Category::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }
}