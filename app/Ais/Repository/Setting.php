<?php

namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\Ais\Ais;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;
use App\TheBadusLibs\Helper\NumberFormat;

use App\Config as Model;

class Setting
{
    private $list = [];

    public function update($key, $value)
    {
        $this->list[] = [
            'key' => $key,
            'value' => $value
        ];
    }

    public static function get($key)
    {
        $value = null;

        $row = Model::where('key', $key)->first();
        if(!empty($row))
            $value = $row->value;

        return $value;
    }

    public function save()
    {
        foreach($this->list as $x) {
            $key = $x['key'];
            $value = $x['value'];

            $row = Model::where('key', $key)->first();
            if(empty($row)) {
                $row = new Model();
                $row->key = $key;
            }

            $row->value = $value;
            $row->save();
        }
    }
}
