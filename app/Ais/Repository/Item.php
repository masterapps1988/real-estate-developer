<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;

use App\Item as ItemModel;
use App\ItemDetail as ItemDetailModel;
use App\User as UserModel;
use App\ItemHistory;
use App\Category;

class Item implements IRepository
{
    protected $model;

    public function __construct(ItemModel $model)
    {
        $this->model = $model;
    }

    public function getItemCategoryId()
    {
        return ItemModel::ITEM;
    }

    public function extraValidation()
    {
        // Validation - More detail(with query if needed)
        // Validation value without query
        $fields = [
            'user_must_supplier' => $this->model->user->role_id == UserModel::ROLE_SUPPLIER,
        ];

        $rules = [
            'user_must_supplier' => 'in:1'
        ];

        $validator = Validator::make($fields, $rules);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    public function getModel()
    {
        return $this->model;
    }

    public function delete()
    {
        // Validation
        $fields = [
            'id' =>  $this->model->id,
            'item_not_exists_in_quotation' => $this->model->id,
            'item_not_exists_in_purchase_order' => $this->model->id
        ];

        $rules = array(
            'id' => 'required',
            'item_not_exists_in_quotation' => 'unique:quotation_detail,item_id',
            'item_not_exists_in_purchase_order' => 'unique:purchase_order_detail,item_id',
        );

        $messages = [
            'item_not_exists_in_quotation.unique'
                => 'Bahan sedang digunakan pada penawaran/hpp 2',
            'item_not_exists_in_purchase_order.unique'
                => 'Bahan sedang digunakan pada PO',
        ];

        $validator = Validator::make($fields, $rules, $messages);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        ItemModel::where('id', $this->model->id)->delete();
    }

    public function save()
    {
        // Validation value without query
        $fields = [
            'ref_no' => $this->model->ref_no,
            'item_category_id' => $this->model->item_category_id,
            'user_id' => $this->model->user_id,
            'name' => $this->model->name,
            'unit' => $this->model->unit,
            'price' => $this->model->price
        ];

        $rules = [
            'ref_no' => 'required|unique:item,ref_no,' . $this->model->id,
            'item_category_id' => 'required|exists:category,id|in:' . $this->getItemCategoryId(),
            'user_id' => 'required|exists:user,id',
            'name' => 'required',
            'unit' => 'required',
            'price' => 'required|numeric'
        ];

        $validator = Validator::make($fields, $rules);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->extraValidation();

        $this->model->save();

        // Update item price history if not same with last one
        $priceHistory = $this->getLastPrice();
        if(empty($priceHistory) || $this->model->price != $priceHistory->price) {
            $this->createNewPriceHistory($this->model->price);
        }
    }

    // Save new price
    private function createNewPriceHistory($newPrice)
    {
        $row = new ItemHistory;
        $row->item_id = $this->model->id;
        $row->price = $newPrice;
        $row->created = new \DateTime;
        $row->save();
    }

    // Get last price
    private function getLastPrice()
    {
        $row = ItemHistory::where('item_id', $this->model->id)
                ->orderBy('created', 'desc')->first();

        return !empty($row) ? $row : null;
    }

    public static function getList($keyword = null)
    {
        return ItemModel::paginate(Ais::getPaginate());
    }

    public static function getItem($params)
    {
        if ($params->role == "supplier") {
            $code = Category::ITEM_SUPPLIER;
        }
        if ($params->role == "mandor") {
            $code = Category::ITEM_MANDOR;
        }
        if ($params->role == "vendor") {
            $code = Category::ITEM_VENDOR;
        }

        if(isset($params->order_by) && !empty($params->order_by)) {
            $column = $params->order_by['column'];
            $ordered = $params->order_by['ordered'];
        } else {
            $column = 'name';
            $ordered = 'asc';
        }

        $data = ItemModel::where("item_category_id", $code)->leftJoin('user', 'user.id', '=', 'item.user_id')->select('item.id AS id', 'item.name AS name', 'item.price AS price', 'user.name AS supplier')->orderBy($column, $ordered)->paginate(Ais::getPaginate());
        
        return $data;
    }
}
