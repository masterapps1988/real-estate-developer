<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\TheBadusLibs\AuthHelper;

use App\Category as Model;
use App\PurchaseOrderPayment as PurchaseOrderPaymentModel;
use App\InvoicePayment as InvoicePaymentModel;
use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

class Bank implements IRepository
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function delete()
    {
        $poPayment = PurchaseOrderPaymentModel::where('bank_category_id',  $this->model->id)
                ->first();
        $invoicePayment = InvoicePaymentModel::where('bank_category_id',  $this->model->id)
                ->first();
                
        // Validation
        $errorMessagePO = 'Bank digunakan dalam pembayaran purchase order';
        $errorMessageInvoice = 'Bank digunakan dalam pembayaran Invoice';
        $fields = [
            'id' =>  $this->model->id,
            'po_payment' => !empty($poPayment),
            'invoice_payment' => !empty($invoicePayment)
        ];

        $rules = array(
            'id' => 'required',
            'po_payment' => 'in:1',
            'invoice_payment' => 'in:1'
        );

        $messages = [
            'po_payment.in' => $errorMessagePO,
            'invoice_payment.in' => $errorMessageInvoice,
        ];

        $validator = Validator::make($fields, $rules, $messages);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if($this->model->group_by == 'bank')
            $this->model->delete();
    }

    public function getModel()
    {
        return $this->model;
    }
    
    public function save()
    {
        // Validation
        $fields = [
            'name' => $this->model->name,
            'label' => $this->model->label,
            'notes' => $this->model->notes,
            'group_by' => $this->model->group_by
        ];

        $rules = array(
            'label' => 'required|max:45',
            'group_by' => 'required'
        );

        $validator = Validator::make($fields, $rules);
        
        if($this->model->group_by != 'bank')
            throw new ValidationException($validator);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // If name is empty then just take from label
        if(empty($this->model->name)) {
            $name = strtolower($this->model->label);
            $name = str_replace(' ', '-', $name);
            $this->model->name = $name;
        }

        if (!$this->model->id) {
            $row = Model::where('name', $this->model->name)->count();
            if($row > 0)
                throw new NotFoundHttpException('Nama bank sudah ada');
        }

        $this->model->save();
    }
    
}
