<?php
namespace App\Ais\Repository;

use Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Support\Arrayable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\TheBadusLibs\Helper\DateFormat;

use App\Ais\Repository\StokOpnameMedia;
use App\Ais\Helpers\ResourceUrl;

use App\StokOpname as Model;
use App\StokOpnameDetail;
use App\StokOpnameMedia as StokOpnameMediaModel;
use App\Item;
use App\Project as ProjectModel;
use App\QuotationDetail as QuotationDetailModel;
use App\Category as Status;

class StokOpname extends AbstractRepository implements Arrayable
{
    private $details = [];
    private $images = [];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function validateStokOpname()
    {
        // Validate value
        $fields = [
            'project_id' => $this->model->project_id,
            'ref_no' => $this->model->ref_no,
            'created' => $this->model->created,
        ];

        $rules = array(
            'project_id' => 'required|exists:project,id',
            'ref_no' => 'required|nullable|unique:stok_opname,ref_no,' . $this->model->id,
            'created' => 'required'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        // Stock opname should not double created in same day
        if (!$this->model->id) {
            $stockOpname = (int) Model::where('project_id', $this->model->project_id)
                                ->where('created', $this->model->created)
                                ->whereNull('user_id')
                                ->count();

            // Validate value
            $fields = [
                'opname_found' => $stockOpname,
            ];

            $rules = [
                'opname_found' => 'required|numeric|max:0',
            ];

            $msg = "Stok opname dengan tanggal " . DateFormat::shortDate(new \DateTime($this->model->created)) .
                    " sudah pernah dibuat";
            $messages = [
                'opname_found.max' => $msg
            ];

            $validator = Validator::make($fields, $rules, $messages);
            self::validOrThrow($validator);
        }
    }

    public function validateDetail()
    {
        // Validate value
        $fields = [
            'detail' => $this->details
        ];

        $rules = [
            'detail.*.item_id' => 'required|exists:item,id',
            'detail.*.qty_before' => 'required',
            'detail.*.qty' => 'required',
        ];

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

    public function prepareValidation()
    {
        $this->validateStokOpname();
        $this->validateDetail();
    }

    public function deleteImage($id)
    {
        $row = StokOpnameMediaModel::find($id);
        if(!empty($row)) {
            $repo = new StokOpnameMedia($row);
            $repo->delete();
        }
    }

    /*
     * @params int stok_opname.id
     * @params File binary
     */
    public function addImage($id, $image)
    {
        $this->images[] = [
            'id' => $id,
            'image' => $image
        ];
    }

    public function addDetail($id, $qtyBefore, $qty, $quotationDetailId)
    {
        $this->_addDetail($id, $qtyBefore, $qty, $quotationDetailId);
    }

    protected function _addDetail($id, $qtyBefore, $qty, $quotationDetailId)
    {
        $difference = $qty - $qtyBefore;
        if ($difference != 0) {

            $quotationDetail = QuotationDetailModel::find($quotationDetailId);

            $this->details[] = [
                'id' => $id,
                'item_id' => $quotationDetail->item_id,
                'qty_before' => $qtyBefore,
                'qty' => $qty,
                'quotation_detail_id' => $quotationDetailId
            ];
        }
    }

    public function saveDetail()
    {
        // Save receipt detail
        foreach($this->details as $x) {
            $detailModel = StokOpnameDetail::findOrNew($x['id']);
            $detailModel->stok_opname_id = $this->model->id;
            $detailModel->quotation_detail_id = $x['quotation_detail_id'];
            $detailModel->item_id = $x['item_id'];
            $detailModel->qty_before = $x['qty_before'];
            // $detailModel->qty_after = $x['qty_after'];
            $detailModel->qty = $x['qty'];
            $detailModel->save();
        }
    }

    public function saveImages()
    {
        // Save images
        foreach($this->images as $x) {
            $image = StokOpnameMediaModel::findOrNew($x['id']);
            $image->stok_opname_id = $this->model->id;
            $repoMedia = new StokOpnameMedia($image);
            $repoMedia->setImage($x['image']);
            $repoMedia->save();
        }
    }

    public function deleteDetail($id)
    {
        $detail = ReceiptDetail::where('id', $id)
                ->where('receipt_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_receipt' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_receipt' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        ReceiptDetail::where('id', $id)->delete();
    }

    public function delete()
    {
        // Delete receipt detail by receipt id
        StokOpnameDetail::where('stok_opname_id', $this->model->id)
                ->delete();

        // Delete child quotation
        $this->model->delete();
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        parent::save();
        $this->saveDetail();
        $this->saveImages();
    }

    public function toArray()
    {
        $return = $this->model->toArray();

        // Get detail
        $details = $this->model->detail;

        $project = ProjectModel::find($this->model->project_id);
        $repoProject = new Project($project);

        $stock = $repoProject->getStock('item', null);

        $list = [];
        foreach ($stock as $s) {
            $item = Item::find($s->item_id);
            $temp = [];
            $temp['name'] = $item->name;
            $temp['unit'] = $item->unit;
            $temp['qty_before'] = $s->qty;
            $temp['quotation_detail_id'] = $s->quotation_detail_id;
            $temp['qty'] = 0;

            foreach ($details as $d) {
                if ($d->item_id == $s->item_id) {
                    $temp['id'] = $d->id;
                    $temp['quotation_detail_id'] = $d->quotation_detail_id;
                    $temp['qty_before'] = $d->qty_before;
                    $temp['qty'] = $d->qty;
                    $temp['item_id'] = $d->item_id;
                    $temp['stok_opname_id'] = $d->stok_opname;
                }
            }

            $list[] = $temp;
        }
        $return['status'] = $this->model->status;
        $return['detail'] = $list;

        // Get images
        $images = $this->model->media;
        $list = [];
        foreach($images as $key => $value) {
            $list[] = [
                'id' => $value['id'],
                'image' => ResourceUrl::opname($value['name']),
                'thumb_image' => ResourceUrl::thumbOpname($value['name'])
            ];
        }
        $return['images'] = $list;

        return $return;
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Status::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Status::STATUS_DRAFT &&
                    $statusId == Status::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }
}
