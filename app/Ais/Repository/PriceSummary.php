<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\Project;

/* Only to handle HPP 3 calculation
 *
 */
class PriceSummary
{
    private $project = null;
    private $result = [];

    public function __construct(Project $project)
    {
        $this->project = $project;
        $this->result = $this->getPriceSummary();
    }

    public function getDetailByQuotationDetailId($quotationDetailId)
    {
        return $this->result->where('quotation_detail_id', $quotationDetailId);
    }

    public function getTotalByQuotationDetailId($quotationDetailId)
    {
        return $this->result->where('quotation_detail_id', $quotationDetailId)
                ->sum(function($row) {
                    return $row->qty * $row->price;
                });
    }

    private function getPriceSummary()
    {
        $sql = <<<EOT
select q.id, q.quotation_id, q.quotation_detail_id, q.item_id, q.name,
	q.unit, pod.qty, q.price, q.group_by
from (
	select * from quotation_detail qd
    where qd.quotation_id = (select price_plan_id from project where id = :project_id:)
) q left join (
	select pod.item_id, pod.unit, sum(qty) qty from purchase_order_detail pod
	join purchase_order po on po.id = pod.purchase_order_id
	join project p on p.id = po.project_id
	where p.id = :project_id:
    group by item_id, unit
) pod on pod.item_id = q.item_id and q.unit = pod.unit;
EOT;

        $projectId = $this->project->id;

        $sql = str_replace(':project_id:', $projectId, $sql);

        $rows = DB::select(DB::raw($sql));

        return collect((array) $rows);
    }
}
