<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\Category as Model;
use App\RolePermission as RoleModel;
use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

class Role implements IRepository
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function delete()
    {
        if($this->model->group_by == 'role')
            $this->model->delete();
    }

    public function getModel()
    {
        return $this->model;
    }

    public function save()
    {
        // Validation
        $id = 0;
        if(!empty($this->model->id))
            $id = $this->model->id;

        $fields = [
            'id' =>  $id,
            'name' => $this->model->name,
            'label' => $this->model->label
        ];

        $rules = array(
            'id' => 'required',
            'name' => 'required|unique:category,name,' .$id,
            'label' => 'required'
        );

        $messages = [
            'name.unique' => 'Index tidak boleh sama',
            'label.required' => 'Nama harus diisi'
        ];
        
        $validator = Validator::make($fields, $rules, $messages);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        
        $name = strtolower($this->model->name);
        $name = str_replace(' ', '-', $name);
        $this->model->name = $name;

        $this->model->group_by = 'role';
        $this->model->save();
    }
}