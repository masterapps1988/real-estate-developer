<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

use App\Item as ItemModel;
use App\Category;

class ItemFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = DB::table((new ItemModel)->getTable());
    }

    public function setRole($role)
    {
        $this->query->where('item.item_category_id', '=', $role);
    }

    // Type ID is category.id group_by=item
    public function setTypeId($typeId)
    {
        for ($i=0; $i < count($typeId); $i++) {
            $this->query->orWhere("item_category_id", $typeId[$i]);
        }
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setKeyword($keyword)
    {
        $this->query->where(function($query) use ($keyword) {
            $list = explode(' ', $keyword);
            $list = array_map('trim', $list);

            foreach($list as $keyword) {
                $pattern = '%' . $keyword . '%';
                $columnList = [
                    'item.name',
                    'item.price'
                ];
                foreach($columnList as $x) {
                    $query->orWhere($x, 'like', $pattern);
                }
                
            }
        });
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'name':
                $this->query->orderBy('item.name', $orderBy);
                break;

            case 'price':
                $this->query->orderBy('item.price', $orderBy);
                break;
        }
    }

    public function get()
    {
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

    private function joinTable()
    {
        $this->query->join('user', 'item.user_id' , '=', 'user.id');

        $this->query->select('item.id', 'item.name AS name', 'user.name AS user_name',
                'item.price', 'item.user_id');
    }
}
