<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

use App\User as UserModel;
use App\Category;

class StockFinder implements IFinder
{
    private $page;
    private $query;

    public function __construct()
    {
        $this->page = null;
        $queryPricePlan = DB::table('quotation_detail')
                ->join('project', 'project.price_plan_id', '=',
                        'quotation_detail.quotation_id')
                ->join('quotation_detail as qd', 'qd.id', '=',
                        'quotation_detail.quotation_detail_id')
                ->select(DB::raw('project.id project_id'),
                        DB::raw('quotation_detail.*, qd.name as group_as'));

        $this->query = DB::table('view_stock')
            // Join with temp table
            ->rightJoin(DB::raw(sprintf('(%s) q', $queryPricePlan->toSql())), function($join) {
                $join->on('q.project_id', '=', 'view_stock.project_id');
                $join->on('q.item_id', '=', 'view_stock.item_id');
            })
            ->join('item', 'item.id', '=', 'q.item_id')
            ->select('q.project_id', 'q.item_id', 'item.name', 'item.price',
                    DB::raw('IF(view_stock.qty IS NULL, 0, view_stock.qty) qty'),
                    'q.unit', 'q.id as quotation_detail_id', 'q.group_as as group_by');
    }

    public function setProjectId($projectId)
    {
        // Filter by project id
        if(!empty($projectId))
            $this->query->where('q.project_id', $projectId);
    }

    // Set item category by string
    public function setType($type)
    {
        // Filter by type
        if(!empty($type)) {
            $list = self::getValidItemCategoryId($type);

            if(!empty($list))
                $this->query->whereIn('item.item_category_id', $list);
        }
    }

    public function setUserId($userId)
    {
        // Filter by role id
        if(!empty($userId) && self::isValidUser($userId)) {
            $this->query->where('user_id', $userId);
        }
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function orderBy($columnName, $orderBy)
    {
        return true;
    }

    public function setKeyword($keyword)
    {
        return true;
    }

    private static function isValidUser($userId)
    {
        $row = UserModel::find($userId);
        if(empty($row)){
            throw new NotFoundHttpException('User tidak ditemukan');
        } else {
            return true;
        }

        return false;
    }

    private static function getValidItemCategoryId($type)
    {
        if(empty($type)) {
            $list = [
                Category::ITEM_ITEM,
                Category::ITEM_MANDOR,
                Category::ITEM_VENDOR
            ];

            return $list;
        } else {
            $temp = explode(',', $type);
            $temp = array_map('trim', $temp);

            $list = [];
            foreach($temp as $x) {
                if ($x == 'bahan') {
                    $x = 'item';
                }
                $list[] = Category::getItemTypeIdByName($x);
            }

            return $list;
        }

        return null;

    }

    public function get()
    {
        // Using raw SQL rather than query builder
//         $sql = <<<EOT
// select q.project_id, q.item_id, i.name, IF(vs.qty IS NULL, 0, vs.qty) qty, q.unit
//     from view_stock vs
// right join (
// select p.id project_id, qd.* from quotation_detail qd
//     join project p on qd.quotation_id = p.price_plan_id
// ) q on q.project_id = vs.project_id and q.item_id = vs.item_id
// join item i on i.id = q.item_id
// where q.project_id = :project_id: AND i.item_category_id IN (:list_item_id:);
// EOT;

        // $sql = str_replace(':project_id:', $projectId, $sql);

        // $list = DB::select($sql);
        // echo $this->query->toSql(); die;

        return $this->query->get();
    }
}
