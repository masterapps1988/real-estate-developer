<?php

namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

use App\Project;

class ProjectFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = Project::whereNotNull('project.ref_no');
        $this->query->where('project.id', '!=', '1');
        $this->query->leftjoin('customer', 'project.customer_id', '=', 'customer.id');
        
        $this->query->select('project.id AS id', 'project.ref_no AS ref_no', 
            'project.start AS project_start', 'project.finish AS project_finish', 
            'customer.name AS customer_name', 'project.name AS name');

    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'ref_no':
                $this->query->orderBy('project.ref_no', $orderBy);
                break;

            case 'customer':
                $this->query->orderBy('customer.name', $orderBy);
                break;

            case 'date':
                $this->query->orderBy('project.start', $orderBy);
                break;

            case 'created':
                $this->query->orderBy('project.created_at', $orderBy);
                break;
        }
    }

    public function setDateStart(\DateTime $start)
    {
        $this->query->where('start', '>=', $start);
    }

    public function setDateFinish(\DateTime $finish)
    {
        $this->query->where('finish', '<=', $finish);
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'project.name',
                        'project.ref_no',
                        'customer.name',
                        'project.start'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }

    public function get()
    {
        // var_dump($this->query->toSql()); die;
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}