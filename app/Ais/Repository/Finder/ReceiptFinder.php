<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

class ReceiptFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = DB::table('receipt');

        $this->query->join('purchase_order', 'purchase_order.id', '=', 'receipt.purchase_order_id');
        $this->query->leftjoin('project', 'purchase_order.project_id', '=', 'project.id');

        $this->query->select('receipt.*', 'purchase_order.id as po_id', 'purchase_order.ref_no as po', 'project.id as project_id', 'project.name as project_name');
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'ref_no':
                $this->query->orderBy('ref_no', $orderBy);
                break;
            case 'po':
                $this->query->orderBy('purchase_order.ref_no', $orderBy);
                break;
        }
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'purchase_order.ref_no',
                        'project.name',
                        'receipt.ref_no'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }
    
    public function setStatusId($statusId)
    {
        $this->query->where('receipt.status_id', $statusId);
    }

    public function setStatusIdGroup($statusId)
    {
        $this->query->whereIn('receipt.status_id', $statusId);
    }

    public function setProjectId($projectId)
    {
        $this->query->where('project.id', $projectId);
    }

    public function setPurchaseOrderId($purchaseOrderId)
    {
        $this->query->where('purchase_order.id', $purchaseOrderId);
    }

    public function get()
    {
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
