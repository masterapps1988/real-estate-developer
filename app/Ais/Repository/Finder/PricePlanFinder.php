<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

use App\Quotation;

class PricePlanFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = Quotation::where('quotation.is_quotation', '0');
        $this->query->whereNull('quotation.quotation_id');

        $this->query->join('project as p', 'p.price_plan_id', '=', 'quotation.id');
        $this->query->join('quotation as q', 'q.id', '=', 'p.quotation_id');

        $this->query->join('customer', 'q.customer_id', '=', 'customer.id');
        $this->query->join('city', 'q.city_id', '=', 'city.id');
        $this->query->join('category', 'quotation.status_id', '=', 'category.id');

        $this->query->select('quotation.id as id', 'quotation.ref_no as ref_no', 'category.name AS status_name', 'category.label AS status_label', 'customer.name as customer_name', 'q.name as name', 'city.name as city', 'quotation.created as created');
    }

    public function setInProject($status)
    {
        if ($status == 0) {
            $this->query->whereRaw('`quotation`.`id` NOT IN (
SELECT `quotation_id` FROM `project` WHERE `project`.`quotation_id` IS NOT NULL) AND `quotation`.`quotation_id` IS NULL');
        }
        if ($status == 1) {
            $this->query->whereRaw('`quotation`.`id` IN (
SELECT `quotation_id` FROM `project` WHERE `project`.`quotation_id` IS NOT NULL) AND `quotation`.`quotation_id` IS NULL');
        }
    }

    public function setStatusId($statusId)
    {
        $this->query->where('quotation.status_id', $statusId);
    }

    // public function setCustomerId($customerId)
    // {
    //     $this->query->where('customer_id', $customerId);
    // }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'ref_no':
                $this->query->orderBy('quotation.ref_no', $orderBy);
                break;

            case 'name':
                $this->query->orderBy('quotation.name', $orderBy);
                break;

            case 'created':
                $this->query->orderBy('quotation.created', $orderBy);
                break;    
        }
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'quotation.ref_no',
                        'q.name',
                        'customer.name',
                        'city.name'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }

    public function get()
    {
        // var_dump($this->query->toSql()); die;
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
