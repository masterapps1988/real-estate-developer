<?php

namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

use App\Invoice;

class InvoiceFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = Invoice::whereNotNull('invoice.ref_no');

        $this->query->leftjoin('project', 'invoice.project_id', '=', 'project.id');
        // $this->query->leftjoin('city', 'invoice.city', '=', 'city.id');

        $this->query->select('invoice.id', 'invoice.project_id', 'invoice.status_id', 'project.name as project_name', 'invoice.ref_no', 
                'invoice.name', 'invoice.address', 'invoice.hp', 'invoice.city',
                'invoice.created');
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setProject($projectId)
    {
        $this->query->where('project.id', $projectId);
    }

    public function setSell($isSell)
    {
        if ($isSell == 'true') {
            $this->query->whereNull('invoice.user_id');
        } else {
            $this->query->whereNotNull('invoice.user_id');
        }
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'ref_no':
                $this->query->orderBy('invoice.ref_no', $orderBy);
                break;

            case 'name':
                $this->query->orderBy('project.name', $orderBy);
                break;
            
            case 'created':
                $this->query->orderBy('invoice.created', $orderBy);
                break;
        }
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'invoice.ref_no',
                        'project.name'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }

    public function setStatusId($statusId)
    {
        $this->query->where('invoice.status_id', $statusId);
    }

    public function setStatusIdGroup($statusId)
    {
        $this->query->whereIn('invoice.status_id', $statusId);
    }

    public function get()
    {
        // var_dump($this->query->toSql()); die;
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}