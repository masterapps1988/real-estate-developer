<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

class PurchaseOrderFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = DB::table('purchase_order');

        $this->query->leftjoin('category', 'purchase_order.status_id', '=', 'category.id');
        $this->query->leftjoin('project', 'purchase_order.project_id', '=', 'project.id');

        $this->query->select('purchase_order.*', 'project.name as project_name', 'category.name as status');
    }

    public function setStatusId($statusId)
    {
        $this->query->where('purchase_order.status_id', $statusId);
    }

    public function setStatusIdGroup($statusId)
    {
        $this->query->whereIn('purchase_order.status_id', $statusId);
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setProject($projectId)
    {
        $this->query->where('project.id', $projectId);
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'ref_no':
                $this->query->orderBy('purchase_order.ref_no', $orderBy);
                break;
            case 'project':
                $this->query->orderBy('project.name', $orderBy);
                break;
        }
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'purchase_order.ref_no',
                        'project.name'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }

    public function get()
    {
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
