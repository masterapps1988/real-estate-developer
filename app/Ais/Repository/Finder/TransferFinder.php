<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

class TransferFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = DB::table('transfer');

        $this->query->join('project as project_from', 'project_from.id', '=', 'transfer.project_id_from');
        $this->query->join('project as project_to', 'project_to.id', '=', 'transfer.project_id_to');

        $this->query->select('transfer.*', 'project_from.name as from_name', 'project_to.name as to_name');
    }

    public function setStatusId($statusId)
    {
        $this->query->where('transfer.status_id', $statusId);
    }

    public function setProjectIdFrom($projectId)
    {
        $this->query->where('transfer.project_id_from', $projectId);
    }

    public function setProjectIdTo($projectId)
    {
        $this->query->where('transfer.project_id_to', $projectId);
    }

    public function setDateFrom(\DateTime $start)
    {
        $this->query->where('transfer.created', '>=', $start);
    }

    public function setDateTo(\DateTime $finish)
    {
        $this->query->where('transfer.created', '<=', $finish);
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'project_from':
                $this->query->orderBy('project_from.id', $orderBy);
                break;
            case 'project_to':
                $this->query->orderBy('project_to.id', $orderBy);
                break;
            case 'created_from':
                $this->query->orderBy('transfer.created', $orderBy);
                break;
            case 'created_to':
                $this->query->orderBy('transfer.created', $orderBy);
                break;
        }
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'project_from.name',
                        'project_to.name',
                        'transfer.created'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }

    public function get()
    {
        // var_dump($this->query->toSql()); die;
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
