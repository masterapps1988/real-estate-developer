<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

class PurchaseOrderPaymentFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = DB::table('purchase_order_payment');

        $this->query->leftjoin('category', 'purchase_order_payment.bank_category_id', '=', 'category.id');

        $this->query->select('purchase_order_payment.*', 'category.label as bank');
    }


    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }


    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'purchase_order_id':
                $this->query->orderBy('purchase_order_payment.purchase_order_id', $orderBy);
                break;
            case 'amount':
                $this->query->orderBy('purchase_order_payment.amount', $orderBy);
                break;
        }
    }

    public function setPurchaseOrder($purchaseOrderId)
    {
        $this->query->where('purchase_order_payment.purchase_order_id', $purchaseOrderId);
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'purchase_order.purchase_order_id',
                        'project.amount'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }

    public function get()
    {
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
