<?php

namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

class PaymentFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = DB::table('invoice_payment');
        $this->query->leftjoin('category', 'category.id', '=', 'invoice_payment.bank_category_id');

        $this->query->select('invoice_payment.*', 'category.label as bank');
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function orderBy($columnName, $orderBy)
    {

    }

    public function setInvoice($invoiceId)
    {
        $this->query->where('invoice_id', $invoiceId);
    }

    public function setKeyword($keyword)
    {
        
    }

    public function get()
    {
        // var_dump($this->query->toSql()); die;
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}