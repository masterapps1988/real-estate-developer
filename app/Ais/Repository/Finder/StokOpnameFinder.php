<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\TheBadusLibs\Interfaces\IFinder;
use App\Ais\Ais;

use App\StokOpname as Model;

class StokOpnameFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = Model::leftjoin('project', 'project.id', '=',
                'stok_opname.project_id');
        $this->query->leftjoin('user', 'user.id', '=', 'stok_opname.user_id');

        $this->query->select('stok_opname.*', 'project.name as project',
                'project.id as project_id', 'user.name as user');
    }

    public function setStatusIdGroup($statusId)
    {
        $this->query->whereIn('stok_opname.status_id', $statusId);
    }

    public function showJobOnly()
    {
        $this->query->whereNotNull('user_id');
    }

    public function showStokOnly()
    {
        $this->query->whereNull('user_id');
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'ref_no':
                $this->query->orderBy('stok_opname.ref_no', $orderBy);
                break;
        }
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'stok_opname.ref_no',
                        'stok_opname.created',
                        'project.name',
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }

    public function get()
    {
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
