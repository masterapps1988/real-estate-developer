<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;
use App\Quotation;

class ScheduleFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = Quotation::whereNotNull('quotation.schedule_status_id');

        $this->query->join('customer', 'quotation.customer_id', '=', 'customer.id');
        $this->query->join('city', 'quotation.city_id', '=', 'city.id');
        $this->query->join('category', 'quotation.status_id', '=', 'category.id');

        $this->query->select('quotation.id', 'quotation.ref_no', 'quotation.name',
                'quotation.status_id', 'category.label AS status_label',
                'customer.name AS customer_name', 'city.name AS city_name',
                'quotation.created');
    }

    public function setStatusId($statusId)
    {
        $this->query->where('schedule_status_id', $statusId);
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'ref_no':
                $this->query->orderBy('quotation.ref_no', $orderBy);
                break;

            case 'name':
                $this->query->orderBy('quotation.name', $orderBy);
                break;

            case 'created':
                $this->query->orderBy('quotation.created', $orderBy);
                break;    
        }
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'quotation.ref_no',
                        'quotation.name',
                        'customer.name',
                        'city.name'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }
    
    public function get()
    {
        // var_dump($this->query->toSql()); die;
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
