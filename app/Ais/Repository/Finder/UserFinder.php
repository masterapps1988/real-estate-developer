<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

use App\User as UserModel;
use App\Category;

class UserFinder implements IFinder
{
    private $query;
    private $isDistributorOnly;
    private $page;
    private $table;

    public function __construct()
    {
        $this->page = null;
        $this->query = DB::table('user');
        $this->isDistributorOnly = null;
    }

    public function exceptUserId($userId)
    {
        $this->query->where('user.id', '!=', $userId);
    }

    public function exceptRoles($roles)
    {
        $roleIdList = Category::select('id')
                ->where('group_by', 'role')
                ->whereNotIn('name', $roles)
                ->get()
                ->pluck('id');

        if(!empty($roleIdList)) {
            $this->query->whereIn('role_id', $roleIdList);
        }
    }

    public function Roles($roles)
    {
        $roleIdList = Category::select('id')
                ->where('group_by', 'role')
                ->whereIn('name', $roles)
                ->get()
                ->pluck('id');

        if(!empty($roleIdList)) {
            $this->query->whereIn('role_id', $roleIdList);
        }
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function storeOnly()
    {
        $this->isDistributorOnly = false;
        $this->query->whereNotNull('reseller_id');
    }

    public function distributorOnly()
    {
        $this->isDistributorOnly = true;
        $this->query->whereNull('reseller_id');
    }

    public function orderBy($columnName, $orderBy)
    {
        $this->joinCategory();

        switch($columnName) {
            case 'role':
                $this->query->orderBy('category.name', $orderBy);
                break;

            case 'username':
                $this->query->orderBy('user.username', $orderBy);
                break;
                
            case 'name':
                $this->query->orderBy('user.name', $orderBy);
                break;        
        }

    }

    public function setKeyword($keyword)
    {
        $list = explode(' ', $keyword);
        $list = array_map('trim', $list);

        $this->query->where(function($query) use ($list) {
            foreach($list as $x) {
                $pattern = '%' . $x . '%';
                $query->orWhere('user.name', 'like', $pattern)
                    ->orWhere('user.email', 'like', $pattern)
                    ->orWhere('user.username', 'like', $pattern)
                    ->orWhere('category.name', 'like', $pattern);
            }
        });
    }

    public function get()
    {
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

    private function joinCategory()
    {
        $this->query->join('category', 'user.role_id', '=', 'category.id');
        $this->query->select('user.*');
    }

}
