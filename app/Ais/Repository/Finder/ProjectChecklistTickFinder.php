<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

class ProjectChecklistTickFinder implements IFinder
{
    private $query;
    private $page;

    public function __construct()
    {
        $this->page = null;
        $this->query = DB::table('project_checklist_detail');
        $this->query->leftjoin('project_checklist', 'project_checklist_detail.project_checklist_id', '=', 'project_checklist.id');
        $this->query->leftjoin('user','project_checklist_detail.user_id','=','user.id');

        $this->query->select('project_checklist_detail.*','project_checklist.name as pcname','user.username as username','user.name as uname','user.id as user_id');
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'ref_no':
                $this->query->orderBy('purchase_order.ref_no', $orderBy);
                break;
            case 'project':
                $this->query->orderBy('project.name', $orderBy);
                break;
        }
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setProject($projectId)
    {
        $this->query->where('project_checklist.project_id', $projectId);
    }

    public function setKeyword($keyword)
    {
        if(!empty($keyword)) {
            $this->query->where(function($query) use ($keyword) {
                // Split keyword first
                $listKeyword = explode(' ', $keyword);
                $listKeyword = array_map('trim', $listKeyword);

                foreach($listKeyword as $keyword) {
                    $pattern = '%' . $keyword . '%';
                    $columnList = [
                        'purchase_order.ref_no',
                        'project.name'
                    ];

                    foreach($columnList as $x) {
                        $query->orWhere($x, 'like', $pattern);
                    }
                }
            });
        }
    }

    public function get()
    {
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
