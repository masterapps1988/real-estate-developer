<?php
namespace App\Ais\Repository\Finder;

use Illuminate\Pagination\LengthAwarePaginator;
use DB;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IFinder;

class StockFlowFinder implements IFinder
{
    private $query;
    private $page;
    private $isList;

    public function __construct()
    {
        $this->isList = false;
        $this->page = null;
        $this->query = DB::table('view_stock_flow AS t');
        $this->query->join('item as i', 'i.id', '=', 't.item_id');
        $this->query->join('category as c', 'c.id', '=', 'i.item_category_id');
        $this->query->select('t.*', 'i.unit as unit', 'i.name as item_name', 'c.name as item_category_name');
    }

    // If null = all page
    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setDateFrom(\DateTime $from)
    {
        $this->query->where('t.created', '>=', $from);
    }

    public function setDateTo(\DateTime $to)
    {
        $this->query->where('t.created', '<=', $to);
    }

    public function setItem($item_id)
    {
        $this->query->where('t.item_id', $item_id);
    }

    public function setCategory($category)
    {
        $this->query->where('t.category', $category);
    }

    public function setProject($project_id)
    {
        $this->query->where('t.project_id', $project_id);
    }

    public function orderBy($columnName, $orderBy)
    {
        switch($columnName) {
            case 'date':
                $this->query->orderBy('t.created', $orderBy);
                break;    
        }
    }

    public function setKeyword($keyword)
    {

    }

    public function get()
    {
        $this->query->orderBy('t.created', 'desc');
        // var_dump($this->query->toSql()); die;
        $result = null;
        switch($this->page) {
            case 'all':
                $rows = $this->query->get();
                $n = max(1, count($rows)); // Prevent to divined by zero

                $result = new LengthAwarePaginator($rows, $n, $n);
                break;

            default:
                $result = $this->query->paginate(Ais::getPaginate());
                break;
        }

        return $result;
    }

}
