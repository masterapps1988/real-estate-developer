<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Support\Arrayable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;
use App\TheBadusLibs\Helper\NumberFormat;

use App\ProjectChecklistDetail as Model;
use App\ProjectChecklist as ProjectChecklistModel;
use App\User as UserModel;

class ProjectChecklistDetail extends AbstractRepository
{
    
    // Hold all workers
    private $detail = [];
    private $checked = [];
    private $unchecked = [];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function check($id)
    {
        $this->checked[] = $id;
    }

    public function unchecked($id)
    {
        $this->unchecked[] = $id;
    }
    
    public function addDetail($id, $is_checked)
    {
        $this->detail[] = [
            'id' => $id,
            'is_checked' => $is_checked
        ];
    }

    private function prepareValidation()
    {

    }

    public function save()
    {
        $this->prepareValidation();

        // Unchecked
        Model::whereIn('project_checklist_id', $this->unchecked)
                ->delete();

        foreach($this->checked as $x) {
            $checklist = Model::findOrNew($x);
            $checklist->project_checklist_id = $x;
            $checklist->user_id = $this->model->user_id;

            $checklist->save();
        }
    }

}
