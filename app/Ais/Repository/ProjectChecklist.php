<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Support\Arrayable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;
use App\Ais\Repository\Project;
use App\Ais\Repository\PurchaseOrder;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;
use App\TheBadusLibs\Helper\NumberFormat;

use App\Project as Model;
use App\ProjectChecklist as ProjectChecklistModel;
use App\ProjectChecklistDetail as ProjectChecklistDetailModel;

class ProjectChecklist extends AbstractRepository implements Arrayable
{
    private $detail = [];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function addDetail($id, $name, $position)
    {
        $this->detail[] = [
            'id' => $id,
            'name' => $name,
            'position' => $position
        ];
    }

    public function validateName()
    {
        
        $nameList = collect($this->detail)->pluck('name');

        $fields = [
            'detail' => $this->detail,
            'name_must_be_unique' => count($this->detail) ==
                count(array_unique($nameList->toArray()))
        ];

        $rules = [
            'name_must_be_unique' => 'in:1',
            'detail.*.name' => 'required',
            'detail.*.position' => 'required'
        ];

        $messages = [
            'name_must_be_unique.in' => 'Nama tidak boleh kembar'
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    private function prepareValidation()
    {
        $this->validateName();
    }

    public function deleteDetail($id)
    {
        // print_r($param['id']);
        ProjectChecklistDetailModel::where('project_checklist_id',$id)
                ->delete();
        ProjectChecklistModel::where('id',$id)
                ->delete();

    }

    public function save()
    {
        $this->prepareValidation();

        // Save quotation detail
        foreach($this->detail as $x) {
            $checklist = ProjectChecklistModel::findOrNew($x['id']);
            $checklist->project_id = $this->model->id;
            $checklist->name = $x['name'];
            $checklist->position = $x['position'];

            $checklist->save();
        }
    }

    public function toArray()
    {
        $list = ProjectChecklistModel::where('project_id', $this->model->id)
                ->orderBy('position', 'asc')
                ->get();

        $detail = [];
        foreach($list as $x) {
            $detail[] = [
                'id' => $x['id'],
                'name' => $x['name'],
                'position' => $x['position']
            ];
        }

        $return = [
            'project_id' => $this->model->id,
            'project_name' => $this->model->name,
            'detail' => $detail
        ];

        return $return;
    }
}
