<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Contracts\Support\Arrayable;

use App\Ais\Ais;
use App\Ais\Repository\Finder\StockFinder;
use App\Ais\Repository\JobOpnameMedia;
use App\Ais\Helpers\ResourceUrl;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Helper\DateFormat;

use App\StokOpname as Model;
use App\StokOpnameDetail as JobOpnameDetail;
use App\JobOpnameMedia as JobOpnameMediaModel;
use App\Item;
use App\Project as ProjectModel;
use App\Category as CategoryModel;
use App\User as UserModel;
use App\QuotationDetail as QuotationDetailModel;
use App\Invoice as InvoiceModel;
use App\Category as Status;

class JobOpname extends AbstractRepository implements Arrayable
{
    const RETENTION = 0.1; // 10%

    private $details = [];
    private $images = [];
    
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function getDetail()
    {
        return $this->details;
    }

    protected function validateJobOpname()
    {
        // Validate value
        $fields = [
            'project_id' => $this->model->project_id,
            'user_id' => $this->model->user_id,
            'ref_no' => $this->model->ref_no,
            'is_stock' => $this->model->is_stock,
            'created' => $this->model->created
        ];

        $rules = array(
            'project_id' => 'required|exists:project,id',
            'user_id' => 'required|exists:user,id',
            'ref_no' => 'required|unique:stok_opname,ref_no,' . $this->model->id,
            'is_stock' => 'required|in:0',
            'created' => 'required'
        );

        if (!$this->model->id) {
            $jobOpname = Model::where('project_id', $this->model->project_id)
                                ->where('user_id', $this->model->user_id)
                                ->where('created', $this->model->created)
                                ->count();
            if ($jobOpname > 0) {
                throw new NotFoundHttpException("Opname pekerjaan dengan tanggal " . $this->model->created . " sudah pernah dibuat");
            }
        }

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

    protected function validateDetail()
    {
        // Validate value
        $fields = [
            'detail' => $this->details
        ];

        $rules = array(
            'detail.*.id' => 'nullable',
            'detail.*.stok_opname_id' => 'nullable',
            'detail.*.quotation_detail_id' => 'required',
            'detail.*.item_id' => 'required',
            'detail.*.qty' => 'required',
        );

        foreach ($this->details as $x) {
            $item = Item::find($x['item_id']);

            if ($item->user_id != $this->model->user_id) {
                throw new NotFoundHttpException("User tidak valid");
            }

            $totalProgress = $x['qty'] + $x['qty_before'];

            $target = QuotationDetailModel::find($x['quotation_detail_id']);

            if ($totalProgress > $target->qty) {
                throw new NotFoundHttpException("Total progress melebihi target");
            }
        }

        $validator = Validator::make($fields, $rules);
        $this->addValidator($validator);
    }

    public function prepareValidation()
    {
        $this->validateJobOpname();
        $this->validateDetail();
    }

    public function deleteImage($id)
    {
        $row = JobOpnameMediaModel::find($id);
        if(!empty($row)) {
            $repo = new JobOpnameMedia($row);
            $repo->delete();
        }
    }

    /*
     * @params int stok_opname.id
     * @params File binary
     */
    public function addImage($id, $image)
    {
        $this->images[] = [
            'id' => $id,
            'image' => $image
        ];
    }

    public function addDetail($id, $quotationDetailId, $progress, $real)
    {
        $this->_addDetail($id, $this->model->id, $quotationDetailId, $progress, $real);
    }

    protected function _addDetail($id, $stokOpnameId, $quotationDetailId, $progress, $real)
    {
        if ($progress != 0) {

            $quotationDetail = QuotationDetailModel::find($quotationDetailId);

            $this->details[] = [
                'id' => $id,
                'stok_opname_id' => $stokOpnameId,
                'quotation_detail_id' => $quotationDetailId,
                'item_id' => $quotationDetail->item_id,
                'qty' => $progress,
                'qty_before' => $real
            ];
        }
    }

    public function deleteDetail($id)
    {
        $detail = JobOpnameDetail::where('id', $id)
                ->where('stok_opname_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_job_opname' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_job_opname' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        JobOpnameDetail::where('id', $id)->delete();
    }

    public function delete()
    {
        // Delete receipt detail by receipt id
        JobOpnameDetail::where('stok_opname_id', $this->model->id)
                ->delete();

        $media = JobOpnameMediaModel::where('stok_opname_id', $this->model->id)
                                        ->get();

        foreach ($media as $x) {
            $this->deleteImage($x->id);
        }

        // Delete child quotation
        $this->model->delete();
    }

    public function saveDetail()
    {
        // Save receipt detail
        foreach($this->details as $x) {
            $detailModel = JobOpnameDetail::findOrNew($x['id']);
            $detailModel->stok_opname_id = $this->model->id;
            $detailModel->quotation_detail_id = $x['quotation_detail_id'];
            $detailModel->item_id = $x['item_id'];
            $detailModel->qty_before = 0;
            $detailModel->qty = $x['qty'];
            $detailModel->save();
        }
    }

    public function saveImages()
    {
        // Save images
        foreach($this->images as $x) {
            $image = JobOpnameMediaModel::findOrNew($x['id']);
            $image->stok_opname_id = $this->model->id;
            $repoMedia = new JobOpnameMedia($image);
            $repoMedia->setImage($x['image']);
            $repoMedia->save();
        }
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        parent::save();
        $this->saveDetail();
        $this->saveImages();
    }

    public function toArray()
    {
        $return = $this->model->toArray();

        // Get user role
        $user = UserModel::find($this->model->user_id);

        // Get All Stock
        $stockFinder = new StockFinder();
        $stockFinder->setProjectId($this->model->project_id);
        $stockFinder->setType($user->category->name);
        $stockFinder->setUserId($user->id);
        $stock = $stockFinder->get();

        $detail = [];
        foreach ($stock as $x) {
            $progress = JobOpnameDetail::where('stok_opname_id', $this->model->id)
                                        ->where('quotation_detail_id', $x->quotation_detail_id)
                                        ->where('item_id', $x->item_id)
                                        ->first();

            $qty = 0;
            $qty_before = 0;
            $id = null;

            if ($progress) {
                $id = $progress['id'];
                $qty = $progress['qty'];
                $qty_before = $progress['qty_before'];
            }

            $target = QuotationDetailModel::find($x->quotation_detail_id);

            $detail[] = [
                'id' => $id,
                'group_by' => $x->group_by,
                'item_id' => $x->item_id,
                'name' => $x->name,
                'price' => $x->price,
                'project_id' => $x->project_id,
                'quotation_detail_id' => $x->quotation_detail_id,
                'unit' => $x->unit,
                'target' => $target->qty,
                'done' => $qty,
                'real' => $qty_before
            ];
        }

        $return['status'] = $this->model->status;
        $return['detail'] = $detail;

        //Find Invoice from opname once a week
        $invoice = InvoiceModel::where('project_id', $this->model->project_id)
                                ->where('user_id', $this->model->user_id)
                                ->get();

        $invoiceState = 0;

        $model = new \DateTime($this->model->created);

        foreach ($invoice as $x) {
            $opname = new \DateTime($x->created);

            if ($opname->format('W') == $model->format('W')) {
                $invoiceState = 1;
                $return['invoice_id'] = $x->id;
            }
        }

        $return['is_invoice_created'] = $invoiceState;

        // Get images
        $images = $this->model->media;
        $list = [];
        foreach($images as $key => $value) {
            $list[] = [
                'id' => $value['id'],
                'image' => ResourceUrl::opname($value['name']),
                'thumb_image' => ResourceUrl::thumbOpname($value['name'])
            ];
        }
        $return['images'] = $list;
        $return['total'] = $this->getTotal();

        return $return;
    }

    public function getTotal()
    {
        return (float) JobOpnameDetail::where('stok_opname_id', $this->model->id)
                ->join('quotation_detail', 'quotation_detail.id', '=',
                        'stok_opname_detail.quotation_detail_id')
                ->sum(DB::raw('stok_opname_detail.qty * quotation_detail.price'));
    }

    public static function substractWithRetention($amount)
    {
        return $amount * (1 - self::RETENTION);
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Status::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Status::STATUS_DRAFT &&
                    $statusId == Status::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }

}