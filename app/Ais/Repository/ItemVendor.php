<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\Item as ItemModel;
use App\User as UserModel;

class ItemVendor extends Item
{
    public function getItemCategoryId()
    {
        return ItemModel::VENDOR;
    }

    public function extraValidation()
    {
        // Validation - More detail(with query if needed)
        // Validation value without query
        $fields = [
            'user_must_vendor' => $this->model->user->role_id == UserModel::ROLE_VENDOR,
        ];

        $rules = [
            'user_must_vendor' => 'in:1'
        ];

        $validator = Validator::make($fields, $rules);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }
}
