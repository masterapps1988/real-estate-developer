<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Ais\Ais;
use App\Ais\Repository\Project;
use App\Ais\Repository\Receipt;

use App\TheBadusLibs\AuthHelper;
use App\TheBadusLibs\Interfaces\IRepository;
use App\TheBadusLibs\Helper\DateFormat;

use App\Project as ProjectModel;
use App\PurchaseOrderPayment as PurchaseOrderPaymentModel;
use App\PurchaseOrder as Model;
use App\PurchaseOrderDetail;
use App\Receipt as ReceiptModel;
use App\Category as Status;
use App\PricePlan as PricePlanModel;

class PurchaseOrder extends AbstractRepository
{
    private $detail = [];
    private $project = null;

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function addProject(Project $project)
    {
        $this->project = $project;
    }

    public function addDetail($id, $itemId, $name, $unit, $price, $qty)
    {
        $this->detail[] = [
            'id' => $id,
            'item_id' => $itemId,
            'name' => $name,
            'unit' => $unit,
            'price' => $price,
            'qty' => (float) $qty
        ];
    }

    public function validatePO()
    {
        // Validate value
        $fields = [
            'ref_no' => $this->model->ref_no,
            'project_id' => $this->model->project_id,
            'status_id' => $this->model->status_id,
            'is_open' => $this->model->is_open,
            'notes' => $this->model->notes,
            'created' => $this->model->created,
        ];

        $rules = array(
            'ref_no' => 'nullable|max:10|unique:purchase_order,ref_no,' . $this->model->id,
            'project_id' => 'required|exists:project,id',
            'status_id' => 'required|in:' . implode(',', Model::VALID_STATUS),
            'is_open' => 'required|boolean',
            'notes' => 'nullable',
            'created' => 'date'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }
    
    public function validateDetail()
    {
        // Validate value
        $fields = [
            'detail_at_least_one' => count($this->detail),
            'detail' => $this->detail,
        ];

        // Regular validation
        $rules = [
            'detail_at_least_one' => 'required|numeric|min:1',

            'detail.*.item_id' => 'required|exists:item,id',
            'detail.*.name' => 'required',
            'detail.*.unit' => 'required',
            'detail.*.price' => 'required|min:0'
        ];

        // Project should not null. It should already checked before get in this method
        $project = new Project(ProjectModel::find($this->model->project_id));
        $availableItems = collect($project->getAvailableItems());
        foreach($this->detail as $key => $val) {
            $item = $availableItems->where('item_id', $val['item_id'])->first();

            // If detail is already saved, then we need to substract
            // So we could get actual max value.
            if(!empty($val['id'])) {
                $oldDetail = PurchaseOrderDetail::find($val['id']);
                $max = $item['qty'] + $oldDetail->qty; // Rollback to before inserted
            } else
                $max = $item['qty'];

            $rules['detail.' . $key . '.qty'] = 'required|numeric|between:0,' . $max;
        }

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

    // Validate $this->project is valid or not
    public function validateProject()
    {
        if(!empty($this->project)) {
            // Validate value
            $fields = [
                // if $this->model->project_id is not empty, $this->project->id
                // must same
                'project_id_not_equal' => !empty($this->model->project_id) &&
                    $this->project->id == $this->model->project_id,
                'project_must_open' => $this->project->status_id
            ];

            $rules = array(
                'project_id_not_equal' => 'in:1',
                'project_must_open' => 'in:' . Status::STATUS_OPEN
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }
    }
    
    public function validateItemNotExceeded()
    {
        
    }

    private function prepareValidation()
    {
        $this->validatePO();
        $this->validateDetail();
        $this->validateProject();
        $this->validateItemNotExceeded();
    }

    public function saveDetail()
    {
        // Save po detail
        foreach($this->detail as $x) {
            $price = $x['price'];

            // If this is not already purchase order. Auto set price from price plan
            if(!$this->isPurchaseOrder())
                $price = $this->getItemPrice($x['item_id']);

            $detailModel = PurchaseOrderDetail::findOrNew($x['id']);
            $detailModel->purchase_order_id = $this->model->id;
            $detailModel->item_id = $x['item_id'];
            $detailModel->name = $x['name'];
            $detailModel->unit = $x['unit'];
            $detailModel->qty = $x['qty'];
            $detailModel->price = $price;
            $detailModel->save();

            // Check price, if change then update to price history
        }
    }

    public function save()
    {
        // print_r($this->model->id);exit;
        if(!empty($this->model->change_notes))
            $this->prepareValidation();

        // If this po is empty, or create new, then is should auto set to pending
        // All purchase order should approved by higher roles.
        if(empty($this->model->id))
            $this->model->status_id = Status::STATUS_DRAFT;

        parent::save();
        $this->saveDetail();
    }

    public function validateReceiptBeforeDelete()
    {
        $name = 'Purchase Order';
        if(!$this->isPurchaseOrder())
            $name = 'Surat Permintaan';

        $errorMessage = ':name: tidak dapat dihapus. Silakan hapus LPB untuk :name: yang bersangkutan';
        $errorMessage = str_replace(':name:', $name, $errorMessage);

        // Validate purchase order id MUST not exists in any receipt table
        $fields = [
            'id' => $this->model->id
        ];

        $rules = [
            'id' => 'unique:receipt,purchase_order_id',
        ];

        $messages = [
            'id.unique' => $errorMessage
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);
    }

    public function isPurchaseOrder()
    {
        return $this->model->status_id == Status::STATUS_APPROVED;
    }

    public function delete()
    {
        // Validation goes here. Throw and validationexception if not valid
        // $this->validateReceiptBeforeDelete();
        $list = ReceiptModel::where('purchase_order_id', $this->model->id)->get();
        foreach($list as $x) {
            $row = new Receipt($x);
            $row->delete();
        }

        // Delete purchase order payment
        PurchaseOrderPaymentModel::where('purchase_order_id', $this->model->id)
                ->delete();

        // Delete po detail by po id
        PurchaseOrderDetail::where('purchase_order_id', $this->model->id)
                ->delete();

        // Delete child po
        $this->model->delete();
    }

    public function deleteDetail($id)
    {
        $detail = PurchaseOrderDetail::where('id', $id)
                ->where('purchase_order_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_purchase_order' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_purchase_order' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        // Check if this quotation is closed then you couldn't delete
        PurchaseOrderDetail::where('id', $id)->delete();
    }

    public function getTotal()
    {        
        $total = PurchaseOrderDetail::where('purchase_order_id', $this->model->id)
                ->leftJoin('purchase_order','purchase_order_detail.purchase_order_id','=','purchase_order.id')
                ->select('purchase_order_detail.*','purchase_order.is_tax as is_tax')->sum(DB::raw('purchase_order_detail.qty * purchase_order_detail.price'));
        
        return $total;
    }

    public function getTotalValidation()
    {        
        $po = PurchaseOrderDetail::where('purchase_order_id', $this->model->id)
                ->leftJoin('purchase_order','purchase_order_detail.purchase_order_id','=','purchase_order.id')
                ->select('purchase_order_detail.*','purchase_order.is_tax as is_tax');
        $sum = $po->sum(DB::raw('purchase_order_detail.qty * purchase_order_detail.price'));
        $ppn = $po->pluck('is_tax')->first() ? ($sum * 10 / 100 ) : 0;
        
        $total = (float)$ppn + (float)$sum;
        
        return $total;
    }

    public function setStatus($statusId)
    {
        $validator = null;
        if($statusId == Status::STATUS_PENDING) {
            // Validation
            $fields = [
                'draft_to_pending' =>
                    $this->model->status_id == Status::STATUS_DRAFT &&
                    $statusId == Status::STATUS_PENDING
            ];

            $rules = array(
                'draft_to_pending' => 'in:1'
            );

            $validator = Validator::make($fields, $rules);
            self::validOrThrow($validator);
        }

        $this->model->status_id = $statusId;
        $this->model->save();
    }

    public function setTax($isTax)
    {
        // Set Tax to 0 / 1
        $this->model->is_tax = $isTax;
        $this->model->save();
    }

    public function getAvailableItems()
    {
        $purchaseOrderId = $this->model->id;

        $sql = <<<EOT
select po.item_id, i.name, IF((po.qty - r.qty) IS NULL, po.qty, (po.qty - r.qty)) qty, po.unit from (
	-- Get purhcase order items
	select item_id, sum(qty) qty, unit from purchase_order po
	join purchase_order_detail pod on po.id = pod.purchase_order_id
    where po.id = :purchase_order_id:
	group by item_id, unit
) po
left join (
	-- Get po items, include SP and PO doesn't really matter
	select item_id, sum(qty) qty from receipt r
    join receipt_detail rd on r.id = rd.receipt_id
	where r.purchase_order_id = :purchase_order_id:
	group by item_id
) r ON r.item_id = po.item_id
join item i on i.id = po.item_id;
EOT;

        $sql = str_replace(':purchase_order_id:', $purchaseOrderId, $sql);

        $rows = DB::select(DB::raw($sql));

        $list = [];
        foreach($rows as $key => $val) {
            $rows[$key]->qty = (float) $rows[$key]->qty;

            $list[] = (array) $rows[$key];
        }

        return $list;
    }

    public function getReceiveItems($itemId)
    {
        $receive = ReceiptModel::join('receipt_detail', 'receipt.id', '=', 'receipt_detail.receipt_id')
                    ->where('receipt.purchase_order_id', $this->model->id)
                    ->where('receipt_detail.item_id', $itemId)
                    ->select(DB::raw('IFNULL(SUM(Qty), 0) as qty'))->first();
        
        return $receive;
    }

    public function getStatusItems($itemId)
    {
        $status = ReceiptModel::join('receipt_detail', 'receipt.id', '=', 'receipt_detail.receipt_id')
                    ->where('receipt.purchase_order_id', $this->model->id)
                    ->where('receipt_detail.item_id', $itemId)
                    ->select(DB::raw('IFNULL(SUM(receipt.status_id), 0) as status_id'))->first();
        $status= Status::find($status->status_id) ? Status::find($status->status_id)['label'] : 'Draft';
        return $status;
    }

    public function downloadSp()
    {
        $date = new \Datetime;
        $filename = 'sp_' . $this->model->ref_no.'_'. $date->format('Ymdhs') . '.pdf';

        $data['row'] = Model::join('project', 'project.id', '=', 'purchase_order.project_id')
                        ->select('purchase_order.*', 'project.name as project_name')
                        ->where('purchase_order.id', $this->model->id)
                        ->first();

        $data['detail'] = \App\PurchaseOrderDetail::where('purchase_order_id', $this->model->id)->get();

        $pdf = PDF::loadView('report.sp', $data);
        $pdf->save('tmp/' . $filename);

        return $filename;
    }

    public function downloadPo()
    {
        $date = new \Datetime;
        $filename = 'po_' . $this->model->ref_no.'_'. $date->format('Ymdhs') . '.pdf';

        $data['row'] = DB::table('purchase_order')
                        ->join('project', 'project.id', '=', 'purchase_order.project_id')
                        ->select('purchase_order.*', 'project.name as project_name')
                        ->where('purchase_order.id', $this->model->id)
                        ->first();
        
        $tanggal = \DateTime::createFromFormat('Y-m-d H:i:s', $data['row']->created);
        $data['tanggal'] = DateFormat::shortDate($tanggal);
        
        $data['detail'] = \App\PurchaseOrderDetail::where('purchase_order_id', $this->model->id)->get();
        $data['total'] = $this->getTotal($this->model->id);
        $data['ppn'] = ($data['total'] * 10) / 100;

        $pdf = PDF::loadView('report.po', $data);
        $pdf->save('tmp/' . $filename);

        return $filename;
    }

    private function getItemPrice($itemId)
    {
        $project = ProjectModel::find($this->model->project_id);
        $pricePlan = DB::table('quotation_detail')
                ->where('quotation_id', $project->price_plan_id)
                ->where('item_id', $itemId)->first();

        return $pricePlan->price;
    }

}
