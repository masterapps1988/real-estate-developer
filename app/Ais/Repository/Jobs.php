<?php
namespace App\Ais\Repository;

use Hash;
use DB;
use PDF;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\Item as Model;
use App\ItemDetail;

use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

/* Quotation is for quotation only(penawaran/hpp 1).
 *
 */
class Jobs extends AbstractRepository
{
    private $detail = [];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function validateDetail()
    {
        // Validate value
        $fields = [
            'detail' => $this->getDetail()
        ];

        $rules = array(
            'detail.*.id' => 'nullable',
            'detail.*.item_id_detail' => 'required|exists:item,id',
            'detail.*.qty' => 'required',
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

    public function addItem($id, $itemId, $qty)
    {
        $this->_addDetail($id, $itemId, $qty);
    }

    protected function _addDetail($id, $itemId, $qty)
    {
        $this->detail[] = [
            'id' => $id,
            'item_id_detail' => $itemId,
            'qty' => $qty
        ];
    }

    public function save()
    {
        $this->prepareValidation();
        // Do validation and save model first

        parent::save();
        if (!empty($this->detail)) {        
            $this->saveDetail();
        }
    }

    public function getDetail()
    {
        return $this->detail;
    }

    // Override save detail from parent
    public function saveDetail()
    {
        if(!empty($this->detail)) {
            // Save quotation detail
            foreach($this->getDetail() as $x) {
                $detailModel = ItemDetail::findOrNew($x['id']);
                $detailModel->item_id = $this->model->id;
                $detailModel->item_id_detail = $x['item_id_detail'];
                $detailModel->qty = $x['qty'];

                $detailModel->save();

                // Check price, if change then update to price history
            }
        }
    }

    public function prepareValidation()
    {
        $this->validateJobs();
        $this->validateDetail();
    }

    private function validateJobs()
    {
        // Validate value
        $fields = [
            'ref_no' => $this->model->ref_no,
            'name' => $this->model->name,
            'qty' => $this->model->qty,
            'unit' => $this->model->unit
        ];

        $rules = array(
            'ref_no' => 'required|unique:item,ref_no,' . $this->model->id,
            'name' => 'required',
            'unit' => 'nullable',
            'qty' => 'nullable'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);
    }

    public function delete()
    {
        // Validation
        $errorMessage = 'Pekerjaan sedang digunakan pada penawaran/hpp 2';
        $fields = [
            'id' =>  $this->model->id,
            'job_not_exists_in_quotation' => $this->model->id
        ];

        $rules = array(
            'id' => 'required',
            'job_not_exists_in_quotation' => 'unique:quotation_detail,item_id',
        );

        $messages = [
            'job_not_exists_in_quotation.unique' => $errorMessage
        ];

        $validator = Validator::make($fields, $rules, $messages);
        self::validOrThrow($validator);

        // Delete receipt detail by receipt id
        ItemDetail::where('item_id', $this->model->id)
                ->delete();

        // Delete child quotation
        $this->model->delete();
    }
    
    public function deleteDetail($id)
    {
        $detail = ItemDetail::where('id', $id)
                ->where('item_id', $this->model->id)
                ->first();

        // Validation
        $fields = [
            'id' => $id,
            'detail_of_item' => !empty($detail)
        ];

        $rules = array(
            'id' => 'required',
            'detail_of_item' => 'in:1'
        );

        $validator = Validator::make($fields, $rules);
        self::validOrThrow($validator);

        ItemDetail::where('id', $id)
                ->delete();
    }

}
