<?php
namespace App\Ais\Repository;

use Hash;
use Auth as AuthLaravel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use App\TheBadusLibs\AuthHelper;

use App\Category as Model;
use App\RolePermission as RoleModel;
use App\Ais\Ais;
use App\TheBadusLibs\Interfaces\IRepository;

class Category implements IRepository
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function delete()
    {
        if($this->model->group_by == 'unit')
            $this->model->delete();
    }

    public function getModel()
    {
        return $this->model;
    }
    
    public function save()
    {
        // Validation
        $fields = [
            'name' => $this->model->name,
            'label' => $this->model->label,
            'notes' => $this->model->notes,
            'group_by' => $this->model->group_by
        ];

        $rules = array(
            'label' => 'required|max:45',
            'group_by' => 'required'
        );

        $validator = Validator::make($fields, $rules);
        
        if($this->model->group_by != 'unit')
            throw new ValidationException($validator);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // If name is empty then just take from label
        if(empty($this->model->name)) {
            $name = strtolower($this->model->label);
            $name = str_replace(' ', '-', $name);
            $this->model->name = $name;
        }

        $this->model->save();
    }

    public static function getList()
    {
        return Model::paginate(Ais::getPaginate());
    }

    public function getChecklistRoleSummary()
    {
        $category = Model::where('group_by', 'permission')->get();
        
        $list = [];
        foreach($category as $x) {
            $listChecklistModel = RoleModel::where(['permission_id' => $x->id,'role_id' => $this->model->id])->count();
            $list[] = [
                'id' => $x->id,
                'label' => $x->label,
                'is_checked' => $listChecklistModel ? true : false
            ];
        }
        
        return $list;

    }
}
