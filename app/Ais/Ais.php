<?php

namespace App\Ais;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Support\Facades\Validator;

use Auth;

use App\TheBadusLibs\AuthHelper;

use App\User;
use App\Category as Role;

class Ais
{
    public static function init()
    {
        $ds = DIRECTORY_SEPARATOR;
        $list = [
            $ds . 'tmp',
            $ds . 'assets',
            $ds . 'assets' . $ds . 'opname'
        ];

        foreach($list as $x) {
            // Check if $x folder is exists
            $path = public_path() . $x;
            if(!file_exists($path) && !mkdir($path, 0755, true)) {
                die('Cannot make ' . $path . ' folder');
            }
        }
    }

    public static function forgotPassword()
    {

    }

    public static function login($username, $password)
    {
        // Validation
        $fields = [
            'username' => $username,
            'password' => $password
        ];

        $rules = array(
            'username' => 'required',
            'password' => 'required'
        );

        $validator = Validator::make($fields, $rules);

        // Validate the input and return correct response
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if (Auth::attempt(['username' => $username, 'password' => $password])) {

            $exceptRole = ['mandor', 'vendor', 'supplier'];

            $roleId = [];
            foreach ($exceptRole as $x) {
                $roleId[] = Role::getRoleIdByName($x);
            }

            $user = Auth::user();

            if (!in_array($user->role_id , $roleId)) {
                // Generate API Token
                $user->generateApiToken();

                // Set cookies
                AuthHelper::setCookie($user->api_token);

                return $user;
            } else {
                throw new UnauthorizedHttpException(null, "Anda tidak memiliki akses untuk login!");
            }
        }

        throw new UnauthorizedHttpException(null, "Username dan password salah!");
    }

    public static function logout(User $user)
    {
        if(!empty($user)) {
            $user->removeApiToken();
            $user->save();

            // Set cookies
            AuthHelper::setCookie(null);

            Auth::logout($user);

            return $user;
        }

        throw new \Exception("Logout error");
    }

    public static function getPaginate()
    {
        return 10;
    }

    public static function getDateFormat()
    {
        return 'd-m-Y';
    }
}
