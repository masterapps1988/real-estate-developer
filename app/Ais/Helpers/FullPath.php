<?php
namespace App\Ais\Helpers;

use Storage;

// Get full path of resource
class FullPath
{
    const DS = DIRECTORY_SEPARATOR;

    // Get path for test assets folder
    public static function testAssets($filename)
    {
        $ds = self::DS;
        $list = ['tests', 'Assets'];

        $path = base_path() . $ds . implode($ds, $list) . $ds . $filename;
        return $path;
    }

    public static function opname($filename)
    {
        $ds = self::DS;
        $list = ['assets', 'opname'];

        $path = public_path() . $ds . implode($ds, $list) . $ds . $filename;
        return $path;
    }
}
