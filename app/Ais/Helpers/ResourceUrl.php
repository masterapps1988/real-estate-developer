<?php
namespace App\Ais\Helpers;

use Storage;

// Get full path of resource
class ResourceUrl
{
    public static function opname($filename)
    {
        if(!$filename || !self::isExists('opname/' . $filename)) {
            return self::getNoImagePath();
        }

        $path = ':host:/assets/opname/:image-name:';
        $path = str_replace(':host:', env('APP_URL'), $path);
        $path = str_replace(':image-name:', $filename, $path);

        return $path;
    }

    public static function thumbOpname($filename)
    {
        if(!$filename || !self::isExists('opname/thumb_' . $filename)) {
            // $filename = 'no-image.jpg';
            return self::getNoImagePath();
        }

        $path = ':host:/assets/opname/thumb_:image-name:';
        $path = str_replace(':host:', self::getHost(), $path);
        $path = str_replace(':image-name:', $filename, $path);

        return $path;
    }

    public static function isExists($filename)
    {
        $path = public_path() . '/assets/' . $filename;
        return file_exists($path);
    }

    public static function testAssets($filename)
    {
        $path = public_path() . '/assets/' . $filename;
        return path();
    }

    public static function getHost()
    {
        // Useful when you use multiple domain name
        return env('APP_URL');
    }

    public static function getNoImagePath()
    {
        $filename = 'no-image.jpg';
        $path = ':host:/img/:image-name:';
        $path = str_replace(':host:', self::getHost(), $path);
        $path = str_replace(':image-name:', $filename, $path);

        return $path;
    }
}
