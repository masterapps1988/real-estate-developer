<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Category;

class RolePermission extends Model
{
    protected $table = 'role_permission';
}
