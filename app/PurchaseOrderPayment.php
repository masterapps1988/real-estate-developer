<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderPayment extends Model
{
    protected $table = 'purchase_order_payment';

    protected $dates = [
        'created'
    ];

}
