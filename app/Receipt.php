<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\ReceiptDetail;

class Receipt extends Model
{
    protected $table = 'receipt';

    protected $dates = [
        'created'
    ];

    public $timestamps = true;

    public function detail()
    {
        return $this->hasMany(ReceiptDetail::class);
    }
}
