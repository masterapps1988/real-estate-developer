<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\ItemDetail;
use DB;

class Item extends Model
{
    const ITEM = 40;
    const MANDOR = 41;
    const VENDOR = 42;

    protected $table = 'item';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function destroyChild($id)
    {
    	DB::table('item_history')->where('item_id', $id)->delete();
    	DB::table('quotation_detail')->where('item_id', $id)->delete();
    }

    public function detail()
    {
        return $this->hasMany(ItemDetail::class);
    }
}
