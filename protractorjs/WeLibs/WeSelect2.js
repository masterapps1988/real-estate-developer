// Handling auto selection
var WeSelect2 = function() {
    /* Select first unselect item
     * Example:
     * one*
     * two
     * three
     *
     * this class would select two(first unselect)
     *
     * @param select2 container. Ex: select2-role-container
     */
    this.selectFirstUnselectItems = function(id) {
        browser.sleep(500);
        browser.actions().mouseDown(element(by.id(id)), ).perform();
        browser.sleep(1000);

        // Select random element
        element.all(by.css('li.select2-results__option[aria-selected="false"]'))
                .first().click();
    };
};

module.exports = new WeSelect2();