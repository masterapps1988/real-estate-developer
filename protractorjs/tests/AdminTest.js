function setRandomData(detail) {
    detail.setName(faker.name.findName())
    detail.setHp(faker.phone.phoneNumber());
    detail.setAddress(faker.address.streetAddress());
    detail.setCity();
    detail.setUsername(faker.internet.userName());
    detail.setEmail(faker.internet.email());
    detail.setRole();

    var password = faker.internet.password();
    detail.setPassword(password);
    detail.setConfirmPassword(password);
};

describe('Admin Test', function() {
  var adminList = require('../libs/AdminList.js');
  it('list page', function() {
    adminList.go();

    // Check title
    expect(adminList.getTitle()).toContain('List Admin');

    // Check keyword
    var keyword = 'test';
    adminList.setKeyword(keyword);
    expect(adminList.getKeyword()).toContain(keyword);
  });

  it('create new', function() {
    var detail = require('../libs/AdminDetail.js');
    detail.create();

    // Check title
    expect(detail.getTitle()).toContain('Tambah/Ubah Admin');

    // Check cities
    var countCity = detail.countCity();
    expect(countCity).toBeGreaterThan(0);

    setRandomData(detail)
    detail.submit();

    expect(detail.getNotif()).toContain('berhasil');
  });

  it('update', function() {
    adminList.go();

    var detail = adminList.goToFirstAdmin();

    // Check title
    expect(detail.getTitle()).toContain('Tambah/Ubah Admin');

    // Check cities
    var countCity = detail.countCity();
    expect(countCity).toBeGreaterThan(0);

    setRandomData(detail);
    detail.submit();

    expect(detail.getNotif()).toContain('berhasil');
  });
});