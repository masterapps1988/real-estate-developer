function setRandomData(detail) {
    detail.setName(faker.name.findName())
    detail.setAddress(faker.address.streetAddress());
    detail.setHp(faker.phone.phoneNumber());
    detail.setNpwp(faker.random.number());
    detail.setCity();
};

describe('Customer Test', function() {
  it('list page', function() {
    var customerList = require('../libs/CustomerList.js');

    // Check title
    expect(customerList.getTitle()).toContain('List Customer');

    // Check keyword
    var keyword = 'test';
    customerList.setKeyword(keyword);
    expect(customerList.getKeyword()).toContain(keyword);
  });

  it('create new', function() {
    var detail = require('../libs/CustomerDetail.js');
    detail.create();

    // Check title
    expect(detail.getTitle()).toContain('Tambah/Ubah Customer');

    // Check cities
    var countCity = detail.countCity();
    expect(countCity).toBeGreaterThan(0);

    setRandomData(detail)
    detail.submit();

    expect(detail.getNotif()).toContain('Customer telah berhasil tersimpan');
  });

  it('update', function() {
    var customerList = require('../libs/CustomerList.js');

    var detail = customerList.goToFirstCustomer();

    // Check title
    expect(detail.getTitle()).toContain('Tambah/Ubah Customer');

    // Check cities
    var countCity = detail.countCity();
    expect(countCity).toBeGreaterThan(0);

    setRandomData(detail);
    detail.submit();

    expect(detail.getNotif()).toContain('Customer telah berhasil tersimpan');
  });
});