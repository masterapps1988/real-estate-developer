var CustomerDetail = function() {
    this._clear = false;

    // Go to create page
    this.create = function() {
        var route = '/#!/customer/create';
        browser.driver.get(browser.baseUrl + route);
    };

    // Go to update page
    this.update = function(id) {
        var route = '/#!/customer/detail/' + id;
        browser.driver.get(browser.baseUrl + route);
    };

    this.getTitle = function() {
        var el = element(by.xpath('/html[1]/body[1]/div[1]/ui-view[1]/div[1]/section[1]/h1[1]'));
        return el.getText();
    };

    this.setName = function(val) {
        element(by.model('vm.data.name')).clear().sendKeys(val);
    };
    
    this.setAddress = function(val) {
        element(by.model('vm.data.address')).clear().sendKeys(val);
    };

    this.setHp = function(val) {
        element(by.model('vm.data.phone')).clear().sendKeys(val);
    };

    this.setNpwp = function(val) {
        element(by.model('vm.data.npwp')).clear().sendKeys(val);
    };

    this.countCity = function() {
        return element.all(by.repeater('x in vm.cities')).count();
    };

    this.setCity = function() {
        // this.select2('#select2-city-results');
        browser.sleep(500);
        browser.actions().mouseDown(element(by.xpath('/html[1]/body[1]/div[1]/ui-view[1]/div[1]/section[2]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[4]/span[1]/span[1]/span[1]/span[2]')), ).perform();
        browser.sleep(1000);
        // Select random element
        element.all(by.css('li.select2-results__option')).first().click();
    };

    this.getNotif = function() {
        var xpath = '/html[1]/body[1]/div[1]/flash-message-stacked[1]/div[1]/div[1]';
        var el = element(by.xpath(xpath));
        return el.getText();
    };

    this.submit = function() {
        element(by.buttonText('Simpan')).click();
    };

    this.cancel = function() {
        element(by.buttonText('Batal')).click();
    };
};

module.exports = new CustomerDetail();