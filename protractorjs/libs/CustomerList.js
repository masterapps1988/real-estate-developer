var CustomerList = function() {
    browser.driver.get(browser.baseUrl + '/#!/customer/list');

    this.getTitle = function() {
        var el = element(by.xpath('/html[1]/body[1]/div[1]/ui-view[1]/div[1]/section[1]/h1[1]'));
        return el.getText();
    };

    this.setKeyword = function(value) {
        this._keywordEl().sendKeys(value);
    };

    this.getKeyword = function() {
        return this._keywordEl().getAttribute('value');
    }

    this._keywordEl = function() {
        return element(by.model('vm.search.keyword'));
    };

    this.submit = function() {
        element(by.buttonText('Cari')).click();
    };

    this.goToFirstCustomer = function() {
        element.all(by.repeater('x in vm.list')).first().element(by.css('a')).click();

        return require('./CustomerDetail.js')
    };
};

module.exports = new CustomerList();