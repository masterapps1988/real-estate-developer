var AdminDetail = function() {
    this._clear = false;

    // Go to create page
    this.create = function() {
        var route = '/#!/admin/create';
        browser.driver.get(browser.baseUrl + route);
    };

    // Go to update page
    this.update = function(id) {
        var route = '/#!/admin/detail/' + id;
        browser.driver.get(browser.baseUrl + route);
    };

    this.getTitle = function() {
        var el = element(by.xpath('/html[1]/body[1]/div[1]/ui-view[1]/div[1]/section[1]/h1[1]'));
        return el.getText();
    };

    this.setName = function(val) {
        element(by.model('vm.data.name')).clear().sendKeys(val);
    };
    
    this.setAddress = function(val) {
        element(by.model('vm.data.address')).clear().sendKeys(val);
    };

    this.setHp = function(val) {
        element(by.model('vm.data.phone')).clear().sendKeys(val);
    };

    this.setNpwp = function(val) {
        element(by.model('vm.data.npwp')).clear().sendKeys(val);
    };

    this.setUsername = function(val) {
        element(by.model('vm.data.username')).clear().sendKeys(val);
    };

    this.setPassword = function(val) {
        element(by.model('vm.data.password')).clear().sendKeys(val);
    };

    this.setConfirmPassword = function(val) {
        element(by.model('vm.data.confirm_password')).clear().sendKeys(val);
    };

    this.setEmail = function(val) {
        element(by.model('vm.data.email')).clear().sendKeys(val);
    };

    this.countCity = function() {
        return element.all(by.repeater('x in vm.cities')).count();
    };

    this.setCity = function() {
        var id = 'select2-city-container';
        WeSelect2.selectFirstUnselectItems(id);
    };

    this.setRole = function() {
        var id = 'select2-role-container';
        WeSelect2.selectFirstUnselectItems(id);
    };

    this.getNotif = function() {
        var xpath = '/html[1]/body[1]/div[1]/flash-message-stacked[1]/div[1]/div[1]';
        var el = element(by.xpath(xpath));
        return el.getText();
    };

    this.submit = function() {
        element(by.buttonText('Simpan')).click();
    };

    this.cancel = function() {
        element(by.buttonText('Batal')).click();
    };
};

module.exports = new AdminDetail();