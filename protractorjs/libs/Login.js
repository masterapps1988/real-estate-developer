var Login = function() {
    // browser.ignoreSynchronization = true;
    browser.driver.get(browser.baseUrl + '/login');
    
    this.getText = function() {
        return element(by.css('b')).getText();
    };

    this.setUsername = function(value) {
        element(by.css('[name="username"]')).sendKeys(value);
    };

    this.setPassword = function(value) {
        element(by.css('[name="password"]')).sendKeys(value);
    };

    this.submit = function() {
        element(by.buttonText('Sign In')).click();
    };
};

module.exports = new Login();