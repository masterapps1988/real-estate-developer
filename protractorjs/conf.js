// conf.js
exports.config = {
    // directConnect: true,
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    baseUrl: 'http://bsi.test',
    specs: ['./tests/*.js'],
    capabilities: {
        'browserName': 'chrome'
    },
    onPrepare: function(){
        global.WeNumber = require('./WeLibs/WeNumber.js');
        global.faker = require('faker');
        global.WeSelect2 = require('./WeLibs/WeSelect2.js');

        // Login first
        browser.waitForAngularEnabled(false);

        var login = require('./libs/Login.js');

        login.setUsername('admin');
        login.setPassword('admin');
        login.submit();

        browser.waitForAngularEnabled(true);
    }
};